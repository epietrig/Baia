/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui.queryWidgets;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

// adapted from http://helpdesk.objects.com.au/java/how-do-add-a-checkbox-to-items-in-a-jlist

public class CheckList extends JComponent {

	protected LinkedList<String> values;
	protected LinkedList<String> selectedValues;
	CheckListItem[] listItems;
	
	protected ArrayList<ChangeListener> listeners;
	JList list;
	CheckListRenderer checkListRenderer;
	
	public CheckList(LinkedList<String> values) {
		super();
		this.values = values;
		this.selectedValues = new LinkedList<String>(values);
		this.listeners = new ArrayList<ChangeListener>();
		
		listItems = new CheckListItem[values.size()];
		for(int i = 0; i < values.size(); i++) {
			listItems[i] = new CheckListItem(values.get(i));
			listItems[i].setSelected(true);
		}
		list = new JList(listItems);
		checkListRenderer = new CheckListRenderer();
		list.setCellRenderer(checkListRenderer);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				JList list = (JList) event.getSource();
				// Get index of item clicked
				int index = list.locationToIndex(event.getPoint());
				CheckListItem item = (CheckListItem)list.getModel().getElementAt(index);
				// Toggle selected state
				item.setSelected(!item.isSelected());
				if(item.isSelected()) {
					selectedValues.add(item.getLabel());
				} else {
					selectedValues.remove(item.getLabel());
				}
				// Repaint cell
				list.repaint(list.getCellBounds(index, index));
				fireChangeEvent();
			}
		});  
		setLayout(new BorderLayout());
		JScrollPane jsp = new JScrollPane(list);
		this.add(jsp);
		jsp.setPreferredSize(new Dimension(200, 80));
	}


	public LinkedList<String> getValues() {
		return values;
	}

	public LinkedList<String> getSelectedValues() {
		return selectedValues;
	}

	public void setSelectedValues(LinkedList<String> selectedValues) {
		this.selectedValues = selectedValues;
		//System.out.println("selected values before "+selectedValues.size());
		setAllCheckBoxItem(true);
		//System.out.println("selected values after "+selectedValues.size());
		for (CheckListItem item : listItems) {
			if (!selectedValues.contains(item.getLabel())) {
				//System.out.println("does not contain !");
				item.setSelected(false);
				//fireChangeEvent();
			}
			((CheckListRenderer)list.getCellRenderer()).setSelected(item.isSelected());

		}

		list.repaint();
	}
	
	public void addChangeListener(ChangeListener listener) {
		listeners.add(listener);
	}

	public void removeChangeListener (ChangeListener listener) {listeners.remove(listener);}
	
	protected void fireChangeEvent() {
		ChangeEvent event = new ChangeEvent(this);
		for (Iterator<ChangeListener> iterator = listeners.iterator(); iterator.hasNext();) {
			ChangeListener changeListener = iterator.next();
			changeListener.stateChanged(event);
		}
	}

	public void setAllCheckBoxItem(boolean selected) {
		//System.out.println("Set all items to "+selected);
		for (CheckListItem item : listItems) {
			item.setSelected(selected);
			((CheckListRenderer)list.getCellRenderer()).setSelected(item.isSelected());
			//fireChangeEvent();
		}
		list.repaint();
//		for (CheckListItem item : listItems) {
//			System.out.println(item.isSelected());
//		}

	}

	public void setEnabled (boolean enabled) {
		for (CheckListItem item : listItems) {
			item.setEnabled(enabled);
			//((CheckListRenderer)list.getCellRenderer()).setEnabled(item.isEnabled());
			//fireChangeEvent();
		}
		list.repaint();

	}
}

// Represents items in the list that can be selected

class CheckListItem {
	private String  label;
	private boolean isSelected = false;
	private boolean isEnabled = true;

	public CheckListItem(String label) {
		this.label = label;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public boolean isEnabled() {return isEnabled;}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public void setEnabled(boolean isEnabled) {this.isEnabled = isEnabled;}

	public String getLabel() {
		return label;
	}
	
	public String toString() {
		return label;
	}

}

// Handles rendering cells in the list using a check box

class CheckListRenderer extends JCheckBox implements ListCellRenderer {
	public Component getListCellRendererComponent(
			JList list, Object value, int index, 
			boolean isSelected, boolean hasFocus) {
		setEnabled(((CheckListItem)value).isEnabled());
		setSelected(((CheckListItem)value).isSelected());
		setFont(list.getFont());
		setBackground(list.getBackground());
		setForeground(list.getForeground());
		setText(value.toString());
		return this;
	}



} 

