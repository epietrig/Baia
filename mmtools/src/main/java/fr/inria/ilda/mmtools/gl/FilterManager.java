/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.filters.GaussianFilter;
import fr.inria.ilda.mmtools.filters.TextureFilter;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.logging.Filter;

import static com.jogamp.opengl.GL.*;

/**
 * Created by mjlobo on 21/04/16.
 */
public class FilterManager {
    MapGLCanvas canvas;
    public FilterManager(MapGLCanvas canvas) {
        this.canvas = canvas;
    }

    public GaussianFilter initializeGaussianFilter(GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        FrameBufferObject frameBufferObject1 = createTextureFBO(drawable);
        FrameBufferObject frameBufferObject2= createTextureFBO(drawable);
        FrameBufferObject frameBufferObject3 = createTextureFBO(drawable);

        int[] vertexArray = new int[1];
        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glBindVertexArray(vertexArray[0]);
        GaussianFilter gaussianFilter = new GaussianFilter(frameBufferObject1, frameBufferObject2, frameBufferObject3, canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.SIMPLE), canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.BLUR), vertexArray[0]);
        gaussianFilter.calculateQuad(canvas.getCamera().getEye(),canvas.getCamera().getRight(), canvas.getCamera().getLeft(), canvas.getCamera().getBottom(), canvas.getCamera().getTop());
        gaussianFilter.getQuad().setShader(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.BLUR));
        gaussianFilter.getQuad().getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(gaussianFilter.getQuad().getShader().getAttribute(ShaderConstants.POSITION));
        gaussianFilter.getQuad().getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(gaussianFilter.getQuad().getShader().getAttribute(ShaderConstants.TEXTURE_POSITION));

        int [] buffers = new int[gaussianFilter.getQuad().getBuffers().size()];
        int j=0;
        gl.glGenBuffers(buffers.length, buffers,0);
        for (BufferGL bufferGL: gaussianFilter.getQuad().getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)gaussianFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)gaussianFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
            bufferGL.setBuffer(buffers[j]);

            j++;
        }


        return gaussianFilter;
    }

    public FrameBufferObject createTextureFBO(GLAutoDrawable drawable) {
        Texture texture = canvas.getTextureManager().createScreenTexture(drawable);
        return createTextureFBO(texture, drawable);
    }

    public FrameBufferObject createTextureFBO(Texture texture, GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        int[] fbo = new int[1];

        gl.glGenFramebuffers(fbo.length, fbo, 0);
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, fbo[0]);
        //System.out.println("Texture id when creating fbo "+texture.getID());
        gl.glFramebufferTexture(gl.GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture.getID(), 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);

        gl.glDrawBuffer(gl.GL_COLOR_ATTACHMENT0);
        if (gl.glCheckFramebufferStatus(gl.GL_FRAMEBUFFER) != gl.GL_FRAMEBUFFER_COMPLETE)
        {
            //System.out.println("Error in creating frame buffer object...");
        }
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);
        FrameBufferObject frameBufferObject = new FrameBufferObject(fbo[0], texture);

        return frameBufferObject;
    }

    public TextureFilter initializeTextureFilter(RasterGeoElement rasterGeoElement, GLAutoDrawable drawable) {
        FrameBufferObject inFrameBuffer = createTextureRasterFBO(rasterGeoElement, drawable);
        GL4 gl = (GL4)drawable.getGL();
        int[] vertexArray = new int[1];
        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glBindVertexArray(vertexArray[0]);
        TextureFilter textureFilter= new TextureFilter(vertexArray[0],inFrameBuffer, canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.ALTITUDE));
        initializeTextureFilterQuad(textureFilter, rasterGeoElement.getTexture(), ShaderManager.RenderedElementTypes.TEXTURED, drawable);
        return textureFilter;
    }

    public TextureFilter initializeTextureFilter(Texture rasterTexture, GLAutoDrawable drawable) {
        FrameBufferObject inFrameBuffer = createTextureRasterFBO(rasterTexture, drawable);
        GL4 gl = (GL4)drawable.getGL();
        int[] vertexArray = new int[1];
        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glBindVertexArray(vertexArray[0]);
        TextureFilter textureFilter= new TextureFilter(vertexArray[0],inFrameBuffer, canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.ALTITUDE));
        initializeTextureFilterQuad(textureFilter, rasterTexture, ShaderManager.RenderedElementTypes.TEXTURED, drawable);
        return textureFilter;
    }

    public FrameBufferObject createTextureRasterFBO(RasterGeoElement rasterGeoElement, GLAutoDrawable drawable) {
        Texture texture = canvas.getTextureManager().createTextureFromTexture(rasterGeoElement, drawable);
        return createTextureFBO(texture, drawable);
    }

    public FrameBufferObject createTextureRasterFBO(Texture rasterTexture, GLAutoDrawable drawable) {
        Texture texture = canvas.getTextureManager().createTextureFromTexture(rasterTexture, drawable);
        return createTextureFBO(texture, drawable);
    }

    public void initializeTextureFilterQuad (TextureFilter textureFilter, Texture texture, ShaderManager.RenderedElementTypes type, GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        //System.out.println("Texture width in initialize Filter quad "+texture.getWidth()+" "+ texture.getHeight());
        gl.glBindVertexArray(textureFilter.getVaoID());
        textureFilter.calculateQuad(texture.getLowerCorner(), texture.getWidth(), texture.getHeight());
        textureFilter.getQuad().setShader(canvas.getShaderManager().getPrograms().get(type));
        textureFilter.getQuad().getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(textureFilter.getQuad().getShader().getAttribute(ShaderConstants.POSITION));
        textureFilter.getQuad().getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(textureFilter.getQuad().getShader().getAttribute(ShaderConstants.TEXTURE_POSITION));

        int [] buffers = new int[textureFilter.getQuad().getBuffers().size()];
        int j=0;
        gl.glGenBuffers(buffers.length, buffers,0);
        for (BufferGL bufferGL: textureFilter.getQuad().getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)textureFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)textureFilter.getQuad().getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
            bufferGL.setBuffer(buffers[j]);

            j++;
        }

    }

    public void attachTextureToFbo(FrameBufferObject fbo, Texture texture, GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, fbo.getVboiD());
        gl.glFramebufferTexture(gl.GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture.getID(), 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glDrawBuffer(gl.GL_COLOR_ATTACHMENT0);
        if (gl.glCheckFramebufferStatus(gl.GL_FRAMEBUFFER) != gl.GL_FRAMEBUFFER_COMPLETE)
        {
            //System.out.println("Error in creating frame buffer object...");
        }
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);

    }


}
