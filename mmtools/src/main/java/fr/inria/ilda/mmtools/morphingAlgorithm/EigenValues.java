/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.morphingAlgorithm;

/**
 * Created by mjlobo on 14/01/2017.
 */
public class EigenValues {
    double[] tangentEigenVector;
    double[] normalEigenVector;
    double tangentEigenValue;
    double normalEigenValue;

    public EigenValues(double[] tangentEigenVector, double[] normalEigenVector, double tangentEigenValue, double normalEigenValue) {
        this.tangentEigenValue = tangentEigenValue;
        this.normalEigenValue = normalEigenValue;
        this.tangentEigenVector = tangentEigenVector;
        this.normalEigenVector = normalEigenVector;
    }

    public EigenValues(EigenValues clone) {
        this.tangentEigenVector = clone.getTangentEigenVector().clone();
        this.normalEigenVector = clone.getNormalEigenVector().clone();
        this.tangentEigenValue = clone.getTangentEigenValue();
        this.normalEigenValue = clone.getNormalEigenValue();
    }


    public double[] getTangentEigenVector() {
        return tangentEigenVector;
    }

    public double[] getNormalEigenVector() {
        return normalEigenVector;
    }

    public double getTangentEigenValue() {
        return tangentEigenValue;
    }

    public double getNormalEigenValue() {
        return normalEigenValue;
    }

}
