/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import fr.inria.ilda.mmtools.ui.queryWidgets.NumericQuery;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 23/06/15.
 */
public class NumericQueryEvent extends Event {

    NumericQuery target;
    double lowerValue;
    double upperValue;
    String typeName;

    public NumericQueryEvent(NumericQuery target, double lowerValue, double upperValue, String typeName) {
        super();
        source = this.target = target;
        this.upperValue = upperValue;
        this.lowerValue = lowerValue;
        this.typeName = typeName;
    }

    public NumericQueryEvent(double lowerValue, double upperValue, String typeName) {
        super();
        this.upperValue = upperValue;
        this.lowerValue = lowerValue;
        this.typeName = typeName;
    }

    private static NumericQueryEventListener listener = new NumericQueryEventListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof NumericQuery ) {
            listener.attachTo( stateMachine, (NumericQuery)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof NumericQuery) {
            listener.detachFrom( stateMachine, (NumericQuery)target );
        }
    }

    public NumericQuery getTarget() {
        return target;
    }

    public double getLowerValue() {return lowerValue;}

    public double getUpperValue() {return upperValue;}

    public String getTypeName() {return typeName;}
}

class NumericQueryEventListener implements ChangeListener {

    private HashMap<Component, HashSet<StateMachine>> listeners;
    public NumericQueryEventListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, NumericQuery target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.getSpinnerUpperBound().getModel().addChangeListener(this);
            target.getSpinnerLowerBound().getModel().addChangeListener(this);
            //System.out.println("Added MouseMoveListener");
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, NumericQuery target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.getSpinnerUpperBound().getModel().removeChangeListener(this);
                target.getSpinnerLowerBound().getModel().removeChangeListener(this);
                //System.out.println("Removed MouseMoveListener");
            }
        }
    }


    @Override
    public void stateChanged(ChangeEvent event) {
        NumericQuery target = (NumericQuery)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            NumericQueryEvent customEvent = new NumericQueryEvent(target, target.getSpinnerLowerBound().getValue(), target.getSpinnerUpperBound().getValue(), target.getTypeName());
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        }
    }
}