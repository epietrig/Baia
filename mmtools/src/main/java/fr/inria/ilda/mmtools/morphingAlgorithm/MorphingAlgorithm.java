/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.morphingAlgorithm;

import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.utilties.Pair;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.*;
import java.util.List;

/**
 * Created by mjlobo on 14/01/2017.
 */
public class MorphingAlgorithm {

    public static Pair<List<Double[]>, List<Double[]>> getContours(Mat initMask, Mat endMask) throws NotEnoughCornersExeption{
        Mat image1 = initMask;

        Mat image2 = endMask;
        List<MatOfPoint> contours1 = CVUtilities.findContoursExternal(initMask);
        List<MatOfPoint> contours2 = CVUtilities.findContoursExternal(endMask);
        Mat contoursImage1 = CVUtilities.drawContours(initMask,contours1,0);
        Mat contoursImage2 = CVUtilities.drawContours(endMask,contours2,0);
        //Imgcodecs.imwrite("contoursImage1.png", contoursImage1);
        //Imgcodecs.imwrite("contoursImage2.png", contoursImage2);
        List<Double[]> boundary1 = CVUtilities.getCountour(contoursImage1,0);
        List<Double[]> boundary2 = CVUtilities.getCountour(contoursImage2,0);
        if (boundary1.size()>3 && boundary2.size()>3) {
            HashMap<Integer, Integer> correspondances = getCorrespondances(boundary1, boundary2, image1, image2);
            Map<Integer, Integer> correspondancesTree = new TreeMap<Integer, Integer>(correspondances);

            List<Integer> boundary1corners = new ArrayList();
            boundary1corners.addAll(correspondancesTree.keySet());
            double totalLength1 = calculateTotalLength(boundary1);

            List<Integer> boundary2corners = new ArrayList<>();
            boundary2corners.addAll(correspondancesTree.values());
            double totalLength2 = calculateTotalLength(boundary2);

            int nPoints = 800;
            boundary1corners.add(boundary1corners.get(0));
            boundary2corners.add(boundary2corners.get(0));

            List<Double[]> generatedPoints1 = new ArrayList<>();
            List<Double[]> generatedPoints2 = new ArrayList<>();

            List<Double[]> allPoints1 = new ArrayList<>();
            List<Double[]> allPoints2 = new ArrayList<>();

            for (int i = 0; i < boundary1corners.size() - 1; i++) {
                Imgproc.circle(image1, new Point(boundary1.get(boundary1corners.get(i))[0], boundary1.get(boundary1corners.get(i))[1]), 1, new Scalar(0, 0, 255));

                double partialLength1 = calculatePartialLength(boundary1, boundary1corners.get(i), boundary1corners.get(i + 1));
                double partialLength2 = calculatePartialLength(boundary2, boundary2corners.get(i), boundary2corners.get(i + 1));

                double proportion = (partialLength1 / totalLength1 + partialLength2 / totalLength2) / 2;

                int nPartialPoints = (int) (proportion * nPoints);


                double sliceLength1 = partialLength1 / (nPartialPoints + 1);
                double sliceLength2 = partialLength2 / (nPartialPoints + 1);

                if (sliceLength1 > sliceLength2) {
                    generatedPoints2 = generatePoints(boundary2corners.get(i), boundary2corners.get(i + 1), boundary2, sliceLength2, nPartialPoints);
                    allPoints2.addAll(generatedPoints2);
                    generatedPoints1 = generatePoints(boundary1corners.get(i), boundary1corners.get(i + 1), boundary1, sliceLength1, generatedPoints2.size());
                    allPoints1.addAll(generatedPoints1);

                } else {
                    generatedPoints1 = generatePoints(boundary1corners.get(i), boundary1corners.get(i + 1), boundary1, sliceLength1, nPartialPoints);
                    allPoints1.addAll(generatedPoints1);
                    generatedPoints2 = generatePoints(boundary2corners.get(i), boundary2corners.get(i + 1), boundary2, sliceLength2, generatedPoints1.size());
                    allPoints2.addAll(generatedPoints2);
                }

            }
            return new Pair<>(allPoints1, allPoints2);
        }

        return new Pair<> (null, null);


    }

    public static Pair<Integer, Integer> getFirstCorrespondance(List<Corner> corners1, List<Corner> corners2) {
        int index1 = -1;
        int index2 = -1;
        double distmin = Double.MAX_VALUE;
        double dist;
        for (int i=0; i<corners1.size(); i++) {
            for (int j=0; j<corners2.size(); j++) {
                dist = calculateDistSq(corners1.get(i).getCoord(), corners2.get(j).getCoord());
                if (calculateDistSq(corners1.get(i).getCoord(), corners2.get(j).getCoord())<distmin) {
                    index1 = i;
                    index2 = j;
                    distmin = dist;
                }
            }
        }
        return new Pair<>(index1, index2);
    }

    public static HashMap<Integer, Integer> getCorrespondances(List<Double[]> boundary1, List<Double[]> boundary2, Mat image1, Mat image2) throws NotEnoughCornersExeption {
        List<Corner> corners1 = getCorners(boundary1, 7, 9, -0.75);
        List<Corner> corners2 = getCorners(boundary2, 7, 9, -0.75);
        HashMap<Integer, Integer> realResult = new HashMap<>();

        if (corners1.size()>3 && corners2.size()>3) {

            Pair<Integer, Integer> first = getFirstCorrespondance(corners1, corners2);
            List<Corner> orderedCorners1 = new ArrayList<>();
            List<Corner> orderedCorners2 = new ArrayList<>();
            orderedCorners1.addAll(corners1.subList(first.getValue1(), corners1.size()));
            orderedCorners1.addAll(corners1.subList(0, first.getValue1()));


            orderedCorners2.addAll(corners2.subList(first.getValue2(), corners2.size()));
            orderedCorners2.addAll(corners2.subList(0, first.getValue2()));

            double wFeatureVariation = 0.33;
            double wFeatureSideVariation = 0.33;
            double wFeatureSize = 0.33;

            setCornersValues(boundary1, orderedCorners1);
            setCornersValues(boundary2, orderedCorners2);

            for (int i = 0; i < orderedCorners1.size(); i++) {
                Imgproc.putText(image1, i + "", new Point(orderedCorners1.get(i).getX(), orderedCorners1.get(i).getY()), Core.FONT_HERSHEY_PLAIN, 1, new Scalar(0, 255, 0));
            }
            for (int i = 0; i < orderedCorners2.size(); i++) {
                Imgproc.putText(image2, i + "", new Point(orderedCorners2.get(i).getX(), orderedCorners2.get(i).getY()), Core.FONT_HERSHEY_PLAIN, 1, new Scalar(0, 255, 0));
            }
            orderedCorners1.add(new Corner(orderedCorners1.get(0)));
            orderedCorners2.add(new Corner(orderedCorners2.get(0)));


            Mat cost = Mat.zeros(orderedCorners1.size(), orderedCorners2.size(), CvType.CV_32FC3);

            int imin;
            int jmin;
            int k = 0;
            int l = 0;
            double mincost;
            double tempCost;
            for (int i = 0; i < orderedCorners1.size(); i++) {
                for (int j = 0; j < orderedCorners2.size(); j++) {
                    imin = -1;
                    jmin = -1;
                    mincost = Double.MAX_VALUE;
                    k = 0;
                    l = 0;
                    while (k <= i) {
                        l = 0;
                        while (l <= j && l <= 2 && k <= 2) {
                            tempCost = calculateDistCostSum(orderedCorners1, i - k, i, wFeatureVariation, wFeatureSideVariation, wFeatureSize) +
                                    calculateDistCostSum(orderedCorners2, j - l, j, wFeatureVariation, wFeatureSideVariation, wFeatureSize) +
                                    calculateSimCost(orderedCorners1.get(i), orderedCorners2.get(j), wFeatureVariation, wFeatureSideVariation, wFeatureSize) +
                                    cost.get(i - k, j - l)[0];
                            if (tempCost < mincost && (k + l > 0 || (i == 0 && j == 0))) {
                                mincost = tempCost;
                                imin = i - k;
                                jmin = j - l;

                            }
                            l++;
                        }
                        k++;
                    }
                    cost.put(i, j, new double[]{mincost, imin, jmin});
                }
            }
//
            HashMap<Integer, Integer> result1 = new HashMap<>();
            HashMap<Integer, Integer> result2 = new HashMap<>();
            int i = orderedCorners1.size() - 1;
            int j = orderedCorners2.size() - 1;
            int oldI;
            int oldJ;
            while (i != 0 && j != 0) {

                if (!result1.containsKey((int) cost.get(i, j)[1]) && !result2.containsKey((int) cost.get(i, j)[2])) {
                    result1.put((int) cost.get(i, j)[1], (int) cost.get(i, j)[2]);
                    result2.put((int) cost.get(i, j)[2], (int) cost.get(i, j)[1]);
                } else {
                    double currentCost = calculateSimCost(orderedCorners1.get((int) cost.get(i, j)[1]), orderedCorners2.get((int) cost.get(i, j)[2]), wFeatureVariation, wFeatureSideVariation, wFeatureSize);
                    if ((result1.containsKey(cost.get(i, j)[1])) && (result2.containsKey(cost.get(i, j)[2])) && (result1.get(cost.get(i, j)) != cost.get(i, j)[2])) {
                        if (currentCost < calculateSimCost(orderedCorners1.get(i), orderedCorners2.get(result1.get(i)), wFeatureVariation, wFeatureSideVariation, wFeatureSize)
                                && currentCost < calculateSimCost(orderedCorners1.get(result2.get(j)), orderedCorners2.get(j), wFeatureVariation, wFeatureSideVariation, wFeatureSize)) {
                            result1.put((int) cost.get(i, j)[1], (int) cost.get(i, j)[2]);
                            result2.put((int) cost.get(i, j)[2], (int) cost.get(i, j)[1]);
                        }
                    } else if (result1.containsKey((int) cost.get(i, j)[1]) && currentCost < calculateSimCost(orderedCorners1.get((int) cost.get(i, j)[1]), orderedCorners2.get(result1.get((int) cost.get(i, j)[1])), wFeatureVariation, wFeatureSideVariation, wFeatureSize)) {
                        result1.put((int) cost.get(i, j)[1], (int) cost.get(i, j)[2]);
                        result2.put((int) cost.get(i, j)[2], (int) cost.get(i, j)[1]);
                    } else if (result2.containsKey((int) cost.get(i, j)[2]) && currentCost < calculateSimCost(orderedCorners1.get(result2.get((int) cost.get(i, j)[2])), orderedCorners2.get((int) cost.get(i, j)[2]), wFeatureVariation, wFeatureSideVariation, wFeatureSize)) {
                        result1.put((int) cost.get(i, j)[1], (int) cost.get(i, j)[2]);
                        result2.put((int) cost.get(i, j)[2], (int) cost.get(i, j)[1]);
                    }
                }
                oldI = i;
                oldJ = j;
                i = (int) cost.get(oldI, oldJ)[1];
                j = (int) cost.get(oldI, oldJ)[2];

            }



            for (Integer i1 : result1.keySet()) {
                if (i1 == result2.get(result1.get(i1))) {
                    realResult.put(orderedCorners1.get(i1).getIndex(), orderedCorners2.get(result1.get(i1)).getIndex());
                }
            }
        }
        else {
            if (corners1.size()<3) {
                if (corners2.size()<3) {
                    throw  new NotEnoughCornersExeption(3);
                }
                else {
                    throw new NotEnoughCornersExeption(1);
                }
            }
            throw new NotEnoughCornersExeption(2);
        }


        return realResult;
    }

    public static List<Corner> getCorners(List<Double[]> boundary, double dmin, double dmax, double cosmin) {
        double dminsq = dmin * dmin;
        double dmaxsq = dmax * dmax;
        double cosmax = -1;
        double adist;
        double bdist;
        double cdist;
        double[] b = new double[2];
        double[] c = new double[2];
        int countSharp = 0;
        List<Double> sharpness = new ArrayList<>();
        List<Double> convex = new ArrayList<>();
        List<Corner> corners = new ArrayList<>();
        for (int i=0; i<boundary.size(); i++) {
            cosmax = -1;
            sharpness.add(new Double(-1));
            convex.add(new Double(0));
            boolean seenUpper = false;

            for (int plus=i+1; plus<boundary.size(); plus++) {
                adist = (boundary.get(i)[0] - boundary.get(plus)[0]) * (boundary.get(i)[0] - boundary.get(plus)[0])
                        + (boundary.get(i)[1] - boundary.get(plus)[1]) * (boundary.get(i)[1] - boundary.get(plus)[1]);
                if (seenUpper && adist > dmaxsq) {
                    break;
                }
                if (adist > dminsq) {
                    seenUpper = true;
                    boolean seenLower = false;
                    for (int minus = i - 1; minus >= 0; minus--) {
                        bdist = (boundary.get(i)[0] - boundary.get(minus)[0]) * (boundary.get(i)[0] - boundary.get(minus)[0])
                                + (boundary.get(i)[1] - boundary.get(minus)[1]) * (boundary.get(i)[1] - boundary.get(minus)[1]);
                        if (seenLower && bdist > dmaxsq) {
                            break;
                        }
                        if (bdist > dminsq) {
                            seenLower = true;
                            cdist = (boundary.get(plus)[0] - boundary.get(minus)[0]) * (boundary.get(plus)[0] - boundary.get(minus)[0])
                                    + (boundary.get(plus)[1] - boundary.get(minus)[1]) * (boundary.get(plus)[1] - boundary.get(minus)[1]);
                            double cos = (adist + bdist - cdist) / (2 * Math.sqrt(adist * bdist));
                            if (cos < cosmin) {
                                break;
                            }
                            if (cosmax < cos) {
                                cosmax = cos;
                                sharpness.set(i, cos);
                                b[0] = boundary.get(i)[0] - boundary.get(minus)[0];
                                b[1] = boundary.get(i)[1] - boundary.get(minus)[1];
                                c[0] = boundary.get(plus)[0] - boundary.get(minus)[0];
                                c[1] = boundary.get(plus)[1] - boundary.get(minus)[1];
                                if (b[0]*c[1]-b[1]*c[0]<=0) {
                                    convex.set(i, new Double(1));
                                }
                                else {
                                    convex.set(i, new Double(-1));
                                }
                            }
                        }
                    }
                }
            }
        }


        int nFound = -1;
        int lastFound = -1;
        for (int i=0; i<boundary.size(); i++) {

            boolean found = false;
            if (sharpness.get(i)>cosmin && sharpness.get(i)!=-1) {
                countSharp++;
                if (lastFound>=0) {
                    double dist = (boundary.get(i)[0] - boundary.get(lastFound)[0]) * (boundary.get(i)[0] - boundary.get(lastFound)[0])
                            + (boundary.get(i)[1] - boundary.get(lastFound)[1]) * (boundary.get(i)[1] - boundary.get(lastFound)[1]);
                    if (dist > dmaxsq) {
                        nFound++;
                        found = true;
                    }
                    else if (sharpness.get(i) > sharpness.get(lastFound)) {
                        found = true;
                    }
                }
                else {
                    nFound++;
                    found = true;
                }
            }
            if (found) {
                lastFound = i;
                if (corners.size()==nFound) {
                    corners.add(new Corner(boundary.get(lastFound), convex.get(lastFound), lastFound));
                } else {
                    corners.set(nFound, new Corner(boundary.get(lastFound), convex.get(lastFound), lastFound));
                }
            }
        }
        return corners;


    }

    public static double calculateDistSq(Double[] p1, Double[] p2) {
        return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]);
    }

    public static void setCornersValues (List<Double[]> boundary, List<Corner> corners) {
        double totalLength = calculateTotalLength(boundary);
        double rightLength;
        double leftLength;

        for (int i=0; i<corners.size(); i++) {
            corners.get(i).setEigenValues(calculateRosPi(boundary, corners.get(i).getIndex(),20));
            corners.get(i).setFeatureVariation(calculateFeatureVariation(corners.get(i).getConvex(),corners.get(i).getEigenValues()));
            if (i==0) {
                rightLength = calculatePartialLength(boundary,corners.get(i).getIndex(), corners.get(corners.size()-1).getIndex());
                leftLength = calculatePartialLength(boundary,corners.get(i).getIndex(), corners.get(i+1).getIndex());
            }
            else if (i==corners.size()-1) {
                rightLength = calculatePartialLength(boundary, corners.get(i).getIndex(), corners.get(i-1).getIndex());
                leftLength = calculatePartialLength(boundary, corners.get(i).getIndex(), corners.get(0).getIndex());
            }
            else {
                rightLength = calculatePartialLength(boundary,corners.get(i).getIndex(), corners.get(i-1).getIndex());
                leftLength = calculatePartialLength(boundary,corners.get(i).getIndex(), corners.get(i+1).getIndex());
            }

            EigenValues rorValues = calculateROR(boundary, corners, i);
            EigenValues rolValues = calculateROL(boundary, corners, i);
            double rolVar = rolValues.getNormalEigenValue()/(rolValues.getNormalEigenValue()+rolValues.getTangentEigenValue());
            double rorVar = rorValues.getNormalEigenValue()/(rorValues.getNormalEigenValue()+rorValues.getTangentEigenValue());

            corners.get(i).setFeatureSize(calculateFeatureSize(totalLength,rightLength, leftLength));
            corners.get(i).setFeatureSideVariation(calculateFeatureSideVariation(rolVar,rorVar));
            corners.get(i).setpL(leftLength/totalLength);
            corners.get(i).setpR(rightLength/totalLength);
            corners.get(i).setVarROL(rolVar);
            corners.get(i).setVarROR(rorVar);
        }

    }

    public static double calculateTotalLength(List<Double[]> boundary) {
        double length = 0;
        for (int i=1; i<boundary.size(); i++) {
            length +=Math.sqrt(calculateDistSq(boundary.get(i-1),boundary.get(i)));
        }
        length +=Math.sqrt(calculateDistSq(boundary.get(0),boundary.get(boundary.size()-1)));
        return length;
    }

    public static double calculatePartialLength(List<Double[]> boundary, int startIndex, int endIndex) {
        double length = 0;
        if (endIndex < startIndex) {
            for (int i=startIndex+1; i<boundary.size()-1; i++) {
                length +=Math.sqrt(calculateDistSq(boundary.get(i-1),boundary.get(i)));
            }
            for (int i=1; i<=endIndex; i++) {
                length +=Math.sqrt(calculateDistSq(boundary.get(i-1),boundary.get(i)));
            }
        }
        else {
            for (int i = startIndex + 1; i <= endIndex; i++) {
                length += Math.sqrt(calculateDistSq(boundary.get(i - 1), boundary.get(i)));
            }
        }
        return length;
    }

    public static  double calculateFeatureVariation(double convex, EigenValues eigenValues) {
        return convex * (eigenValues.getNormalEigenValue()/(eigenValues.getNormalEigenValue()+eigenValues.getTangentEigenValue()));
    }

    public static double calculateFeatureSideVariation(double rolVar, double rorVar) {
        return (rolVar+rorVar)/2;
    }

    public static double calculateFeatureSize(double leftLength, double rightLength, double totalLength) {
        double proportionLeft = leftLength/totalLength;
        double proportionRight = rightLength/totalLength;
        return (proportionLeft + proportionRight)/2;
    }

    public static double calculateSimCost(Corner c1, Corner c2, double wFeatureVariation, double wFeatureSideVariation, double wFeatureSize) {
        double cost = Math.max(c1.getFeatureSize(), c2.getFeatureSize()) * (
                wFeatureVariation*Math.abs(c1.getFeatureVariation()-c2.getFeatureVariation()) +
                        wFeatureSideVariation*1/2 *(Math.abs(c1.getVarROL()-c2.getVarROL())+Math.abs(c1.getVarROR()-c2.getVarROR())) +
                        wFeatureSize*1/2*(Math.abs(c1.getpL() - c2.getpL())+Math.abs(c1.getpR()-c2.getpR()))
        );
        return cost;
    }

    public static double calculateDisCost(Corner corner, double wFeatureVariation, double wFeatureSideVariation, double wFeatureSize) {
        return corner.getFeatureSize()*(
                wFeatureVariation * Math.abs(corner.getFeatureVariation())+
                        wFeatureSideVariation * Math.abs(corner.getFeatureSideVariation()) +
                        wFeatureSize * Math.abs(corner.getFeatureSize())
        );
    }

    public static double calculateDistCostSum(List<Corner> corners, int startIndex, int endIndex, double wFeatureVariation, double wFeatureSideVariation, double wFeatureSize) {
        double sum=0;
        for (int i=startIndex+1; i<endIndex; i++) {
            sum+=calculateDisCost(corners.get(i), wFeatureVariation, wFeatureSideVariation, wFeatureSize);
        }
        return sum;
    }



    public static EigenValues calculateRosPi(List<Double[]> boundary, int cornerIndex, int h) {
        List<Double[]> points = new ArrayList<>();

        for (int i=cornerIndex-h ; i<= cornerIndex+h; i++) {
            if (i<0) {
                points.add(boundary.get(boundary.size()+i));
            }
            else if (i>=boundary.size()) {
                points.add(boundary.get(i-boundary.size()));
            }
            else {
                points.add(boundary.get(i));
            }
        }
        return calculateEigenValues(points);
    }

    public static EigenValues calculateROL(List<Double[]> boundary, List<Corner> corners, int cornerIndex) {
        List<Double[]> points = new ArrayList<>();
        int currentIndex = cornerIndex;
        int leftIndex = cornerIndex-1;
        if (currentIndex == 0) {
            leftIndex = corners.size()-1;
        }
        if (corners.get(leftIndex).getIndex()>corners.get(currentIndex).getIndex()) {
            points.addAll(boundary.subList(leftIndex,boundary.size()));
            points.addAll(boundary.subList(0, currentIndex));
        }
        else {
            points.addAll(boundary.subList(corners.get(leftIndex).getIndex(), corners.get(currentIndex).getIndex()));
        }

//        if (cornerIndex == 0) {
//            for (int i=corners.get(corners.size()-1).getIndex(); i<boundary.size()-1; i++) {
//                points.add(boundary.get(i));
//            }
//            for (int i=0; i<=corners.get(cornerIndex).getIndex(); i++) {
//                points.add(boundary.get(i));
//            }
//        }
//        else {
//            for (int i=corners.get(cornerIndex-1).getIndex(); i<=corners.get(cornerIndex).getIndex(); i++) {
//                points.add(boundary.get(i));
//            }
//        }
        return calculateEigenValues(points);
    }

    public static EigenValues calculateROR(List<Double[]> boundary, List<Corner> corners, int cornerIndex) {
        List<Double[]> points = new ArrayList<>();
        int currentIndex = cornerIndex;
        int rightIndex = cornerIndex + 1;
        if (currentIndex == corners.size()-1) {
            rightIndex = 0;
        }

        if (corners.get(currentIndex).getIndex() < corners.get(rightIndex).getIndex()) {
            for (int i=corners.get(currentIndex).getIndex(); i<=corners.get(rightIndex).getIndex(); i++) {
                points.add(boundary.get(i));
            }
        }
        else {
            for (int i = corners.get(currentIndex).getIndex(); i<boundary.size()-1; i++) {
                points.add(boundary.get(i));
            }
            for (int i=0; i<=corners.get(rightIndex).getIndex(); i++) {
                points.add(boundary.get(i));
            }
        }
        return calculateEigenValues(points);
    }

    public static EigenValues calculateEigenValues(List<Double[]> points) {
        Mat cov = Mat.zeros(2,2,CvType.CV_32F);
        Double[] center = new Double[2];

        double sumx = 0;
        double sumy = 0;
        for (int i=0; i<points.size(); i++) {
            sumx += points.get(i)[0];
            sumy += points.get(i)[1];
        }

        center[0] = sumx/points.size();
        center[1] = sumy/points.size();

        Mat firstTerm = new Mat(2,1,CvType.CV_32F);
        Mat secondTerm = new Mat(1,2,CvType.CV_32F);
        Mat result = new Mat(2,2,CvType.CV_32F);

        for (int i=0; i<points.size(); i++) {
            firstTerm.put(0,0,points.get(i)[0]-center[0]);
            firstTerm.put(1,0,points.get(i)[1]-center[1]);
            Core.transpose(firstTerm, secondTerm);
            Core.gemm(firstTerm, secondTerm,1,new Mat(), 0, result);
            Core.add(cov, result, cov);
        }

        Mat eigenVectors = new Mat();
        Mat eigenValues = new Mat();
        Core.eigen(cov, eigenValues, eigenVectors);

        double[] featureVectorLine = new double[2];
        //System.out.println("points size "+points.size());
        featureVectorLine[0] = points.get(points.size()-1)[0] - points.get(0)[0];
        featureVectorLine[1] = points.get(points.size()-1)[1] - points.get(0)[1];

        double[] vector1 = new double[2];
        vector1[0] = eigenVectors.get(0,0)[0];
        vector1[1] = eigenVectors.get(0,1)[0];

        double[] vector2 = new double[2];
        vector2[0] = eigenVectors.get(1,0)[0];
        vector2[1] = eigenVectors.get(1,1)[0];

        double vector1Angle = vectorAngle(vector1, featureVectorLine);
        vector1Angle = Math.min(Math.PI-vector1Angle, vector1Angle);

        double vector2Angle = vectorAngle(vector2, featureVectorLine);
        vector2Angle = Math.min(Math.PI-vector2Angle, vector2Angle);

        EigenValues eigenValuesResult = null;
        if (vector1Angle<vector2Angle) {
            eigenValuesResult = new EigenValues(vector2,vector1,eigenValues.get(1,0)[0], eigenValues.get(0,0)[0]);
        }
        else {
            eigenValuesResult = new EigenValues(vector1, vector2, eigenValues.get(0,0)[0], eigenValues.get(1,0)[0]);
        }


        return eigenValuesResult;

    }

    public static double vectorAngle(double[] v1, double[] v2) {
        return Math.acos(dotProduct(v1,v2)/(vectorLength(v1)*vectorLength(v2)));
    }

    public static double dotProduct(double[] v1, double[] v2) {
        return v1[0]*v2[0]+v1[1]*v2[1];
    }

    public static double vectorLength(double[] v) {
        return Math.sqrt(v[0]*v[0]+v[1]*v[1]);
    }

    public static List<Double[]> generatePoints(int indexStart, int indexEnd, List<Double[]> boundary, double sliceLength, int nPoints) {
        double dist = 0;
        double tempdist = 0;
        int pointCount =0;
        List<Double[]> resultPoints = new ArrayList<>();
        List<Double[]> pointsToConsider = new ArrayList<>();
        if (indexStart > indexEnd) {
            pointsToConsider.addAll(boundary.subList(indexStart, boundary.size()));
            pointsToConsider.addAll(boundary.subList(0, indexEnd));
        }
        else {
            pointsToConsider.addAll(boundary.subList(indexStart, indexEnd));
        }
        for(int i=0; i<pointsToConsider.size()-1; i++) {
            tempdist = Math.sqrt(calculateDistSq(pointsToConsider.get(i), pointsToConsider.get(i+1)));
            if (dist + tempdist>= sliceLength && pointCount<nPoints) {
                double x = pointsToConsider.get(i)[0]+((sliceLength-dist)*(pointsToConsider.get(i)[0]-pointsToConsider.get(i+1)[0]))/tempdist;
                double y = pointsToConsider.get(i)[1]+((sliceLength-dist)*(pointsToConsider.get(i)[1]-pointsToConsider.get(i+1)[1]))/tempdist;
                resultPoints.add(new Double[]{x,y});
                pointCount++;
                dist = -(sliceLength-dist);
            }
            dist += tempdist;
        }
        return resultPoints;
    }



}
