/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.geo.Tile;
import org.opencv.core.Mat;


/**
 * Created by mjlobo on 20/05/15.
 */
public class Texture {
    private String name;
    private double pixels_per_unit;
    protected double[] pixels_per_unit_array;
    private double width;
    private double height;
    private int pixelWidth;
    private int pixelHeight;
    double[] lowerCorner;
    int ID;
    int privateID;
    static int count = 0;
    Mat data;
    String path;

    public Texture (double pixels_per_unit, double width, double height, double[] lowerCorner, int pixelWidth, int pixelHeight) {
        this(null, pixels_per_unit, width, height, lowerCorner, pixelWidth, pixelHeight);
    }

    public Texture (String name, double pixels_per_unit, double width, double height, double[] lowerCorner, int pixelWidth, int pixelHeight) {
        this.name = name;
        this.pixels_per_unit = pixels_per_unit;
        this.width = width;
        this.height = height;
        this.lowerCorner = lowerCorner;
        this.pixelWidth = pixelWidth;
        this.pixelHeight = pixelHeight;
        privateID = count;
        count++;
    }

    public Texture (RasterGeoElement rasterGeoElement) {
        pixels_per_unit = (rasterGeoElement.getImageWidth()/rasterGeoElement.getRealWidth());
        pixels_per_unit_array = new double [] {pixels_per_unit, rasterGeoElement.getImageHeight()/rasterGeoElement.getRealHeight()};

        width = rasterGeoElement.getRealWidth();
        height = rasterGeoElement.getRealHeight();
        lowerCorner = rasterGeoElement.getLowerCorner();
        name=null;
        privateID = count;
        pixelHeight = rasterGeoElement.getImageHeight();
        pixelWidth = rasterGeoElement.getImageWidth();
        count++;
    }


    public Texture(Tile tile) {
        pixels_per_unit = tile.getPixels_per_unit();
        pixels_per_unit_array = new double[] {tile.getWidth()/tile.getRealWidth(), tile.getHeight()/tile.getRealHeight()};
        width = tile.getRealWidth();
        height = tile.getRealHeight();
        lowerCorner = new double[] {tile.getX()-tile.getRealWidth()/2, tile.getY()-tile.getRealHeight()/2};
        privateID = count;
        count++;

    }

    public Texture(RasterGeoElement rasterGeoElement, Mat data) {
        this(rasterGeoElement);
        this.data = data;
    }

    public Texture(Texture otherTexture, Mat data) {
        pixels_per_unit = otherTexture.getPixels_per_unit();
        pixels_per_unit_array = otherTexture.getPixels_per_unit_array();
        width = otherTexture.getWidth();
        height = otherTexture.getHeight();
        lowerCorner = otherTexture.getLowerCorner();
        this.data = data.clone();
        privateID = count;
        count++;
        pixelHeight = data.rows();
        pixelWidth = data.cols();
    }

    public Texture (Texture otherTexture) {
        this(otherTexture, otherTexture.getData());
    }

    public void setID (int ID) {
        this.ID = ID;
    }

    public int getID () {
        return ID;
    }

    public double getPixels_per_unit() {
        return pixels_per_unit;
    }

    public double getWidth () {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double[] getLowerCorner() {
        return lowerCorner;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setLowerCorner(double[] lowerCorner) {
        this.lowerCorner = lowerCorner;
    }

    public void setPixels_per_unit(double pixels_per_unit) {
        this.pixels_per_unit = pixels_per_unit;
    }

    public void setPixels_per_unit_array(double[] pixels_per_unit_array) {this.pixels_per_unit_array = pixels_per_unit_array; }

    public double[] getPixels_per_unit_array() {return pixels_per_unit_array;}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void print() {
        System.out.println("Texture Lower Corner "+lowerCorner[0]+" "+lowerCorner[1]);
        System.out.println("Height "+height);
        System.out.println("Width "+width);
    }

    public Mat getData() {
        return data;
    }

    public void setData(Mat data) {
        this.data = data;
    }

    public int getPixelWidth () {
        return pixelWidth;
    }

    public int getPixelHeight() {
        return pixelHeight;
    }


}
