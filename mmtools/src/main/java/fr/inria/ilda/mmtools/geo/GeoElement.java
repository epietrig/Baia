/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;


import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.geom.util.AffineTransformation;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
import fr.inria.ilda.mmtools.gl.*;
import org.opencv.core.Mat;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3f;
import java.nio.Buffer;
import java.util.*;
import java.util.List;

/**
 * Created by mjlobo on 09/02/15.
 */
public abstract class GeoElement {
    double x;
    double y;
    protected int textureID;
    protected boolean isComplete;
    protected List<double[]> coordinates = Collections.synchronizedList(new ArrayList<double[]>());
    protected ArrayList<float[]> normalsList;
    protected ArrayList<Integer> indexesList;

    protected float [] vertices = new float[0];
    protected Hashtable <BufferGL, Buffer> buffers;
    protected float z= 0.0f;
    GeometryFactory geometryFactory = new GeometryFactory();

    Geometry geometry;

    double[] offset;
    boolean isIn = false;

    ArrayList<ShapeGL> shapes;
    ArrayList<double[]> originalCoordinates;

    boolean leavingTrace;

    protected Texture blurredTexture;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean getLeavingTrace() {
        return leavingTrace;
    }

    public void setLeavingTrace(boolean leavingTrace) {
        this.leavingTrace = leavingTrace;
    }

    protected ShaderManager.RenderedElementTypes renderingType;
    Vector3f fillColor;
    Vector3f outlineColor = new Vector3f(0.0f,1.0f,1.0f);
    boolean translucentBorder;
    protected double buffer=0.0;
    protected boolean isDocked;
    BoundingBox boundingBox;
    //List<Mat> lbpHists;
    HashMap<String, List<Mat>> lbpHists = new HashMap<>();
    protected static Vector3f DEFAULT_OUTLINE_COLOR = new Vector3f(0f,1.0f,1.0f);
    protected static Vector3f DEFAULT_FILL_COLOR = new Vector3f(1.0f,1.0f,1.0f);
    protected static float SELECTION_LINE_WIDTH = 2.0f;
    protected static float SELECTION_OUTLINE_WIDTH = 5.0f;
    protected static float OUTLINE_LINE_WIDTH= 4.0f;
    protected static Vector3f SELECTION_COLOR_OUT = new Vector3f(1.0f,1.0f,1.0f);
    protected static Vector3f SELECTION_COLOR_IN = new Vector3f(0.0f,0.0f,0.0f);



    public List<double[]> getCoordinates() {
        return coordinates;
    }



    protected int coordinatesContains(double[] point) {
        for(int j = 0; j< coordinates.size(); j++) {
            double[] coord = coordinates.get(j);
            if(coord[0] == point[0] && coord[1] == point[1]) {
                //System.out.println("Coordinate not added because repeated: " + coord[0] +";" +coord[1]);
                //System.out.println("Coordinate not added because repeated: " + point[0] +";" +point[1]);
                return j;
            }
        }
        return -1;
    }

    protected int coordinatesContains(double[] point, List<double[]> coordinates) {
        for(int j = 0; j< coordinates.size(); j++) {
            double[] coord = coordinates.get(j);
            if(coord[0] == point[0] && coord[1] == point[1]) {
                return j;
            }
        }
        return -1;
    }



    public ArrayList<ShapeGL> getShapes() {
        return shapes;
    }

    protected void  calculateGeometry() {
       geometry = calculateGeometry(coordinates);
    }

    protected Geometry calculateGeometry(List<double[]> coordinates) {
        Coordinate[] coordinatesArray = new Coordinate[coordinates.size()+1];
        for (int i=0; i<coordinatesArray.length-1; i++) {
            coordinatesArray[i] = new Coordinate(coordinates.get(i)[0], coordinates.get(i)[1]);
        }
        coordinatesArray[coordinatesArray.length-1]=coordinatesArray[0];
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinatesArray);
        LinearRing linearRing = new LinearRing(coordinateArraySequence, geometryFactory);
        Geometry geometry = new Polygon(linearRing, null, geometryFactory);
        geometry.buffer(0);
        return geometry;

    }

    public boolean containsPoint(double x, double y) {
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(new Coordinate[]{new Coordinate(x,y)});
        Point point = new Point(coordinateArraySequence, geometryFactory);
        return geometry.contains(point);
    }

    public void select() {}

    public void deselect() {}

    public void inElement() {}

    public void outElement() {}

    public void offsetCoordinates () {
        if (offset != null) {
            for (double[] coord : coordinates) {
                coord[0] = coord[0] + offset[0];
                coord[1] = coord[1] + offset[1];
            }

            calculateGeometry();

            for (ShapeGL shape : shapes) {
                shape.offsetCoordinates();
            }

        }
    }

    public void offsetCoordinates(double[] offset) {
        for (double[] coord : coordinates) {
            coord[0] = coord[0] + offset[0];
            coord[1] = coord[1] + offset[1];
        }
        calculateGeometry();
    }

    public void setOffset(double[] offset) {
        this.offset = offset;
        for(ShapeGL shape : shapes) {
            shape.setOffset(offset);
        }
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setVisible (boolean visible) {
        for (ShapeGL shape : shapes) {
            if(shape instanceof PolygonGL) {
                shape.setVisible(visible);
            }
        }
    }

    double distancePointToLine(Coordinate p1, Coordinate p2, double x0, double y0) {
        return Math.abs((p2.y-p1.y)*x0-(p2.x-p1.x)*y0+p2.x*p1.y-p2.y*p1.x)/Math.sqrt(Math.pow((p2.y-p1.y),2)+Math.pow((p2.x-p1.x),2));

    }

    double minDistanteToEdge(double[] p) {
        double minDistance = Double.MAX_VALUE;
        for (int i=0; i<geometry.getCoordinates().length-1; i++) {
            double currentDistance = distancePointToLine(geometry.getCoordinates()[i], geometry.getCoordinates()[i+1], p[0], p[1]);
            if ( currentDistance < minDistance) {
                minDistance = currentDistance;
            }
        }
        return minDistance;
    }

    ArrayList<Double> calculateAngles() {

        ArrayList<Double> angles = new ArrayList<>();
        Vector2d vn0 = new Vector2d(coordinates.get(coordinates.size()-1)[0]-coordinates.get(0)[0],coordinates.get(coordinates.size()-1)[1]-coordinates.get(0)[1]);
        Vector2d v10 = new Vector2d(coordinates.get(1)[0]-coordinates.get(0)[0], coordinates.get(1)[1]-coordinates.get(0)[1]);
        double dotProduct = vn0.dot(v10);
        double cos = dotProduct/(v10.length()*vn0.length());
        angles.add(Math.acos(cos));
        for (int i=1; i<coordinates.size()-1; i++) {
            Vector2d v1 = new Vector2d(coordinates.get(i-1)[0]-coordinates.get(i)[0], coordinates.get(i-1)[1]-coordinates.get(i)[1]);
            Vector2d v2 = new Vector2d(coordinates.get(i+1)[0]-coordinates.get(i)[0], coordinates.get(i+1)[1]-coordinates.get(i)[1]);
            dotProduct = v1.dot(v2);
            cos = dotProduct/(v1.length()*v2.length());
            angles.add(Math.acos(cos));
        }

        return angles;
    }

    ArrayList<Double[]> getBissectors() {
        ArrayList<Double[]> bissectors = new ArrayList<>();
        Vector2d vn0 = new Vector2d(coordinates.get(coordinates.size()-1)[0]-coordinates.get(0)[0],coordinates.get(coordinates.size()-1)[1]-coordinates.get(0)[1]);
        Vector2d v10 = new Vector2d(coordinates.get(1)[0]-coordinates.get(0)[0], coordinates.get(1)[1]-coordinates.get(0)[1]);
        vn0.normalize();
        v10.normalize();
        return bissectors;

    }

    public double[] getOffset() {
        return offset;
    }

    public void bufferCoordinates(double buffer) {

        geometry = bufferCoordinates(buffer, geometry, coordinates);
    }

    public Geometry bufferCoordinates(double buffer, Geometry geometry, List<double[]> coordinates) {
        coordinates.clear();
        BufferOp bufferOp = new BufferOp(geometry);
        bufferOp.setEndCapStyle(BufferOp.CAP_FLAT);

        //Geometry newGeometry = bufferOp.getResultGeometry(buffer);

        //Geometry newGeometry = geometry.buffer(buffer, 100);
        Geometry newGeometry = geometry.buffer(buffer,10,BufferOp.CAP_SQUARE);
        newGeometry = DouglasPeuckerSimplifier.simplify(newGeometry, Math.abs(buffer) / 100);
        for (int i=0; i<newGeometry.getCoordinates().length-1; i++) {
            coordinates.add(new double[] {newGeometry.getCoordinates()[i].x, newGeometry.getCoordinates()[i].y});
        }
        geometry = newGeometry;
        return geometry;

    }

    public void refreshGeometry (Geometry newGeometry) {
        coordinates.clear();
        for (int i=0; i<newGeometry.getCoordinates().length-1; i++) {
            coordinates.add(new double[] {newGeometry.getCoordinates()[i].x, newGeometry.getCoordinates()[i].y});
        }
        geometry = newGeometry;
    }


    public abstract void bufferPolygon(double bufferOffset);

    public Coordinate getLocation () {
        return geometry.getCentroid().getCoordinate();
    }

    public void setTexture(Texture texture) {}


    public void transformCoordinates(AffineTransformation affineTransformation) {
        for (double[] coord:coordinates) {
            //System.out.println("old coordinate " + coord[0] +":" +coord[1]);
            Coordinate newCoord = new Coordinate(coord[0], coord[1]);

            affineTransformation.transform(newCoord, newCoord);
            //System.out.println("old coordinate " + newCoord.x +":" +newCoord.y);
            coord[0] = newCoord.x;
            coord[1] = newCoord.y;
        }
        calculateGeometry();
    }

    public void revertTransform() {
    }

    public void printCoordinates() {
        System.out.println("--------------Geo Element Coordinate ------------------");
        for (double[] coordinate: coordinates) {
            System.out.println("Coordinate: "+coordinate[0]+":"+coordinate[1]);
        }
    }

    public void copyInOriginalCoordinates() {
        if (originalCoordinates==null) {
            originalCoordinates = new ArrayList<>();
            for (double[] coord : coordinates) {
                originalCoordinates.add(new double[]{coord[0], coord[1]});
            }
        }
    }

    public void setTranslucentBorder(boolean translucent) {
        this.translucentBorder = translucent;
    }

    public boolean getTranslucentBorder() {
        return translucentBorder;
    }

    public void setBlurredTexture (Texture blurredTexture) {
        for (ShapeGL shape : shapes) {
            if (shape instanceof PolygonGL) {
                ((PolygonGL) shape).setBlurredTexture(blurredTexture);
            }
        }
        this.blurredTexture = blurredTexture;
    }

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }



    public Texture getBlurredTexture() {
        return blurredTexture;
    }

    public void setRenderingType(ShaderManager.RenderedElementTypes renderingType) {
        this.renderingType = renderingType;
    }

    public ShaderManager.RenderedElementTypes getRenderingType() {
        return renderingType;
    }

    public void setFillColor(Vector3f fillColor) {
        this.fillColor = fillColor;
    }

    public Vector3f getFillColor() {
        return fillColor;
    }

    public void setOutlineColor(Vector3f outlineColor) {
        this.outlineColor = outlineColor;
    }

    public Vector3f getOutlineColor() {
        return outlineColor;
    }

    public double getBuffer() {
        return buffer;
    }

    public void setBuffer(double buffer) {
        this.buffer = buffer;
    }

    public boolean getIsIn() {
        return isIn;
    }

    public boolean getIsDocked() {
        return isDocked;
    }

    public void setIsDocked(boolean isDocked) {
        this.isDocked = isDocked;
    }

    public HashMap<String, List<Mat>> getLbpHists() {
        return lbpHists;
    }

    public void addLbpHist (Mat lbpHist, String method) {
        if (lbpHists.get(method) == null) {
            lbpHists.put(method, new ArrayList<Mat>());
        }
        lbpHists.get(method).add(lbpHist);
    }





}
