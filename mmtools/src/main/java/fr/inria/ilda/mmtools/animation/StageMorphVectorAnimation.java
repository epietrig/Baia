/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;


import fr.inria.ilda.mmtools.cv.CVUtilities;
import org.opencv.core.Mat;

import java.util.List;

/**
 * Created by mjlobo on 13/01/2017.
 */
public class StageMorphVectorAnimation extends StageMorphAnimation {

    List<Double[]> startBoundary;
    List<Double[]> endBoundary;
    int nSteps;


    public StageMorphVectorAnimation(Type type, float duration, List<Double[]> startBoundary, List<Double[]> endBoundary,
                                     int nSteps) {
        super(type, duration);
        this.startBoundary = startBoundary;
        this.endBoundary = endBoundary;
        this.nSteps = nSteps;
    }

    public StageMorphVectorAnimation(AbstractAnimation.Type type, float duration, boolean blurBorder, int blurWidth, List<Double[]> startBoundary, List<Double[]> endBoundary,
                                     int nSteps) {
        super(type, duration, blurBorder, blurWidth);
        this.startBoundary = startBoundary;
        this.endBoundary = endBoundary;
        this.nSteps = nSteps;
    }

    public StageMorphVectorAnimation(Type type) {
        super(type);
    }

    public List<Double[]> getStartBoundary() {
        return startBoundary;
    }

    public List<Double[]> getEndBoundary() {
        return endBoundary;
    }

    public int getnSteps() {
        return nSteps;
    }
}


