/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.opencv.core.Mat;

/**
 * Created by mjlobo on 19/02/16.
 */
public class ImageModel {
    Mat mat;
    Texture texture;
    Constants.ImageModelName name;

    public ImageModel(Mat mat, RGBRasterGeoElement raster, Constants.ImageModelName name) {
        this.mat = mat;
        this.texture = calculateTexture(raster);
        this.name = name;
    }

    Texture calculateTexture(RGBRasterGeoElement raster) {
        double[] lowerCorner = new double[] {raster.getLowerCorner()[0],raster.getLowerCorner()[1]};
        texture = new Texture(raster.getPixels_per_unit(),raster.getRealWidth(),raster.getRealHeight(), lowerCorner, raster.getImageWidth(), raster.getImageHeight());
        return  texture;
    }

    public Mat getMat() {
        return mat;
    }

    public Texture getTexture() {
        return texture;
    }

    public Constants.ImageModelName getName() {
        return name;
    }

}
