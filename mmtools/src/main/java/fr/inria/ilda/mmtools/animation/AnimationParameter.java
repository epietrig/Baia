/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 04/04/2017.
 */
public class AnimationParameter {

    String name;
    String value;
    List<AnimationParameter> attributes;

    public AnimationParameter(String name, String value) {
        this.name = name;
        this.value = value;
        attributes = new ArrayList<AnimationParameter>();
    }

    public AnimationParameter(String name) {
        this.name = name;
        attributes = new ArrayList<AnimationParameter>();
    }

    public void addAttribute(String name, String value) {
        attributes.add(new AnimationParameter(name,value));
    }

    public void addAttribute(AnimationParameter animationParameter) {
        attributes.add(animationParameter);
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public List<AnimationParameter> getAttributes() {
        return attributes;
    }

}
