/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui;


import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.stateMachine.MenuActionEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 07/07/15.
 */
public class PopUpMenu {
    JPopupMenu popup;
    Collection<String> actions;
    HashMap <String, JMenuItem> menuItems;

    public PopUpMenu(List<String> actions) {
        this.actions = actions;
        menuItems = new HashMap<>();
        createPopUpMenuActions();
    }

    public void createPopupMenu() {
        popup = new JPopupMenu();

        for (String action: actions) {
            final JMenuItem menuItem = new JMenuItem(action);
            menuItems.put(action, menuItem);
            popup.add(menuItem);
        }
    }

    public void createPopUpMenuActions() {
        popup = new JPopupMenu();
        for (String action: actions) {
            final JMenuItem menuItem = new JMenuItem(action);
            menuItems.put(action, menuItem);
            popup.add(menuItem);
        }
    }

    public void addListeners(final StateMachine stateMachine) {
        for (final JMenuItem item: menuItems.values()) {
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    stateMachine.handleEvent(new MenuActionEvent(item, item.getText()));
                }
            });
        }
    }





    public void showPopUpMenu(int x, int y, Component component) {
        popup.setVisible(true);
        popup.show(component,x,y);
    }

    public void showPopUpMenu(int x, int y, Component component, boolean showActions) {
        if (showActions) {
            for (String action:actions) {
                menuItems.get(action).setEnabled(true);
            }
        }
        else {
            for (String action:actions) {
                menuItems.get(action).setEnabled(false);
            }
        }
        popup.setVisible(true);
        popup.show(component,x,y);
    }

    public void enableMenuItem(String item, boolean enabled) {
        menuItems.get(item).setEnabled(enabled);
    }

    public HashMap<String, JMenuItem> getMenuItems() {
        return menuItems;
    }






}

class PopupListener extends MouseAdapter {
    JPopupMenu popup;

    PopupListener(JPopupMenu popupMenu) {
        popup = popupMenu;
    }

    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            popup.show(e.getComponent(),
                    e.getX(), e.getY());
        }
    }
}

