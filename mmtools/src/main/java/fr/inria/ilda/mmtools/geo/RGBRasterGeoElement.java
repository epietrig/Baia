/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.gl.PolygonGL;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.gl.ShapeGL;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.geotools.coverage.grid.GridCoverage2D;
import org.opencv.core.Mat;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mjlobo on 21/07/15.
 */
public class RGBRasterGeoElement extends RasterGeoElement {

    byte[] imageData;
    Color[][] colors;
    Mat originalMat;
    HashMap<Color, float[]> colorBands;
    java.util.List<Tile> tiles;
    boolean isTiled = false;

    java.util.List<ImageModel> imageModels = new ArrayList<>();
    PolygonGL colorSelectionPolygon;
    Texture maskTexture;



    public RGBRasterGeoElement(GridCoverage2D coverage) {
        super (coverage);
        imageData = getRGBAPixelData(coverage.getRenderedImage());
        getTexture().setData(mat.clone());
        //System.out.println("mat "+mat);
        colorBands = CVUtilities.calculateHist(mat);
        originalMat = mat;
        ImageModel cieLab = new ImageModel(CVUtilities.RGBtoCIELab(mat),this, Constants.ImageModelName.CIELAB);
        ImageModel hsv = new ImageModel(CVUtilities.RGBtoHSV(mat),this, Constants.ImageModelName.HSV);
        imageModels.add(cieLab);
        imageModels.add(hsv);
        colorSelectionPolygon = new PolygonGL(polygon);
        colorSelectionPolygon.setVisible(false);
        colorSelectionPolygon.setTexture(cieLab.getTexture());
        colorSelectionPolygon.setTexCoords(texCoords);
        shapes.add(colorSelectionPolygon);
    }

    public RGBRasterGeoElement(Mat mat) {
        super(mat);
        this.mat = mat;
        getTexture().setData(mat.clone());
        imageData = CVUtilities.matToByteArrayBGR(mat);
        colorBands = CVUtilities.calculateHist(mat);
        originalMat = mat;
        ImageModel cieLab = new ImageModel(CVUtilities.RGBtoCIELab(mat),this, Constants.ImageModelName.CIELAB);
        ImageModel hsv = new ImageModel(CVUtilities.RGBtoHSV(mat),this, Constants.ImageModelName.HSV);
        imageModels.add(cieLab);
        imageModels.add(hsv);
        colorSelectionPolygon = new PolygonGL(polygon);
        colorSelectionPolygon.setVisible(false);
        colorSelectionPolygon.setTexture(cieLab.getTexture());
        colorSelectionPolygon.setTexCoords(texCoords);
        shapes.add(colorSelectionPolygon);

    }

    public RGBRasterGeoElement(Mat mat, ShaderManager.RenderedElementTypes renderedElementType) {
        super(mat, renderedElementType);
        this.mat = mat;
        getTexture().setData(mat.clone());
        //imageData = CVUtilities.matToByteArray(mat);
        imageData = CVUtilities.matToByteArrayBGR(mat);
        colorBands = CVUtilities.calculateHist(mat);
        originalMat = mat;
        ImageModel cieLab = new ImageModel(CVUtilities.RGBtoCIELab(mat),this, Constants.ImageModelName.CIELAB);
        ImageModel hsv = new ImageModel(CVUtilities.RGBtoHSV(mat),this, Constants.ImageModelName.HSV);
        imageModels.add(cieLab);
        imageModels.add(hsv);
        colorSelectionPolygon = new PolygonGL(polygon);
        colorSelectionPolygon.setVisible(false);
        colorSelectionPolygon.setTexture(cieLab.getTexture());
        colorSelectionPolygon.setTexCoords(texCoords);
        shapes.add(colorSelectionPolygon);

    }




    public RGBRasterGeoElement(GridCoverage2D coverage, boolean isTiled) {
        super(coverage);
        imageData = getRGBAPixelData(coverage.getRenderedImage());
        colorBands = CVUtilities.calculateHist(mat);
        originalMat = mat;
        this.isTiled = isTiled;
        if (isTiled) {
            tiles = CVUtilities.tileImage(this,Constants.MAX_TEXTURE_SIZE);
            for (Tile tile: tiles) {
                shapes.add(tile.getPolygon());
            }
        }
    }

    public byte[] getImageData() {
        return  imageData;
    }

    public byte[] getRGBAPixelData(RenderedImage img) {
        //System.out.println("number of bands " + img.getColorModel().getNumComponents());
        byte[] imgRGBA;
        int height = img.getHeight();
        int width = img.getWidth();

        //ComponentColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8}, false, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
        ComponentColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8}, false, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);


        WritableRaster raster  = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, 3, null);
        BufferedImage newImage = new BufferedImage(colorModel, raster, false, null);
        DataBufferByte dataBuf = (DataBufferByte) raster.getDataBuffer();
        java.awt.geom.AffineTransform gt = new java.awt.geom.AffineTransform();
        gt.translate(0, height);
        gt.scale(1,-1d);
        Graphics2D g = newImage.createGraphics();
        g.drawRenderedImage(img,gt);
        g.dispose();
        imgRGBA = dataBuf.getData();
        mat = CVUtilities.bufferedImageToMat(newImage);
        //CVUtilities.getELBP(CVUtilities.grayScale(mat),1,4);

        return imgRGBA;
    }

    private Color[][] convertTo2DWithoutUsingGetRGB(byte[] pixels, BufferedImage image) {

        final int width = image.getWidth();
        final int height = image.getHeight();
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;



        int[][] result = new int[height][width];
        Color[][] resultColor = new Color[width][height];
        if (hasAlphaChannel) {
            final int pixelLength = 4;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;

                int red = ((int) pixels[pixel] & 0xff);
                int green = (((int) pixels[pixel+1] & 0xff)); // green
                int blue = (((int) pixels[pixel + 2] & 0xff));
                int alpha = (((int) pixels[pixel] & 0xff) << 24);

                result[row][col] = argb;
                resultColor[col][height-row-1] = new Color (red, green, blue);
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        } else {
            final int pixelLength = 3;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += -16777216; // 255 alpha
                argb += ((int) pixels[pixel] & 0xff); // blue
                argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
                resultColor[col][row] = new Color (argb);
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        }

        return resultColor;
    }




    public Color[][] getColors() {
        return colors;
    }

    public Mat getMat() {
        return  mat;
    }

    public HashMap<Color, float[]> getColorBands() {
        return colorBands;
    }

    public void setMat(Mat mat) {
        this.mat = mat;
        imageData = CVUtilities.matToByteArrayBGR(mat);
        colorBands = CVUtilities.calculateHist(mat);
    }

    public void revertToOriginal() {
        this.mat = originalMat;
        imageData = CVUtilities.matToByteArrayBGR(mat);
        colorBands = CVUtilities.calculateHist(mat);
    }

    public void setVisible (boolean visible) {
        for (ShapeGL shape : shapes) {
            if(shape instanceof PolygonGL && shape != colorSelectionPolygon) {
                shape.setVisible(visible);
            }
        }
    }


    public boolean getIsTiled () {
        return isTiled;
    }

    public java.util.List<Tile> getTiles() {
        return tiles;
    }


    public java.util.List<ImageModel> getImageModels() {
        return imageModels;
    }

    public PolygonGL getColorSelectionPolygon() {
        return colorSelectionPolygon;
    }

    public void setMaskTexture(Texture maskTexture) {
        this.maskTexture = maskTexture;
    }

    public Texture getMaskTexture() {
        return maskTexture;
    }

    public ImageModel getImageModelByType(Constants.ImageModelName imageModelName) {
        for (ImageModel imageModel : imageModels) {
            if (imageModel.getName() == imageModelName) {
                return imageModel;
            }
        }
        return null;
    }


}
