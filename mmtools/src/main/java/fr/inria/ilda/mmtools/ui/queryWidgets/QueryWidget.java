/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui.queryWidgets;

import fr.inria.ilda.mmtools.stateMachine.StateMachine;

import javax.swing.*;

public abstract class QueryWidget extends JComponent {
	public QueryWidget() {
		super();
	}
	public abstract String getQuery();
	public abstract void attachToStateMachine (StateMachine stateMachine);
	public abstract void addListeners(final StateMachine stateMachine);
	public abstract void resetValues();
}
