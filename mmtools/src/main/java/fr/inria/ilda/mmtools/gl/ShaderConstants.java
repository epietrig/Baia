/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

/**
 * Created by mjlobo on 20/04/16.
 */
public class ShaderConstants {
    public static String MV_MATRIX = "mv_matrix";
    public static String PROJ_MATRIX = "proj_matrix";
    public static String POSITION = "position";
    public static String TEXTURE_POSITION = "texPos";
    public static String SAMPLER = "s";
    public static String OFFSET = "offset";
    public static String TEXTURE_DIMENSION = "textureDimension";
    public static String TEXTURE_LOWER_CORNER = "textureLowerCorner";
    public static  String ALPHA = "alpha";
    public static  String OFFSET_TEXTURE = "offsetTexture";
    public static  String LINE_WIDTH = "u_linewidth";
    public static  String NORMAL = "normal";
    public static  String COLOR = "color";
    public static  String BLURRED_SAMPLER = "blurred";
    public static  String BLUR_MULTIPLY_VEC = "blurMultiplyVec";
    public static  String PIXEL_SIZE = "pixel_size";
    public static  String BLUR_RADIUS = "radius";
    public static  String START_COLOR_RGB = "startColorRGB";
    public static  String END_COLOR_RGB = "endColorRGB";
    public static  String DURATION = "duration";
    public static  String TIC = "tic";
    public static  String INIT_TEXTURE = "initTexture";
    public static  String MIN_VALUE = "minValue";
    public static  String MAX_VALUE = "maxValue";
    public static  String MIN = "min";
    public static  String MAX = "max";
    public static  String START_TEXTURE = "startTexture";
    public static  String END_TEXTURE = "endTexture";
    public static  String MASK_TEXTURE = "maskTexture";
    public static  String BG_TEXTURE = "bgTexture";
    public static  String INIT_MASK = "initMask";
    public static  String END_MASK = "endMask";
    public static  String PIXEL_WIDTH = "pixel_width";
    public static  String PIXEL_HEIGHT = "pixel_height";
    public static  String IN_MASK_TEXTURE = "inMaskTexture";
    public static  String SEMANTIC_TEXTURE = "senmanticTexture";
    public static  String BACKGROUND_TEXTURE = "backgroundTexture";
    public static  String MASK = "mask";
    public static  String COMPARE_COLOR = "compareColor";
    public static  String THRESHOLD = "threshold";



}
