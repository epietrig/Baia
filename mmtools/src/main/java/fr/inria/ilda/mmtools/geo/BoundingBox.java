/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

/**
 * Created by mjlobo on 02/02/16.
 */
public class BoundingBox {
    double width;
    double height;
    double[] lowerCorner;
    Geometry geometry;
    GeometryFactory geometryFactory;

    public BoundingBox(Geometry envelope) {
        double x1 = envelope.getCoordinates()[0].x;
        double x2 = envelope.getCoordinates()[2].x;
        double y1 = envelope.getCoordinates()[0].y;
        double y2 = envelope.getCoordinates()[1].y;
        lowerCorner = new double[] {x1, y1};
        width = x2-x1;
        height = y2-y1;
        this.geometry = envelope;

    }

    public BoundingBox(double width, double height, double[] lowerCorner) {
        this.width = width;
        this.height = height;
        this.lowerCorner = lowerCorner;
        geometryFactory = new GeometryFactory();
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(new Coordinate[]{new Coordinate(lowerCorner[0],lowerCorner[1]), new Coordinate(lowerCorner[0], lowerCorner[1]+height),
                new Coordinate(lowerCorner[0]+width,lowerCorner[1]+height),new Coordinate(lowerCorner[0]+width, lowerCorner[1]+height),
                new Coordinate(lowerCorner[0],lowerCorner[1])});
        LinearRing linearRing = new LinearRing(coordinateArraySequence,geometryFactory);
        geometry = new Polygon(linearRing, new LinearRing[0],geometryFactory);
    }

    public void union(Geometry newGeometry) {
        geometry = geometry.union(newGeometry);
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double[] getLowerCorner() {
        return lowerCorner;
    }

    public Geometry getGeometry() {
        return geometry;
    }


}
