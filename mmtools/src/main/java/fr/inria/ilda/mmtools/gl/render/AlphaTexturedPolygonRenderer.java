/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3f;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 25/04/16.
 */
public class AlphaTexturedPolygonRenderer extends AbstractPolygonGLRenderer {
    ShaderProgram program;

    public AlphaTexturedPolygonRenderer(ShaderProgram program) {
        super(program);
    }

    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Texture blurredTexture, boolean docked) {
        // System.out.println("rendering translucent polygon ");
        ShaderProgram drawn = program;
        GL4 gl = (GL4)drawable.getGL();
        PolygonGL polygon = (PolygonGL)shapeGL;
        int vertexAttributeId = drawn.getAttribute(ShaderConstants.POSITION);
        int textureAttributeId = drawn.getAttribute(ShaderConstants.TEXTURE_POSITION);
        gl.glUseProgram(drawn.getProgramId());
        gl.glUniformMatrix4fv(drawn.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
        gl.glUniformMatrix4fv(drawn.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        BufferGL texBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
        gl.glEnableVertexAttribArray(vertexAttributeId);
        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(textureAttributeId);
        gl.glBindBuffer(texBuffer.getTarget(), texBuffer.getBuffer());
        gl.glVertexAttribPointer(textureAttributeId, texBuffer.getSize(), GL.GL_FLOAT, false, 0,0);
        gl.glUniform2f(drawn.getUniform("offset"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        if (docked) {
            gl.glUniform2f(drawn.getUniform("offsetTexture"),0.0f,0.0f);
        }
        else {
            gl.glUniform2f(drawn.getUniform("offsetTexture"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        }
        gl.glUniform2f(drawn.getUniform("textureDimension"), (float)(polygon.getTexture().getWidth()), (float)(polygon.getTexture().getHeight()));
        gl.glUniform1f(drawn.getUniform("alpha"), polygon.getAlpha());
        gl.glUniform2f(drawn.getUniform("bgLowerCorner"), (float)polygon.getTexture().getLowerCorner()[0], (float)polygon.getTexture().getLowerCorner()[1]);
        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        gl.glActiveTexture(gl.GL_TEXTURE0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, ((PolygonGL) polygon).getTexture().getID());
        gl.glActiveTexture(gl.GL_TEXTURE1);
        gl.glBindTexture(gl.GL_TEXTURE_2D, blurredTexture.getID());
        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);

    }

    @Override
    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Texture secondaryTexture, Vector3f color, LayerManager layerManager) {
        render(shapeGL, drawable, camera, secondaryTexture, false);
    }
}
