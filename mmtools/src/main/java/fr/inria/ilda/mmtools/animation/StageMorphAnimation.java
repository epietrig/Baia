/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 11/09/16.
 */
public class StageMorphAnimation extends StagePlanAnimation {

    public StageMorphAnimation(Type type, float duration) {
        super(type, duration);
    }

    public StageMorphAnimation(AbstractAnimation.Type type, float duration, boolean blurBorder, int blurWidth) {
        super(type, duration, blurBorder, blurWidth);
    }

    public StageMorphAnimation(Type type) {
        super(type);
    }



    public boolean isComplete() {
        if (type == Type.ERODE && initMask!=null) {
            //System.out.println("ERODE!");
            return true;
        }
        if (initMask!=null && endMask!=null) {
            //System.out.println("initMask!=null && endMask!=null");
            return true;
        }
        else {
            return false;
        }
    }

    public void writeMasks(String path) {
        CVUtilities.writeImage(initMask, path+"/"+Constants.BEFORE_MASK_FILE_NAME);
        CVUtilities.writeImage(endMask, path+"/"+Constants.AFTER_MASK_FILE_NAME);
        CVUtilities.writeImage(maskMat, path+"/"+Constants.MASK_FILE_NAME);
    }

    public List<AnimationParameter> getValuesToSave() {
        List<AnimationParameter> valuesToSave = super.getValuesToSave();
        valuesToSave.add(new AnimationParameter(Constants.END_MASK_TAG, Constants.AFTER_MASK_FILE_NAME));
        return valuesToSave;
    }

    public List<String> getValuesToRetrieve() {
        java.util.List<String> valuestoRetrieve = super.getValuesToRetrieve();
        valuestoRetrieve.add(Constants.END_MASK_TAG);
        return valuestoRetrieve;
    }
}
