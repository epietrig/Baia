/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import com.vividsolutions.jts.geom.Geometry;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mjlobo on 13/02/15.
 */
public class VectorLayer extends Layer {

    String name;
    List<ShapeFileAttribute> attributes;
    String longestLocalName;
    HashSet <ShapeFileFeature> features;
    Vector3f color;
    boolean isPointLayer = false;
    public VectorLayer(boolean isVisible, String name) {

        super(isVisible);
        this.name = name;
        attributes = new ArrayList<>();
        features = new HashSet<>();
    }

    public VectorLayer(boolean isVisible, String name, ArrayList<GeoElement> elements) {
        super(isVisible);
        this.name = name;
        this.elements = elements;
    }

    public ArrayList<GeoElement> getElements() {
        return elements;
    }

    public void addElement (GeoElement geoElement) {
        elements.add(geoElement);
    }

    public void addElement(GeoElement geoElement, boolean isVisible) {
        elements.add(geoElement);
        geoElement.setVisible(isVisible);
    }

    public void setElements(ArrayList<GeoElement> elements) {
        this.elements = elements;

    }

    public String getName() {
        return name;
    }

    public void addAttribute (ShapeFileAttribute attribute) {
        attributes.add(attribute);
    }

    public void setAttributes (List<ShapeFileAttribute> attributes) {
        this.attributes = attributes;
        longestLocalName = "";
        for (ShapeFileAttribute attribute: attributes) {
            if (attribute.getLocalName().length()>longestLocalName.length()) {
                longestLocalName = attribute.getLocalName();
            }
        }
    }

    public String getLongestLocalName () {
        return longestLocalName;
    }

    public List<ShapeFileAttribute> getAttributes() {
        return attributes;
    }

    public void setFeatures(HashSet<ShapeFileFeature> features) {
        this.features = features;
    }

    public HashSet<ShapeFileFeature> getFeaturesInNumericAttributeRange (ShapeFileNumericAttribute attribute, double minValue, double maxValue) {
       return getFeaturesInNumericAttributeRange(attribute, minValue, maxValue, features);
    }

    public HashSet<ShapeFileFeature> getFeaturesInNumericAttributeRange (ShapeFileNumericAttribute attribute, double minValue, double maxValue, HashSet<ShapeFileFeature> featuresToConsider) {
        HashSet <ShapeFileFeature> result = new HashSet<>();
        //System.out.println("minValue "+minValue);
        //System.out.println("maxValue"+maxValue);
        for (ShapeFileFeature feature: featuresToConsider) {
            double value = feature.getShapeFileNumericAttributeValue(attribute);

            if (minValue <= value && value <= maxValue) {
                //System.out.println("Value "+value);
                result.add(feature);
            }
        }
        //System.out.println("result in getFeaturesInNUmericAttributeRange "+result.size());
        return result;
    }

    public HashSet <GeoElement> getElementsByStringAttributeValue (ShapeFileStringAttribute attribute, List<String> strings) {
        HashSet <GeoElement> result = new HashSet<>();
        for (ShapeFileFeature feature: features) {
            String value = feature.getShapeFileStringAttributeValue(attribute);
                if (strings.contains(value)) {
                    for (VectorGeoElement ge: feature.getVectorGeoElements()) {
                        result.add(ge);
                    }

                }
            }
        return result;
    }

    public HashSet<ShapeFileFeature> getFeaturesByStringAttributeValue(ShapeFileStringAttribute attribute, List<String> strings, HashSet<ShapeFileFeature> featuresToConsider) {
        HashSet <ShapeFileFeature> result = new HashSet<>();
        for (ShapeFileFeature feature: featuresToConsider) {
            String value = feature.getShapeFileStringAttributeValue(attribute);
            if (strings.contains(value)) {
                result.add(feature);
            }
        }
        return result;
    }

    public HashSet<ShapeFileFeature> getFeaturesByStringAttributeValue(ShapeFileStringAttribute attribute, List<String> strings) {
       return getFeaturesByStringAttributeValue(attribute, strings, features);
    }

    public HashSet<ShapeFileFeature> getFeaturesIntersectingGeometry (Geometry geometry, HashSet<ShapeFileFeature> featuresToConsider) {
        isVisible = true;
        HashSet<ShapeFileFeature> result = new HashSet<>();
        for (ShapeFileFeature feature : featuresToConsider) {
            for (GeoElement ge : feature.getVectorGeoElements()) {
                if (geometry.intersects(ge.getGeometry())) {
                    result.add(feature);
                }
            }
        }

        return result;
    }

    public HashSet <ShapeFileFeature> getFeaturesIntersectingGeometry (Geometry geometry) {
        return getFeaturesIntersectingGeometry(geometry, features);
    }

    public ShapeFileNumericAttribute getShapeFileNumericAttribute (String name) {
        for (ShapeFileAttribute attribute: attributes) {
            if (attribute.getLocalName()==name && attribute instanceof ShapeFileNumericAttribute) {
                return (ShapeFileNumericAttribute)attribute;
            }
        }
        return null;
    }

    public ShapeFileStringAttribute getShapeFileStringAttribute(String name) {
        for (ShapeFileAttribute attribute: attributes) {
            if (attribute.getLocalName() == name && attribute instanceof ShapeFileStringAttribute) {
                return (ShapeFileStringAttribute)attribute;
            }
        }
        return null;
    }

    public HashSet<ShapeFileFeature> getFeatures() {
        return features;
    }

    public ShapeFileFeature getFeatureForVectorGeoELement(VectorGeoElement vectorGeoElement) {
        for (ShapeFileFeature feature: features) {
            if (feature.getVectorGeoElements().contains(vectorGeoElement)) {
                return feature;
            }
        }

        return null;
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }

    public Vector3f getColor () {
        return color;
    }

    public boolean getIsPointLayer () {
        return isPointLayer;
    }

    public void setIsPointLayer(boolean isPointLayer) {
        this.isPointLayer = isPointLayer;
    }

}
