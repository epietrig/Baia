/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

/**
 * Created by mjlobo on 08/06/16.
 */
public class AbstractAnimation {
    public enum Type {SIMPLE,MASK_INIT,MASK_END, BAKGROUND_INIT, BACKGROUND_END, DILATE, ERODE, DIRECTION, SEMANTIC_INC, SEMANTIC_DEC, SEMANTIC_DEC_MASK,
        SEMANTIC_ERODE, SEMANTIC_DILATE, BLEND, RADIAL_IN, RADIAL_OUT, DEFORM, SEMANTIC, EQ, VECTOR_MORPH}
    float duration = 2;
    Type type;

    public float getDuration() {
        return duration;
    }

    public Type getType() {return type;}

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
