/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.animation.AbstractStagedAnimation;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.filters.TextureFilter;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.render.AnimationPlanRenderer;
import fr.inria.ilda.mmtools.gl.render.HeightMapRenderer;
import fr.inria.ilda.mmtools.gl.render.MaskRenderer;
import fr.inria.ilda.mmtools.gl.render.ThresholdedColoredRenderer;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.mmtools.utilties.FfmpegRecorder;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import javax.vecmath.Vector3d;
import java.awt.image.BufferedImage;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import static com.jogamp.opengl.GL.*;

/**
 * Created by mjlobo on 21/04/16.
 */
public class TextureManager {

    MapGLCanvas canvas;
    public TextureManager(MapGLCanvas canvas) {
        this.canvas = canvas;
    }
    TextureFilter mntSimpleFilter;
    TextureFilter maskTextureFilter;

    public int loadTextureFromRasterTiff(GLAutoDrawable drawable, RGBRasterGeoElement rasterGeoElement) {
        GL3 gl = (GL3)drawable.getGL();
        byte[] imgRGBA = rasterGeoElement.getImageData();

        ByteBuffer wrappedRGBA = ByteBuffer.wrap(imgRGBA);

        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];
        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);
        gl.glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, wrappedRGBA);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        rasterGeoElement.setTextureID(textureID);
        canvas.getLayerManager().settextureId(rasterGeoElement.getTexture(), textureID);
        return textureID;

    }

    public void loadImageModelTexture(GLAutoDrawable drawable, RGBRasterGeoElement rgbRasterGeoElement) {
        for (ImageModel imageModel : rgbRasterGeoElement.getImageModels()) {
            int textureID = loadTextureFloat3C(drawable, imageModel.getMat());
            canvas.getLayerManager().settextureId(imageModel.getTexture(), textureID);
            imageModel.getTexture().setID(textureID);
        }
    }

    public int loadTextureFloat3C(GLAutoDrawable drawable, Mat mat) {
        GL3 gl = (GL3)drawable.getGL();
        FloatBuffer buffer = FloatBuffer.wrap(CVUtilities.mat3CFToByteArray(mat));
        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];

        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);

        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_RGB16F, mat.cols(), mat.rows(), 0, gl.GL_RGB, GL.GL_FLOAT, buffer);

        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


        return textureID;

    }

    public int loadTextureFromTile(GLAutoDrawable drawable, Tile tile) {
        GL3 gl = (GL3)drawable.getGL();
        byte[] imgRGBA = tile.getTileData();

        ByteBuffer wrappedRGBA = ByteBuffer.wrap(imgRGBA);
        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];

        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);
        gl.glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        gl.glTexStorage2D(GL_TEXTURE_2D, 2, GL_RGB8, tile.getWidth(), tile.getHeight());
        gl.glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, tile.getWidth(), tile.getHeight(), GL_RGB, GL_UNSIGNED_BYTE, wrappedRGBA);
        gl.glGenerateMipmap(GL_TEXTURE_2D);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

        tile.getPolygon().getTexture().setID(textureID);
        return  textureID;
    }

    public Texture createScreenTexture(GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        int [] texID = new int[1];
        gl.glGenTextures(texID.length, texID, 0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, texID[0]);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_BASE_LEVEL, 0);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_MAX_LEVEL, 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        if (canvas.isRetina()) {
            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, canvas.getWidth() * 2, canvas.getHeight() * 2, 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
        }
        else {
            gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, canvas.getWidth(), canvas.getHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
        }
        double pixels_per_unit = canvas.getCamera().getCamera_pixels_per_unit();

        Texture texture = new Texture(pixels_per_unit,canvas.getCamera().getScreenRealWidth(),canvas.getCamera().getScreenRealHeight(), canvas.getCamera().getLowerCorner(), canvas.getWidth(), canvas.getHeight());
        texture.setID(texID[0]);
        return texture;
    }

    public void loadMntTexture (MNTRasterGeoElement mntRasterGeoElement, Layer layer, GLAutoDrawable drawable) {
        loadTextureFromRasterMNT(drawable, (MNTRasterGeoElement)mntRasterGeoElement);
        mntSimpleFilter =canvas.getFilterManager().initializeTextureFilter((RasterGeoElement)mntRasterGeoElement, drawable);
        canvas.getFilterManager().attachTextureToFbo(mntSimpleFilter.getFrameBufferObject(), mntSimpleFilter.getFrameBufferObject().getTexture(), drawable);
        drawRasterMNT(mntRasterGeoElement, drawable, layer);
        mntRasterGeoElement.setTexture(mntSimpleFilter.getFrameBufferObject().getTexture());
        mntRasterGeoElement.getTexture().setName(mntRasterGeoElement.getName());
        canvas.getLayerManager().settextureId(mntRasterGeoElement.getTexture(), mntRasterGeoElement.getTexture().getID());

    }

    public void drawRasterMNT (MNTRasterGeoElement mntRasterGeoElement, GLAutoDrawable drawable, Layer layer) {
        GL3 gl = (GL3) drawable.getGL();
        mntSimpleFilter.getFrameBufferObject().refreshTexture(mntRasterGeoElement.getRealWidth(), mntRasterGeoElement.getRealHeight(), mntRasterGeoElement.getLowerCorner(), mntRasterGeoElement.getOriginalTexture().getPixels_per_unit(), mntRasterGeoElement.getOriginalTexture().getPixels_per_unit_array());
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, mntSimpleFilter.getFrameBufferObject().getVboiD());
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glBindVertexArray(layer.getVao());
        FloatBuffer byteBuffer = FloatBuffer.allocate((int)mntRasterGeoElement.getImageWidth()*(int)mntRasterGeoElement.getImageHeight()*3);
        gl.glViewport(0, 0, mntRasterGeoElement.getImageWidth(), mntRasterGeoElement.getImageHeight());
        ShaderProgram program;
        if (mntRasterGeoElement.getInterpolationMethod() == Constants.SIGMOID_INTERPOLATION) {
            program = canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.SIGMOID);
        }
        else {
            program = canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.ALTITUDE);
        }
        HeightMapRenderer heightMapRenderer = new HeightMapRenderer(program);
        heightMapRenderer.render(mntRasterGeoElement,drawable,canvas.getCamera(), canvas.getLayerManager());
        gl.glViewport(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    public int loadTextureFromRasterMNT (GLAutoDrawable drawable, MNTRasterGeoElement rasterGeoElement) {
        GL3 gl = (GL3)drawable.getGL();
        Buffer buffer = FloatBuffer.wrap(rasterGeoElement.getMntData());
        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];
        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_R16F, rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight(), 0, gl.GL_RED, GL.GL_FLOAT, buffer);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        rasterGeoElement.setTextureID(textureID);
        rasterGeoElement.setOriginalTexture(rasterGeoElement.getTexture());

        return textureID;
    }

    public Texture createTextureFromTexture( RasterGeoElement rasterGeoElement, GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        int [] texID = new int[1];
        gl.glGenTextures(texID.length, texID, 0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, texID[0]);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_BASE_LEVEL, 0);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_MAX_LEVEL, 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
        double pixels_per_unit =rasterGeoElement.getPixels_per_unit();
        Texture texture = new Texture(pixels_per_unit,rasterGeoElement.getRealWidth(),rasterGeoElement.getRealHeight(), rasterGeoElement.getLowerCorner(), rasterGeoElement.getImageWidth(), rasterGeoElement.getImageHeight());
        texture.setID(texID[0]);
        return texture;
    }

    public Texture createTextureFromTexture( Texture rasterTexture, GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        int [] texID = new int[1];
        gl.glGenTextures(texID.length, texID, 0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, texID[0]);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_BASE_LEVEL, 0);
        gl.glTexParameteri(GL_TEXTURE_2D, gl.GL_TEXTURE_MAX_LEVEL, 0);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, rasterTexture.getPixelWidth(), rasterTexture.getPixelHeight(), 0, GL_BGRA, GL_UNSIGNED_BYTE, null);
        double pixels_per_unit =rasterTexture.getPixels_per_unit();
        Texture texture = new Texture(pixels_per_unit,rasterTexture.getWidth(),rasterTexture.getHeight(), rasterTexture.getLowerCorner(), rasterTexture.getPixelWidth(), rasterTexture.getPixelHeight());
        texture.setID(texID[0]);
        return texture;
    }

    public int loadTextureFromMatRGB(GLAutoDrawable drawable, Mat mat) {
        GL3 gl = (GL3)drawable.getGL();

//
        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];

        reloadTextureFromMatRGB(drawable, mat, textureID);

        return textureID;

    }

    public void reloadTextureFromMatRGB(GLAutoDrawable drawable, Mat mat, int textureID) {
        GL3 gl = (GL3)drawable.getGL();
        Buffer buffer;
        buffer = ByteBuffer.wrap(CVUtilities.matToByteArrayBGR(mat));

        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);


        gl.glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, mat.cols(), mat.rows(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, buffer);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    }

    public int loadTextureFromMatOneBand(GLAutoDrawable drawable, Mat mat) {

        Buffer buffer;

        GL3 gl = (GL3)drawable.getGL();

        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];

       reloadTextureFromMatOneBand(drawable, mat, textureID);

        return textureID;
    }

    public void reloadTextureFromMatOneBand(GLAutoDrawable drawable, Mat mat, int textureID) {
        Buffer buffer = ByteBuffer.wrap(CVUtilities.matOneBandToByteArray(mat));
        GL3 gl = (GL3)drawable.getGL();
        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);


        gl.glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_RED, mat.cols(), mat.rows(), 0, gl.GL_RED, GL.GL_UNSIGNED_BYTE, buffer);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    }

    public int loadTextureOneBandFloat (GLAutoDrawable drawable, Mat mat) {
        GL3 gl = (GL3)drawable.getGL();

//
        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];


        reloadTextureOneBandFloat(drawable, mat, textureID);

        return textureID;
    }

    public void reloadTextureOneBandFloat(GLAutoDrawable drawable, Mat mat, int textureID) {
        GL3 gl = (GL3)drawable.getGL();
        FloatBuffer buffer = FloatBuffer.wrap(CVUtilities.bandToByteArray(mat));

//        for (int i =0; i<buffer.limit(); i++) {
//            System.out.println("Float value when loading texture "+buffer.get(i))

        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);

        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_R16F, mat.cols(), mat.rows(), 0, gl.GL_RED, GL.GL_FLOAT, buffer);

        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    public int loadTexture2BandFloat(GLAutoDrawable drawable, Mat mat) {
        //System.out.println("loadTexture2BandFloat");

        GL3 gl = (GL3)drawable.getGL();

        int[] textureIDs = new int [1];
        gl.glGenTextures(1,textureIDs,0);
        int textureID=textureIDs[0];

        reloadTexture2BandFloat(drawable, mat, textureID);
        return textureID;
    }

    public void reloadTexture2BandFloat(GLAutoDrawable drawable, Mat mat, int textureID) {
        GL3 gl = (GL3)drawable.getGL();
        FloatBuffer buffer = FloatBuffer.wrap(CVUtilities.bandToByteArray(mat));
        gl.glBindTexture(GL.GL_TEXTURE_2D, textureID);
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_RG16F, mat.cols(), mat.rows(), 0, gl.GL_RG, GL.GL_FLOAT, buffer);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    public void loadTexureFromMat(Texture texture, Mat mat, GLAutoDrawable drawable) {
        int textureID=0;
        if (mat.channels()==3) {
            textureID = loadTextureFromMatRGB(drawable, mat);
        }
        else if (mat.channels() == 1) {
            if (mat.type() == CvType.CV_32F) {
                textureID = loadTextureOneBandFloat(drawable, mat);
            } else {
                textureID = loadTextureFromMatOneBand(drawable, mat);
            }
        }
        else if (mat.channels() == 2) {
            if (mat.type() == CvType.CV_32FC2) {
                textureID = loadTexture2BandFloat(drawable, mat);
            }
        }
        texture.setID(textureID);
        canvas.getLayerManager().settextureId(texture, textureID);
        //System.out.println("LOAD TEXTURE FROM MAT" + textureID);
    }

    public void reloadTexureFromMat(Texture texture, Mat mat, GLAutoDrawable drawable) {
        int textureID = canvas.getLayerManager().getTextureId(texture);
        if (mat.channels()==3) {
            reloadTextureFromMatRGB(drawable, mat, textureID);
        }
        else if (mat.channels() == 1) {
            //System.out.println("one band");
            if (mat.type() == CvType.CV_32F) {
                reloadTextureOneBandFloat(drawable, mat, textureID);
            } else {
                reloadTextureFromMatOneBand(drawable, mat, textureID);
            }
        }
        else if (mat.channels() == 2) {
            if (mat.type() == CvType.CV_32FC2) {
                reloadTexture2BandFloat(drawable, mat, textureID);
            }
        }
    }

    public void updateTextureFromMat(Texture texture, Mat mat, GLAutoDrawable drawable) {
        if (canvas.getLayerManager().getTextureIds().containsKey(texture)) {
            reloadTexureFromMat(texture, mat, drawable);
        }
        else {
            loadTexureFromMat(texture, mat, drawable);
        }
    }


    public void drawMask(Layer layer, GeoElement ge, GLAutoDrawable drawable) {

        ColorThresholdGeoElement colorThresholdGeoElement = (ColorThresholdGeoElement)ge;
        GL3 gl = (GL3) drawable.getGL();
        if (maskTextureFilter == null) {
            maskTextureFilter = canvas.getFilterManager().initializeTextureFilter(colorThresholdGeoElement.getTextureToThreshold(), drawable);
        }
        Texture maskTexture = null;
        if (colorThresholdGeoElement.getMaskedTexture()== null) {
            maskTexture = createTextureFromTexture(colorThresholdGeoElement.getTextureToThreshold(), drawable);
        }
        else {
            maskTexture = colorThresholdGeoElement.getMaskedTexture();
        }
        maskTextureFilter.getFrameBufferObject().setTexture(maskTexture);
        maskTextureFilter.getFrameBufferObject().refreshTexture(colorThresholdGeoElement.getTextureToThreshold().getWidth(), colorThresholdGeoElement.getTextureToThreshold().getHeight(), colorThresholdGeoElement.getTextureToThreshold().getLowerCorner(), colorThresholdGeoElement.getTextureToThreshold().getPixels_per_unit(), colorThresholdGeoElement.getTexture().getPixels_per_unit_array());
        maskTextureFilter.refreshQuadCoordinates(colorThresholdGeoElement.getTextureToThreshold().getLowerCorner(),colorThresholdGeoElement.getTextureToThreshold().getWidth(),colorThresholdGeoElement.getTextureToThreshold().getHeight());

        canvas.getFilterManager().attachTextureToFbo(maskTextureFilter.getFrameBufferObject(), maskTextureFilter.getFrameBufferObject().getTexture(), drawable);

        drawColorSelectionPolygon(colorThresholdGeoElement,drawable,layer, maskTextureFilter);
        colorThresholdGeoElement.setMaskedTexture(maskTexture);
    }

    public void drawColorSelectionPolygon(ColorThresholdGeoElement ge,GLAutoDrawable drawable, Layer layer, TextureFilter textureFilter) {
        GL3 gl = (GL3) drawable.getGL();
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, textureFilter.getFrameBufferObject().getVboiD());
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);
        gl.glBindVertexArray(layer.getVao());
        gl.glViewport(0, 0, ge.getTextureToThreshold().getPixelWidth(), ge.getTextureToThreshold().getPixelHeight());
        Vector3d eye = new Vector3d(ge.getTextureToThreshold().getLowerCorner()[0], ge.getTextureToThreshold().getLowerCorner()[1],canvas.getCamera().getEye().z);
        Vector3d center = new Vector3d(eye.x, eye.y, canvas.getCamera().getCenter().z);
        double right = ge.getTextureToThreshold().getWidth();
        double top = ge.getTextureToThreshold().getHeight();
        if (ge.getRenderingType() == ShaderManager.RenderedElementTypes.COLOR_DIFF) {
            ThresholdedColoredRenderer thresholdedColoredRenderer = new ThresholdedColoredRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.COLOR_DIFF));
            thresholdedColoredRenderer.render(ge, drawable, canvas.getCamera(), canvas.getLayerManager(), 1.0f, eye, center, 0, right, 0, top);
        }
        else if (ge.getRenderingType() == ShaderManager.RenderedElementTypes.MASK) {
            MaskRenderer maskRenderer = new MaskRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.MASK));
            maskRenderer.render(ge, drawable, canvas.getCamera(), canvas.getLayerManager(), 1.0f, eye, center, 0, right, 0, top);

        }

        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER,0);

        Mat image = new Mat((int)ge.getTextureToThreshold().getPixelHeight(), (int)ge.getTextureToThreshold().getPixelWidth(), CvType.CV_8UC1);
        ByteBuffer byteBuffer = ByteBuffer.allocate((int)ge.getTextureToThreshold().getPixelWidth()*ge.getTextureToThreshold().getPixelHeight());
        gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, textureFilter.getFrameBufferObject().getVboiD());
        gl.glPixelStorei(GL_PACK_ALIGNMENT,1);
        gl.glReadPixels(0,0,(int)ge.getTextureToThreshold().getPixelWidth(),(int)ge.getTextureToThreshold().getPixelHeight(),gl.GL_RED,GL_UNSIGNED_BYTE,byteBuffer);

        canvas.updateViewport(drawable);
        gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, 0);

        int   pixelCount  = (int)ge.getTextureToThreshold().getPixelWidth() * (int)ge.getTextureToThreshold().getPixelHeight();
        byte[] pixelValues = new byte[ pixelCount * 1 ];
        for( int i = 0; i < pixelCount; i++ )
        {
            int line        = (int)ge.getTextureToThreshold().getPixelHeight() - 1 - (i / (int)ge.getTextureToThreshold().getPixelWidth()); // flipping the image upside down
            int column      = i % (int)ge.getTextureToThreshold().getPixelWidth();
            int bufferIndex = ( line * (int)ge.getTextureToThreshold().getPixelWidth() + column ) * 1;

            pixelValues[bufferIndex + 0 ] = (byte)(byteBuffer.get(bufferIndex + 0) & 0xFF);
        }
        image.put(0,0,pixelValues);
        CVUtilities.flipMat(image);
        textureFilter.getFrameBufferObject().getTexture().setData(image);
    }


    public void exportAnimation(Layer animationLayer, RasterGeoElement animationRaster, GLAutoDrawable drawable, List<AbstractStagedAnimation> animations, LayerManager layerManager, int framesPerSecond, String fileName) {

        GL3 gl = (GL3) drawable.getGL();
        TextureFilter exportFilter = canvas.getFilterManager().initializeTextureFilter(animationRaster, drawable);
        Texture outTexture = null;
        outTexture = createTextureFromTexture(animationRaster, drawable);

        exportFilter.getFrameBufferObject().setTexture(outTexture);
        exportFilter.getFrameBufferObject().refreshTexture(animationRaster.getRealWidth(), animationRaster.getRealHeight(), animationRaster.getLowerCorner(), animationRaster.getPixels_per_unit(), animationRaster.getPixels_per_unit_array());
        exportFilter.refreshQuadCoordinates(animationRaster.getLowerCorner(), animationRaster.getImageWidth(), animationRaster.getImageHeight());

        canvas.getFilterManager().attachTextureToFbo(exportFilter.getFrameBufferObject(), exportFilter.getFrameBufferObject().getTexture(), drawable);

        drawAnimationFrames(drawable, animations, exportFilter, animationRaster, animationLayer, layerManager, framesPerSecond, fileName);
        //can
    }

    public List<BufferedImage> drawAnimationFrames(GLAutoDrawable drawable, List<AbstractStagedAnimation> animations, TextureFilter textureFilter, RasterGeoElement animationRaster, Layer animationLayer, LayerManager layerManager, int framesPerSecond, String fileName) {

        GL3 gl = (GL3) drawable.getGL();
        gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, textureFilter.getFrameBufferObject().getVboiD());
        gl.glClear(gl.GL_COLOR_BUFFER_BIT);

        gl.glBindVertexArray(animationLayer.getVao());
        gl.glViewport(0, 0,animationRaster.getImageWidth(), animationRaster.getImageHeight());
        Vector3d eye = new Vector3d(animationRaster.getLowerCorner()[0], animationRaster.getLowerCorner()[1],canvas.getCamera().getEye().z);
        Vector3d center = new Vector3d(eye.x, eye.y, canvas.getCamera().getCenter().z);
        double right = animationRaster.getRealWidth();
        double top = animationRaster.getRealHeight();
        float duration = 0;
        List <BufferedImage> images = new ArrayList<>();
        for (AbstractStagedAnimation stagedAnimation : animations) {
            duration+=stagedAnimation.getDuration();
        }


        AnimationPlanRenderer animationPlanRenderer = new AnimationPlanRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.ANIMATION_PLAN));

        int nFrames = 0;
        Mat image;
        ByteBuffer buffer;
        System.out.println("animations size "+animations.size());
        float totalDuration=0;
        for (AbstractStagedAnimation stagedAnimation : animations) {
            totalDuration += stagedAnimation.getDuration();
        }
        int totalFrames = (int)(totalDuration*framesPerSecond);
        int frameCount =0;
        for(AbstractStagedAnimation stagedAnimation : animations) {
            nFrames = (int)(stagedAnimation.getDuration()*framesPerSecond);
            for (int i=0; i<nFrames; i++) {
                float tic = (float)i/(float)framesPerSecond*1000;
                gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, textureFilter.getFrameBufferObject().getVboiD());
                animationPlanRenderer.render(stagedAnimation, drawable, canvas.getCamera(), eye, center, 0, right, 0, top, tic, layerManager, animationRaster);
                gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0);
                image = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8UC3);
                buffer = ByteBuffer.allocate(animationRaster.getImageWidth()*animationRaster.getImageHeight()*3);
                gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, textureFilter.getFrameBufferObject().getVboiD());
                gl.glPixelStorei(GL_PACK_ALIGNMENT,1);
                gl.glReadPixels(0,0,(int)animationRaster.getImageWidth(),(int)animationRaster.getImageHeight(),gl.GL_RGB,GL_UNSIGNED_BYTE,buffer);
                gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, 0);
                int   pixelCount  = (int)animationRaster.getImageWidth() * animationRaster.getImageHeight();
                byte[] pixelValues = new byte[ pixelCount*3];
                for( int p = 0; p < pixelCount; p++ )
                {
                    int line        = (int)animationRaster.getImageHeight() - 1 - (p / (int)animationRaster.getImageWidth()); // flipping the image upside down
                    int column      = p % (int)animationRaster.getImageWidth();
                    int bufferIndex = ( line * (int)animationRaster.getImageWidth()+ column ) * 3;
                    //System.out.println("buffer index "+bufferIndex);
                    pixelValues[bufferIndex + 2 ] = (byte)(buffer.get(bufferIndex + 0) & 0xFF);
                    pixelValues[bufferIndex + 1 ] = (byte)(buffer.get(bufferIndex + 1) & 0xFF);
                    pixelValues[bufferIndex + 0 ] = (byte)(buffer.get(bufferIndex + 2) & 0xFF);
                }
                image.put(0,0,pixelValues);
                CVUtilities.flipMat(image);
                CVUtilities.writeImage(image, Constants.tempPath+"/export"+frameCount+".png");
                images.add(CVUtilities.mattoBufferedImage(image));
                frameCount++;
            }
        }
        canvas.updateViewport(drawable);
        gl.glBindFramebuffer(gl.GL_READ_FRAMEBUFFER, 0);
        FfmpegRecorder.recordMovie(fileName, (int)duration, images, canvas);
        return images;


    }

}
