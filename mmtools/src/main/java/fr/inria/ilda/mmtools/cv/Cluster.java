/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.cv;

import org.opencv.core.Mat;

import java.util.List;

/**
 * Created by mjlobo on 24/03/16.
 */
public class Cluster {
    double[] center;
    double[] means;
    double [] stddevs;
    Mat mask;
    int id;

    public Cluster(double[] center, int id, int channels) {
        this.id = id;
        this.center = center;
        means = new double[channels];
        stddevs = new double[channels];
    }

    public void setMeans(double[] means) {
        this.means = means;
    }

    public void setStddevs(double [] stddevs) {
        this.stddevs = stddevs;
    }

    public void setMask(Mat mask) {
        this.mask = mask;
    }

    public double[] getMeans() {
        return means;
    }

    public double[] getStddevs() {
        return stddevs;
    }

    public Mat getMask() {
        return mask;
    }

    public int getId() {
        return id;
    }

    public double[] getCenter() {
        return center;
    }

    public double distance(Cluster otherCluster) {
        double sum = 0;
        for (int i=0; i<center.length; i++) {
            sum += (otherCluster.getCenter()[i]-center[i])*(otherCluster.getCenter()[i]-center[i]);
        }
        return Math.sqrt(sum);
    }

    public double distance (double[] color) {
        double sum = 0;
        for (int i=0; i<center.length; i++) {
            sum += (color[i]-center[i])*(color[i]-center[i]);
        }
        return Math.sqrt(sum);
    }

    public double distance(double colorcomponent, int index) {
        return Math.abs(colorcomponent-center[index]);
    }



}
