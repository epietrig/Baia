/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.Camera;
import fr.inria.ilda.mmtools.gl.ShaderProgram;
import fr.inria.ilda.mmtools.gl.ShapeGL;
import fr.inria.ilda.mmtools.gl.Texture;

import javax.vecmath.Vector3f;

/**
 * Created by mjlobo on 25/04/16.
 */
public abstract class AbstractPolygonGLRenderer {
    protected ShaderProgram program;
    public AbstractPolygonGLRenderer(ShaderProgram program) {
        this.program = program;
    }
    public abstract void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Texture secondaryTexture, Vector3f color, LayerManager layerManager);
}

