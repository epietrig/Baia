/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

import fr.inria.ilda.mmtools.gl.PointGL;

/**
 * Created by mjlobo on 01/07/15.
 */
public class CanvasPointEvent extends CanvasEvent {

    PointGL pointGL;

    public CanvasPointEvent(PointGL pointGL, String event) {
        super(event);
        this.pointGL = pointGL;
    }

    public PointGL getPointGL() {
        return pointGL;
    }
}
