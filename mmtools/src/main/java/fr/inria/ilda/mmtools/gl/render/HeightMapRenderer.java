/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.geo.MNTRasterGeoElement;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3d;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 25/04/16.
 */
public class HeightMapRenderer extends AbstractGeoRenderer {
    ShaderProgram program;

    public HeightMapRenderer(ShaderProgram program) {
        this.program = program;
    }
    @Override
    public void render(GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager) {
        GL4 gl = (GL4) drawable.getGL();
        MNTRasterGeoElement mntRasterGeoElement = (MNTRasterGeoElement)ge;
        ShaderProgram altitude = program;
        int vertexAttributeId = altitude.getAttribute(ShaderConstants.POSITION);
        int textureAttributeId = altitude.getAttribute(ShaderConstants.TEXTURE_POSITION);
        Vector3d eye = new Vector3d(mntRasterGeoElement.getLowerCorner()[0], mntRasterGeoElement.getLowerCorner()[1], camera.getEye().z);
        Vector3d center = new Vector3d(mntRasterGeoElement.getLowerCorner()[0], mntRasterGeoElement.getLowerCorner()[1], camera.getCenter().z);
        double right = mntRasterGeoElement.getRealWidth();
        double top = mntRasterGeoElement.getRealHeight();
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOfseted() && shape instanceof PolygonGL) {
                gl.glUseProgram(altitude.getProgramId());
                gl.glUniformMatrix4fv(altitude.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(eye, center), 0);
                gl.glUniformMatrix4fv(altitude.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(0,right,0, top), 0);
                gl.glUniform3f(altitude.getUniform("startColorRGB"),mntRasterGeoElement.getMinColor().x, mntRasterGeoElement.getMinColor().y, mntRasterGeoElement.getMinColor().z);
                gl.glUniform3f(altitude.getUniform("endColorRGB"), mntRasterGeoElement.getMaxColor().x, mntRasterGeoElement.getMaxColor().y, mntRasterGeoElement.getMaxColor().z);
                gl.glUniform1f(altitude.getUniform("max"), mntRasterGeoElement.getMaxValue());
                gl.glUniform1f(altitude.getUniform("min"), mntRasterGeoElement.getMinValue());


                BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                BufferGL textureBuffer = shape.getBuffer(BufferGL.Type.TEXTURE);
                if (vertexBuffer != null) {
                    gl.glEnableVertexAttribArray(vertexAttributeId);
                    gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                    gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    gl.glEnableVertexAttribArray(textureAttributeId);
                    gl.glBindBuffer(textureBuffer.getTarget(), textureBuffer.getBuffer());
                    gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                    BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                    gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                    gl.glActiveTexture(gl.GL_TEXTURE0);
                    gl.glBindTexture(gl.GL_TEXTURE_2D, mntRasterGeoElement.getOriginalTexture().getID());
                    gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                }


            }
        }
    }
}
