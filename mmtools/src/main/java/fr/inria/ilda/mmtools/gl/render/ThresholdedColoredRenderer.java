/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.sun.prism.ps.Shader;
import fr.inria.ilda.mmtools.geo.ColorThresholdGeoElement;
import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 24/05/16.
 */
//Only works for ColorThresholdedGeoElement!
public class ThresholdedColoredRenderer extends AbstractGeoRenderer {

    ShaderProgram program;

    public ThresholdedColoredRenderer(ShaderProgram program) {
        this.program = program;
    }
    @Override
    public void render(GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager) {

        ColorThresholdGeoElement colorThresholdGeoElement = (ColorThresholdGeoElement)ge;
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    ShaderProgram diffcolor = program;
                    int vertexAttributeId = diffcolor.getAttribute(ShaderConstants.POSITION);
                    int textureAttributeId = diffcolor.getAttribute(ShaderConstants.TEXTURE_POSITION);
                    PolygonGL polygon = (PolygonGL)shape;
                    GL4 gl = (GL4)drawable.getGL();
                    gl.glUseProgram(diffcolor.getProgramId());
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
                    gl.glUniform1f(diffcolor.getUniform("alpha"), 0.2f);
                    gl.glUniform3f(diffcolor.getUniform("compareColor"), colorThresholdGeoElement.getCompareColor().x, colorThresholdGeoElement.getCompareColor().y, colorThresholdGeoElement.getCompareColor().z);
                    gl.glUniform1f(diffcolor.getUniform("threshold"), (float)colorThresholdGeoElement.getThreshold());
                    BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
                    BufferGL textureBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
                    if (vertexBuffer != null) {
                        gl.glEnableVertexAttribArray(vertexAttributeId);
                        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        gl.glEnableVertexAttribArray(textureAttributeId);
                        gl.glBindBuffer(textureBuffer.getTarget(), textureBuffer.getBuffer());
                        gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
                        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                        gl.glActiveTexture(gl.GL_TEXTURE0);
                        gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(colorThresholdGeoElement.getTextureToThreshold()));
                        gl.glActiveTexture(gl.GL_TEXTURE1);
                        gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(colorThresholdGeoElement.getMaskedTexture()));
                        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                    }
                }
            }
        }
    }

    public void render(GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager, float alpha, Vector3d eye, Vector3d center,
                       double left, double right, double bottom, double top) {
        ColorThresholdGeoElement colorThresholdGeoElement = (ColorThresholdGeoElement)ge;
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    ShaderProgram diffcolor = program;
                    int vertexAttributeId = diffcolor.getAttribute(ShaderConstants.POSITION);
                    int textureAttributeId = diffcolor.getAttribute(ShaderConstants.TEXTURE_POSITION);
                    PolygonGL polygon = (PolygonGL)shape;
                    GL4 gl = (GL4)drawable.getGL();
                    gl.glUseProgram(diffcolor.getProgramId());
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(eye,center), 0);
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(left,right,bottom,top), 0);
                    gl.glUniform1f(diffcolor.getUniform("alpha"), alpha);
                    gl.glUniform3f(diffcolor.getUniform("compareColor"), colorThresholdGeoElement.getCompareColor().x, colorThresholdGeoElement.getCompareColor().y, colorThresholdGeoElement.getCompareColor().z);
                    gl.glUniform1f(diffcolor.getUniform("threshold"), (float)colorThresholdGeoElement.getThreshold());
                    BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
                    BufferGL textureBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
                    if (vertexBuffer != null) {
                        gl.glEnableVertexAttribArray(vertexAttributeId);
                        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
                        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        gl.glEnableVertexAttribArray(textureAttributeId);
                        gl.glBindBuffer(textureBuffer.getTarget(), textureBuffer.getBuffer());
                        gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
                        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                        gl.glActiveTexture(gl.GL_TEXTURE0);
                        gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(colorThresholdGeoElement.getTextureToThreshold()));
                        gl.glActiveTexture(gl.GL_TEXTURE1);
                        gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(colorThresholdGeoElement.getMaskedTexture()));
                        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                    }
                }
            }
        }
    }
}
