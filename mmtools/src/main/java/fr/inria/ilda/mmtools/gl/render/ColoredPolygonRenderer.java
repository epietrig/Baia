/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3f;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 25/04/16.
 */
public class ColoredPolygonRenderer extends AbstractPolygonGLRenderer {

    public ColoredPolygonRenderer(ShaderProgram program) {
        super(program);
    }

    @Override
    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Texture secondarytexture, Vector3f color, LayerManager layerManager) {
        render(shapeGL, drawable, camera, color, shapeGL.getAlpha(), layerManager);
    }

    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Vector3f color, LayerManager layerManager) {
        render(shapeGL, drawable, camera, color, shapeGL.getAlpha(), layerManager);
    }

    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Vector3f color, float alpha, LayerManager layerManager) {
        ShaderProgram colorFill = program;
        PolygonGL polygon = (PolygonGL)shapeGL;
        GL4 gl = (GL4)drawable.getGL();
        int vertexAttributeId = colorFill.getAttribute(ShaderConstants.POSITION);

        gl.glUseProgram(colorFill.getProgramId());
        gl.glUniformMatrix4fv(colorFill.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
        gl.glUniformMatrix4fv(colorFill.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
        gl.glUniform2f(colorFill.getUniform("offset"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        gl.glUniform1f(colorFill.getUniform("alpha"), alpha);
        gl.glUniform3f(colorFill.getUniform("color"),color.x, color.y, color.z);

        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        if (vertexBuffer != null && layerManager.getBufferVbo(vertexBuffer) != null)  {
            gl.glEnableVertexAttribArray(vertexAttributeId);
            gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
            gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
            BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
            gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
            gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
        }
    }
}
