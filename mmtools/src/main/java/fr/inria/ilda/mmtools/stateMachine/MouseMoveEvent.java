/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 22/03/15.
 */



public class MouseMoveEvent extends Event {

    java.awt.Component target;
    int x;
    int y;

    public MouseMoveEvent(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public MouseMoveEvent(java.awt.Component target, int x, int y) {
        super();
        source = this.target = target;
        this.x = x;
        this.y = y;
    }

    private static MouseMoveEventListener listener = new MouseMoveEventListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.attachTo( stateMachine, (java.awt.Component)target );            
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.detachFrom( stateMachine, (java.awt.Component)target );
        }
    }

    public java.awt.Component getTarget() {
        return target;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}

class MouseMoveEventListener extends java.awt.event.MouseAdapter {

    private HashMap<java.awt.Component, HashSet<StateMachine>> listeners;

    public MouseMoveEventListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, java.awt.Component target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addMouseMotionListener(this);
            //System.out.println("Added MouseMoveListener");
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, java.awt.Component target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeMouseListener( this );
                //System.out.println("Removed MouseMoveListener");
            }
        }
    }

    @Override
    public void mouseMoved( java.awt.event.MouseEvent event ) {
        java.awt.Component target = (java.awt.Component)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            MouseMoveEvent customEvent = new MouseMoveEvent( target, event.getX(), event.getY() );
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        }
    }

    public void mouseDragged( java.awt.event.MouseEvent event ) {
        // A mouse drag is also a mouse move
        mouseMoved( event );
    }


}
