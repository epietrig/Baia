/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 01/06/16.
 */
public class AbstractAnimationManager {
    LayerManager layerManager;
    MapGLCanvas canvas;
    AbstractStagedAnimation currentStagedAnimation;
    List<AbstractStagedAnimation> stagedAnimations;
    RasterGeoElement animationRaster;
    Layer animationLayer;

    public AbstractAnimationManager(LayerManager layerManager, MapGLCanvas canvas) {
        this.layerManager = layerManager;
        this.canvas = canvas;
        stagedAnimations = new ArrayList<>();
    }

    public AbstractStagedAnimation getCurrentStagedAnimation() {
        return currentStagedAnimation;
    }

    public void setCurrentStagedAnimation(AbstractStagedAnimation currentStagedAnimation) {
        this.currentStagedAnimation = currentStagedAnimation;
    }

    public RasterGeoElement getAnimationRaster(){
        return animationRaster;
    }

    public Layer getAnimationLayer() {
        return animationLayer;
    }

    public void maskUpdated(GeoElement ge, Texture texture){}

    public List<AbstractStagedAnimation> getStagedAnimations() {
        return stagedAnimations;
    }

    public Pair<Float,AbstractStagedAnimation> getCurrentAnimation(float currentTime) {
        float accumulatedTime = 0.0f;
        int index = 0;
        Pair <Float, AbstractStagedAnimation> currentAnimation = null;
        if (stagedAnimations.size()>0) {
            if (currentTime >= getTotalDuration()) {
                currentAnimation = new Pair<>(getTotalDuration() - stagedAnimations.get(stagedAnimations.size() - 1).getDuration(), stagedAnimations.get(stagedAnimations.size() - 1));
                return currentAnimation;
            } else {
                for (AbstractStagedAnimation stagedAnimation : stagedAnimations) {
                    //System.out.println("current time " + currentTime);

                    if (accumulatedTime <= currentTime && currentTime <= stagedAnimation.getDuration() + accumulatedTime) {
                        //System.out.println("accumulated time "+accumulatedTime);
                        currentAnimation = new Pair<>(accumulatedTime, stagedAnimation);
                        return currentAnimation;
                    } else {
                        accumulatedTime += stagedAnimation.getDuration();
                        index++;
                    }
                }
            }
        }
        return null;
    }

    public float getTotalDuration() {
        float totalTime = 0;
        for (AbstractStagedAnimation stagedAnimation : stagedAnimations) {
            totalTime += stagedAnimation.getDuration();
        }
        return totalTime;
    }
    public AbstractStagedAnimation getStagedAnimation (int index) {
        return stagedAnimations.get(index);
    }

    public int getStagedAnimationsCount () {
        return stagedAnimations.size();
    }
 }
