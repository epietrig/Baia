/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui;

        import fr.inria.ilda.mmtools.stateMachine.FloatSliderChangeEvent;
        import fr.inria.ilda.mmtools.stateMachine.FloatSliderFinishedChangingEvent;
        import fr.inria.ilda.mmtools.stateMachine.SliderChangeEvent;
        import fr.inria.ilda.mmtools.stateMachine.StateMachine;

        import javax.swing.*;
        import javax.swing.event.ChangeEvent;
        import javax.swing.event.ChangeListener;

/**
 * Created by mjlobo on 18/03/16.
 */
public class FloatSlider extends JSlider {
    float startVal;
    float endVal;
    float floatValue;
    boolean fireEvent = true;

    public FloatSlider(float startVal, float endVal, float value) {
        super((int)(startVal*100), (int)(endVal*100), (int)(value*100));
        this.startVal = startVal;
        this.endVal = endVal;
        this.floatValue = value;

    }

    public FloatSlider(float startVal, float endVal, float value, String name) {
        super((int)(startVal*100), (int)(endVal*100), (int)(value*100));
        this.startVal = startVal;
        this.endVal = endVal;
        this.floatValue = value;
        setName(name);
    }

    public void setValue(float value) {
        fireEvent = false;
        this.floatValue = value;

        //System.out.println("set value! "+value);
        if (super.getValue()!= (int)(value*100)) {
            super.setValue((int) (value * 100));
        }
        fireEvent = true;
    }

    public void setEndVal(float endVal) {
        this.endVal = endVal;
        super.setMaximum((int)(endVal*100));
    }

    public float getFloatValue() {
        return super.getValue()/100f;
    }

    public void addListeners(final StateMachine stateMachine) {
        this.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (fireEvent) {
                    JSlider source = ((JSlider) e.getSource());
                    stateMachine.handleEvent(new FloatSliderChangeEvent(FloatSlider.this, getFloatValue()));
                    if (!source.getValueIsAdjusting()) {
                        stateMachine.handleEvent(new FloatSliderFinishedChangingEvent(FloatSlider.this, getFloatValue()));
                    }
                }
            }
        });
    }

}
