/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.magicWand;

import java.util.LinkedList;

/**
 * Created by mjlobo on 07/09/15.
 */
public class Segment {

    public LinkedList<Pixel> pixels;
    Pixel p1;
    Pixel p2;

    public Segment () {
        pixels = new LinkedList<Pixel>();
    }

    public Segment(Pixel p1, Pixel p2){
        this();
        pixels.add(p1);
        pixels.add(p2);
        this.p1 = p1;
        this.p2 = p2;
    }

    public void addPixel (Pixel pixel) {
        pixels.add(pixel);
    }

    public LinkedList<Pixel> getPixels() {
        return pixels;
    }

    public Pixel getP1() {
        return p1;
    }

    public Pixel getP2() {
        return p2;
    }

}
