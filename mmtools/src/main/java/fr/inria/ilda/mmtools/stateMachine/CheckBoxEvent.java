/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 31/03/15.
 */
public class CheckBoxEvent extends Event {

    Component target;
    boolean isChecked;
    String text;

    public CheckBoxEvent (boolean isChecked, String text) {
        this.isChecked = isChecked;
        this.text = text;
    }

    public CheckBoxEvent(String text, Component target) {
        this.text = text;
        this.target = target;
    }

    private static CheckBoxListener listener = new CheckBoxListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.attachTo( stateMachine, (java.awt.Component)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.detachFrom( stateMachine, (java.awt.Component)target );
        }
    }

    public java.awt.Component getTarget() {
        return target;
    }


    public String getText() {
        return text;
    }

    public boolean getIsChecked() {
        return isChecked;
    }
}

class CheckBoxListener implements ChangeListener {

    private HashMap<Component, HashSet<StateMachine>> listeners;

    public CheckBoxListener() {listeners = new HashMap<Component, HashSet<StateMachine>>();}

    public void attachTo( StateMachine stateMachine, java.awt.Component target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        JCheckBox checkBox = (JCheckBox)target;
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            checkBox.addChangeListener(this);
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, java.awt.Component target ) {
        JCheckBox checkBox = (JCheckBox)target;
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                checkBox.removeChangeListener(this);

            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent event) {
        JCheckBox target = (JCheckBox)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get(target);
            // Create custom event
            CheckBoxEvent customEvent = new CheckBoxEvent(target.getText(), target);
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
//        } else {
//            java.awt.Component parent = target.getParent();
//            if (parent != null) {
//                parent.dispatchEvent(event);
//            }
        }
    }
}
