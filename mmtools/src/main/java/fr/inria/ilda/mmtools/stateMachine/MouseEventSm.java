/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
/**
 * Created by mjlobo on 22/03/15.
 */
package fr.inria.ilda.mmtools.stateMachine;

public class MouseEventSm extends Event {

    int x;
    int y;

    public MouseEventSm(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
