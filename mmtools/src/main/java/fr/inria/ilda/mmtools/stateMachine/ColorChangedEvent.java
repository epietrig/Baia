/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import java.awt.*;

/**
 * Created by mjlobo on 07/08/15.
 */
public class ColorChangedEvent extends Event {
    Component target;
    Color color;

    public ColorChangedEvent (Component target, Color color) {
        this.target = target;
        this.color = color;
    }

    public Component getTarget() {
        return target;
    }

    public Color getColor() {
        return color;
    }
}
