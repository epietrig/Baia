/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import javax.print.DocFlavor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mjlobo on 22/03/15.
 */
public class StateMachine {
    HashMap<String, State> states;
    HashMap<String, Object> sourceObjects;
    HashMap <Object, String> sourceNames;
    State state;

    public StateMachine() {
        states = new HashMap<String, State>();
        sourceObjects = new HashMap<String, Object>();
        sourceNames = new HashMap<Object, String>();
    }

    public StateMachine addState(String stateName, State state) {
        if (this.state == null) {
            this.state = state;
        }
        states.put(stateName, state);
        return this;
    }

    public StateMachine addToState(String stateName, State state) {
        if (states.get(stateName) == null) {
            addState(stateName, state);
        }
        return this;
    }

    public StateMachine removeState( String stateName ) {
        states.remove( stateName );
        return this;
    }

    public void handleEvent( Event event ) {
        if ( state != null ) {
            String nextStateName = state.handleEvent( this, event );
            if ( nextStateName != null ) {
                setState( states.get( nextStateName ) );
            }
        }
    }

    public State getState() {
        return state;
    }

    public State getState(String stateName) {
        return states.get(stateName);
    }

    public void setState( State state ) {
        StateChangeEvent event = new StateChangeEvent( this, this.state );
        if (state != this.state) {
            this.state.stop(this);
            this.state = state;
            this.state.start(this);
        }
        handleEvent( event );
    }

    public void setState(State state, boolean throwChangeEvent) {
        if (throwChangeEvent) {
            if (state!=this.state) {
                setState(state);
            }
            else {
                StateChangeEvent event = new StateChangeEvent( this, this.state );
                handleEvent( event );
            }
        }
    }

    public void setState (String stateName) {
        setState(states.get(stateName));
    }

    public StateMachine setSource (String sourceName, Object target) {
        Object oldSource = sourceObjects.get(sourceName);
        if (oldSource != null) {
           sourceNames.remove(oldSource);
        }
        sourceNames.put(target, sourceName);
        sourceObjects.put(sourceName, target);
        return this;
    }

    public String getNameOfSource(Object source) {
        return sourceNames.get(source);
    }

    public Object getSourceOfName(String nameSource) {
        return  sourceObjects.get(nameSource);
    }

    public void start () {
        setState(state);
        state.start(this);
    }

    public void stop () {
        state.stop(this);
    }

    public String getStateName() {
        for (String s: states.keySet()) {
            if (states.get(s) == state) {
                return s;
            }
        }
        return null;
    }

    public String getStateName(State state) {
        for (Map.Entry<String, State> entry : states.entrySet()) {
            String key = entry.getKey();
            State value = entry.getValue();
            if (state == value) {
                return key;
            }
        }
        return  null;
    }


}
