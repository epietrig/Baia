/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

/**
 * Created by mjlobo on 25/03/15.
 */
public class RightMouseButtonPressEvent extends MouseEventSm {
    public RightMouseButtonPressEvent(int x, int y) {
        super(x,y);
    }
}
