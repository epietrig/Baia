/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;


import com.vividsolutions.jts.algorithm.MinimumBoundingCircle;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;
import fr.inria.ilda.mmtools.gl.*;
import fr.inria.ilda.mmtools.utilties.Pair;

import javax.sound.sampled.Line;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by mjlobo on 16/02/15.
 */
public class DrawnGeoElement extends GeoElement{


    float z = 2.0f;


    List<double[]> coordinatesOffseted;
    Geometry offsetedGeometry;
    double borderOffset = 0.000002;
    double alphaBorder = 0.000002;
    float alpha = 1.0f;
    protected Texture blurredTexture;
    LineStripGL lineStripGL;
    protected boolean isComposite = false;
    Vector3f previousLineColor;
    float previousLineWidth = -1;
    List<PolygonGL> polygons;
    List<PolygonGL> inPolygons;
    List<LineStripGL> selectionLines;
    List<LineStripGL> selectionLinesBis;
    List<LineStripGL> lines;
    List<PolygonGL> offsetedPolygons;
    boolean outlineVisible;
    double[] offsetFromOriginal;
    boolean selected = false;
    boolean isPoint;

    static Vector3f IN_COLOR = new Vector3f(1.0f,0.5f, 0f);

    public DrawnGeoElement() {
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        polygons = new ArrayList<>();
        offsetedPolygons = new ArrayList<>();
        shapes = new ArrayList<>();
        isComplete = false;
        coordinates = new ArrayList<double[]>();
        coordinatesOffseted = new ArrayList<double[]>();
        inPolygons = new ArrayList<>();
        LineStripGL line= new LineStripGL();
        line.setColor(SELECTION_COLOR_IN);
        lines.add(line);
        shapes.add(line);
        renderingType = ShaderManager.RenderedElementTypes.TEXTURED;
        fillColor = new Vector3f(DEFAULT_FILL_COLOR.x, DEFAULT_FILL_COLOR.y, DEFAULT_FILL_COLOR.z);
        outlineColor = new Vector3f(DEFAULT_OUTLINE_COLOR.x, DEFAULT_OUTLINE_COLOR.y, DEFAULT_OUTLINE_COLOR.z);
        offsetFromOriginal = new double[]{0,0};
        offset = new double[] {0,0};
        isDocked = false;

    }

    public DrawnGeoElement(Geometry geometry) {
        shapes = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        polygons = new ArrayList<>();
        offsetedPolygons = new ArrayList<>();
        coordinates = new ArrayList<>();
        coordinatesOffseted = new ArrayList<double[]>();
        inPolygons = new ArrayList<>();
        for (Coordinate coord: geometry.getCoordinates()) {
            coordinates.add(new double[] {coord.x, coord.y});
        }
        coordinates.remove(coordinates.size()-1);
        LineStripGL line = new LineStripGL(coordinates, OUTLINE_LINE_WIDTH, DEFAULT_OUTLINE_COLOR);
        LineStripGL selectionLine = new LineStripGL(coordinates,SELECTION_LINE_WIDTH);
        LineStripGL selectionLineBis = new LineStripGL(coordinates, SELECTION_OUTLINE_WIDTH, SELECTION_COLOR_OUT);
        selectionLine.setVisible(false);
        selectionLine.setColor(IN_COLOR);
        selectionLineBis.setVisible(false);
        selectionLinesBis.add(selectionLineBis);
        shapes.add(selectionLineBis);
        shapes.add(selectionLine);
        selectionLines.add(selectionLine);
        lines.add(line);
        shapes.add(line);

        renderingType = ShaderManager.RenderedElementTypes.TEXTURED;
        fillColor = new Vector3f(DEFAULT_FILL_COLOR.x, DEFAULT_FILL_COLOR.y, DEFAULT_FILL_COLOR.z);
        outlineColor = new Vector3f(DEFAULT_OUTLINE_COLOR.x, DEFAULT_OUTLINE_COLOR.y, DEFAULT_OUTLINE_COLOR.z);
        offsetFromOriginal = new double[]{0,0};
        offset = new double[] {0,0};
        isDocked = false;
        boundingBox = new BoundingBox(geometry.getEnvelope());
        this.geometry = geometry;

    }

    public DrawnGeoElement(List<double[]> coordinates) {
        shapes = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        polygons = new ArrayList<>();
        offsetedPolygons = new ArrayList<>();
        inPolygons = new ArrayList<>();
        coordinatesOffseted = new ArrayList<double[]>();
        this.coordinates = coordinates;
        coordinates.remove(coordinates.size()-1);
        LineStripGL line = new LineStripGL(coordinates, OUTLINE_LINE_WIDTH,  IN_COLOR);
        lines.add(line);
        shapes.add(line);
        renderingType = ShaderManager.RenderedElementTypes.TEXTURED;
        fillColor = new Vector3f(DEFAULT_FILL_COLOR.x, DEFAULT_FILL_COLOR.y, DEFAULT_FILL_COLOR.z);
        outlineColor = new Vector3f(DEFAULT_OUTLINE_COLOR.x, DEFAULT_OUTLINE_COLOR.y, DEFAULT_OUTLINE_COLOR.z);
        offsetFromOriginal = new double[]{0,0};
        offset = new double[] {0,0};
        isDocked = false;
    }

    public DrawnGeoElement(VectorGeoElement vectorGeoElement, double alphaRadius) {
        renderingType = ShaderManager.RenderedElementTypes.TEXTURED;
        polygons = new ArrayList<>();
        lines = new ArrayList<>();
        selectionLines = new ArrayList<>();
        selectionLinesBis = new ArrayList<>();
        shapes = new ArrayList<>();
        coordinates = new ArrayList<double[]>();
        coordinatesOffseted = new ArrayList<double[]>();
        inPolygons = new ArrayList<>();
        offsetedPolygons = new ArrayList<>();


        fillColor = new Vector3f(DEFAULT_FILL_COLOR.x, DEFAULT_FILL_COLOR.y, DEFAULT_FILL_COLOR.z);
        outlineColor = new Vector3f(DEFAULT_OUTLINE_COLOR.x, DEFAULT_OUTLINE_COLOR.y, DEFAULT_OUTLINE_COLOR.z);
        coordinates.addAll(vectorGeoElement.getCoordinates());
        coordinatesOffseted.addAll(vectorGeoElement.getCoordinatesOffseted());

        for (LineStripGL lineStrip : vectorGeoElement.getLines()) {
            LineStripGL newLine = new LineStripGL(lineStrip);
            lines.add(newLine);
            shapes.add(newLine);
        }

        for (PolygonGL shape: vectorGeoElement.getOriginalPolygons()) {
            PolygonGL newPolygon = new PolygonGL(shape);
            polygons.add(newPolygon);
            shapes.add(newPolygon);

        }

        for (LineStripGL lineStrip: vectorGeoElement.getSelectionLinesBis()) {
            LineStripGL newLine = new LineStripGL(lineStrip);
            selectionLinesBis.add(newLine);
            shapes.add(newLine);
        }
        //System.out.println("created lines polygons from drawn!");
        for (LineStripGL lineStrip: vectorGeoElement.getSelectionLines()) {
            LineStripGL newLine = new LineStripGL(lineStrip);
            selectionLines.add(newLine);
            shapes.add(newLine);
        }
        //System.out.println("created selection lines from drawn!");
        calculateGeometry();
        if(coordinatesOffseted.size()>0) {
            for (PolygonGL offsetedPolygon : vectorGeoElement.getPolygonsOffseted()) {
                PolygonGL newPolygon = new PolygonGL(offsetedPolygon);
                newPolygon.setVisible(false);
                offsetedPolygons.add(newPolygon);
                shapes.add(newPolygon);
            }
            offsetedGeometry = calculateGeometry(coordinatesOffseted);
        }
        else {
            calculatePolygonOffseted(alphaRadius);
        }

        setTranslucentBorder(false);
        offsetFromOriginal = new double[] {0,0};
        offset = new double[] {0,0};
        isDocked = true;
        boundingBox = new BoundingBox(geometry.getEnvelope());

    }



    public void addPoint(double[] point) {
        if (coordinates.size()<1) {
            coordinates.add(point);
            for (LineStripGL line : lines) {
               line.addFirstPoint(point);
            }
        }
        else if(coordinatesContains(point)==-1) {
            coordinates.add(point);
            for (LineStripGL line : lines) {
                line.addPoint(point);
            }
        }


    }

    public void addLastPoint(double[] point) {
        coordinates.set(coordinates.size()-1, point);
        for (LineStripGL line : lines) {
            line.addLastPoint(point);
        }
    }

    public void drawCompletePolygon(double alphaRadius, Texture texture){
          calculateGeometry();

          PolygonGL polygon = new PolygonGL(coordinates, texture);
          PolygonGL inPolygon = new PolygonGL(polygon);
          polygons.add(polygon);
          inPolygons.add(inPolygon);


          lines.get(0).refreshCoordinates(polygon.getCoordinates());
          lines.get(0).setColor(DEFAULT_OUTLINE_COLOR);
          lines.get(0).setLineWidth(OUTLINE_LINE_WIDTH);
          LineStripGL selectionLine = new LineStripGL(polygon.getCoordinates(),SELECTION_LINE_WIDTH);
          LineStripGL selectionLineBis = new LineStripGL(polygon.getCoordinates(), SELECTION_OUTLINE_WIDTH, SELECTION_COLOR_OUT);
          selectionLine.setVisible(false);
          selectionLine.setColor(IN_COLOR);
          selectionLineBis.setVisible(false);

          selectionLinesBis.add(selectionLineBis);
          shapes.add(polygon);
          shapes.add(selectionLineBis);
          shapes.add(selectionLine);
          shapes.add(inPolygon);
          selectionLines.add(selectionLine);
          polygon.updateBuffers();
          inPolygon.updateBuffers();
          polygon.setOnlyInFirst(true);
          polygon.setVisible(false);
          inPolygon.setVisible(false);
          calculatePolygonOffseted(alphaRadius);
          setTranslucentBorder(false);
        boundingBox = new BoundingBox(geometry.getEnvelope());



    }

    public void refreshCoordinates (Geometry geometry) {
        coordinates.clear();
        for (Coordinate coord: geometry.getCoordinates()) {
            coordinates.add(new double[] {coord.x, coord.y});
        }
        coordinates.remove(coordinates.size()-1);
        lines.get(0).refreshCoordinates(coordinates);
    }

    public void refreshCoodinatesValues(List<double[]> newCoordinates) {
        coordinates.clear();
        for (double[] coord: newCoordinates) {
            coordinates.add(new double[] {coord[0], coord[1]});
        }
        for (PolygonGL polygonGL:polygons) {
            polygonGL.refreshCoordinatesValues(newCoordinates);
        }
        for (PolygonGL polygonGL:inPolygons) {
            polygonGL.refreshCoordinatesValues(newCoordinates);
        }
    }


    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public boolean isDrawable () {
        if(coordinates.size()<3) {
            return false;
        }
        else {
            return true;
        }

    }


    public PolygonGL getPolygon() {
        return polygons.get(0);
    }



    public LineStripGL getLine() {
        return lines.get(0);
    }



    public void setTexture(Texture texture) {

        for (ShapeGL shape : shapes) {
            if(shape instanceof PolygonGL) {
                ((PolygonGL)shape).setTexture(texture);
            }
        }

    }

    public void select(){
        selected = true;
        isIn=false;

        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setColor(SELECTION_COLOR_IN);
            selectionLine.setVisible(true);
        }
        for (LineStripGL selectionLineBis : selectionLinesBis) {
            selectionLineBis.setColor(SELECTION_COLOR_OUT);
            selectionLineBis.setVisible(true);
        }
    }

    public void deselect() {
        selected = false;
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setColor(SELECTION_COLOR_IN);
            selectionLine.setVisible(false);
        }
        for (LineStripGL selectionLineBis : selectionLinesBis) {
            selectionLineBis.setVisible(false);
        }
    }

    public void inElement() {
        isIn = true;
        for (LineStripGL selectionLine: selectionLines) {
            selectionLine.setVisible(true);
        }
    }

    public void outElement() {
        isIn = false;
        if (!selected) {
            for (LineStripGL selectionLine : selectionLines) {
                selectionLine.setVisible(false);
            }
        }
    }





    public Coordinate getLocation () {
        return geometry.getCentroid().getCoordinate();
    }

    public float getRadius() {
        MinimumBoundingCircle minimumBoundingCircle = new MinimumBoundingCircle(geometry);
        return (float)minimumBoundingCircle.getRadius();
    }


    public float[] getEnvelopeBounds() {
        float[] bounds = new float[4];
        bounds[0] = (float)geometry.getCoordinates()[0].x;
        bounds[1] = (float)geometry.getCoordinates()[0].y;
        bounds[2] = (float)geometry.getCoordinates()[2].x;
        bounds[3] = (float)geometry.getCoordinates()[2].y;
        return bounds;
    }


    public void bufferPolygon(double bufferOffset) {
        super.bufferCoordinates(bufferOffset);
        Texture texture = polygons.get(0).getTexture();
        updatePolygons(geometry.getCoordinates(), texture);

        offsetedGeometry=bufferCoordinates(bufferOffset, offsetedGeometry, coordinatesOffseted);
        Pair<List<PolygonGL>, List<double[]>> pair = updatePolygons(offsetedGeometry.getCoordinates(), texture, offsetedPolygons);
        offsetedPolygons = pair.getValue1();
        coordinatesOffseted = pair.getValue2();
        for (PolygonGL polygonOffseted: offsetedPolygons) {
            polygonOffseted.setOffseted(true);
            if (!shapes.contains(polygonOffseted)) {
                shapes.add(polygonOffseted);
            }
        }


    }

    public void refreshGeometry (Geometry newGeometry, double alphaRadius) {
        super.refreshGeometry(newGeometry);
        Texture texture = polygons.get(0).getTexture();
        updatePolygons(geometry.getCoordinates(), texture);
        calculatePolygonOffseted(alphaRadius);
        offsetedPolygons.clear();
        for (PolygonGL polygonOffseted: offsetedPolygons) {
            //polygonOffseted.updateBuffers();
            polygonOffseted.setOffseted(true);
            if (!shapes.contains(polygonOffseted)) {
                shapes.add(polygonOffseted);
            }
        }

    }

    public void calculatePolygonOffseted(double bufferOffset) {

        coordinatesOffseted.clear();
        BufferOp bufferOp = new BufferOp(geometry);
        Geometry newGeometry = bufferOp.getResultGeometry(bufferOffset);
        newGeometry = DouglasPeuckerSimplifier.simplify(newGeometry, bufferOffset / 100);
        offsetedGeometry = newGeometry;
        Pair<List<PolygonGL>, List<double[]>> pair =  generatePolygons(newGeometry.getCoordinates(), getTexture(), false);
        offsetedPolygons= pair.getValue1();
        coordinatesOffseted = pair.getValue2();
        for (PolygonGL polygonOffseted: offsetedPolygons) {
            polygonOffseted.setOffseted(true);
            shapes.add(polygonOffseted);
        }




    }

    public void offsetCoordinates() {
        offsetFromOriginal[0]+=offset[0];
        offsetFromOriginal[1]+=offset[1];
        if (offset != null) {
            for (double[] coord : coordinatesOffseted) {
                coord[0] = coord[0] + offset[0];
                coord[1] = coord[1] + offset[1];
            }
            offsetedGeometry=calculateGeometry(coordinatesOffseted);
        }

        super.offsetCoordinates();

    }


    public void setTranslucentBorder(boolean translucent) {
        super.setTranslucentBorder(translucent);
        for (PolygonGL polygon: offsetedPolygons) {
            polygon.setVisible(translucent);
        }
        for (PolygonGL polygon: polygons) {
            polygon.setOnlyInFirst(translucent);
        }
    }

    public LineStripGL getSelectionLine() {
        return selectionLines.get(0);
    }

    public Texture getTexture() {
        return polygons.get(0).getTexture();
    }

    public void setColor(Vector3f color) {
        fillColor = color;
    }

    public void setOutlineColor(Vector3f outlineColor) {
        this.outlineColor = outlineColor;
        for (LineStripGL line: lines) {
            System.out.println("setting color in line! "+outlineColor.x + " "+outlineColor.y);
            line.setColor(outlineColor);
        }

    }

    public void setOutlineWidth(float width) {
        for(LineStripGL line : lines) {
            line.setLineWidth(width);
        }
    }

    public void toggleOutlineVisible() {
        lines.get(0).setVisible(!lines.get(0).getVisible());
    }

    public void setLineVisible(boolean visible) {
        outlineVisible = visible;
        for (LineStripGL line: lines) {
            line.setVisible(visible);
            if (previousLineWidth != -1 && previousLineColor!=null) {
                line.setColor(previousLineColor);
                line.setLineWidth(previousLineWidth);
            }
            else {
                line.setColor(DEFAULT_OUTLINE_COLOR);
                line.setLineWidth(OUTLINE_LINE_WIDTH);
            }
        }

    }

    public void setComposite(boolean isComposite) {
        this.isComposite = isComposite;
        if (isComposite) {
            //System.out.println("isComposite!");
            if (translucentBorder) {
                for (PolygonGL polygonOffseted: offsetedPolygons) {
                    polygonOffseted.setVisible(true);
                }
            }
            for (PolygonGL polygon: polygons) {
                polygon.setVisible(true);
            }

            if (previousLineWidth != -1 && previousLineColor != null) {
                for (LineStripGL line: lines) {
                    line.setColor(previousLineColor);
                    line.setLineWidth(previousLineWidth);
                }
            }
            else {
                for (LineStripGL line: lines) {
                    line.setVisible(false);
                }
            }
        }
        else {
            for (PolygonGL polygon: polygons) {
                polygon.setVisible(false);
            }
            for (PolygonGL polygonOffseted: offsetedPolygons) {
                polygonOffseted.setVisible(false);
            }
            previousLineColor = lines.get(0).getColor();
            previousLineWidth = lines.get(0).getLineWidth();
            for (LineStripGL line: lines) {
                line.setVisible(true);
                line.setColor(DEFAULT_OUTLINE_COLOR);
                line.setLineWidth(OUTLINE_LINE_WIDTH);
            }

        }
    }

    Pair<List<PolygonGL>, List<double[]>> generatePolygons (Coordinate[] coords, Texture texture, boolean addLines) {
        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> polygons = new ArrayList<>();


        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {

                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                if (addLines) {
                    LineStripGL line = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), OUTLINE_LINE_WIDTH, outlineColor);
                    LineStripGL selectionLine = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()),SELECTION_LINE_WIDTH);
                    LineStripGL selectionLineBis = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), OUTLINE_LINE_WIDTH, SELECTION_COLOR_OUT);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    selectionLinesBis.add(selectionLineBis);
                }
                PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon,coordinates.size()), texture,z);
                polygon.calculateGeometry();
                polygons.add(polygon);
                startPolygon = coordinates.size();
            }

        }
        return new Pair(polygons, coordinates);
    }

    Pair<List<PolygonGL>, List<double[]>> updatePolygons (Coordinate[] coords, Texture texture, List<PolygonGL> polygons) {

        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> newPolygons = new ArrayList<>();
        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        int polygonCount = 0;
        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {
                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                if (polygonCount < polygons.size()) {
                    polygons.get(polygonCount).refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    newPolygons.add(polygons.get(polygonCount));
                    polygonCount++;
                }
                else {
                    PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon, coordinates.size()), texture, z);
                    polygons.add(polygon);

                }
                startPolygon = coordinates.size();
            }

        }
        return new Pair(newPolygons, coordinates);
    }

    void updatePolygons(Coordinate[] coords, Texture texture) {
        List<double[]> coordinates = new ArrayList<>();
        List<PolygonGL> newPolygons = new ArrayList<>();
        List<LineStripGL> newLines = new ArrayList<>();
        List<LineStripGL> newSelectionLines = new ArrayList<>();
        List<LineStripGL> newSelectionLinesBis = new ArrayList<>();
        CoordinateList newCoordinates = new CoordinateList(coords,false);
        int indexContains;
        int startPolygon = 0;
        int polygonCount = 0;

        for (int k=0; k<newCoordinates.size();k++) {
            indexContains =
                    coordinatesContains(new double[]{newCoordinates.getCoordinate(k).x,newCoordinates.getCoordinate(k).y}, coordinates);
            if(indexContains==-1) {
                coordinates.add(new double[]{newCoordinates.getCoordinate(k).x, newCoordinates.getCoordinate(k).y});
            }
            else if (indexContains==startPolygon) {
                if (polygonCount < polygons.size() && !polygons.get(polygonCount).getIsHole()) {
                    PolygonGL current = polygons.get(polygonCount);
                    PolygonGL inCurrent = inPolygons.get(polygonCount);
                    LineStripGL currentLine = lines.get(polygonCount);
                    LineStripGL currentSelectinLine = selectionLines.get(polygonCount);
                    LineStripGL currentSelectionLineBis = selectionLinesBis.get(polygonCount);
                    current.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentLine.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentSelectinLine.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    currentSelectionLineBis.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    inCurrent.refreshCoordinates(coordinates.subList(startPolygon, coordinates.size()));
                    newPolygons.add(current);
                    newLines.add(currentLine);
                    newSelectionLines.add(currentSelectinLine);
                    polygonCount++;
                }
                else {
                    PolygonGL polygon = new PolygonGL(coordinates.subList(startPolygon, coordinates.size()), texture, z);
                    LineStripGL line = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()), OUTLINE_LINE_WIDTH, outlineColor);
                    LineStripGL selectionLine = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()),SELECTION_LINE_WIDTH);
                    LineStripGL selectionLineBis = new LineStripGL(coordinates.subList(startPolygon, coordinates.size()),SELECTION_OUTLINE_WIDTH, SELECTION_COLOR_OUT);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    selectionLinesBis.add(selectionLineBis);
                    polygons.add(polygon);
                    newPolygons.add(polygon);
                    newLines.add(line);
                    lines.add(line);
                    selectionLines.add(selectionLine);
                    newSelectionLines.add(selectionLine);
                    newSelectionLinesBis.add(selectionLineBis);
                    selectionLinesBis.add(selectionLineBis);
                    polygonCount++;

                }
                startPolygon = coordinates.size();
            }

        }
        this.coordinates = coordinates;
        polygons = newPolygons;
        selectionLines = newSelectionLines;
        lines = newLines;
    }

    public void setIsPoint(boolean isPoint) {

        this.isPoint = isPoint;
        Color fillColorJava = new Color(fillColor.x, fillColor.y, fillColor.z);
        float hsbVals[] = Color.RGBtoHSB(fillColorJava.getRed(),
                fillColorJava.getGreen(),
                fillColorJava.getBlue(), null);
        Color highlight = Color.getHSBColor(hsbVals[0], hsbVals[1], 0.5f * (1f + hsbVals[2]));
        outlineColor.set(highlight.getRed() / 255.0f, highlight.getGreen() / 255.0f, highlight.getBlue() / 255.0f);
        for (LineStripGL line : lines) {
            line.setColor(outlineColor);
        }
    }




    public boolean getIsComposite() {
        return isComposite;
    }

    public boolean getOutlineVisible() {
        return outlineVisible;
    }



    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
        for (PolygonGL polygon: polygons) {
            polygon.setAlpha(alpha);
        }
        for (PolygonGL polygon: offsetedPolygons) {
            polygon.setAlpha(alpha);
        }
    }

    public List<LineStripGL> getSelectionLines() {
        return selectionLines;
    }

    public List<LineStripGL> getSelectionLinesBis() {
        return selectionLinesBis;
    }

    public List<PolygonGL> getInPolygons() {return  inPolygons;}

    public void addLine(Vector3f color, int width) {
        if (isComplete) {
            LineStripGL newLine = new LineStripGL(coordinates, width, color);
            lines.add(newLine);
            shapes.add(newLine);
        }
        else {
            LineStripGL newLine = new LineStripGL(color,width);
            lines.add(newLine);
            shapes.add(newLine);
        }
    }

    public void addLineFirst(Vector3f color, int width) {
        if (isComplete) {
            LineStripGL newLine = new LineStripGL(coordinates, width, color);
            lines.add(0,newLine);
            shapes.add(0,newLine);
        }
        else {
            LineStripGL newLine = new LineStripGL(color,width);
            lines.add(0,newLine);
            shapes.add(0,newLine);
        }
    }




















}
