/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.filters;

import fr.inria.ilda.mmtools.gl.FrameBufferObject;
import fr.inria.ilda.mmtools.gl.PolygonGL;
import fr.inria.ilda.mmtools.gl.ShaderProgram;
import fr.inria.ilda.mmtools.gl.Texture;

import javax.vecmath.Vector3d;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 29/05/15.
 */
public class GaussianFilter {
    private FrameBufferObject fboBlackAndWhite;
    private FrameBufferObject fboFirstPass;
    private FrameBufferObject fboSecondPass;
    ShaderProgram blackAndWhiteShader;
    ShaderProgram filterShader;
    PolygonGL quad;
    private HashMap<Integer, Texture> textures;
    private int vaoID;
    private FrameBufferObject currentReadBuffer;
    private FrameBufferObject currentWriteBuffer;
    private FrameBufferObject resultBuffer;
    private float radius;

    public GaussianFilter(FrameBufferObject fboBlackAndWhite, FrameBufferObject fboFirstPass, FrameBufferObject fboSecondPass,  ShaderProgram blackAndWhiteShader, ShaderProgram filterShader, int vaoID, float radius) {
        this.fboBlackAndWhite = fboBlackAndWhite;
        this.fboFirstPass = fboFirstPass;
        this.fboSecondPass = fboSecondPass;
        this.blackAndWhiteShader = blackAndWhiteShader;
        this.filterShader = filterShader;
        this.vaoID = vaoID;
        this.radius = radius;
    }

    public GaussianFilter(FrameBufferObject fboBlackAndWhite, FrameBufferObject fboFirstPass, FrameBufferObject fboSecondPass, ShaderProgram blackAndWhiteShader, ShaderProgram filterShader, int vaoID) {
       this(fboBlackAndWhite, fboFirstPass, fboSecondPass, blackAndWhiteShader, filterShader, vaoID, 1.0f);
    }

    public void calculateQuad (Vector3d eye, double right, double left, double bottom, double up) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{eye.x+left, eye.y+bottom});
        coordinates.add(new double[]{eye.x+right, eye.y+bottom});
        coordinates.add(new double[]{eye.x+left, eye.y+up});
        coordinates.add(new double[]{eye.x+right, eye.y+up});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad = new PolygonGL(coordinates, indexes, fboBlackAndWhite.getTexture());


    }

    public void refreshQuadCoordinates (Vector3d eye, double right, double left, double bottom, double up) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{eye.x+left, eye.y+bottom});
        coordinates.add(new double[]{eye.x+right, eye.y+bottom});
        coordinates.add(new double[]{eye.x+left, eye.y+up});
        coordinates.add(new double[]{eye.x+right, eye.y+up});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad.setCoordinates(coordinates);
        quad.setIndexes(indexes);
        quad.refreshCoordinates();
        quad.updateBuffers();



    }

    public void calculateQuad (double [] sceneBounds) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{sceneBounds[0], sceneBounds[1]});
        coordinates.add(new double[]{sceneBounds[2], sceneBounds[1]});
        coordinates.add(new double[]{sceneBounds[0], sceneBounds[3]});
        coordinates.add(new double[]{sceneBounds[2], sceneBounds[3]});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad = new PolygonGL(coordinates, indexes, fboBlackAndWhite.getTexture());

    }

    public void refreshQuadCoordinates (double[] lowerCorner, double width, double height) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{lowerCorner[0] + width, lowerCorner[1] + height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]+height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]});
        coordinates.add(new double[]{lowerCorner[0]+width, lowerCorner[1]});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad.setCoordinates(coordinates);
        quad.setIndexes(indexes);
        quad.refreshCoordinates();
        quad.updateBuffers();



    }

    public void calculateQuad (double[] lowerCorner, double width, double height) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{lowerCorner[0] + width, lowerCorner[1] + height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]+height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]});
        coordinates.add(new double[]{lowerCorner[0]+width, lowerCorner[1]});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad = new PolygonGL(coordinates, indexes, fboBlackAndWhite.getTexture());


    }

    public int getfboBlackAndWhiteId () {
        return fboBlackAndWhite.getVboiD();
    }

    public ShaderProgram getFilterShader() {
        return filterShader;
    }

    public PolygonGL getQuad() {
        return quad;
    }

    public int getVaoID() {
        return vaoID;
    }

    public void setVaoID(int vaoID) {
        this.vaoID = vaoID;
    }

    public FrameBufferObject getFboFirstPass() {
        return fboFirstPass;
    }

    public FrameBufferObject getFboSecondPass() {
        return fboSecondPass;
    }

    public FrameBufferObject getCurrentReadBuffer() {
            if (currentWriteBuffer == fboFirstPass) {
                return fboSecondPass;
            }
            else {
                return fboFirstPass;
            }

    }

    public FrameBufferObject getCurrentWriteBuffer() {
        if (currentWriteBuffer == null) {
            currentWriteBuffer = fboFirstPass;
        }
        else {
            if (currentWriteBuffer == fboFirstPass) {
                currentWriteBuffer = fboSecondPass;

            }
            else {
                currentWriteBuffer = fboFirstPass;
            }
        }

        return currentWriteBuffer;
    }



    public FrameBufferObject getFboBlackAndWhite() {
        return fboBlackAndWhite;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }





}
