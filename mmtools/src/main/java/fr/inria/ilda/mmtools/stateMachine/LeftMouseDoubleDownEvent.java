/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

/**
 * Created by mjlobo on 23/07/15.
 */
public class LeftMouseDoubleDownEvent extends MouseDownEvent {

    public LeftMouseDoubleDownEvent(int x, int y) {
        super (x, y);
    }

    public LeftMouseDoubleDownEvent(java.awt.Component target, int x, int y) {
        super (target, x, y);
    }
}
