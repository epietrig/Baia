/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 01/06/16.
 */
public class StagedAnimationByPlan extends AbstractStagedAnimation{
    //each one two masks, to be able to show them after


    TimedStagePlanAnimation currentAnimation;
    Texture initTexture;
    Texture endTexture;
    Texture planTexture;
    RasterGeoElement animationRaster;
    AnimationPlanManager.StagedAnimationByPlanType stagedAnimationByPlanType = AnimationPlanManager.StagedAnimationByPlanType.STAGED;
    StagedAnimationByPlan equalizedAnimation;
    HashMap<Integer, List<TimedStagePlanAnimation>> timedStages = new HashMap<>();





    public StagedAnimationByPlan() {

        stages = new ArrayList<>();
        renderingType = ShaderManager.RenderedElementTypes.ANIMATION_PLAN;
    }

    public StagedAnimationByPlan(RasterGeoElement initRaster, RasterGeoElement endRaster) {
        this();
        this.initTexture = initRaster.getTexture();
        this.endTexture = endRaster.getTexture();

    }

    public StagedAnimationByPlan (Texture initTexture, Texture endTexture) {
        this();
        this.initTexture = initTexture;
        this.endTexture = endTexture;
    }

    public StagedAnimationByPlan(RasterGeoElement initRaster, RasterGeoElement endRaster, HashMap<Integer, List<TimedStagePlanAnimation>> timedStages) {
        this(initRaster, endRaster);
        this.timedStages = timedStages;
        for (int level : timedStages.keySet()) {
            for (TimedStagePlanAnimation timedStagePlanAnimation : timedStages.get(level)) {
                stages.add(timedStagePlanAnimation.getAnimation());
            }
        }
    }

    public Texture getInitTexture() {
        return initTexture;
    }

    public Texture getEndTexture() {
        return endTexture;
    }

    public void setPlanTexture(Texture planTexture) {
        this.planTexture = planTexture;
    }

    public Texture getPlanTexture() {
        return planTexture;
    }

    public RasterGeoElement getAnimationRaster() {
        return animationRaster;
    }

    public void setCurrentAnimation(TimedStagePlanAnimation currentAnimation) {
        this.currentAnimation = currentAnimation;
    }

    public TimedStagePlanAnimation getCurrentAnimation() {
        return currentAnimation;
    }



    public void addTimedStage(StagePlanAnimation stage, float startTime, float endTime, int level) {
        stages.add(stage);
        duration += stage.getDuration();
        currentAnimation = new TimedStagePlanAnimation(startTime, endTime, stage, level);
        if (timedStages.get(level)==null) {
            timedStages.put(level, new ArrayList<TimedStagePlanAnimation>());
        }
        timedStages.get(level).add(currentAnimation);
    }

    public void setInitTexture(Texture initTexture) {
        //System.out.println("setting init raster "+initRaster.getName());
        this.initTexture = initTexture;
    }

    public void setEndTexture(Texture endTexture) {
        this.endTexture = endTexture;
    }

    public List<AbstractAnimation> getStages() {
        return stages;
    }

    public float getDuration() {
        return getDurationTimed();
    }

    public float getDurationTimed() {
        return getDurationTimed(true);
    }

    public float getDurationTimed (boolean withEqualized) {
        float max = -Float.MAX_VALUE;
        if (stages.size()>0) {
            for (Integer level : timedStages.keySet()) {
                for (TimedStagePlanAnimation timedStage : timedStages.get(level)) {
                    if (timedStage.getEndTime() > max) {
                        max = timedStage.getEndTime();
                    }
                }
            }
            if (withEqualized) {
                if (equalizedAnimation != null) {
                    max += equalizedAnimation.getDuration();
                }
            }
            return max;
        }
        else {
            return 0;
        }
    }

    public float getNLevels() {
        int maxLevel = -Integer.MAX_VALUE;
        for (Integer level : timedStages.keySet()) {
            if (level>maxLevel) {
                maxLevel = level;
            }
        }
        return maxLevel;
    }

    public void deleteStage(StagePlanAnimation animation) {
        deleteTimedStage(animation);

    }

    public void deleteTimedStage(StagePlanAnimation animation) {
        stages.remove(animation);
        int targetLevel = -1;
        for (Integer level : timedStages.keySet()) {
            List<TimedStagePlanAnimation> timeStagesAtLevel = timedStages.get(level);
            System.out.println("level "+level);
            System.out.println("timedStages.get(targetLevel)"+timedStages.get(targetLevel));
            for (int i = 0; i < timeStagesAtLevel.size(); i++) {
                if (timeStagesAtLevel.get(i).getAnimation() == animation) {
                    timeStagesAtLevel.remove(timeStagesAtLevel.get(i));
                    targetLevel = level;
                }
            }
        }
        System.out.println("timedStages.get(targetLevel)"+timedStages.get(targetLevel).size());
        if (timedStages.get(targetLevel).size()==0) {
            System.out.println("removing ! "+targetLevel);
            timedStages.remove(targetLevel);
        }
    }



    public void setStagedAnimationByPlanType(AnimationPlanManager.StagedAnimationByPlanType stagedAnimationByPlanType) {
        this.stagedAnimationByPlanType = stagedAnimationByPlanType;
    }

    public AnimationPlanManager.StagedAnimationByPlanType getStagedAnimationByPlanType() {
        return stagedAnimationByPlanType;
    }

    public StagePlanAnimation getLastStagePlanAnimation() {
        return (StagePlanAnimation)stages.get(stages.size()-1);
    }

    public void setEqualizedAnimation(StagedAnimationByPlan equalizedAnimation) {
        this.equalizedAnimation = equalizedAnimation;
    }

    public StagedAnimationByPlan getEqualizedAnimation() {
        return equalizedAnimation;
    }

    public boolean containsStage(StagePlanAnimation stagePlanAnimation) {
        return stages.contains(stagePlanAnimation);
    }

    public AbstractAnimation getStagePlanTimeAnimationAtTime(float time) {
        //System.out.println("time in getstageplantimeanimation "+time);
        float accumulatedTime = 0.0f;
        if (time>=getDuration()) {
            if (stages.size()>0) {
                return stages.get(stages.size() - 1);
            }
        }
        for (AbstractAnimation animation : stages) {
            if (accumulatedTime<= time && time<=animation.getDuration()+accumulatedTime) {
                return animation;
            }
            else {
                accumulatedTime += animation.getDuration();
            }
        }
        return null;
    }

    public void replaceStage(TimedStagePlanAnimation oldAnimation, TimedStagePlanAnimation newAnimation) {
        super.replaceStage(oldAnimation.getAnimation(), newAnimation.getAnimation());
        if (currentAnimation==oldAnimation) {
            currentAnimation = newAnimation;
        }
    }

    public void changeTimedStageLevel(TimedStagePlanAnimation animation, int level) {
        //System.out.println("level in changetimedstagelevel "+level);
        timedStages.get(animation.getLevel()).remove(animation);
        if (timedStages.get(animation.getLevel()).size()==0) {
            timedStages.remove(animation.getLevel());
        }
        if (timedStages.get(level)==null) {
            timedStages.put(level, new ArrayList<TimedStagePlanAnimation>());
        }
        timedStages.get(level).add(animation);
        animation.setLevel(level);
    }

    public HashMap<Integer, List<TimedStagePlanAnimation>> getTimedStages() {
        return timedStages;
    }

    public int getMaxLevel() {
        int max = -Integer.MAX_VALUE;
        //System.out.println("timedStages.keySet() "+timedStages.keySet().size());
        for (int level : timedStages.keySet()) {
            if (level>max) {
                max = level;
            }
        }
        return max;
    }





}
