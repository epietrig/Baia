/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;
/**
 * Created by mjlobo on 22/05/15.
 */
public class RightMouseUpEvent extends MouseUpEvent {
    public RightMouseUpEvent(int x, int y) {
        super(x,y);
    }

    public RightMouseUpEvent(java.awt.Component target, int x, int y) {
        super(target, x, y);
    }
}

