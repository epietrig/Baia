/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.geo.ColorThresholdGeoElement;
import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3d;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 13/06/16.
 */
public class MaskRenderer extends AbstractGeoRenderer {

    ShaderProgram program;

    public MaskRenderer (ShaderProgram program) {
        this.program = program;
    }
    @Override
    public void render(GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager) {
        ColorThresholdGeoElement colorThresholdGeoElement = (ColorThresholdGeoElement)ge;
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    //System.out.println("rendering thresholded color polygon! 1");
                    ShaderProgram diffcolor = program;
                    int vertexAttributeId = diffcolor.getAttribute(ShaderConstants.POSITION);
                    int textureAttributeId = diffcolor.getAttribute(ShaderConstants.TEXTURE_POSITION);
                    PolygonGL polygon = (PolygonGL)shape;
                    GL4 gl = (GL4)drawable.getGL();
                    gl.glUseProgram(diffcolor.getProgramId());
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
                    gl.glUniform1f(diffcolor.getUniform("alpha"), 0.2f);
                    gl.glUniform3f(diffcolor.getUniform(ShaderConstants.COLOR),colorThresholdGeoElement.getMaskColor().x, colorThresholdGeoElement.getMaskColor().y, colorThresholdGeoElement.getMaskColor().z);
                    BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
                    BufferGL textureBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
                    if (vertexBuffer != null  && layerManager.getBufferVbo(vertexBuffer) != null && layerManager.getBufferVbo(textureBuffer) != null) {
                        gl.glEnableVertexAttribArray(vertexAttributeId);
                        gl.glBindBuffer(vertexBuffer.getTarget(), layerManager.getBufferVbo(vertexBuffer));
                        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        gl.glEnableVertexAttribArray(textureAttributeId);
                        gl.glBindBuffer(textureBuffer.getTarget(), layerManager.getBufferVbo(textureBuffer));
                        gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
                        gl.glBindBuffer(indexBuffer.getTarget(), layerManager.getBufferVbo(indexBuffer));
                        gl.glActiveTexture(gl.GL_TEXTURE0);
                        gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(colorThresholdGeoElement.getMaskedTexture()));
                        if (colorThresholdGeoElement.isChangedMaskTexture()) {
                            Buffer buffer = ByteBuffer.wrap(CVUtilities.matOneBandToByteArray(colorThresholdGeoElement.getMaskedTexture().getData()));
                            gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, gl.GL_RED, colorThresholdGeoElement.getMaskedTexture().getData().cols(), colorThresholdGeoElement.getMaskedTexture().getData().rows(), 0, gl.GL_RED,GL.GL_UNSIGNED_BYTE, buffer);
                            colorThresholdGeoElement.setChangedMaskTexture(false);
                        }
                        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                    }
                }
            }
        }
    }

    public void render (GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager, float alpha, Vector3d eye, Vector3d center,
                        double left, double right, double bottom, double top) {
        ColorThresholdGeoElement colorThresholdGeoElement = (ColorThresholdGeoElement)ge;
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    ShaderProgram diffcolor = program;
                    int vertexAttributeId = diffcolor.getAttribute(ShaderConstants.POSITION);
                    int textureAttributeId = diffcolor.getAttribute(ShaderConstants.TEXTURE_POSITION);
                    PolygonGL polygon = (PolygonGL)shape;
                    GL4 gl = (GL4)drawable.getGL();
                    gl.glUseProgram(diffcolor.getProgramId());
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(eye,center), 0);
                    gl.glUniformMatrix4fv(diffcolor.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(left,right,bottom,top), 0);
                    gl.glUniform1f(diffcolor.getUniform("alpha"),alpha);
                    gl.glUniform3f(diffcolor.getUniform(ShaderConstants.COLOR),colorThresholdGeoElement.getMaskColor().x, colorThresholdGeoElement.getMaskColor().y, colorThresholdGeoElement.getMaskColor().z);
                    BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
                    BufferGL textureBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
                    if (vertexBuffer != null  && layerManager.getBufferVbo(vertexBuffer) != null && layerManager.getBufferVbo(textureBuffer) != null) {
                        gl.glEnableVertexAttribArray(vertexAttributeId);
                        gl.glBindBuffer(vertexBuffer.getTarget(),  layerManager.getBufferVbo(vertexBuffer));
                        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        gl.glEnableVertexAttribArray(textureAttributeId);
                        gl.glBindBuffer(textureBuffer.getTarget(), layerManager.getBufferVbo(textureBuffer));
                        gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
                        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
                        gl.glActiveTexture(gl.GL_TEXTURE0);
                        gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(colorThresholdGeoElement.getTextureToThreshold()));
                        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                    }
                }
            }
        }
    }
}
