/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import java.awt.*;

/**
 * Created by mjlobo on 08/08/15.
 */
public class ComboBoxEvent extends Event{
    String selectedText;
    Component target;

    public ComboBoxEvent(String selectedText, Component target) {
        this.selectedText = selectedText;
        this.target = target;
    }

    public String getSelectedText() {
        return selectedText;
    }

    public Component getTarget() {
        return target;
    }
}
