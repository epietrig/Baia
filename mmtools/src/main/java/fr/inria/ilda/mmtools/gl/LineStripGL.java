/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.opengl.GL;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by mjlobo on 27/07/15.
 */
public class LineStripGL extends ShapeGL {

    BufferGL vertexBufferGL;
    BufferGL indexesBufferGL;
    //Vector3f color;
    float lineWidth = 2.0f;
    //float alpha = 1.0f;


    public LineStripGL (List<double[]> coordinates) {
        this (coordinates, 2.0f);
    }

    public LineStripGL (List<double[]> coordinates, float lineWidth) {
        this(coordinates, lineWidth, new Vector3f(1.0f,0.0f,0.0f));
    }

    public LineStripGL(List<double[]> coordinates, float lineWidth, Vector3f color) {
        buffers = new Hashtable<>();

        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        indexes = new ArrayList<>();
        calculateCoordinates();
        updateBuffers();
        this.lineWidth = lineWidth;
        this.color = color;
    }



    public LineStripGL(LineStripGL lineStripGL) {
        buffers = new Hashtable<>();
        coordinates = new ArrayList<>();
        indexes = new ArrayList<>();
        for (double[] coord : lineStripGL.getCoordinates()) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        for (Integer index : lineStripGL.getIndexes()) {
            indexes.add(index);
        }
        color = new Vector3f(lineStripGL.getColor().x, lineStripGL.getColor().y, lineStripGL.getColor().z);
        lineWidth = lineStripGL.getLineWidth();
        updateBuffers();

    }

    public LineStripGL () {
        this(new Vector3f(1.0f,0.0f,0.0f));
    }

    public LineStripGL(Vector3f color) {
        //super();
        this(color,2.0f);

    }

    public LineStripGL(Vector3f color, float lineWidth) {
        buffers = new Hashtable<>();
        coordinates = new ArrayList<>();
        indexes = new ArrayList<>();
        this.color =  color;
        this.lineWidth = lineWidth;
    }

    protected void calculateCoordinates() {
        for (int i=0; i<coordinates.size(); i++) {
            indexes.add(i);
        }
    }

    @Override
    public void updateBuffers() {
        if (buffers.size() == 0) {
            vertexBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.VERTEX, BufferGL.DataType.FLOAT, 3);
            indexesBufferGL = new BufferGL(GL.GL_ELEMENT_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.INDEX, BufferGL.DataType.INT, 1);
        }
        buffers.put(vertexBufferGL, getVertices());
        buffers.put(indexesBufferGL, getIndex());
    }



    public void addPointToList(double[] point) {
        coordinates.add(point);
        indexes.add(coordinates.size()-1);


    }

    public void removeLastPoint() {
        //System.out.println(parentCoordinates.size());
        coordinates.remove(coordinates.size()-1);
        indexes.remove(indexes.size()-1);


    }

    public void addPoint(double[] point) {
        coordinates.add(coordinates.size()-2, new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        updateBuffers();

    }

    public void addLastPoint(double[] point) {
        coordinates.set(coordinates.size()-1, new double[]{point[0], point[1]});
        vertices = new float[0];
        indexes.add(coordinates.size()-1);
        updateBuffers();
    }

    public void addFirstPoint(double[] point) {
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        coordinates.add(new double[]{point[0], point[1]});
        indexes.add(coordinates.size()-1);
        updateBuffers();
    }

    public ShaderManager.RenderedElementTypes getType() {
        return ShaderManager.RenderedElementTypes.LINE_STRIP;
    }

    public void offsetCoordinates() {
        super.offsetCoordinates();
        updateBuffers();
    }

    public float getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(float lineWidth) {
        this.lineWidth = lineWidth;
    }


    public void refreshCoordinates(List<double[]> coordinates) {
        super.refreshCoordinates();
        this.coordinates.clear();
        indexes.clear();
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});
        this.coordinates.add(new double[] {coordinates.get(0)[0], coordinates.get(0)[1]});

        calculateCoordinates();
        updateBuffers();
    }

    public Vector3f getColor() {
        return color;
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }


}
