/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.LayerManager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Scanner;
import java.util.Vector;

/**
 * Created by mjlobo on 20/04/16.
 */
public class GLUtils {
    public static String[] readShaderSource(String filename) {
        Vector<String> lines = new Vector<String>();
        Scanner sc;
        try {
            sc = new Scanner(new File(filename));
        } catch (IOException e) {
            System.err.println("IOException reading shader file: " + e);
            return null;
        }
        while (sc.hasNext()) {
            lines.addElement(sc.nextLine());
        }
        String[] program = new String[lines.size()];
        for (int i = 0; i < lines.size(); i++) {
            program[i] = (String) lines.elementAt(i) + "\n";
        }

        return program;
    }

    public static String[] readShaderSource(InputStream stream) {
        Vector<String> lines = new Vector<String>();
        Scanner sc;
        sc = new Scanner(stream);
        while (sc.hasNext()) {
            lines.addElement(sc.nextLine());
        }
        String[] program = new String[lines.size()];
        for (int i = 0; i < lines.size(); i++) {
            program[i] = (String) lines.elementAt(i) + "\n";
        }

        return program;
    }


    public static int createShaderPrograms(GLAutoDrawable drawable, String vs, String fs) {
        GL4 gl = (GL4) drawable.getGL();
        InputStream vsStream = GLUtils.class.getClassLoader().getResourceAsStream(vs);
        InputStream fsStream = GLUtils.class.getClassLoader().getResourceAsStream(fs);
        int vertexShader = createShaderProgram(drawable, vsStream, gl.GL_VERTEX_SHADER);
        int fragmentShader = createShaderProgram(drawable, fsStream, gl.GL_FRAGMENT_SHADER);

        int[] linked = new int[1];

        int vfprogram = gl.glCreateProgram();
        gl.glAttachShader(vfprogram, vertexShader);
        gl.glAttachShader(vfprogram, fragmentShader);
        gl.glLinkProgram(vfprogram);
        checkOpenGLError(drawable);
        gl.glGetProgramiv(vfprogram, GL3.GL_LINK_STATUS, linked, 0);
        if (linked[0] == 1)
        {
            System.out.println("... linking succeeded.");
        } else
        {
            System.out.println("... linking failed.");
            printProgramLog(drawable, vfprogram);
        }
        gl.glDeleteShader(vertexShader);
        gl.glDeleteShader(fragmentShader);
        return vfprogram;
    }


    public static int createShaderProgram(GLAutoDrawable drawable, String shaderSource, int shaderType) {
        GL4 gl = (GL4) drawable.getGL();
        int shaderInt = gl.glCreateShader(shaderType);

        int[] compiled = new int[1];


        String[] shaderSourceArray = readShaderSource(shaderSource);

        int[] lengths = new int[shaderSourceArray.length];
        for (int i = 0; i < lengths.length; i++) {
            lengths[i] = shaderSourceArray[i].length();
        }
        gl.glShaderSource(shaderInt, shaderSourceArray.length, shaderSourceArray, lengths, 0);

        gl.glCompileShader(shaderInt);
        checkOpenGLError(drawable);
        gl.glGetShaderiv(shaderInt, GL3.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 1)
        {
            System.out.println("... compilation succeeded.");
        } else {
            System.out.println("... compilation failed.");
        }

        return shaderInt;
    }

    public static int createShaderProgram(GLAutoDrawable drawable, InputStream shaderSource, int shaderType) {
        GL4 gl = (GL4) drawable.getGL();
        int shaderInt = gl.glCreateShader(shaderType);

        int[] compiled = new int[1];


        String[] shaderSourceArray = readShaderSource(shaderSource);

        int[] lengths = new int[shaderSourceArray.length];
        for (int i = 0; i < lengths.length; i++) {
            lengths[i] = shaderSourceArray[i].length();
        }
        gl.glShaderSource(shaderInt, shaderSourceArray.length, shaderSourceArray, lengths, 0);

        gl.glCompileShader(shaderInt);
        checkOpenGLError(drawable);
        gl.glGetShaderiv(shaderInt, GL3.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 1)
        {
            System.out.println("... compilation succeeded.");
        } else {
            System.out.println("... compilation failed.");
        }

        return shaderInt;
    }

    public static int createShaderPrograms(GLAutoDrawable drawable, String vs, String fs, String gs) {
        GL4 gl = (GL4) drawable.getGL();
        InputStream vsStream = GLUtils.class.getClassLoader().getResourceAsStream(vs);
        InputStream fsStream = GLUtils.class.getClassLoader().getResourceAsStream(fs);
        InputStream gsStream = GLUtils.class.getClassLoader().getResourceAsStream(gs);
        int vertexShader = createShaderProgram(drawable, vsStream, gl.GL_VERTEX_SHADER);
        int fragmentShader = createShaderProgram(drawable, fsStream, gl.GL_FRAGMENT_SHADER);
        int geometryShader = createShaderProgram(drawable, gsStream, gl.GL_GEOMETRY_SHADER);

        int[] linked = new int[1];

        int vfprogram = gl.glCreateProgram();
       // System.out.println("create shader program with geometric!");
        gl.glAttachShader(vfprogram, vertexShader);
        gl.glAttachShader(vfprogram, fragmentShader);
        gl.glAttachShader(vfprogram, geometryShader);
        //System.out.println("create shader program with geometric!");
        gl.glLinkProgram(vfprogram);
        //System.out.println("create shader program with geometric!");
        checkOpenGLError(drawable);
        //System.out.println("create shader program with geometric!");
        gl.glGetProgramiv(vfprogram, GL3.GL_LINK_STATUS, linked, 0);

        if (linked[0] == 1)
        {
            System.out.println("... linking succeeded.");
        } else
        {
            System.out.println("... linking failed.");
            printProgramLog(drawable, vfprogram);
        }
        gl.glDeleteShader(vertexShader);
        gl.glDeleteShader(fragmentShader);
        gl.glDeleteShader(geometryShader);
        return vfprogram;
    }

    public static int createShaderPrograms(GLAutoDrawable drawable, InputStream vs, InputStream fs, InputStream gs) {
        GL4 gl = (GL4) drawable.getGL();

        int vertexShader = createShaderProgram(drawable, vs, gl.GL_VERTEX_SHADER);
        int fragmentShader = createShaderProgram(drawable, fs, gl.GL_FRAGMENT_SHADER);
        int geometryShader = createShaderProgram(drawable, gs, gl.GL_GEOMETRY_SHADER);

        int[] linked = new int[1];

        int vfprogram = gl.glCreateProgram();
        //System.out.println("create shader program with geometric!");
        gl.glAttachShader(vfprogram, vertexShader);
        gl.glAttachShader(vfprogram, fragmentShader);
        gl.glAttachShader(vfprogram, geometryShader);
       // System.out.println("create shader program with geometric!");
        gl.glLinkProgram(vfprogram);
        //System.out.println("create shader program with geometric!");
        checkOpenGLError(drawable);
        //System.out.println("create shader program with geometric!");
        gl.glGetProgramiv(vfprogram, GL3.GL_LINK_STATUS, linked, 0);

        if (linked[0] == 1)
        {
            System.out.println("... linking succeeded.");
        } else
        {
            System.out.println("... linking failed.");
            printProgramLog(drawable, vfprogram);
        }
        gl.glDeleteShader(vertexShader);
        gl.glDeleteShader(fragmentShader);
        gl.glDeleteShader(geometryShader);
        return vfprogram;
    }

    public static boolean checkOpenGLError(GLAutoDrawable drawable) {
        GL4 gl = (GL4) drawable.getGL();
        // examples of places to check for OpenGL and GLSL errors:
        boolean foundError = false;
        int glErr = gl.glGetError();
        while (glErr != GL.GL_NO_ERROR) {
            //System.err.println("glError: " + glu.gluErrorString(glErr));
            foundError = true;
            glErr = gl.glGetError();
        }
        return foundError;
    }

    public static void printProgramLog(GLAutoDrawable drawable, int prog) {
        GL4 gl = (GL4) drawable.getGL();
        int[] len = new int[1];
        int[] charsWritten = new int[1];
        byte[] log = null;
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetProgramiv(prog, GL4.GL_INFO_LOG_LENGTH, intBuffer);
        if (intBuffer.get(0)!=1) {
            gl.glGetProgramInfoLog(prog, len[0], charsWritten, 0, log, 0);
            for (int i = 0; i < log.length; i++) {
                System.out.print((char) log[i]);
            }
            gl.glGetProgramiv(prog, gl.GL_INFO_LOG_LENGTH,intBuffer);
            int size = intBuffer.get(0);
            if (size>0){
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetProgramInfoLog(prog, size, intBuffer, byteBuffer);
                gl.glGetProgramInfoLog(prog, size, intBuffer, byteBuffer);
                for (byte b:byteBuffer.array()){
                    System.err.print((char)b);
                }
            }

        }
    }

    //Data utilities

    //Upload data when a shape has been created or modified
    public static void shapeBufferData(ShapeGL shape, GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();
        for (BufferGL bufferGL : shape.getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), bufferGL.getBuffer());
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)shape.getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)shape.getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
        }
    }

    public static void shapeBufferData(ShapeGL shape, GLAutoDrawable drawable, LayerManager layerManager) {
        GL3 gl = (GL3) drawable.getGL();
        for (BufferGL bufferGL : shape.getBuffers().keySet()) {
            gl.glBindBuffer(bufferGL.getTarget(), layerManager.getBufferVbo(bufferGL));
            if(bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                FloatBuffer data = (FloatBuffer)shape.getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
            }
            else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                IntBuffer data = (IntBuffer)shape.getBuffers().get(bufferGL);
                gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
            }
        }
    }



}
