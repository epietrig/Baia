/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3f;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 25/04/16.
 */
public class AlphaColoredPolygonRenderer extends AbstractPolygonGLRenderer {

    public AlphaColoredPolygonRenderer(ShaderProgram program) {
        super(program);
    }


    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Texture secondaryTexture, LayerManager layerManager) {
        render(shapeGL, drawable, camera, secondaryTexture, shapeGL.getColor(), layerManager);
    }
    @Override
    public void render(ShapeGL polygon, GLAutoDrawable drawable, Camera camera, Texture blurredTexture, Vector3f color, LayerManager
                       layerManager) {
        ShaderProgram translucentColorFill = program;
        GL4 gl = (GL4)drawable.getGL();
        int vertexAttributeId = translucentColorFill.getAttribute(ShaderConstants.POSITION);
        int textureAttributeId = translucentColorFill.getAttribute(ShaderConstants.TEXTURE_POSITION);
        gl.glUseProgram(translucentColorFill.getProgramId());
        gl.glUniformMatrix4fv(translucentColorFill.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
        gl.glUniformMatrix4fv(translucentColorFill.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        BufferGL texBuffer = polygon.getBuffer(BufferGL.Type.TEXTURE);
        gl.glEnableVertexAttribArray(vertexAttributeId);
        gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(textureAttributeId);
        gl.glBindBuffer(texBuffer.getTarget(), texBuffer.getBuffer());
        gl.glVertexAttribPointer(textureAttributeId, texBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
        gl.glUniform2f(translucentColorFill.getUniform("offset"), (float) polygon.getOffset()[0], (float) polygon.getOffset()[1]);
        gl.glUniform1f(translucentColorFill.getUniform("alpha"), polygon.getAlpha());
        gl.glUniform3f(translucentColorFill.getUniform("color"), color.x, color.y, color.z);
        BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
        gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
        gl.glActiveTexture(gl.GL_TEXTURE0);
        gl.glBindTexture(gl.GL_TEXTURE_2D, 0);
        gl.glActiveTexture(gl.GL_TEXTURE1);
        gl.glBindTexture(gl.GL_TEXTURE_2D, blurredTexture.getID());
        gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);

    }
}
