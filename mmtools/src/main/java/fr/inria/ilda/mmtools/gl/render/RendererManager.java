/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.animation.AbstractAnimationManager;
import fr.inria.ilda.mmtools.animation.AbstractStagedAnimation;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.Camera;
import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by mjlobo on 25/04/16.
 */
public class RendererManager {

    HashMap<ShaderManager.RenderedElementTypes, AbstractGeoRenderer> renderers = new HashMap<>();
    HashMap<ShaderManager.RenderedElementTypes, AbstractAnimationRenderer> animationRenderers = new HashMap<>();
    MapGLCanvas canvas;
    LayerManager layerManager;
    Texture backgroundTexture;
    boolean animating = false;
    boolean pauseAnimation = false;

    public RendererManager (MapGLCanvas canvas, LayerManager layerManager) {
        this.canvas = canvas;
        this.layerManager = layerManager;
        renderers.put(ShaderManager.RenderedElementTypes.RASTER, new RasterRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.RASTER)));
        renderers.put(ShaderManager.RenderedElementTypes.TEXTURED, new AlphaTexturedRenderer(new TexturedPolygonRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.TEXTURED)),
                new LineStripRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.LINE_STRIP))));
        renderers.put(ShaderManager.RenderedElementTypes.DRAWN, new AlphaTexturedRenderer(new AlphaTexturedPolygonRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.DRAWN)),
                new LineStripRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.LINE_STRIP))));
        renderers.put(ShaderManager.RenderedElementTypes.FILLED, new AlphaTexturedRenderer(new ColoredPolygonRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.FILLED)),
                new LineStripRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.LINE_STRIP))));
        renderers.put(ShaderManager.RenderedElementTypes.FILLED_TRANSLUCENT, new AlphaTexturedRenderer(new AlphaColoredPolygonRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.FILLED_TRANSLUCENT)),
                new LineStripRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.LINE_STRIP))));
        renderers.put(ShaderManager.RenderedElementTypes.COLOR_DIFF, new ThresholdedColoredRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.COLOR_DIFF)));
        renderers.put(ShaderManager.RenderedElementTypes.MASK, new MaskRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.MASK)));

        animationRenderers.put(ShaderManager.RenderedElementTypes.ANIMATION_PLAN, new AnimationPlanRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.ANIMATION_PLAN)));


    }

    public RendererManager (MapGLCanvas canvas, LayerManager layerManager, AbstractAnimationManager animationManager) {
        this(canvas, layerManager);
        canvas.setAnimationManager(animationManager);
    }

    public void render (GLAutoDrawable drawable, Camera camera) {
        GL4 gl = (GL4) drawable.getGL();
        if (animating) {
            renderAnimation(drawable, camera, canvas.getAnimationManager());
        }
        else {
            for (Layer layer : layerManager.getLayers()) {
                if (layer instanceof RasterLayer) {

                    if (layerManager.getLayerVao(layer) != null) {
                        gl.glBindVertexArray(layerManager.getLayerVao(layer));
                        if (layerManager.getRenderingFeatures().get(layer) != null) {
                            for (GeoElement ge : layerManager.getRenderingFeatures().get(layer).keySet()) {
                                if (renderers.get(ge.getRenderingType()) != null) {
                                    if (layerManager.getRenderingFeatures().get(layer).get(ge) != null && layerManager.getRenderingFeatures().get(layer).get(ge).getVisible()) {
                                        renderers.get(ge.getRenderingType()).render(ge, drawable, camera, layerManager);
                                    }
                                }
                            }
                        }
                    }
                } else if (layer.getVisibility()) {
                    gl.glBindVertexArray(layerManager.getLayerVao(layer));
                    for (int i=0; i<layer.getElements().size(); i++){
                        GeoElement ge = layer.getElements().get(i);
                        if (ge instanceof ColorThresholdGeoElement) {
                            renderers.get(ge.getRenderingType()).render(ge, drawable, camera, layerManager);
                        } else {
                            AlphaTexturedRenderer renderer = (AlphaTexturedRenderer) renderers.get(ge.getRenderingType());
                            if (backgroundTexture != null) {
                                renderer.setHoleTexture(backgroundTexture);
                                renderer.setHoleRenderer(new AlphaTexturedPolygonRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.TEXTURED)));
                            } else {
                                renderer.setHoleRenderer(new ColoredPolygonRenderer(canvas.getShaderManager().getPrograms().get(ShaderManager.RenderedElementTypes.FILLED)));
                            }
                            renderers.get(ge.getRenderingType()).render(ge, drawable, camera, layerManager);
                        }

                    }

                }
            }
        }
    }

    //ANIMATION RENDERING

    long startAnimationTime = -1;
    int index = 0;
    long accumulatedTimeAnimation;
    long lastTime;

    public void renderStagedAnimation(GLAutoDrawable drawable, Camera camera, List<AbstractStagedAnimation> stagedAnimations) {
        GL4 gl = (GL4) drawable.getGL();
        AbstractStagedAnimation stagedAnimation = stagedAnimations.get(index);
        if (startAnimationTime == -1) {
            startAnimationTime = System.currentTimeMillis();
            accumulatedTimeAnimation=0;
            lastTime = startAnimationTime;

        }


        gl.glBindVertexArray(layerManager.getLayerVao(canvas.getAnimationManager().getAnimationLayer()));
        animationRenderers.get(stagedAnimation.getRenderingType()).render(stagedAnimation, drawable, camera, accumulatedTimeAnimation, layerManager, canvas.getAnimationManager().getAnimationRaster());

        if (!pauseAnimation) {
            accumulatedTimeAnimation += System.currentTimeMillis() - lastTime;
        }
        if (accumulatedTimeAnimation>stagedAnimation.getDuration()*1000) {
            pauseAnimation = true;
            canvas.throwAnimationPaused(accumulatedTimeAnimation/1000f);
        }
        else {
            if (!pauseAnimation) {
                canvas.throwAnimationStep(accumulatedTimeAnimation / 1000f);
            }
        }
        lastTime = System.currentTimeMillis();
    }

    long initTime;
    long testTime;
    public void renderAnimation(GLAutoDrawable drawable, Camera camera, AbstractAnimationManager animationManager) {
        GL4 gl = (GL4) drawable.getGL();
        testTime = System.currentTimeMillis();
        if (startAnimationTime == -1) {
            startAnimationTime = System.currentTimeMillis();
            accumulatedTimeAnimation=0;
            lastTime = startAnimationTime;
        }
        if (!pauseAnimation) {
            accumulatedTimeAnimation = System.currentTimeMillis() - startAnimationTime;
        }
        Pair<Float,AbstractStagedAnimation> currentAnimation = animationManager.getCurrentAnimation(accumulatedTimeAnimation/1000f);
        if (currentAnimation != null) {
            long currentAnimationTime = accumulatedTimeAnimation-(long)(currentAnimation.getValue1()*1000);
            gl.glBindVertexArray(layerManager.getLayerVao(canvas.getAnimationManager().getAnimationLayer()));
            animationRenderers.get(currentAnimation.getValue2().getRenderingType()).render(currentAnimation.getValue2(), drawable, camera, currentAnimationTime, layerManager, canvas.getAnimationManager().getAnimationRaster());
        }

        if (accumulatedTimeAnimation>animationManager.getTotalDuration()*1000 && pauseAnimation==false) {
            pauseAnimation = true;
            canvas.throwAnimationPaused(accumulatedTimeAnimation / 1000f);
        }
        else {
            if (!pauseAnimation) {
                canvas.throwAnimationStep(accumulatedTimeAnimation / 1000f);
            }
        }

    }

    public void setAnimating (boolean animating) {
        this.animating = animating;
    }

    public void setPauseAnimation(boolean pauseAnimation) {
        this.pauseAnimation = pauseAnimation;
        if (pauseAnimation) {
            canvas.throwAnimationPaused(accumulatedTimeAnimation/1000f);
        }
        if (!pauseAnimation && accumulatedTimeAnimation>=canvas.getAnimationManager().getTotalDuration()*1000) {
            accumulatedTimeAnimation = 0;
            startAnimationTime = System.currentTimeMillis() - accumulatedTimeAnimation;
            canvas.throwAnimationStep(0);
        }
        else if (!pauseAnimation) {
            startAnimationTime = System.currentTimeMillis() - accumulatedTimeAnimation;
        }
    }

    public boolean getPauseAnimation(){
        return pauseAnimation;
    }

    public boolean isAnimating() {
        return animating;
    }

    public void setAccumulatedTimeAnimation(long accumulatedTimeAnimation) {
        this.accumulatedTimeAnimation = accumulatedTimeAnimation;
        canvas.throwAnimationStep(accumulatedTimeAnimation/1000f);
    }

    public LayerManager getLayerManager() {
        return layerManager;
    }




}
