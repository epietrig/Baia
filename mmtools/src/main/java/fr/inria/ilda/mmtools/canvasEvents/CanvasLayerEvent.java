/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.Layer;

/**
 * Created by mjlobo on 24/03/2017.
 */
public class CanvasLayerEvent extends CanvasEvent {

    String event;
    Layer layer;


    public CanvasLayerEvent(String event, Layer layer) {
        super (event);
        this.event = event;
        this.layer = layer;
    }



    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Layer getLayer() {
        return layer;
    }
}
