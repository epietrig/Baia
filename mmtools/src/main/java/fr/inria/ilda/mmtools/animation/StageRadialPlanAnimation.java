/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.utilties.Constants;

import java.util.List;

/**
 * Created by mjlobo on 19/06/16.
 */
public class StageRadialPlanAnimation extends StagePlanAnimation {

    int [] centerCoords;

    public StageRadialPlanAnimation(Type type, float duration, int[] centerCoords) {
        super(type, duration);
        this.centerCoords = centerCoords;
    }

    public StageRadialPlanAnimation (Type type, float duration, int[] centerCoords, boolean blurBorder, int blurWidth) {
        super(type, duration, blurBorder, blurWidth);
        this.centerCoords = centerCoords;
    }

    public StageRadialPlanAnimation (Type type) {
        super(type);
    }

    public int[] getCenterCoords(){
        return centerCoords;
    }

    public boolean isComplete() {
        if (centerCoords!=null) {
            return true;
        }
        else {
            return false;
        }
    }

    public List<AnimationParameter> getValuesToSave() {
        List<AnimationParameter> valuesToSave = super.getValuesToSave();
        AnimationParameter centerParameter = new AnimationParameter(Constants.CENTER_TAG);
        centerParameter.addAttribute(Constants.X_TAG, centerCoords[0]+"");
        centerParameter.addAttribute(Constants.Y_TAG, centerCoords[1]+"");
        valuesToSave.add(centerParameter);
        return valuesToSave;
    }

    public List<String> getValuesToRetrieve() {
        java.util.List<String> valuestoRetrieve = super.getValuesToRetrieve();
        valuestoRetrieve.add(Constants.CENTER_TAG);
        return valuestoRetrieve;
    }

    public void setCenterCoords(int [] centerCoords) {
        this.centerCoords = centerCoords;
    }

}
