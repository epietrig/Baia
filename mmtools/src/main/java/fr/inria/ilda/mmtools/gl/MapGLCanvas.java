/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.common.nio.Buffers;
import com.jogamp.newt.awt.NewtCanvasAWT;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.*;
import fr.inria.ilda.mmtools.animation.AbstractAnimationManager;
import fr.inria.ilda.mmtools.canvasEvents.*;
import fr.inria.ilda.mmtools.filters.GaussianFilter;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.render.AbstractAnimationRenderer;
import fr.inria.ilda.mmtools.gl.render.AbstractGeoRenderer;
import fr.inria.ilda.mmtools.gl.render.RasterRenderer;
import fr.inria.ilda.mmtools.gl.render.RendererManager;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import javax.vecmath.Vector3f;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.jogamp.opengl.GL.*;

/**
 * Created by mjlobo on 20/04/16.
 */
public class MapGLCanvas extends NewtCanvasAWT implements GLEventListener{
    LayerManager layerManager;
    Camera camera;
    ShaderManager shaderManager;
    TextureManager textureManager;
    FilterManager filterManager;
    RendererManager rendererManager;
    AbstractAnimationManager animationManager;
    private ConcurrentLinkedQueue<CanvasEvent> canvasEvents;
    boolean isRetina;
    List<MapGLCanvasListener> listeners;

    int width;
    int height;

    private int tVertexArray[];
    private GaussianFilter gaussianFilter;
    Vector3f clearColor;

    public MapGLCanvas(LayerManager layerManager, GLWindow window, boolean isRetina) {
        super(window);
        this.layerManager = layerManager;
        camera = new Camera(this);
        canvasEvents = new ConcurrentLinkedQueue<CanvasEvent>();
        this.isRetina = isRetina;
        clearColor = new Vector3f(0.0f,0.0f,0.0f);
        listeners = new ArrayList<>();

    }

    public MapGLCanvas(LayerManager layerManager, AbstractAnimationManager animationManager, GLWindow window, boolean isRetina) {
        this(layerManager, window, isRetina);
        this.animationManager = animationManager;

    }

    HashMap<ShaderManager.RenderedElementTypes, AbstractGeoRenderer> geoRenderers = new HashMap<>();


    @Override
    public void init(GLAutoDrawable drawable) {

        GL4bc gl = (GL4bc) drawable.getGL();
        drawable.getAnimator().setUpdateFPSFrames(3, null);
        gl.setSwapInterval(0);
        shaderManager = new ShaderManager(gl);
        textureManager = new TextureManager(this);
        filterManager = new FilterManager(this);
        shaderManager.initializeShaders(drawable);
        rendererManager = new RendererManager(this, layerManager);
        gl.glEnable(gl.GL_PROGRAM_POINT_SIZE);
        gl.glClampColor(gl.GL_CLAMP_READ_COLOR, GL_FALSE);

        for (Layer layer : layerManager.getLayers()) {
            for (GeoElement ge : layer.getElements()) {
                if (layer instanceof RasterLayer) {

                    if (ge instanceof RGBRasterGeoElement) {
                        //System.out.println("instance of RGB!");
                        if (((RGBRasterGeoElement)ge).getImageWidth()<= Constants.MAX_TEXTURE_SIZE && ((RGBRasterGeoElement)ge).getImageHeight()<=Constants.MAX_TEXTURE_SIZE) {

                            textureManager.loadTextureFromRasterTiff(drawable, (RGBRasterGeoElement) ge);
                            textureManager.loadImageModelTexture(drawable,(RGBRasterGeoElement)ge);

                        }
                        else {
                            for (Tile tile: ((RGBRasterGeoElement)ge).getTiles()) {
                                textureManager.loadTextureFromTile(drawable, tile);
                            }

                        }

                    }
                    else if (ge instanceof MNTRasterGeoElement) {
                        pushEvent(new CanvasRasterEvent(CanvasEvent.NEW_MNT_RASTERGEOELEMENT, (MNTRasterGeoElement) ge, layer));

                    }

                }
                for (ShapeGL shape : ge.getShapes()) {
                    if (shape instanceof PolygonGL) {
                        shape.setShader(shaderManager.getPrograms().get(ShaderManager.RenderedElementTypes.TEXTURED));
                        shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(shape.getShader().getAttribute(ShaderConstants.POSITION));
                        shape.getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(shape.getShader().getAttribute(ShaderConstants.TEXTURE_POSITION));
                    }

                    if (shape instanceof LineGL) {
                        shape.setShader(shaderManager.getPrograms().get(ShaderManager.RenderedElementTypes.LINE));
                        shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(shape.getShader().getAttribute(ShaderConstants.POSITION));
                        shape.getBuffer(BufferGL.Type.NORMALS).setShaderAttributeId(shape.getShader().getAttribute(ShaderConstants.NORMAL));
                    }

                    if (shape instanceof LineStripGL) {
                        shape.setShader(shaderManager.getPrograms().get(ShaderManager.RenderedElementTypes.LINE_STRIP));
                        shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(shape.getShader().getAttribute(ShaderConstants.POSITION));
                    }



                }
                if (ge instanceof DrawnGeoElement) {
                    ((DrawnGeoElement) ge).getPolygon().getBuffer(BufferGL.Type.ALPHA).setShaderAttributeId(((DrawnGeoElement) ge).getPolygon().getShader().getAttribute("alphaPos"));
                }
            }
        }



        List<Layer> layers = layerManager.getLayers();

        tVertexArray = new int[layerManager.getLayers().size()];
        gl.glGenVertexArrays(tVertexArray.length, tVertexArray, 0);
        int totalElements = 0;

        for (int i = 0; i < layers.size(); i++) {
            layers.get(i).setVao(tVertexArray[i]);
            layerManager.setLayerVao(layers.get(i), tVertexArray[i]);
            for (GeoElement ge : layers.get(i).getElements()) {
                for (ShapeGL shape : ge.getShapes()) {
                    totalElements += shape.getBuffers().size();
                }
            }
        }
        if (totalElements>0) {
        int [] buffers = new int[totalElements];
        int j = 0;
        gl.glGenBuffers(buffers.length, buffers, 0);
        for (int i = 0; i < layers.size(); i++) {
            gl.glBindVertexArray(tVertexArray[i]);

            for (GeoElement ge : layers.get(i).getElements()) {
                for (ShapeGL shape : ge.getShapes()) {
                    for (BufferGL bufferGL : shape.getBuffers().keySet()) {
                        gl.glBindBuffer(bufferGL.getTarget(), buffers[j]);
                        if (bufferGL.getDataType() == BufferGL.DataType.FLOAT) {
                            FloatBuffer data = (FloatBuffer) shape.getBuffers().get(bufferGL);
                            gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_FLOAT, data, bufferGL.getUsage());
                        } else if (bufferGL.getDataType() == BufferGL.DataType.INT) {
                            IntBuffer data = (IntBuffer) shape.getBuffers().get(bufferGL);
                            gl.glBufferData(bufferGL.getTarget(), data.limit() * Buffers.SIZEOF_INT, data, bufferGL.getUsage());
                        }
                        //System.out.println("buffers[j] "+buffers[j]);
                        bufferGL.setBuffer(buffers[j]);
                        layerManager.setBufferVbo(bufferGL,buffers[j]);
                        j++;
                    }
                }

            }

        }




        }
        gaussianFilter = filterManager.initializeGaussianFilter(drawable);
        for (ShaderProgram shaderProgram: shaderManager.getPrograms().values()) {
            gl.glUseProgram(shaderProgram.getProgramId());
            for (int i=0; i<shaderProgram.getSamplers().size(); i++) {
                gl.glUniform1i(shaderProgram.getUniform(shaderProgram.getSamplers().get(i)),i);
            }
        }

        geoRenderers.put(ShaderManager.RenderedElementTypes.RASTER, new RasterRenderer(shaderManager.getPrograms().get(ShaderManager.RenderedElementTypes.RASTER)));

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL4bc gl = (GL4bc) glAutoDrawable.getGL();
        gl.glEnable(GL_BLEND);
        gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0);
        gl.glClear(GL_COLOR_BUFFER_BIT);
        gl.glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);

        popEvents(glAutoDrawable);

        rendererManager.render(glAutoDrawable, camera);

    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int width, int height) {
        this.width = width;
        this.height = height;
        GL4 gl = glAutoDrawable.getGL().getGL4();

        gl.glViewport(0, 0, width, height);
        if(isRetina) {
            double topratio = camera.getTop()/(camera.getTop()-camera.getBottom());
            double bottomRatio = -camera.getBottom()/(camera.getTop()-camera.getBottom());
            double leftRatio = -camera.getLeft()/(camera.getRight()-camera.getLeft());
            double rightRatio = camera.getRight()/(camera.getRight()-camera.getLeft());

            camera.setTop((float)(height / (camera.getPixels_per_unit()[1]*2)*topratio));
            camera.setRight((float)(width / (camera.getPixels_per_unit()[0]*2)*rightRatio));
            camera.setBottom(-(float)(height / (camera.getPixels_per_unit()[1]*2)*bottomRatio));
            camera.setLeft(-(float)(width / (camera.getPixels_per_unit()[0]*2)*leftRatio));


        }
        else {
            double topratio = camera.getTop()/(camera.getTop()-camera.getBottom());
            double bottomRatio = -camera.getBottom()/(camera.getTop()-camera.getBottom());
            double leftRatio = -camera.getLeft()/(camera.getRight()-camera.getLeft());
            double rightRatio = camera.getRight()/(camera.getRight()-camera.getLeft());
            camera.setTop((float)(height / (camera.getPixels_per_unit()[1])*topratio));
            camera.setRight((float)(width / (camera.getPixels_per_unit()[0])*rightRatio));
            camera.setBottom(-(float)(height / (camera.getPixels_per_unit()[1])*bottomRatio));
            camera.setLeft(-(float)(width / (camera.getPixels_per_unit()[0])*leftRatio));

        }
        camera.lookAt();
        camera.getProjectionMatrix();
        camera.calculateMatrices();

    }

    public void newGeoElement(Layer layer, GeoElement ge, GLAutoDrawable drawable) {
        GL4 gl_context = (GL4) drawable.getGL();
        int totalShapes = 0;
        if (layer.getVao() == -1) {
            int[] layervao = new int[1];
            gl_context.glGenVertexArrays(1, layervao, 0);
            layer.setVao(layervao[0]);
            layerManager.setLayerVao(layer, layer.getVao());
        }
        System.out.println("layer veo "+layer.getVao());
        //gl_context.glBindVertexArray(layer.getVao());
        gl_context.glBindVertexArray(layerManager.getLayerVao(layer));
        for (ShapeGL shape : ge.getShapes()) {
            shape.setShader(shaderManager.getPrograms().get(shape.getType()));
            totalShapes += shape.getBuffersSize();
            if (shape instanceof LineGL) {
                shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId((shape.getShader().getAttribute(ShaderConstants.POSITION)));
                shape.getBuffer(BufferGL.Type.NORMALS).setShaderAttributeId(shape.getShader().getAttribute("normal"));
            }
            if (shape instanceof LineStripGL) {
                shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId((shape.getShader().getAttribute(ShaderConstants.POSITION)));
            }
        }

        int[]buffersDrawn = new int[totalShapes];
        gl_context.glGenBuffers(buffersDrawn.length, buffersDrawn, 0);
        int j=0;
        for (ShapeGL shape : ge.getShapes()) {
            for (BufferGL bufferGL : shape.getBuffers().keySet()) {
                bufferGL.setBuffer(buffersDrawn[j]);
                layerManager.setBufferVbo(bufferGL,buffersDrawn[j]);
                j++;
            }
            GLUtils.shapeBufferData(shape, drawable, layerManager);
        }
        System.out.println("buffers "+layerManager.getBufferVbos().size() + " in canvas "+getName());
    }

    public void newLayer(Layer layer, GLAutoDrawable drawable) {
        GL4 gl_context = (GL4) drawable.getGL();
        int[] layervao = new int[1];
        gl_context.glGenVertexArrays(1, layervao, 0);
        layer.setVao(layervao[0]);
        layerManager.setLayerVao(layer, layervao[0]);
    }

    private void finishDrawn(Layer drawnLayer, DrawnGeoElement ge, GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();
        gl.glBindVertexArray(drawnLayer.getVao());
        for (ShapeGL shape : ge.getShapes()) {
            if (shape instanceof PolygonGL) {
                int[] buffersDrawnComplete = new int [shape.getBuffersSize()];
                gl.glGenBuffers(buffersDrawnComplete.length, buffersDrawnComplete, 0);
                int i =0;
                for (BufferGL bufferGL: shape.getBuffers().keySet()) {
                    bufferGL.setBuffer(buffersDrawnComplete[i]);
                    i++;
                }

                GLUtils.shapeBufferData(shape, drawable);
            }
        }
        for (LineStripGL selectionLine: ge.getSelectionLines()) {
            int[] buffersDrawnCompleteSelectionLine = new int[selectionLine.getBuffersSize()];
            gl.glGenBuffers(buffersDrawnCompleteSelectionLine.length, buffersDrawnCompleteSelectionLine, 0);
            int i = 0;
            for (BufferGL bufferGL : selectionLine.getBuffers().keySet()) {
                bufferGL.setBuffer(buffersDrawnCompleteSelectionLine[i]);
                i++;
            }

            GLUtils.shapeBufferData(selectionLine, drawable);
        }

        for (LineStripGL selectionLine: ge.getSelectionLinesBis()) {
            int[] buffersDrawnCompleteSelectionLine = new int[selectionLine.getBuffersSize()];
            gl.glGenBuffers(buffersDrawnCompleteSelectionLine.length, buffersDrawnCompleteSelectionLine, 0);
            int i = 0;
            for (BufferGL bufferGL : selectionLine.getBuffers().keySet()) {
                bufferGL.setBuffer(buffersDrawnCompleteSelectionLine[i]);
                i++;
            }

            GLUtils.shapeBufferData(selectionLine, drawable);
        }



        refreshGeoElement(drawnLayer, ge, drawable);
        for (ShapeGL shape:ge.getShapes()) {
            if(shape instanceof PolygonGL) {
                PolygonGL polygon = (PolygonGL)shape;
                shape.setShader(shaderManager.getPrograms().get(ShaderManager.RenderedElementTypes.DRAWN));
                shape.getBuffer(BufferGL.Type.VERTEX).setShaderAttributeId(polygon.getShader().getAttribute(ShaderConstants.POSITION));
                shape.getBuffer(BufferGL.Type.TEXTURE).setShaderAttributeId(polygon.getShader().getAttribute(ShaderConstants.TEXTURE_POSITION));
            }
        }

    }

    public void refreshGeoElement(Layer drawnLayer, DrawnGeoElement ge, GLAutoDrawable drawable) {

        GL3 gl = (GL3) drawable.getGL();
        gl.glBindVertexArray(drawnLayer.getVao());
        for (ShapeGL shapeGL : ge.getShapes()) {
            GLUtils.shapeBufferData(shapeGL, drawable);
        }

    }

    public void updateViewport(GLAutoDrawable drawable) {
        GL4 gl = (GL4)drawable.getGL();
        if (isRetina) {
            gl.glViewport(0,0,getWidth()*2, getHeight()*2);
        }
        else {
            gl.glViewport(0,0,getWidth(), getHeight());
        }

    }



    CanvasEvent lastAddedEvent;
    public void pushEvent(CanvasEvent event) {
        canvasEvents.add(event);
    }

    public void pushEvent(CanvasEvent event, boolean replaceLast) {
        if (replaceLast) {
            if (lastAddedEvent != null && lastAddedEvent.getEvent().equals(event.getEvent())) {
                if (lastAddedEvent.getEvent() == CanvasEvent.MASK_UPDATED) {
                    if (((CanvasDrawnEvent)event).getGeoElement() == ((CanvasDrawnEvent)lastAddedEvent).getGeoElement()) {
                        canvasEvents.remove(lastAddedEvent);
                        canvasEvents.add(event);
                        lastAddedEvent = event;
                    }
                    else {
                        pushEvent(event);
                    }
                }
                else {
                    pushEvent(event);
                }
            }
            else {
                pushEvent(event);
            }
        }
        else {
            pushEvent(event);
        }
    }

    private void popEvents(GLAutoDrawable drawable) {
        //System.out.println("In pop events");
        CanvasEvent current;
        Iterator<CanvasEvent> iterator;

        while((current=canvasEvents.poll())!=null) {
            switch (current.getEvent()) {
                case CanvasEvent.NEW_MNT_RASTERGEOELEMENT:
                    textureManager.loadMntTexture(((MNTRasterGeoElement)((CanvasRasterEvent)current).getMntRasterGeoElement()),((CanvasRasterEvent)current).getLayer(), drawable);
                    break;
                case CanvasEvent.DRAWN_CREATED:
                    newGeoElement(((CanvasDrawnEvent)current).getLayer(), ((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.NEW_TEXTURE_FROM_MAT:
                    textureManager.updateTextureFromMat(((CanvasNewTextureMatEvent)current).getTexture(),((CanvasNewTextureMatEvent)current).getMat(), drawable);
                    break;
                case CanvasEvent.MASK_UPDATED:
                    textureManager.drawMask(((CanvasDrawnEvent)current).getLayer(), ((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.TEXTURE_MAT_EDITED:
                    break;
                case CanvasEvent.POINT_ADDED:
                    refreshGeoElement(((CanvasDrawnEvent)current).getLayer(),(DrawnGeoElement)((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.DRAWN_FINISHED:
                    finishDrawn(((CanvasDrawnEvent)current).getLayer(),(DrawnGeoElement)((CanvasDrawnEvent)current).getGeoElement(), drawable);
                    break;
                case CanvasEvent.DRAW_STAGED_ANIMATIONS:
                    CanvasStagedAnimationsEvent canvasStagedAnimationsEvent = (CanvasStagedAnimationsEvent)current;
                    textureManager.exportAnimation(canvasStagedAnimationsEvent.getAnimationLayer(), canvasStagedAnimationsEvent.getAnimationRaster(), drawable, canvasStagedAnimationsEvent.getAnimations(), layerManager,
                            30, canvasStagedAnimationsEvent.getParam(Constants.EXPORT_PATH));
                    break;
                case CanvasEvent.NEW_LAYER:
                    CanvasLayerEvent canvasLayerEvent = (CanvasLayerEvent)current;
                    newLayer(canvasLayerEvent.getLayer(),drawable);
                    break;
                case CanvasEvent.NEW_RASTER:
                    CanvasRasterEvent canvasRasterEvent = (CanvasRasterEvent)current;
                    textureManager.loadImageModelTexture(drawable,(RGBRasterGeoElement)canvasRasterEvent.getMntRasterGeoElement());
                    break;
                case CanvasEvent.FINISHED_LOADING_RASTERS:
                    throwFinishedLoadingRasters();
                    break;
                case CanvasEvent.FINISHED_EXPORTING_MOVIE:
                    throwFinishedExportingMovie();
                    break;


            }



        }

    }

    public void startAnimating() {

        rendererManager.setAnimating(true);
        rendererManager.setPauseAnimation(false);
    }

    public void stopAnimating() {
        rendererManager.setPauseAnimation(false);
        rendererManager.setAnimating(false);
    }

    public void pauseAnimation(boolean pauseAnimation) {
        rendererManager.setPauseAnimation(pauseAnimation);
    }

    public boolean getPauseAnimation() {
        return rendererManager.getPauseAnimation();
    }

    public boolean getIsAnimating() {
        return rendererManager.isAnimating();
    }

    public void setAnimationTime(long time) {
        rendererManager.setAccumulatedTimeAnimation(time);
    }


    public void setAnimationManager(AbstractAnimationManager animationManager) {
        this.animationManager = animationManager;
    }

    public AbstractAnimationManager getAnimationManager() {
        return animationManager;
    }

    //Listeners operations
    public void throwAnimationStep(float accumulatedTime) {
        for (MapGLCanvasListener listener : listeners) {
            listener.animationStep(accumulatedTime, this);
        }
    }

    public void throwAnimationPaused(float accumulatedTime) {
        for (MapGLCanvasListener listener: listeners) {
            listener.animationPaused(accumulatedTime);

        }
    }

    public void throwFinishedLoadingRasters(){
        for (MapGLCanvasListener listener: listeners) {
            listener.finishedLoadingRasters(this);

        }
    }

    public void throwFinishedExportingMovie(){
        for (MapGLCanvasListener listener: listeners) {
            listener.finishedExportingMovie();
        }
    }


    public void addListener (MapGLCanvasListener listener) {
        listeners.add(listener);
    }

    //Setters and Getters
    public Camera getCamera() {
        return camera;
    }

    public boolean isRetina() {
        return isRetina;
    }

    public ShaderManager getShaderManager() {
        return shaderManager;
    }

    public TextureManager getTextureManager() {
        return textureManager;
    }

    public FilterManager getFilterManager() { return filterManager;}

    public LayerManager getLayerManager() {
        return layerManager;
    }
}
