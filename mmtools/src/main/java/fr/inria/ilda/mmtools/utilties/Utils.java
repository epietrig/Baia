/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.utilties;

import org.opencv.core.Point;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by mjlobo on 20/06/16.
 */
public class Utils {
    public static double distance(Point p1, Point p2) {
        return Math.sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    }

    public static boolean hasRetinaDisplay() {
        Object obj = Toolkit.getDefaultToolkit()
                .getDesktopProperty(
                        "apple.awt.contentScaleFactor");
        System.out.println("object class "+obj);
        if (obj instanceof Float) {
            Float f = (Float) obj;
            System.out.println("content scale factor "+f);
            int scale = f.intValue();
            return (scale == 2); // 1 indicates a regular mac display.
        }
        return false;
    }

    public static Element appendDOMChildValue(Document doc, Element parent, String childName, String childValue) {
        Element child = doc.createElement(childName);
        parent.appendChild(child);
        if (childValue!=null) {
            child.appendChild(doc.createTextNode(childValue));
        }
        return child;
    }

    public static Element appendDOMChildValue(Document doc, Element parent, String childName) {
        Element child = doc.createElement(childName);
        parent.appendChild(child);
        return child;
    }




    // Code from https://www.mkyong.com/java/how-to-compress-files-in-zip-format/
    public static void zipIt(java.util.List<String> fileList, String zipFile, String pathToZip){

        byte[] buffer = new byte[1024];

        try{

            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFile);

            for(String file : fileList){

                System.out.println("File Added : " + file);
                ZipEntry ze= new ZipEntry(file);
                zos.putNextEntry(ze);

                FileInputStream in =
                        new FileInputStream(pathToZip + File.separator + file);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zos.closeEntry();
            //remember close it
            zos.close();

            System.out.println("Done");
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     * @param node file or directory
     */
    public static java.util.List<String> generateFileList(File node, String pathToZip){

        System.out.println("generate file list "+node.getAbsolutePath());
        java.util.List<String> fileList = new ArrayList<>();

        //add file only
        if(node.isFile()){
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString(), pathToZip));
        }

        if(node.isDirectory()){
            String[] subNote = node.list();
            for(String filename : subNote){
                fileList.addAll(generateFileList(new File(node, filename), pathToZip));
            }
        }

        return fileList;

    }

    /**
     * Format the file path for zip
     * @param file file path
     * @return Formatted file path
     */
    private static String generateZipEntry(String file, String pathToZip){
        System.out.println("file.substring(pathToZip.length()+1, file.length()) " + file.substring(pathToZip.length()+1, file.length()));
        return file.substring(pathToZip.length()+1, file.length());
    }

    /**
     * Unzip it
     * @param zipFile input zip file
     */
    public static void unZipIt(String zipFile, String outputFolder){

        byte[] buffer = new byte[1024];

        try{

            //create output directory is not exists
            File folder = new File(outputFolder);
            if(!folder.exists()){
                folder.mkdir();
            }

            //get the zip file content
            ZipInputStream zis =
                    new ZipInputStream(new FileInputStream(zipFile));
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while(ze!=null){

                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                System.out.println("file unzip : "+ newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            System.out.println("Done");

        }catch(IOException ex){
            ex.printStackTrace();
        }
    }


}
