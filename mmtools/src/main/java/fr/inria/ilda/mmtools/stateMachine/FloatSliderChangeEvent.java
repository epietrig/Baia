/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;

/**
 * Created by mjlobo on 30/10/15.
 */
public class FloatSliderChangeEvent extends Event {
    Component target;
    float value;

    public FloatSliderChangeEvent (Component target, float value) {
        this.target = target;
        this.value = value;
    }


    public Component getTarget() {
        return target;
    }

    public float getValue() {
        return value;
    }
}

