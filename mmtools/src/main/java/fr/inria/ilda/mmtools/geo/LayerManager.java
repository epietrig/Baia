/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.gl.BufferGL;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.gl.render.RenderFeatures;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.opencv.core.Mat;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by mjlobo on 20/04/16.
 */
public class LayerManager {
    List<Layer> layers;
    HashMap<Layer, HashMap<GeoElement,RenderFeatures>> renderingFeatures;
    HashMap<Layer, Integer> layerVaos = new HashMap<>();
    HashMap<BufferGL, Integer> bufferVbos = new HashMap<>();
    HashMap<Texture, Integer> textureIds = new HashMap<>();
    public LayerManager() {
        layers = new ArrayList<>();
        renderingFeatures = new HashMap<>();
    }
    public LayerManager(List<Layer> layers) {
        this.layers = Collections.synchronizedList(layers);
    }
    public List<Layer> getLayers() {
        return layers;
    }


    public void addLayer (Layer layer) {
        addLayer(layer, false);
    }

    public void addLayer(Layer layer, boolean visible) {
        renderingFeatures.put(layer,new HashMap<GeoElement, RenderFeatures>());
        for (GeoElement ge:layer.getElements()) {
            renderingFeatures.get(layer).put(ge,new RenderFeatures(visible));
        }
        layers.add(layer);
    }

    public void removeLayer(Layer layer) {
        layers.remove(layer);
        renderingFeatures.remove(layer);
    }

    public void addLayer(Layer layer, boolean visible, int index) {
        //System.out.println("Adding layer visible "+visible);

        renderingFeatures.put(layer,new HashMap<GeoElement, RenderFeatures>());
        for (GeoElement ge:layer.getElements()) {
            System.out.println("adding element!");
            renderingFeatures.get(layer).put(ge,new RenderFeatures(visible));
        }
        layers.add(index,layer);
    }



    public void addElementToLayer (GeoElement ge, Layer layer, boolean visible) {

        renderingFeatures.get(layer).put(ge, new RenderFeatures(visible));
        if (!layer.getElements().contains(ge)) {
            layer.addElement(ge);
        }
    }

    public void setGeoElementVisible(GeoElement ge, Layer layer, boolean visible) {
        renderingFeatures.get(layer).get(ge).setVisible(visible);
        if (!layer.getElements().contains(ge)) {
            layer.addElement(ge);
        }
    }

    public Layer initVectorLayers(boolean isVisible, String path, String name, Texture texture, double borderForPolygonsOffset, BoundingBox sceneBox) {
        VectorLayer vectorLayer = new VectorLayer(isVisible, name);
        ArrayList<ShapeFileAttribute> attributes = GeoToolsAdapter.getShapeFileAttributes(path);
        vectorLayer.setAttributes(attributes);
        HashSet<ShapeFileFeature> shapeFileFeatures = GeoToolsAdapter.getPolygons(path, texture, borderForPolygonsOffset,sceneBox);
        vectorLayer.setFeatures(shapeFileFeatures);
        for (ShapeFileFeature shapeFileFeature: shapeFileFeatures) {
            for (ShapeFileAttribute attribute: attributes) {
                shapeFileFeature.addAttribute(attribute);
                attribute.update(shapeFileFeature.getFeature().getAttribute(attribute.getTypeName()));
            }
            for (VectorGeoElement v : shapeFileFeature.getVectorGeoElements()) {
                if (v.getIsPoint() && !vectorLayer.getIsPointLayer()) {
                    vectorLayer.setIsPointLayer(true);
                }

                vectorLayer.addElement(v, isVisible);
                v.setTexture(texture);
            }
        }
        layers.add(vectorLayer);
        return vectorLayer;
    }

    public static Layer initRasterLayers (String tiffName, boolean isVisible, String name, Constants.EXT ext) {
        //System.out.println("INIT RASTER LAYER ___________________________________________");
        RasterLayer rasterLayer = new RasterLayer(isVisible, name);
        RasterGeoElement rasterGeoElement = null;
        if ((ext == Constants.EXT.TIF)) {
            rasterGeoElement = GeoToolsAdapter.getRasterPixelData(tiffName);
        } else if (ext == Constants.EXT.JPG) {
            try {
                File img = new File(tiffName);
                BufferedImage image = ImageIO.read(img);
                ComponentColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8}, false, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
                WritableRaster raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, image.getWidth(), image.getHeight(), 3, null);
                BufferedImage newImage = new BufferedImage(colorModel, raster, false, null);
                DataBufferByte dataBuf = (DataBufferByte) raster.getDataBuffer();
                java.awt.geom.AffineTransform gt = new java.awt.geom.AffineTransform();
                gt.translate(0, image.getHeight());
                gt.scale(1, -1d);
                Graphics2D g = newImage.createGraphics();
                g.drawRenderedImage(image, gt);
                g.dispose();
                Mat imageData = CVUtilities.bufferedImageToMat(newImage);
                rasterGeoElement = new RGBRasterGeoElement(imageData, ShaderManager.RenderedElementTypes.RASTER);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (ext == Constants.EXT.ECW) {
            rasterGeoElement = GeoToolsAdapter.getRasterPixelDataECW(tiffName);
        }
        rasterGeoElement.setName(name);
        rasterGeoElement.setPath(tiffName);
        rasterLayer.addElement(rasterGeoElement);
        rasterGeoElement.getTexture().setName(name);
        return rasterLayer;
    }

    public static RasterGeoElement getRasterGeoElement(String path, String name, Constants.EXT ext) {
        RasterGeoElement rasterGeoElement = null;
        if ((ext == Constants.EXT.TIF)) {
            rasterGeoElement = GeoToolsAdapter.getRasterPixelData(path);
        } else if (ext == Constants.EXT.JPG) {
            try {
                File img = new File(path);
                BufferedImage image = ImageIO.read(img);
                ComponentColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8}, false, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
                WritableRaster raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, image.getWidth(), image.getHeight(), 3, null);
                BufferedImage newImage = new BufferedImage(colorModel, raster, false, null);
                DataBufferByte dataBuf = (DataBufferByte) raster.getDataBuffer();
                java.awt.geom.AffineTransform gt = new java.awt.geom.AffineTransform();
                gt.translate(0, image.getHeight());
                gt.scale(1, -1d);
                Graphics2D g = newImage.createGraphics();
                g.drawRenderedImage(image, gt);
                g.dispose();
                Mat imageData = CVUtilities.bufferedImageToMat(newImage);
                rasterGeoElement = new RGBRasterGeoElement(imageData, ShaderManager.RenderedElementTypes.RASTER);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (ext == Constants.EXT.ECW) {
            rasterGeoElement = GeoToolsAdapter.getRasterPixelDataECW(path);
        }
        rasterGeoElement.setName(name);
        rasterGeoElement.setPath(path);
        rasterGeoElement.getTexture().setName(name);
        return rasterGeoElement;
    }

    public HashMap<Layer,HashMap<GeoElement,RenderFeatures>> getRenderingFeatures() {
        return renderingFeatures;
    }

    public Layer getRasterLayerByName(String name) {
        //System.out.println("get raster layer by name! "+name);
        for (Layer layer: layers) {
            if (layer instanceof RasterLayer) {
                //System.out.println("raster layer name "+((RasterLayer) layer).getName());
                if (((RasterLayer) layer).getName()!=null && ((RasterLayer) layer).getName().equals(name)) {
                    return layer;
                }
            }
        }
        return null;
    }

    public void setLayerVao(Layer layer, int vao ) {
        layerVaos.put(layer, new Integer(vao));
    }

    public Integer getLayerVao(Layer layer) {
        return layerVaos.get(layer);
    }

    // Not the best solution
    public void setBufferVbo(BufferGL bufferGL, int vbo) {
        bufferVbos.put(bufferGL, new Integer(vbo));
    }

    public Integer getBufferVbo(BufferGL bufferGL) {
        return bufferVbos.get(bufferGL);
    }

    public void settextureId(Texture texture, int textureId) {
        textureIds.put(texture, textureId);
    }

    public int getTextureId(Texture texture) {
        if (textureIds.get(texture)!=null) {
            return textureIds.get(texture);
        }
        else {
            System.out.println("texture nuuuuuuull");
            return 0;
        }
    }

    public HashMap<Texture, Integer> getTextureIds() {
        return textureIds;
    }

    public HashMap<BufferGL, Integer> getBufferVbos() {
        return bufferVbos;
    }




}
