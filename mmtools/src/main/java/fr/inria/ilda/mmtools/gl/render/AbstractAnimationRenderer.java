/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.animation.AbstractStagedAnimation;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.gl.Camera;

/**
 * Created by mjlobo on 27/04/16.
 */
public abstract class AbstractAnimationRenderer {

    abstract void render (AbstractStagedAnimation stagedAnimation, GLAutoDrawable drawable, Camera camera, float tic, LayerManager layerManager,
                          RasterGeoElement animationRaster);
}
