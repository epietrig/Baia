/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import fr.inria.ilda.mmtools.animation.AbstractAnimation;

/**
 * Created by mjlobo on 15/03/16.
 */
public class AnimationViewEvent extends Event {
    AbstractAnimation animation;
    String action;

    public AnimationViewEvent(AbstractAnimation animation, String action) {
        this.animation = animation;
        this.action = action;
    }

    public AbstractAnimation getAnimation() {
        return animation;
    }

    public String getAction() {
        return action;
    }
}
