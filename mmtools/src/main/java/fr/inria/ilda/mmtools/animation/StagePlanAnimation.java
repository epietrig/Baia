/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 08/06/16.
 */
public class StagePlanAnimation extends AbstractAnimation {
    Mat initMask;
    Mat endMask;
    Mat planMat;
    Mat maskMat;
    boolean blurBorder;
    int blurWidth;

    public StagePlanAnimation(AbstractAnimation.Type type, float duration) {
        this(type, duration, false, 0);
    }

    public StagePlanAnimation(AbstractAnimation.Type type, float duration, boolean blurBorder, int blurWidth) {
        this.type = type;
        this.duration = duration;
        this.blurBorder = blurBorder;
        this.blurWidth = blurWidth;
    }

    public StagePlanAnimation(AbstractAnimation.Type type, float duration, boolean blurBorder, int blurWidth, Mat maskMat, Mat planMat) {
        this(type, duration, blurBorder, blurWidth);
        this.maskMat = maskMat;
        this.planMat = planMat;
    }

    public StagePlanAnimation (float duration, Mat planMat) {
        this.duration = duration;
        this.planMat = planMat;
    }

    public StagePlanAnimation() {}

    public StagePlanAnimation(Type type) {
        this.type = type;
    }

    public void setInitMask(Mat initMask) {
        this.initMask = initMask;
    }

    public Mat getInitMask() {
        return initMask;
    }

    public void setPlanMat(Mat planMat) {
        this.planMat = planMat;
    }

    public Mat getPlanMat() {
        return planMat;
    }

    public void setMaskMat(Mat maskMat) {
        this.maskMat = maskMat;
    }

    public Mat getMaskMat() {
        return maskMat;
    }

    public boolean isBlurBorder() {
        return blurBorder;
    }

    public int getBlurWidth() {
        return blurWidth;
    }

    public void setBlurBorder(boolean blurBorder) {
        this.blurBorder = blurBorder;
    }

    public void setBlurWidth(int blurWidth) {
        this.blurWidth = blurWidth;
    }

    public boolean isComplete() {
        return true;
    }

    public void writeMasks(String path) {
        if (initMask!=null) {
            CVUtilities.writeImage(initMask, path + "/beforemask.png");
        }
        CVUtilities.writeImage(maskMat, path+"/mask.png");
    }

    public Mat getAnimationPlanToSave () {
        Mat result = planMat.clone();
        List<Mat> bands = new ArrayList<>();
        Core.split(result, bands);
        Mat thirdBand = new Mat(result.size(), CvType.CV_32F);
        bands.add(thirdBand);
        Mat result3bands = new Mat();
        Core.merge(bands, result3bands);
        return result3bands;

    }

    public List<AnimationParameter> getValuesToSave() {
        List<AnimationParameter> valuesToSave = new ArrayList<>();
        valuesToSave.add(new AnimationParameter(Constants.TYPE_TAG, type.name()));
        if (initMask!=null) {
            valuesToSave.add(new AnimationParameter(Constants.INIT_MASK_TAG, Constants.BEFORE_MASK_FILE_NAME));
        }
        valuesToSave.add(new AnimationParameter(Constants.ANIMATION_PLAN_TAG, Constants.PLAN_FILE_NAME));
        valuesToSave.add(new AnimationParameter(Constants.MASK_TAG, Constants.MASK_FILE_NAME));
        valuesToSave.add(new AnimationParameter(Constants.BLUR_BORDER_TAG, "" + blurWidth));
        return valuesToSave;
    }

    public List<String> getValuesToRetrieve() {
        java.util.List<String> valuestoRetrieve = new ArrayList<>();
        //valuestoRetrieve.add(Constants.TYPE_TAG);
        valuestoRetrieve.add(Constants.MASK_TAG);
        valuestoRetrieve.add(Constants.INIT_MASK_TAG);
        valuestoRetrieve.add(Constants.ANIMATION_PLAN_TAG);
        valuestoRetrieve.add(Constants.BLUR_BORDER_TAG);
        return valuestoRetrieve;
    }

    public void setEndMask(Mat endMask) {
        this.endMask = endMask;
    }

    public Mat getEndMask() {
        return endMask;
    }



}
