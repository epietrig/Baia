/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

/**
 * Created by mjlobo on 11/04/2017.
 */
public class CanvasEventBasic extends CanvasEvent {

    public CanvasEventBasic(String event) {
        super(event);
    }

}
