/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.processing.Operations;

import javax.media.jai.Interpolation;
import javax.media.jai.RenderedOp;
import javax.vecmath.Vector3f;
import java.awt.image.*;

/**
 * Created by mjlobo on 21/07/15.
 */
public class MNTRasterGeoElement extends RasterGeoElement {

    float[] mntData;

    DataBuffer buffer;
    double noValue;
    Vector3f minColor = new Vector3f(0.0f,1.0f,0.0f);
    Vector3f maxColor = new Vector3f(1.0f,0.0f,0.0f);
    String interpolationMethod;
    Texture originalTexture;
    float maxValue;
    float minValue;

    public MNTRasterGeoElement(GridCoverage2D coverage) {
        super(coverage);
        javax.media.jai.Interpolation interp = Interpolation.getInstance(Interpolation.INTERP_BICUBIC);
        GridCoverage2D covinterpol =  (GridCoverage2D) Operations.DEFAULT.interpolate(coverage, interp);
        RenderedImage ri = covinterpol.getRenderedImage();
        getTerrainPixelData(ri);
        calculateColorValues();
        interpolationMethod = Constants.LINEAR_INTERPOLATION;


    }

    public MNTRasterGeoElement(GridCoverage2D coverage, double noValue, Vector3f minColor, Vector3f maxColor) {
        super(coverage);
        this.noValue = noValue;
        javax.media.jai.Interpolation interp = Interpolation.getInstance(Interpolation.INTERP_BICUBIC);
        GridCoverage2D covinterpol =  (GridCoverage2D) Operations.DEFAULT.interpolate(coverage, interp);
        RenderedImage ri = covinterpol.getRenderedImage();
        //System.out.println("interpolated image buffer type "+ri.getData().getDataBuffer().getClass());
        getTerrainPixelData(ri);
        calculateColorValues(noValue);
        this.maxColor = maxColor;
        this.minColor = minColor;


    }

    public MNTRasterGeoElement(GridCoverage2D coverage, double noValue) {
        super(coverage);
        this.noValue = noValue;
        javax.media.jai.Interpolation interp = Interpolation.getInstance(Interpolation.INTERP_BICUBIC);
        GridCoverage2D covinterpol =  (GridCoverage2D) Operations.DEFAULT.interpolate(coverage, interp);
        RenderedImage ri = covinterpol.getRenderedImage();
        System.out.println("interpolated image buffer type "+ri.getData().getDataBuffer().getClass());
        getTerrainPixelData(ri);
        calculateColorValues(noValue);
        interpolationMethod = Constants.LINEAR_INTERPOLATION;
        mat = CVUtilities.matFromFloatArray(mntData, imageWidth, imageHeight);
        getTexture().setData(mat.clone());



    }

    public DataBuffer getTerrainPixelData(RenderedImage img) {

        DataBufferDouble dataBuf = null;
        RenderedOp ri = (RenderedOp)img;
        BufferedImage newImage = ri.getAsBufferedImage();
        buffer = ri.getData().getDataBuffer();
        //System.out.println("-----BUFFER "+buffer);
        //System.out.println("RGB: "+newImage.getRGB(100,100));


        return buffer;
    }




    public void calculateColorValues () {
        calculateColorValues(Double.NaN);
    }

    public void calculateColorValues (double noValue) {
        if (buffer instanceof DataBufferDouble) {
            double max= -Double.MAX_VALUE;
            double min = Double.MAX_VALUE;
            double[] imgRGBA = ((DataBufferDouble)buffer).getData();
            for (int i=0; i<imgRGBA.length; i++) {
                if (imgRGBA[i] > max && imgRGBA[i]!=noValue) {
                    max = imgRGBA[i];
                }
                if (imgRGBA[i]<min && imgRGBA[i]!= noValue) {
                    min = imgRGBA[i];
                }
            }

            float[] floatRGBA = new float[imgRGBA.length];
            for (int i=0; i<imgRGBA.length; i++) {
                floatRGBA[i] = (float)imgRGBA[i];
            }
            minValue = (float)min;
            maxValue = (float)max;
            mntData =floatRGBA;
        }
        else if (buffer instanceof DataBufferShort) {
            //System.out.println("DATA BUFFER SHORT");
            System.out.println("no value "+(float)noValue);
            short max= -Short.MAX_VALUE;
            short min = Short.MAX_VALUE;
            short[] imgRGBA = ((DataBufferShort)buffer).getData();
            for (int i=0; i<imgRGBA.length; i++) {
                if (imgRGBA[i]>max && imgRGBA[i]!= (float)noValue) {
                    max = imgRGBA[i];
                }
                if (imgRGBA[i]<min && imgRGBA[i]!= (float)noValue) {
                    min = imgRGBA[i];
                }
            }
            float[] floatRGBA = new float[imgRGBA.length];
            for (int i=0; i<imgRGBA.length; i++) {
                floatRGBA[i] = (float)imgRGBA[i];
                //System.out.println("image RGBA "+floatRGBA[i]);
            }
            mntData =floatRGBA;
            mntData = flipImageVertically(floatRGBA);
            minValue = min;
            maxValue = max;
            System.out.println("min value "+minValue);
            System.out.println("max value "+maxValue);
        }
        else if (buffer instanceof DataBufferFloat) {
            float[] imgRGBA = ((DataBufferFloat) buffer).getData();
            float[] newImgRGBA = imgRGBA.clone();
            float max = -Float.MAX_VALUE;
            float min = Float.MAX_VALUE;
            for (int i=0; i<imgRGBA.length; i++) {
                if (imgRGBA[i]>max && imgRGBA[i]!= (float)noValue) {
                    //System.out.println("imgRGBA[i]"+imgRGBA[i]);
                    max = imgRGBA[i];
                }
                if (imgRGBA[i]<min && imgRGBA[i]!= (float)noValue) {
                    min = imgRGBA[i];
                }
            }

            minValue = min;
            maxValue = max;
            mntData = flipImageVertically(imgRGBA);
        }

    }

    public float[] flipImageVertically (float[] array) {
        int rows = imageHeight;
        int columns = imageWidth;
        float[] flipped = new float [array.length];
        int countrows = 0;
        int countColumns = 0;
        for (int i=0; i<rows; i++) {
            for (int j=0; j<columns ; j++) {
                flipped[i * columns + j] = array[(rows-i-1)*columns+j];
            }
        }
        return flipped;
    }

    public float[] getMntData() {
        return mntData;
    }

    public Vector3f getMinColor() {
        return minColor;
    }

    public Vector3f getMaxColor() {
        return maxColor;
    }

    public void setMinColor(Vector3f minColor) {
        this.minColor = minColor;
    }

    public void setMaxColor (Vector3f maxColor) {
        this.maxColor = maxColor;
    }

    public String getInterpolationMethod() {return interpolationMethod;}

    public void setOriginalTexture(Texture originalTexture) {
        this.originalTexture = originalTexture;
    }

    public Texture getOriginalTexture() {
        return originalTexture;
    }

    public void setInterpolationMethod(String interpolationMethod) {
        this.interpolationMethod = interpolationMethod;
    }

    public float getMinValue() {
        return minValue;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public double getNoValue() {
        return noValue;
    }



}
