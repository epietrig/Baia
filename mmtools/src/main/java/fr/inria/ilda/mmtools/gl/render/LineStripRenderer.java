/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3f;

/**
 * Created by mjlobo on 25/04/16.
 */
public class LineStripRenderer extends AbstractLineStripRenderer {

    ShaderProgram program;
    public LineStripRenderer(ShaderProgram program) {
        //super(program);
        this.program = program;
    }

    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, LayerManager layerManager) {
        render(shapeGL, drawable, camera,shapeGL.getColor(), layerManager);
    }

    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Vector3f color, LayerManager layerManager) {
        //System.out.println("render line! ");
        //shapeGL.printCoordinates();
        GL4 gl = (GL4) drawable.getGL();
        LineStripGL lineStrip = (LineStripGL) shapeGL;
        ShaderProgram linesStrip = program;
        int vertexAttributeId = linesStrip.getAttribute(ShaderConstants.POSITION);
        gl.glUseProgram(linesStrip.getProgramId());
        gl.glUniformMatrix4fv(linesStrip.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
        gl.glUniformMatrix4fv(linesStrip.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);

        BufferGL vertexBuffer = lineStrip.getBuffer(BufferGL.Type.VERTEX);
        BufferGL indexBuffer = lineStrip.getBuffer(BufferGL.Type.INDEX);
        if (vertexBuffer != null && layerManager.getBufferVbo(vertexBuffer) != null && layerManager.getBufferVbo(indexBuffer) != null) {
            gl.glEnableVertexAttribArray(vertexAttributeId);
            gl.glBindBuffer(vertexBuffer.getTarget(), layerManager.getBufferVbo(vertexBuffer));
            gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
            gl.glUniform2f(linesStrip.getUniform("offset"), (float) lineStrip.getOffset()[0], (float) lineStrip.getOffset()[1]);
            gl.glUniform1f(linesStrip.getUniform("THICKNESS"), lineStrip.getLineWidth());
            gl.glUniform2f(linesStrip.getUniform("WIN_SCALE"), camera.getCanvasWidth(), camera.getCanvasHeight());
            gl.glUniform3f(linesStrip.getUniform("color"), color.x, color.y, color.z);
            gl.glUniform1f(linesStrip.getUniform("alpha"), lineStrip.getAlpha());

            gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
            gl.glDrawElements(gl.GL_LINE_STRIP_ADJACENCY, lineStrip.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
        }
    }

}
