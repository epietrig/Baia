/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui;


import fr.inria.ilda.mmtools.stateMachine.ComboBoxEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

/**
 * Created by mjlobo on 30/09/15.
 */
public class StringComboPanel extends JPanel {
    JLabel label;
    JComboBox comboBox;
    String text;
    java.util.List<String> comboValues;
    boolean fireActionEventComboBox = true;
    DefaultComboBoxModel<String> values;

    public StringComboPanel(String text, java.util.List<String> comboValues) {
        this.text = text;
        this.comboValues = comboValues;
        Vector<String> vectorValues = new Vector<>();
        vectorValues.addAll(comboValues);
        values = new DefaultComboBoxModel<String>(vectorValues);
        initUI();
    }

    public StringComboPanel (String text, java.util.List<String> comboValues, String name) {
        this(text,comboValues);
        comboBox.setName(name);
    }



    void initUI() {

        setLayout(new FlowLayout(FlowLayout.LEFT));
        this.setBorder(BorderFactory.createEmptyBorder(2,5,2,2));
        label = new JLabel(text);
        add(label);
        String[] comboArray = new String[comboValues.size()];
        for (int i=0; i<comboArray.length; i++) {
            comboArray[i] = comboValues.get(i);
        }
        comboBox = new JComboBox(values);
        comboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        comboBox.setMaximumSize(new Dimension(((int)(300-label.getPreferredSize().getWidth())), 100));
        add(comboBox);

    }

    public void addListeners(final StateMachine stateMachine) {
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fireActionEventComboBox) {
                    //System.out.println("new selected item "+ comboBox.getSelectedItem());
                    stateMachine.handleEvent(new ComboBoxEvent((String) comboBox.getSelectedItem(), comboBox));
                }
            }
        });

    }

    public void update(String text) {
        //comboBox.setSelectedIndex(comboValues.indexOf(text));
        fireActionEventComboBox = false;
        comboBox.setSelectedItem(text);
        fireActionEventComboBox = true;
    }

    public String getStringSelectedItem() {
        return (String)comboBox.getSelectedItem();
    }

    public JComboBox getComboBox() {
        return comboBox;
    }

    public void addItem(String item) {
        fireActionEventComboBox = false;
        values.addElement(item);
        revalidate();
        repaint();
        fireActionEventComboBox = true;
    }

    public void removeAllItems() {
        values.removeAllElements();
    }

}
