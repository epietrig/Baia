/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

/**
 * Created by mjlobo on 15/03/16.
 */
public class SemanticAnimationEvent extends Event {
    String semanticMapName;

    public SemanticAnimationEvent(String semanticMapName) {
        this.semanticMapName = semanticMapName;
    }

    public String getSemanticMapName() {
        return semanticMapName;
    }
}
