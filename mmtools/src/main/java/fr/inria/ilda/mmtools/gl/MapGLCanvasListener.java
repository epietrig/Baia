/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

/**
 * Created by mjlobo on 15/06/16.
 */
public interface MapGLCanvasListener {
    public void animationStep(float accumulatedTime, MapGLCanvas canvas);

    public void animationPaused(float accumulatedTime);

    public void finishedLoadingRasters(MapGLCanvas canvas);

    public void finishedExportingMovie();
}
