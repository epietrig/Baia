/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.morphingAlgorithm;

/**
 * Created by mjlobo on 06/02/2017.
 */
public class NotEnoughCornersExeption extends Exception {
    int nContour;
    public NotEnoughCornersExeption(int nContour) {
        this.nContour = nContour;
    }

    public String getMessage(){
        if (nContour == 1) {
            return "Please add more points to the first mask";
        }
        else if (nContour == 2){
            return "Please add more points to the second mask";
        }
        else {
            return "Please add more points to both masks";
        }
    }
}
