/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.filters;

import fr.inria.ilda.mmtools.gl.FrameBufferObject;
import fr.inria.ilda.mmtools.gl.PolygonGL;
import fr.inria.ilda.mmtools.gl.ShaderProgram;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 22/07/15.
 */
public class TextureFilter {
    PolygonGL quad;
    private int vaoID;
    FrameBufferObject frameBufferObject;
    ShaderProgram drawShader;

    public TextureFilter (int vaoID, FrameBufferObject frameBufferObject, ShaderProgram drawShader) {
        this.frameBufferObject = frameBufferObject;
        this.vaoID = vaoID;
        this.drawShader = drawShader;
    }

    public void calculateQuad (double[] lowerCorner, double width, double height) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{lowerCorner[0] + width, lowerCorner[1] + height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]+height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]});
        coordinates.add(new double[]{lowerCorner[0]+width, lowerCorner[1]});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad = new PolygonGL(coordinates, indexes, frameBufferObject.getTexture());


    }
    

    public void refreshQuadCoordinates (double[] lowerCorner, double width, double height) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{lowerCorner[0] + width, lowerCorner[1] + height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]+height});
        coordinates.add(new double[]{lowerCorner[0], lowerCorner[1]});
        coordinates.add(new double[]{lowerCorner[0]+width, lowerCorner[1]});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad.setCoordinates(coordinates);
        quad.setIndexes(indexes);
        quad.refreshCoordinates();
        quad.updateBuffers();



    }

    public PolygonGL getQuad() {
        return quad;
    }

    public FrameBufferObject getFrameBufferObject() {
        return frameBufferObject;
    }

    public int getVaoID() {
        return vaoID;
    }
}
