/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

import fr.inria.ilda.mmtools.gl.Texture;

import java.util.List;


/**
 * Created by mjlobo on 04/03/16.
 */
public class CanvasNewTextureMatListEvent extends CanvasEvent{
    List<Texture> textureList;

    public CanvasNewTextureMatListEvent(List<Texture> textureList, String event) {
        super(event);
        this.textureList = textureList;

    }

    public List<Texture> getTextureList() {
        return  textureList;
    }



}
