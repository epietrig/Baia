/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.newt.awt.NewtCanvasAWT;
import com.jogamp.opengl.util.PMVMatrix;
import org.opencv.core.Mat;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.nio.FloatBuffer;

/**
 * Created by mjlobo on 20/04/16.
 */
public class Camera {
    double top = 3.5;
    double bottom = -3.5;
    double right = 5.0;
    double left = -5.0f;
    float zNear = 2.5f;
    float zFar = 100.0f;
    Vector3d eye = new Vector3d(0.0,0.0,5.0);
    Vector3d center=new Vector3d(0.0,0.0,0.0);
    Vector3d up = new Vector3d(0.0,1.0,0.0);
    double zoomFactor = 1.01;
    double camera_pixels_per_unit;
    double [] pixels_per_unit;

    PMVMatrix pmvMatrix;
    NewtCanvasAWT canvas;
    boolean changed;

    Matrix4f projectionMat;
    Matrix4f mvMat;
    float[] viewMatrix;
    float[] projMatrix;


    public Camera (NewtCanvasAWT canvas) {

        pmvMatrix = new PMVMatrix();
        this.canvas = canvas;
        camera_pixels_per_unit = 1.0;
        pixels_per_unit = new double[]{1,1};
    }

    public float[] lookAt() {
        pmvMatrix.glMatrixMode(PMVMatrix.GL_MODELVIEW);
        pmvMatrix.glLoadIdentity();
        pmvMatrix.gluLookAt((float)(eye.x), (float)(eye.y), (float)(eye.z), (float)(center.x), (float)(center.y), (float)(center.z), (float)(up.x), (float)(up.y), (float)(up.z));
        pmvMatrix.update();
        FloatBuffer v_matrixBuffer = pmvMatrix.glGetMvMatrixf();
        float [] viewMatrix = new float[16];
        int index = 0;
        for (int i=v_matrixBuffer.position(); i<v_matrixBuffer.limit();i++){
            viewMatrix[index]=v_matrixBuffer.get(i);
            index++;
        }
        this.viewMatrix = viewMatrix;
        return viewMatrix;
    }

    public float[] lookAt(Vector3d eye, Vector3d center) {
        PMVMatrix temporaryPmvMatrix = new PMVMatrix();
        temporaryPmvMatrix.glMatrixMode(PMVMatrix.GL_MODELVIEW);
        temporaryPmvMatrix.glLoadIdentity();
        temporaryPmvMatrix.gluLookAt((float)(eye.x), (float)(eye.y), (float)(eye.z), (float)(center.x), (float)(center.y), (float)(center.z), (float)(up.x), (float)(up.y), (float)(up.z));
        temporaryPmvMatrix.update();
        FloatBuffer v_matrixBuffer = temporaryPmvMatrix.glGetMvMatrixf();
        float [] viewMatrix = new float[16];
        int index = 0;
        for (int i=v_matrixBuffer.position(); i<v_matrixBuffer.limit();i++){
            viewMatrix[index]=v_matrixBuffer.get(i);
            index++;
        }
        return viewMatrix;
    }

    public float[] getProjectionMatrix () {
        pmvMatrix.glMatrixMode(PMVMatrix.GL_PROJECTION);
        pmvMatrix.glLoadIdentity();
        pmvMatrix.glOrthof((float)left,(float)right,(float)bottom,(float)top,zNear,zFar);
        pmvMatrix.update();
        FloatBuffer proj_matrixBuffer = pmvMatrix.glGetPMatrixf();
        float [] projMatrix = new float[16];
        int index = 0;
        for (int i=proj_matrixBuffer.position(); i<proj_matrixBuffer.limit();i++){
            //System.out.println("proj_matrixBuffer"+proj_matrixBuffer.get(i));
            projMatrix[index]=proj_matrixBuffer.get(i);
            //System.out.println("projMatrix"+projMatrix[index]);
            index++;
        }
        this.projMatrix = projMatrix;
        return projMatrix;
    }

    public float[] getProjectionMatrix(double left, double right, double bottom, double top) {
        PMVMatrix temporaryPmvMatrix = new PMVMatrix();
        temporaryPmvMatrix.glMatrixMode(PMVMatrix.GL_PROJECTION);
        temporaryPmvMatrix.glLoadIdentity();
        temporaryPmvMatrix.glOrthof((float) left, (float) right, (float) bottom, (float) top, zNear, zFar);
        temporaryPmvMatrix.update();
        FloatBuffer proj_matrixBuffer = temporaryPmvMatrix.glGetPMatrixf();
        float [] projMatrix = new float[16];
        int index = 0;
        for (int i=proj_matrixBuffer.position(); i<proj_matrixBuffer.limit();i++){
            projMatrix[index]=proj_matrixBuffer.get(i);
            index++;
        }

        return projMatrix;
    }

    public void calculateMatrices () {
        if (projMatrix == null) {
            getProjectionMatrix();
        }
        if (viewMatrix == null) {
            lookAt();
        }
        projectionMat = new Matrix4f(projMatrix);
        projectionMat.transpose();
        mvMat = new Matrix4f(viewMatrix);
        mvMat.transpose();
        projectionMat.invert();
        mvMat.mul(-1);
    }



    public double[] screenToWorld(int x, int y) {
        return screenToWorldMatrixes(x,y);

    }

    public double[] screenToWorldMatrixes(int x, int y) {
        double xworld = (2.0 * x/canvas.getWidth()- 1)*projectionMat.getElement(0,0)+projectionMat.getElement(0,3)+mvMat.getElement(0,3);
        double yworld = (-2.0 * y/canvas.getHeight() + 1)*projectionMat.getElement(1,1)+projectionMat.getElement(1,3)+mvMat.getElement(1,3);
        return new double[] {xworld, yworld};

    }

    public double[] worldToScreen(double[] worldCoord) {
        double xScreen = (((worldCoord[0]-mvMat.getElement(0,3)-projectionMat.getElement(0,3))/projectionMat.getElement(0,0))+1)*canvas.getWidth()/2;
        double yScreen = ((((worldCoord[1]-mvMat.getElement(1,3)-projectionMat.getElement(1,3))/projectionMat.getElement(1,1))-1)*canvas.getHeight()/-2);
        return new double[] {xScreen, yScreen};
    }

    public void zoomIn(int x, int y) {

        double[] zoomCenter = screenToWorld(x, y);
        zoomInToCenter(zoomCenter);


    }

    public void zoomInToCenter(double[] zoomCenter) {
        double [] screenCoords = worldToScreen(zoomCenter);
        double x = screenCoords[0];
        double y = screenCoords[1];
        eye.set(zoomCenter[0], zoomCenter[1], eye.z);
        center.set(eye.x, eye.y, center.z);

        camera_pixels_per_unit = camera_pixels_per_unit*zoomFactor;

        pixels_per_unit [0] *= zoomFactor;
        pixels_per_unit [1] *= zoomFactor;

        left = -x/pixels_per_unit[0];
        right = (canvas.getWidth()-x)/pixels_per_unit[0];
        top = y/pixels_per_unit[1];
        bottom = -(canvas.getHeight()-y)/pixels_per_unit[1];
        changed = true;
        calculateMatrices();

    }

    public void zoomOut(int x, int y) {
        double[] zoomCenter = screenToWorld(x, y);
        zoumOutToCenter(zoomCenter);

    }

    public void zoumOutToCenter(double[] zoomCenter) {
        double [] screenCoords = worldToScreen(zoomCenter);
        double x = screenCoords[0];
        double y = screenCoords[1];
        eye.set(zoomCenter[0], zoomCenter[1], eye.z);
        center.set(eye.x, eye.y, center.z);

        camera_pixels_per_unit = camera_pixels_per_unit*zoomFactor;

        pixels_per_unit [0] /= zoomFactor;
        pixels_per_unit [1] /= zoomFactor;

        left = -x/pixels_per_unit[0];
        right = (canvas.getWidth()-x)/pixels_per_unit[0];
        top = y/pixels_per_unit[1];
        bottom = -(canvas.getHeight()-y)/pixels_per_unit[1];
        changed = true;
        calculateMatrices();
    }



    public void moveCamera (int dx, int dy) {
        double dxWorld = dx/pixels_per_unit[0];
        double dyWorld = dy/pixels_per_unit[1];
        eye.set((float)(eye.x + dxWorld), (float)(eye.y + dyWorld), eye.z);
        center.set (eye.x, eye.y, center.z);
        changed = true;
        calculateMatrices();
    }

    public void moveCamera (float dx, float dy) {
        eye.set(eye.x + dx, eye.y + dy, eye.z);
        center.set (eye.x, eye.y, center.z);
    }

    public double[] getLowerCorner() {
        double lower_corner_x = eye.x+left;
        double lower_corner_y = eye.y + bottom;
        return new double[] {lower_corner_x, lower_corner_y};
    }

    public Matrix4f getMatrixFromFloatArray(float[] matrixArray) {
        return new Matrix4f(matrixArray[0], matrixArray[4], matrixArray[8], matrixArray[12],
                matrixArray[1], matrixArray[5], matrixArray[9], matrixArray[13],
                matrixArray[2], matrixArray[6], matrixArray[10], matrixArray[14],
                matrixArray[3], matrixArray[7], matrixArray[11], matrixArray[15]);
    }

    public void updateParameters(boolean isRetina, int width, int height, int[] border) {
        if(isRetina) {
            setTop(border[0]+(float)(height / (getPixels_per_unit()[1]*2)));
            setRight(border[1]+(float)(width / (getPixels_per_unit()[0]*2)));
            setBottom(-border[2]-(float)(height / (getPixels_per_unit()[1]*2)));
            setLeft(-border[3]-(float)(width/ (getPixels_per_unit()[0]*2)));

        }
        else {

            setTop(border[0]+(float)(height / (2*getPixels_per_unit()[1])));
            setRight(border[1]+(float)(width /  (2*getPixels_per_unit()[0])));
            setBottom(-border[2]-(float)(height / (2 * getPixels_per_unit()[1])));
            setLeft(-border[3]-(float)(width/ (2 * getPixels_per_unit()[0])));

        }
        lookAt();
        getProjectionMatrix();
        calculateMatrices();

    }

    public double getCamera_pixels_per_unit() {
        return camera_pixels_per_unit;
    }

    public void setCamera_pixels_per_unit(double camera_pixels_per_unit) {
        this.camera_pixels_per_unit = camera_pixels_per_unit;
    }

    public Vector3d getEye() {
        return eye;
    }

    public Vector3d getCenter() {return center;}

    public double getRight() {
        return right;
    }

    public double getLeft() {
        return left;
    }

    public double getTop() {
        return top;
    }

    public double getBottom() {
        return bottom;
    }

    public void setPixels_per_unit(double[] pixels_per_unit) {
        this.pixels_per_unit = pixels_per_unit;
    }

    public double[] getPixels_per_unit(){
        return pixels_per_unit;
    }

    public double getScreenRealWidth() {
        return right - left;
    }

    public double getScreenRealHeight() {
        return top - bottom;
    }

    public boolean getChanged() {
        return changed;
    }

    public void setChanged (boolean changed) {
        this.changed = changed;
    }

    public void setEye (Vector3d eye) {
        this.eye = eye;
    }

    public void setCenter (Vector3d center) {
        this.center = center;
    }

    public void setUp(Vector3d up) {
        this.up = up;
    }

    public Vector3d getUp() {
        return up;
    }

    public void setTop(double top) {
        this.top = top;
    }

    public void setBottom (double bottom) {
        this.bottom = bottom;
    }

    public void setRight (double right) {
        //System.out.println("Setting right "+right);
        this.right = right;
    }

    public void setLeft(double left) {
        this.left = left;
    }

    public int getCanvasWidth() {
        return canvas.getWidth();
    }

    public int getCanvasHeight() {
        return canvas.getHeight();
    }

    public void printCameraParameters() {
        System.out.println("------------------- Camera Parameters --------------");
        System.out.println("top "+top);
        System.out.println("bottom "+bottom);
        System.out.println("rigth "+right);
        System.out.println("left "+left);
        System.out.println("pixels per unit array "+pixels_per_unit[0]+ ": "+pixels_per_unit[1]);
    }


}
