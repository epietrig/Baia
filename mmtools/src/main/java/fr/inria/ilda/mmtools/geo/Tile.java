/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.gl.PolygonGL;
import fr.inria.ilda.mmtools.gl.Texture;
import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 04/12/15.
 */
public class Tile {
    double x;
    double y;
    int width;
    int height;
    double realWidth;
    double realHeight;
    double pixels_per_unit;
    Mat mat;
    PolygonGL polygon;

    public Tile (double x, double y, int width, int height, Mat mat, double realWidth, double realHeight, double pixels_per_unit) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.mat = mat;
        this.realHeight = realHeight;
        this.realWidth = realWidth;
        this.pixels_per_unit = pixels_per_unit;
        calculatePolygon();
//        System.out.println("Tile x "+x);
//        System.out.println("Tile y "+y);
//        System.out.println("Tile width "+width);
//        System.out.println("Tile Height "+height);
//        System.out.println()
    }

    private void calculatePolygon() {
        List<double[]> coordinates = new ArrayList<>();
        ArrayList<float[]> texCoords = new ArrayList<>();
        coordinates.add(new double[] {x+realWidth/2,y+realHeight/2});
        texCoords.add(new float[] {1.0f,1.0f});
        coordinates.add(new double[] {x-realWidth/2,y+realHeight/2});
        texCoords.add(new float[] {0.0f,1.0f});
        coordinates.add(new double[]{x-realWidth/2,y-realHeight/2});
        texCoords.add(new float[] {0.0f,0.0f});
        coordinates.add(new double[] {x+realWidth/2,y-realHeight/2});
        texCoords.add(new float[] {1.0f,0.0f});

        ArrayList<Integer>indexesList = new ArrayList<>();
        indexesList.add(0);
        indexesList.add(1);
        indexesList.add(2);
        indexesList.add(0);
        indexesList.add(2);
        indexesList.add(3);

        Texture texture = new Texture(this);

        polygon = new PolygonGL(coordinates,indexesList,texture);
        polygon.setTexCoords(texCoords);
        polygon.printCoordinates();

    }

    public double getPixels_per_unit() {
        return pixels_per_unit;
    }

    public double getX() {
        return x;

    }

    public double getY() {
        return y;
    }

    public double getRealWidth(){
        return realWidth;
    }

    public double getRealHeight() {
        return realHeight;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public byte[] getTileData() {
        return CVUtilities.matToByteArrayBGR(mat);
    }

    public PolygonGL getPolygon() {
        return polygon;
    }


}
