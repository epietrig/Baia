/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import fr.inria.ilda.mmtools.ui.queryWidgets.TextQuery;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Created by mjlobo on 25/06/15.
 */
public class TextQueryEvent extends Event{
    TextQuery target;
    LinkedList<String> values;
    String typeName;

    public TextQueryEvent(TextQuery target,LinkedList<String> values, String typeName) {
        this.target = target;
        this.values = values;
        this.typeName = typeName;
    }

    private static TextQueryEventListener listener = new TextQueryEventListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof TextQuery) {
            listener.attachTo( stateMachine, (TextQuery)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof TextQuery) {
            listener.detachFrom( stateMachine, (TextQuery)target );
        }
    }

    public TextQuery getTarget() {
        return target;
    }

    public LinkedList<String> getValues() {return values;}

    public String getTypeName() {return typeName;}


}

class TextQueryEventListener implements ChangeListener {

    private HashMap<Component, HashSet<StateMachine>> listeners;
    public TextQueryEventListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, TextQuery target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.getCheckList().addChangeListener(this);
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, TextQuery target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.getCheckList().removeChangeListener(this);
                target.getCheckList().removeChangeListener(this);
                //System.out.println("Removed MouseMoveListener");
            }
        }
    }


    @Override
    public void stateChanged(ChangeEvent event) {
        TextQuery target = (TextQuery)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            TextQueryEvent customEvent = new TextQueryEvent(target, target.getCheckList().getSelectedValues(), target.getTypeName());
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        }
    }
}
