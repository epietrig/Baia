/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.cv;

/**
 * Created by mjlobo on 15/04/16.
 */
public class SamplePixel {
    int row;
    int col;
    double[] values;
    double stdDev;

    public SamplePixel (int row, int col, double[] values) {
        this.row = row;
        this.col = col;
        this.values = values;
    }

    public SamplePixel (int row, int col, double[] values, double stdDev) {
        this.row = row;
        this.col = col;
        this.values = values;
        this.stdDev = stdDev;
    }

    public void setStdDev (double stdDev) {
        this.stdDev = stdDev;
    }

    public double[] getValues() {
        return values;
    }

    public double getStdDev() {
        return stdDev;
    }
}
