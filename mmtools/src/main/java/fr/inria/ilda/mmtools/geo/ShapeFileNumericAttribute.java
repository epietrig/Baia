/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

/**
 * Created by mjlobo on 22/06/15.
 */

import java.util.LinkedList;
import org.opengis.feature.type.Name;

public class ShapeFileNumericAttribute extends ShapeFileAttribute {

    protected LinkedList<Double> values;
    protected double minValue;
    protected double maxValue;

    public ShapeFileNumericAttribute(Name typeName, String localName) {
        super(typeName, localName);
        this.values = new LinkedList<Double>();
    }

    @Override
    public void update(Object o) {
        double d = 0;
        if (o!=null) {
            try {
                d = Double.parseDouble(o.toString());
            } catch (NumberFormatException nfe) {
                return;
            }
            if (!values.contains(d)) {
                values.add(d);
            }
        }
    }


    public LinkedList<Double> getValues() {
        return values;
    }

    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void calculateMinMaxValues() {
        double max = Double.MIN_VALUE;
        double min = Double.MAX_VALUE;
        for (Double d: values) {
            if (d>max) {
                maxValue = d;
            }
            if (d<min) {
                minValue = d;
            }
        }
    }

}
