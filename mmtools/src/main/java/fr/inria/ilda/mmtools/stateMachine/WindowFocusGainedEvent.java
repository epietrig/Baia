/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

/**
 * Created by mjlobo on 24/06/15.
 */

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 24/06/15.
 */
public class WindowFocusGainedEvent extends Event {
    Component target;

    public WindowFocusGainedEvent(Window target) {
        super();
        source = this.target = target;
    }

    private static WindowFocusGainedListener listener = new WindowFocusGainedListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof Window ) {
            listener.attachTo( stateMachine, (Window)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof Window ) {
            listener.detachFrom( stateMachine, (Window)target );
        }
    }

    public java.awt.Component getTarget() {
        return target;
    }


}

class WindowFocusGainedListener extends WindowAdapter {

    private HashMap<Component, HashSet<StateMachine>> listeners;

    public WindowFocusGainedListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, Window target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addWindowFocusListener(this);
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, Window target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeWindowFocusListener(this);

            }
        }
    }

    @Override
    public void windowGainedFocus(WindowEvent event) {
        Window target = (Window)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            WindowFocusGainedEvent customEvent = new WindowFocusGainedEvent(target);
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        } else {
            java.awt.Component parent = target.getParent();
            if (parent != null) {
                parent.dispatchEvent(event);
            }
        }

    }



}