/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.HashSet;


/**
 * Created by mjlobo on 05/06/15.
 */
public class KeyPressedEvent extends Event{
    Component target;
    char keyChar;
    int keyCode;


    public KeyPressedEvent (char keyChar, int keyCode)
    {
        this.keyChar = keyChar;
        this.keyCode = keyCode;
    }

    public KeyPressedEvent (Component target, char keyChar, int keyCode) {
        super();
        source = this.target = target;
        this.keyChar = keyChar;
        this.keyCode = keyCode;
    }

    private static KeyPressedListener listener = new KeyPressedListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.attachTo( stateMachine, (java.awt.Component)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.detachFrom( stateMachine, (java.awt.Component)target );
        }
    }

    public java.awt.Component getTarget() {
        return target;
    }

    public char getKeyChar() {
        return keyChar;
    }

    public int getKeyCode() {return keyCode;}

}

class KeyPressedListener extends KeyAdapter {

    private HashMap<Component, HashSet<StateMachine>> listeners;

    public KeyPressedListener() {
        listeners = new HashMap<Component, HashSet<StateMachine>>();
    }

    public void attachTo( StateMachine stateMachine, java.awt.Component target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addKeyListener(this);
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, java.awt.Component target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeKeyListener(this);

            }
        }
    }

    public void keyPressed (KeyEvent event) {
        //System.out.println("key pressed!");
        Component target = (Component)event.getSource();
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            KeyPressedEvent customEvent = new KeyPressedEvent(target, event.getKeyChar(), event.getKeyCode());
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        } else {
            java.awt.Component parent = target.getParent();
            if (parent != null) {
                parent.dispatchEvent(event);
            }
        }

    }


}
