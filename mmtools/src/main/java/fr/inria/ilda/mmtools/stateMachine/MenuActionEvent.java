/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by mjlobo on 07/07/15.
 */
public class MenuActionEvent extends Event{
    JMenuItem target;
    String text;

    public MenuActionEvent (JMenuItem target, String text) {
        super();
        source = this.target = target;
        this.text = text;
    }

    private static MenuActionListener listener = new MenuActionListener();

    public static void attachTo( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.attachTo( stateMachine, (JMenuItem)target );
        }
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
        if ( target instanceof java.awt.Component ) {
            listener.detachFrom( stateMachine, (JMenuItem)target );
        }
    }

    public java.awt.Component getTarget() {
        return target;
    }

    public String getText() {return text;}

}

class MenuActionListener implements ActionListener {

    private HashMap<Component, HashSet<StateMachine>> listeners;

    public MenuActionListener () {listeners = new HashMap<>();}

    public void attachTo( StateMachine stateMachine, JMenuItem target ) {
        HashSet<StateMachine> stateMachines;
        // Get state machines listening to this target
        if ( listeners.containsKey( target ) ) {
            stateMachines = listeners.get( target );
        } else {
            // Target was not found, start listening to it
            stateMachines = new HashSet<StateMachine>();
            listeners.put( target, stateMachines );
            target.addActionListener(this);
        }
        // Add this state machine
        if ( !stateMachines.contains( stateMachine ) ) {
            stateMachines.add( stateMachine );
        }
    }

    public void detachFrom( StateMachine stateMachine, JMenuItem  target ) {
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            if ( stateMachines.contains( stateMachine ) ) {
                // Remove this state machine
                stateMachines.remove( stateMachine );
            }
            // If no state machines are listening, remove the listener
            if ( stateMachines.isEmpty() ) {
                listeners.remove( target );
                target.removeActionListener(this);

            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        JMenuItem target = (JMenuItem)(event.getSource());
        if ( listeners.containsKey( target ) ) {
            HashSet<StateMachine> stateMachines = listeners.get( target );
            // Create custom event
            MenuActionEvent customEvent = new MenuActionEvent(target, target.getText());
            // Send it to attached state machines
            for ( StateMachine stateMachine : stateMachines ) {
                stateMachine.handleEvent( customEvent );
            }
        } else {
            java.awt.Component parent = target.getParent();
            if (parent != null) {
                parent.dispatchEvent(event);
            }
        }
    }
}
