/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.opengl.GL;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by mjlobo on 01/07/15.
 */
public class PointGL extends ShapeGL {
    BufferGL vertexBufferGL;
    BufferGL indexesBufferGL;

    Vector3f color;
    Vector2d coordinate;

    public PointGL(Vector3f color, Vector2d coordinate) {
        this.color = color;
        this.coordinate = coordinate;
        buffers = new Hashtable<>();
        coordinates = new ArrayList<>();
        indexes = new ArrayList<>();
        coordinates.add(new double[] {coordinate.x, coordinate.y});
        coordinates.add(new double[] {coordinate.x+10, coordinate.y+10});
        indexes.add(0);
        updateBuffers();

    }

    public PointGL (Vector3f color, double[] coordinate) {
        this(color, new Vector2d(coordinate[0], coordinate[1]));
    }

    @Override
    public void updateBuffers() {
        if (buffers.size() == 0) {
            vertexBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.VERTEX, BufferGL.DataType.FLOAT, 3);
            indexesBufferGL = new BufferGL(GL.GL_ELEMENT_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.INDEX, BufferGL.DataType.INT, 1);
        }
        buffers.put(vertexBufferGL, getVertices());
        buffers.put(indexesBufferGL, getIndex());
    }

    @Override
    public ShaderManager.RenderedElementTypes getType() {
        return ShaderManager.RenderedElementTypes.POINTS;
    }

    public Vector3f getColor() {
        return color;
    }

    public Vector2d getCoordinate() {
        return coordinate;
    }
}
