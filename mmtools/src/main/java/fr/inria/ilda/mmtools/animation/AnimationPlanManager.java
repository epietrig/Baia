/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.canvasEvents.CanvasDrawnEvent;
import fr.inria.ilda.mmtools.canvasEvents.CanvasEvent;
import fr.inria.ilda.mmtools.canvasEvents.CanvasNewTextureMatEvent;
import fr.inria.ilda.mmtools.canvasEvents.CanvasStagedAnimationsEvent;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.mmtools.utilties.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.w3c.dom.*;

import javax.media.jai.JAI;
import javax.media.jai.TiledImage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by mjlobo on 01/06/16.
 */
public class AnimationPlanManager extends AbstractAnimationManager {

    String initTexture;
    String endTexture;
    ConcurrentLinkedQueue<StagedAnimationByPlan> animationQueue;
    public static enum StagedAnimationByPlanType {EQ, STAGED};


    public AnimationPlanManager(LayerManager layerManager, MapGLCanvas canvas, RasterGeoElement sampleRaster) {
        super(layerManager, canvas);
        animationQueue = new ConcurrentLinkedQueue<>();
        animationLayer = new RasterLayer(true);
        animationRaster = new RasterGeoElement(sampleRaster);
        animationLayer.addElement(animationRaster);
        layerManager.addLayer(animationLayer, false);
    }

    public AnimationPlanManager(LayerManager layerManager, MapGLCanvas canvas, RasterGeoElement startRaster, RasterGeoElement endRaster, String initTexture, String endTexture) {
        super(layerManager, canvas);
        currentStagedAnimation = new StagedAnimationByPlan(startRaster, endRaster);
        animationQueue = new ConcurrentLinkedQueue<>();
        stagedAnimations.add(currentStagedAnimation);
        animationRaster = new RasterGeoElement(startRaster);
        animationLayer = new RasterLayer(true);
        animationLayer.addElement(animationRaster);
        layerManager.addLayer(animationLayer, true);
        this.initTexture = initTexture;
        this.endTexture = endTexture;

    }

    public AnimationPlanManager(LayerManager layerManager, MapGLCanvas canvas) {
        super(layerManager, canvas);
        animationQueue = new ConcurrentLinkedQueue<>();
        animationLayer = new RasterLayer(true);

        layerManager.addLayer(animationLayer, true);
    }

    public void createStagedAnimation(RasterGeoElement startRaster, RasterGeoElement endRaster) {
        currentStagedAnimation = new StagedAnimationByPlan(startRaster, endRaster);
        stagedAnimations.add(currentStagedAnimation);
    }

    public void createStagedAnimation(Texture startTexture, Texture endTexture) {
        currentStagedAnimation = new StagedAnimationByPlan(startTexture, endTexture);
        stagedAnimations.add(currentStagedAnimation);
    }

    public void createStagedAnimation() {
        currentStagedAnimation = new StagedAnimationByPlan();
        stagedAnimations.add(currentStagedAnimation);
    }

    public void addStagedAnimation() {
        if (((StagedAnimationByPlan)currentStagedAnimation).getStagedAnimationByPlanType() == StagedAnimationByPlanType.EQ) {
            createStagedAnimation(((StagedAnimationByPlan) currentStagedAnimation).getInitTexture(), ((StagedAnimationByPlan) currentStagedAnimation).getEndTexture());
        }
    }

    public void addStagedAnimation(StagedAnimationByPlan stagedAnimationByPlan) {
        currentStagedAnimation = stagedAnimationByPlan;
        stagedAnimations.add(currentStagedAnimation);
        if (animationRaster == null) {
            animationRaster = new RasterGeoElement(stagedAnimationByPlan.getInitTexture().getData());
            System.out.println("animation layer vao "+layerManager.getLayerVao(animationLayer));
            layerManager.addElementToLayer(animationRaster, animationLayer, false);
            canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, animationRaster, animationLayer));
        }
        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }
//
    public void setAnimationPlan(RasterGeoElement initRaster, RasterGeoElement endRaster, Mat animationPlan) {
        if (animationRaster!=null) {
            //System.out.println("remove animation raster");
            animationLayer.removeElement(animationRaster);
            layerManager.getRenderingFeatures().get(animationLayer).remove(animationRaster);
        }
        animationRaster = new RasterGeoElement(initRaster);
        layerManager.addElementToLayer(animationRaster, animationLayer, true);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, animationRaster, animationLayer));


        if (currentStagedAnimation == null) {
            currentStagedAnimation = new StagedAnimationByPlan(initRaster, endRaster);
            stagedAnimations.add(currentStagedAnimation);
        }
        else {
            ((StagedAnimationByPlan)currentStagedAnimation).setInitTexture(initRaster.getTexture());
            ((StagedAnimationByPlan)currentStagedAnimation).setEndTexture(endRaster.getTexture());
            ((StagedAnimationByPlan) currentStagedAnimation).getStages().clear();
            ((StagedAnimationByPlan) currentStagedAnimation).getTimedStages().clear();
        }
        List<Mat> bands = new ArrayList<>();
        animationPlan.convertTo(animationPlan, CvType.CV_32F);
        Core.split(animationPlan, bands);
        double max = CVUtilities.getMinMaxMat(bands.get(1))[1];
        bands = bands.subList(0,2);
        Core.divide(bands.get(0),new Scalar(max), bands.get(0));
        Core.divide(bands.get(1), new Scalar(max), bands.get(1));
        Core.merge(bands,animationPlan);
        ((StagedAnimationByPlan)currentStagedAnimation).addTimedStage(new StagePlanAnimation(AbstractAnimation.Type.BLEND, (float)max),0,(float)max,0);
        Texture planTexture = new Texture(animationRaster, animationPlan);
        ((StagedAnimationByPlan)currentStagedAnimation).setPlanTexture(planTexture);
        canvas.pushEvent(new CanvasNewTextureMatEvent(planTexture, animationPlan, CanvasEvent.NEW_TEXTURE_FROM_MAT));


    }

    public void setAnimationPlan(RasterGeoElement initRaster, RasterGeoElement endRaster) {
        if (animationRaster!=null) {
            //System.out.println("remove animation raster");
            animationLayer.removeElement(animationRaster);
            layerManager.getRenderingFeatures().get(animationLayer).remove(animationRaster);
        }
        animationRaster = new RasterGeoElement(initRaster);
        layerManager.addElementToLayer(animationRaster, animationLayer, true);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, animationRaster, animationLayer));
        currentStagedAnimation = null;
        stagedAnimations.clear();
    }


    public StagedAnimationByPlan createBlendAnimationPlan (StagedAnimationByPlan stagedAnimation, int level) {
        float startTime;
        if (stagedAnimation.getTimedStages().get(level) == null) {
            startTime = 0f;
        }
        else {
            startTime = stagedAnimation.getDurationTimed(false);

        }
        stagedAnimation.addTimedStage(new StagePlanAnimation(AbstractAnimation.Type.BLEND,2.0f), startTime, startTime+2.0f, level);
        List<Mat> times = new ArrayList<>();
        times.add(Mat.zeros(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F));
        times.add(Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F));
        Mat mask = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        stagedAnimation.getCurrentAnimation().getAnimation().setMaskMat(mask);
        Mat animationPlan = new Mat();
        Core.merge(times, animationPlan);
        stagedAnimation.getCurrentAnimation().getAnimation().setPlanMat(animationPlan);

        updateCompleteAnimationPlanTimed(stagedAnimation);


        if (stagedAnimation.getEqualizedAnimation()!=null) {
            stagedAnimation.setInitTexture(stagedAnimation.getEqualizedAnimation().getEndTexture());
        }
        return stagedAnimation;
    }




    public StagedAnimationByPlan createBlendAnimationPlan (int level) {
        createBlendAnimationPlan((StagedAnimationByPlan)currentStagedAnimation, level);
        return (StagedAnimationByPlan)currentStagedAnimation;
    }



    public void updateStagePlan(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, AbstractAnimation.Type transition,
                                Texture initMaskTexture, Texture endMaskTexture, int blendFactor, boolean blurBorder) {
        Mat initMask= null;
        Mat endMask = null;
        if (initMaskTexture==null || initMaskTexture.getData() == null) {
            initMask = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        } else {
            initMask = initMaskTexture.getData();
        }
        if (endMaskTexture==null || endMaskTexture.getData() == null) {
            endMask = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        } else {
            endMask = endMaskTexture.getData();
        }
        switch(transition) {
            case BLEND:
                updateBlend(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture);
                break;
            case ERODE:
                updateErode(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, endMaskTexture,blendFactor, blurBorder);
                break;
            case DILATE:
                updateDilate(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, endMaskTexture, blendFactor, blurBorder);
                break;
            case DEFORM:
                updateDeform(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, endMaskTexture, blendFactor, blurBorder);
                break;
            case RADIAL_IN:
                updateRadial(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, true, blendFactor, blurBorder);
                break;
            case RADIAL_OUT:
                updateRadial(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, false, blendFactor, blurBorder);
                break;
            case DIRECTION:
                updateDirection(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, blendFactor, blurBorder);
                break;
            case SEMANTIC:
                updateTexturedAnimationPlan(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, blurBorder, blendFactor);
                break;
            case VECTOR_MORPH:
                updateMorphVectorAnimationPlan(stagedAnimationByPlan, stagePlanAnimation, initMaskTexture, endMaskTexture,blendFactor);
                break;

        }
    }

    public void updateBlend(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture maskTexture) {
        stagePlanAnimation.setType(AbstractAnimation.Type.BLEND);
        Mat maskMat;
        if (maskTexture == null) {
            maskMat = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        } else {
            maskMat = maskTexture.getData();
            stagePlanAnimation.setInitMask(maskMat.clone());
        }
        stagePlanAnimation.setInitMask(maskMat.clone());
        List<Mat> times = new ArrayList<>();
        Mat ones = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        Mat startTimeBackground = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        startTimeBackground.setTo(new Scalar(-1));
        Mat startTimeMask = Mat.zeros(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);

        startTimeMask.copyTo(startTimeBackground, maskMat);
        times.add(startTimeBackground);
        times.add(Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F));
        Mat animationPlan = new Mat();
        Core.merge(times, animationPlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(animationPlan);
        Mat maskStage = Mat.zeros(startTimeMask.size(), CvType.CV_8U);
        ones.copyTo(maskStage, maskMat);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(maskMat);
        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }





    public void updateErode(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture initMaskTexture, Texture endMaskTexture, int blendFactor,
                            boolean blurBorder) {

        StageMorphAnimation stageMorphAnimation = (StageMorphAnimation)stagePlanAnimation;

        stagePlanAnimation.setInitMask(initMaskTexture.getData().clone());
        stageMorphAnimation.setEndMask(endMaskTexture.getData().clone());
        Mat startMask = initMaskTexture.getData();
        Mat endMask = endMaskTexture.getData();
        Mat diff = CVUtilities.calculateDiff(startMask, endMask);
        boolean isZero = CVUtilities.isZero(diff);
        Mat eroded = startMask.clone();
        Mat maskedEroded = new Mat();
        Mat plan = new Mat(eroded.rows(), eroded.cols(), CvType.CV_32F);
        eroded.copyTo(plan);
        plan.convertTo(plan, CvType.CV_32F, 1.0f/255);
        Mat maskErodedFloat = new Mat(eroded.rows(), eroded.cols(), CvType.CV_32F);
        Mat maskErodedStart;
        int count =0;
        Mat endMaskErode = CVUtilities.and(initMaskTexture.getData(), endMaskTexture.getData());
        while(!isZero) {
            eroded = CVUtilities.erode(eroded);
            maskedEroded = CVUtilities.or(eroded, endMaskErode);
            maskErodedStart = CVUtilities.or(eroded, endMaskErode, startMask);
            maskErodedStart.convertTo(maskErodedFloat,CvType.CV_32F, 1.0f/255);
            plan = CVUtilities.add(plan, maskErodedFloat);
            isZero = CVUtilities.isZero(CVUtilities.calculateDiff(maskedEroded, endMaskErode));

            count ++;
        }
        Mat planErodedInt = new Mat(plan.size(), CvType.CV_8U);
        plan.convertTo(planErodedInt, CvType.CV_8U, 255.0);

        Mat planEnd = plan.clone();
        if (blendFactor !=0) {
            Core.add(planEnd, new Scalar(blendFactor), planEnd);
            Core.divide(plan, new Scalar(count + blendFactor + 1), plan);
            Core.divide(planEnd, new Scalar(count + blendFactor + 1), planEnd);
        }
        else {
            Core.divide(plan, new Scalar(count + 1), plan);
            Core.divide(planEnd, new Scalar(count + 1), planEnd);
        }

        List<Mat> times = new ArrayList<>();
        times.add(plan.clone());
        Mat zeros = Mat.zeros(times.get(0).size(), CvType.CV_32F);
        zeros.copyTo(times.get(0), endMask);
        times.add(planEnd.clone());
        Mat ones = Mat.ones(times.get(1).size(), CvType.CV_32F);
        ones.copyTo(times.get(1), endMask);
        Mat stagePlan = new Mat();
        Core.merge(times, stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(CVUtilities.or(initMaskTexture.getData(),endMaskTexture.getData()));
        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);


        if (stagedAnimationByPlan.getEqualizedAnimation()!=null) {
            stagedAnimationByPlan.setInitTexture(stagedAnimationByPlan.getEqualizedAnimation().getEndTexture());
        }

        //System.out.println("min and max in animation plan " +CVUtilities.getMinMaxMat(plan)[0]+ " "+ CVUtilities.getMinMaxMat(plan)[1]);
    }


    public void updateDilate(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture initMaskTexture, Texture endMaskTexture, int blendFactor, boolean blurBorder) {
        stagePlanAnimation.setInitMask(initMaskTexture.getData().clone());
        ((StageMorphAnimation)stagePlanAnimation).setEndMask(endMaskTexture.getData().clone());
        Mat diff = CVUtilities.calculateDiff(initMaskTexture.getData(), endMaskTexture.getData());
        boolean isZero = CVUtilities.isZero(diff);
        Mat dilated = initMaskTexture.getData();
        Mat maskedDilated = new Mat();
        int count =0;
        Mat plan = new Mat(dilated.rows(), dilated.cols(), CvType.CV_32F);
        dilated.copyTo(plan);
        plan.convertTo(plan, CvType.CV_32F, 1.0f/255);
        Mat maskDilatedFloat = new Mat(dilated.rows(), dilated.cols(), CvType.CV_32F);
        Mat previous = dilated;
        while(!isZero) {
            dilated = CVUtilities.dilate(dilated);
            maskedDilated = CVUtilities.and(dilated, endMaskTexture.getData());
            isZero = CVUtilities.isZero(CVUtilities.calculateDiff(maskedDilated, endMaskTexture.getData())) || CVUtilities.isZero(CVUtilities.calculateDiff(maskedDilated, previous));
            count ++;
            maskedDilated.convertTo(maskDilatedFloat,CvType.CV_32F, 1.0f/255);
            plan = CVUtilities.add(plan, maskDilatedFloat);
            dilated = maskedDilated;
            previous = dilated;
        }
        Mat diffDilated = CVUtilities.calculateDiff(dilated, endMaskTexture.getData());
        double planMax = CVUtilities.getMinMaxMat(plan)[1];
        plan = CVUtilities.makeMaxMin(plan, endMaskTexture.getData());
        Mat orInitDiff = CVUtilities.or(diffDilated, initMaskTexture.getData());
        Mat planEnd = plan.clone();
        if (blendFactor !=0) {
            Core.add(planEnd, new Scalar(blendFactor), planEnd);

            Core.divide(plan, new Scalar(count +blendFactor + 1), plan);
            Core.divide(planEnd, new Scalar(count +blendFactor + 1), planEnd);
        }
        else {
            Core.divide(plan, new Scalar(count + 1), plan);
            Core.divide(planEnd, new Scalar(count + 1), planEnd);
        }
        List<Mat> times = new ArrayList<>();
        times.add(plan.clone());
        Mat zeros = Mat.zeros(times.get(0).size(), CvType.CV_32F);
        zeros.copyTo(times.get(0), orInitDiff);
        times.add(planEnd.clone());
        Mat ones = Mat.ones(times.get(1).size(), CvType.CV_32F);
        ones.copyTo(times.get(1), orInitDiff);
        Mat stagePlan = new Mat();
        Core.merge(times, stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(stagePlan);
        Mat maskMat = CVUtilities.or(initMaskTexture.getData(), endMaskTexture.getData());
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(maskMat);



        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }



    public void updateDeform(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture initMaskTexture, Texture endMaskTexture, int blendFactor, boolean blurBorder) {
        stagePlanAnimation.setInitMask(initMaskTexture.getData().clone());
        ((StageMorphAnimation)stagePlanAnimation).setEndMask(endMaskTexture.getData().clone());
        Mat diff = CVUtilities.calculateDiff(initMaskTexture.getData(), endMaskTexture.getData());

        Mat endMaskDilate = CVUtilities.or(CVUtilities.calculateDiff(diff, initMaskTexture.getData()), initMaskTexture.getData());
        Mat diffDilate = CVUtilities.calculateDiff(initMaskTexture.getData(), endMaskDilate);


        Mat endMaskErode = CVUtilities.and(initMaskTexture.getData(), endMaskTexture.getData());
        Mat diffErode = CVUtilities.calculateDiff(initMaskTexture.getData(), endMaskErode);


        boolean isZeroDilate = CVUtilities.isZero(diffDilate);
        boolean isZeroErode = CVUtilities.isZero(diffErode);
        Mat dilated = initMaskTexture.getData().clone();
        Mat eroded = initMaskTexture.getData().clone();
        Mat maskedDilated;
        Mat maskedEroded;
        Mat planDilated = new Mat(dilated.rows(), dilated.cols(), CvType.CV_32F);
        dilated.copyTo(planDilated);
        planDilated.convertTo(planDilated, CvType.CV_32F, 1.0f/255);
        Mat maskDilatedFloat = new Mat(dilated.rows(), dilated.cols(), CvType.CV_32F);

        Mat planEroded = new Mat(eroded.size(), CvType.CV_32F);
        eroded.copyTo(planEroded);
        planEroded.convertTo(planEroded, CvType.CV_32F, 1.0f/255);
        Mat maskErodedFloat = new Mat(eroded.rows(), eroded.cols(), CvType.CV_32F);
        Mat maskErodedStart;
        Mat previous = dilated;

        int count =0;
        while (!isZeroDilate || !isZeroErode) {
            if (!isZeroDilate) {
                dilated = CVUtilities.dilate(dilated);
                maskedDilated = CVUtilities.and(dilated, endMaskDilate);
                isZeroDilate = CVUtilities.isZero(CVUtilities.calculateDiff(maskedDilated, endMaskDilate)) || CVUtilities.isZero(CVUtilities.calculateDiff(maskedDilated, previous));
                maskedDilated.convertTo(maskDilatedFloat, CvType.CV_32F, 1.0f / 255);
                planDilated = CVUtilities.add(planDilated, maskDilatedFloat);
                dilated = maskedDilated;
                previous = dilated;

            }
            if (!isZeroErode) {
                eroded = CVUtilities.erode(eroded);
                maskedEroded = CVUtilities.or(eroded, endMaskErode);
                isZeroErode = CVUtilities.isZero(CVUtilities.calculateDiff(maskedEroded, endMaskErode));

                maskErodedStart = CVUtilities.or(eroded, endMaskErode, initMaskTexture.getData());
                maskErodedStart.convertTo(maskErodedFloat, CvType.CV_32F, 1.0f / 255);
                planEroded = CVUtilities.add(planEroded, maskErodedFloat);
            }
            count++;

        }

        planDilated = CVUtilities.makeMaxMin(planDilated, endMaskDilate);
        double max = Math.max(CVUtilities.getMinMaxMat(planDilated)[1], CVUtilities.getMinMaxMat(planEroded)[1]);
        Mat planDilatedEnd = planDilated.clone();
        Mat planErodedEnd = planEroded.clone();
        if (blendFactor !=0) {
            Core.add(planDilatedEnd, new Scalar(blendFactor), planDilatedEnd);
            Core.add(planErodedEnd, new Scalar(blendFactor), planErodedEnd);

            Core.divide(planDilatedEnd, new Scalar(max+blendFactor), planDilatedEnd);
            Core.divide(planErodedEnd, new Scalar(max+blendFactor), planErodedEnd);
            Core.divide(planDilated, new Scalar(max+blendFactor), planDilated);
            Core.divide(planEroded, new Scalar(max+blendFactor), planEroded);
        }
        else {
            Core.divide(planDilatedEnd, new Scalar(max), planDilatedEnd);
            Core.divide(planErodedEnd, new Scalar(max), planErodedEnd);
            Core.divide(planDilated, new Scalar(max), planDilated);
            Core.divide(planEroded, new Scalar(max), planEroded);
        }
        Mat planErodedInt = new Mat(planEroded.size(), CvType.CV_8U);
        planEroded.convertTo(planErodedInt, CvType.CV_8U, 255.0);
        List<Mat> times = new ArrayList<>();
        Mat fullPlanStarted = Mat.zeros(planDilated.size(), CvType.CV_32F);
        Mat fullPlanEnded = fullPlanStarted.clone();
        Mat ones = Mat.ones(fullPlanEnded.size(), CvType.CV_32F);
        ones.copyTo(fullPlanEnded, CVUtilities.or(endMaskTexture.getData(), initMaskTexture.getData()));
        planDilated.copyTo(fullPlanStarted, CVUtilities.calculateDiff(endMaskTexture.getData(), CVUtilities.and(endMaskTexture.getData(), initMaskTexture.getData())));
        planEroded.copyTo(fullPlanStarted, CVUtilities.calculateDiff(initMaskTexture.getData(), CVUtilities.and(endMaskTexture.getData(), initMaskTexture.getData())));
        planErodedEnd.copyTo(fullPlanEnded, CVUtilities.calculateDiff(initMaskTexture.getData(), CVUtilities.and(endMaskTexture.getData(), initMaskTexture.getData())));
        planDilatedEnd.copyTo(fullPlanEnded, CVUtilities.calculateDiff(endMaskTexture.getData(), CVUtilities.and(endMaskTexture.getData(), initMaskTexture.getData())));

        Mat diffDilated = CVUtilities.calculateDiff(dilated, endMaskDilate);
        Mat orInitDiff = CVUtilities.or(diffDilated, initMaskTexture.getData());

        ones.copyTo(fullPlanEnded, diffDilated);
        Mat zeroes = Mat.zeros(fullPlanEnded.size(), CvType.CV_32F);
        zeroes.copyTo(fullPlanStarted, diffDilated);

        Mat planstartInt = new Mat(fullPlanStarted.size(), CvType.CV_8U);
        fullPlanStarted.convertTo(planstartInt, CvType.CV_8U, 255.0);

        times.add(fullPlanStarted);
        times.add(fullPlanEnded);
        Mat stagePlan = new Mat();
        Core.merge(times, stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(stagePlan);

        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(CVUtilities.or(initMaskTexture.getData(), endMaskTexture.getData()));



        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }



    public void updateRadial (StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture maskTexture, boolean toCenter, int blendFactor, boolean blurBorder) {
        Mat maskMat;
        if (maskTexture == null || maskTexture.getData() == null) {
            maskMat = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        } else {
            maskMat = maskTexture.getData();
            stagePlanAnimation.setInitMask(maskMat.clone());
        }

        StageRadialPlanAnimation stageRadialPlanAnimation = (StageRadialPlanAnimation)stagePlanAnimation;


        stagedAnimationByPlan.setDuration(stagedAnimationByPlan.getDuration()+stagePlanAnimation.getDuration());

        Mat semanticMap = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        for (int i=0; i<semanticMap.cols(); i++) {
            for (int j=0; j<semanticMap.rows(); j++) {
                double distance = Math.sqrt((stageRadialPlanAnimation.getCenterCoords()[0]-i)*(stageRadialPlanAnimation.getCenterCoords()[0]-i)+(stageRadialPlanAnimation.getCenterCoords()[1]-j)*(stageRadialPlanAnimation.getCenterCoords()[1]-j));
                semanticMap.put(j,i,distance);
            }
        }
        if (toCenter) {
            semanticMap = CVUtilities.makeMaxMin(semanticMap, maskMat);
        }
        Mat maskedSemanticMap = Mat.zeros(semanticMap.size(), CvType.CV_32F);
        semanticMap.copyTo(maskedSemanticMap,maskMat);
        double[] minMax = CVUtilities.getMinMaxMat(maskedSemanticMap, maskMat);
        Core.subtract(maskedSemanticMap, new Scalar(minMax[0]), maskedSemanticMap, maskMat);
        Mat planEnd = maskedSemanticMap.clone();
        double newMax = (minMax[1]-minMax[0]);
        if (blendFactor !=0) {

            Core.add(planEnd, new Scalar(blendFactor), planEnd);

            Core.divide(maskedSemanticMap, new Scalar(newMax  + blendFactor), maskedSemanticMap);
            Core.divide(planEnd, new Scalar(newMax  + blendFactor), planEnd);
        }
        else {
            Core.divide(maskedSemanticMap, new Scalar(newMax), maskedSemanticMap);
            Core.divide(planEnd, new Scalar(newMax), planEnd);
        }
        List<Mat> times = new ArrayList<>();
        times.add(maskedSemanticMap);
        times.add(planEnd.clone());
        Mat stagePlan = new Mat();
        Core.merge(times, stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(maskMat);
        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);

    }

    public void updateMorphVectorAnimationPlan(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture initMaskTexture, Texture endMaskTexture, int blendFactor) {
        StageMorphVectorAnimation stageMorphVectorAnimation = (StageMorphVectorAnimation)stagePlanAnimation;
        stageMorphVectorAnimation.setInitMask(initMaskTexture.getData().clone());
        if (endMaskTexture!=null) {
            stageMorphVectorAnimation.setEndMask(endMaskTexture.getData());
        }

        double maxDist = -Double.MAX_VALUE;
        double sumDist = 0;
        for (int i=0; i<stageMorphVectorAnimation.getStartBoundary().size(); i++) {
                double newDist = (stageMorphVectorAnimation.getStartBoundary().get(i)[0]-stageMorphVectorAnimation.getEndBoundary().get(i)[0])*(stageMorphVectorAnimation.getStartBoundary().get(i)[0]-stageMorphVectorAnimation.getEndBoundary().get(i)[0])+
                        (stageMorphVectorAnimation.getStartBoundary().get(i)[1]-stageMorphVectorAnimation.getEndBoundary().get(i)[1])*(stageMorphVectorAnimation.getStartBoundary().get(i)[1]-stageMorphVectorAnimation.getEndBoundary().get(i)[1]);
            sumDist+=Math.sqrt(newDist);
                if (newDist>maxDist) {
                    maxDist = newDist;
            }
        }
        sumDist=sumDist/stageMorphVectorAnimation.getStartBoundary().size();
        blendFactor = (int)(blendFactor*stageMorphVectorAnimation.getnSteps()/sumDist);
        long currentTime = System.currentTimeMillis();
        Mat andMaskMat = CVUtilities.and(initMaskTexture.getData(), endMaskTexture.getData());
        Mat orMaskMat = CVUtilities.or(initMaskTexture.getData(), endMaskTexture.getData());

        Mat semanticMap = Mat.zeros(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        List<Double[]> newPoints = new ArrayList<>();
        List<Point> points = new ArrayList<>();
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        for(int n=0; n<stageMorphVectorAnimation.getnSteps(); n++) {
            Mat contour = Mat.zeros(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
            double t = ((double)n)/stageMorphVectorAnimation.getnSteps();
            points.clear();
            matOfPoints.clear();
            for (int i = 0; i < stageMorphVectorAnimation.getStartBoundary().size(); i++) {
                //newPoints.add(new Double[]{stageMorphVectorAnimation.getStartBoundary().get(i)[0] * (1 - t) + stageMorphVectorAnimation.getEndBoundary().get(i)[0] * t, stageMorphVectorAnimation.getStartBoundary().get(i)[1] * (1 - t) + stageMorphVectorAnimation.getEndBoundary().get(i)[1] * t});
                points.add(new Point(stageMorphVectorAnimation.getStartBoundary().get(i)[0] * (1 - t) + stageMorphVectorAnimation.getEndBoundary().get(i)[0] * t, stageMorphVectorAnimation.getStartBoundary().get(i)[1] * (1 - t) + stageMorphVectorAnimation.getEndBoundary().get(i)[1] * t));
            }
            Point[] pointsArray = new Point[points.size()];
            for (int i=0; i<points.size(); i++) {
                pointsArray[i] = points.get(i);
            }
            MatOfPoint matOfPoint = new MatOfPoint(pointsArray);
            matOfPoints.add(matOfPoint);
            Imgproc.fillPoly(contour,matOfPoints,new Scalar(1));
            Core.add(semanticMap, contour, semanticMap);
        }
        Mat semanticMapInverse = CVUtilities.makeMaxMin(semanticMap, orMaskMat);
        double[] minMax = CVUtilities.getMinMaxMat(semanticMap);
        Core.subtract(semanticMap, new Scalar(minMax[0]), semanticMap, orMaskMat);
        Mat planEnd = semanticMap.clone();
        Mat planInversedEnd = semanticMapInverse.clone();
        double newMax = (minMax[1]-minMax[0]);
        currentTime = System.currentTimeMillis();
        if (blendFactor !=0) {

            Core.add(planEnd, new Scalar(blendFactor), planEnd);
            Core.add(planInversedEnd, new Scalar(blendFactor), planInversedEnd);
            Core.divide(semanticMap, new Scalar(newMax  + blendFactor), semanticMap);
            Core.divide(semanticMapInverse, new Scalar(newMax+blendFactor), semanticMapInverse);
            Core.divide(planEnd, new Scalar(newMax  + blendFactor), planEnd);
            Core.divide(planInversedEnd, new Scalar(newMax+blendFactor), planInversedEnd);
        }
        else {
            Core.divide(semanticMap, new Scalar(newMax), semanticMap);
            Core.divide(planEnd, new Scalar(newMax), planEnd);
            Core.divide(semanticMapInverse, new Scalar(newMax), semanticMapInverse);
            Core.divide(planInversedEnd, new Scalar(newMax), planInversedEnd);
        }
        semanticMapInverse.copyTo(semanticMap, CVUtilities.calculateDiff(endMaskTexture.getData(), CVUtilities.and(endMaskTexture.getData(), initMaskTexture.getData())));
        planInversedEnd.copyTo(planEnd, CVUtilities.calculateDiff(endMaskTexture.getData(), CVUtilities.and(endMaskTexture.getData(), initMaskTexture.getData())));
        List<Mat> times = new ArrayList<>();
        Mat zeros = Mat.zeros(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        Mat ones = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        times.add(semanticMap.clone());
        zeros.copyTo(times.get(0),andMaskMat);
        times.add(planEnd.clone());
        ones.copyTo(times.get(1), andMaskMat);
        Mat stagePlan = new Mat();
        Core.merge(times, stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(CVUtilities.or(initMaskTexture.getData(), endMaskTexture.getData()));
        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);

    }



    public void updateTexturedAnimationPlan(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture maskTexture,
                                            boolean blurBorder, int blendFactor) {
        Mat textureMat = ((StageTexturedPlanAnimation)stagePlanAnimation).getImportedMat().clone();
        Size animationRasterSize = new Size(animationRaster.getImageWidth(), animationRaster.getImageHeight());
        if (textureMat.size() != animationRasterSize) {
            Imgproc.resize(textureMat, textureMat, animationRasterSize);
        }
        Mat maskMat;
        if (maskTexture == null || maskTexture.getData()==null) {
            maskMat = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        }
        else {
            maskMat = maskTexture.getData();
            if (maskMat.size()!=textureMat.size()) {
                Imgproc.resize(maskMat, maskMat, textureMat.size());
            }
        }
        stagePlanAnimation.setInitMask(maskMat.clone());

        Mat semanticMap = textureMat.clone();
        float[] newBounds = CVUtilities.cumulativeCountCut(0.001f,0.999f,semanticMap);
        CVUtilities.minMaxThresholdFloat(newBounds[0], newBounds[1], semanticMap);


        double[] minMax = CVUtilities.getMinMaxMat(semanticMap, maskMat);
        Core.subtract(semanticMap, new Scalar(minMax[0]), semanticMap, maskMat);
        double[] minMax2 = CVUtilities.getMinMaxMat(semanticMap, maskMat);
        if (((StageTexturedPlanAnimation)stagePlanAnimation).isDec()) {
            semanticMap = CVUtilities.makeMaxMin(semanticMap);
        }
        Mat maskedSemanticMap = Mat.zeros(semanticMap.size(), CvType.CV_32F);
        semanticMap.copyTo(maskedSemanticMap,maskMat);
        Mat planEnd = maskedSemanticMap.clone();
        double newMax = (minMax[1]-minMax[0]);

        if (blendFactor !=0) {

            Core.add(planEnd, new Scalar(blendFactor), planEnd);

            Core.divide(maskedSemanticMap, new Scalar(newMax  + blendFactor), maskedSemanticMap);
            Core.divide(planEnd, new Scalar(newMax  + blendFactor), planEnd);
        }
        else {
            Core.divide(maskedSemanticMap, new Scalar(newMax), maskedSemanticMap);
            Core.divide(planEnd, new Scalar(newMax), planEnd);
        }

        List<Mat> times = new ArrayList<>();
        times.add(maskedSemanticMap);
        times.add(planEnd);
        Mat stagePlan = new Mat();
        Core.merge(times, stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(maskMat);
        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }

    public void updateDirection(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture maskTexture, int blendFactor, boolean blurBorder) {
        int[] startDirectionPointPx =  ((StageDirectionPlanAnimation)stagePlanAnimation).getFirstStartPoint();
        int[] endDirectionPointPx = ((StageDirectionPlanAnimation)stagePlanAnimation).getFirstEndPoint();

        Mat maskMat;
        if (maskTexture == null || maskTexture.getData() == null) {
            maskMat = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        } else {
            maskMat = maskTexture.getData();
            stagePlanAnimation.setInitMask(maskMat);
        }


        stagedAnimationByPlan.setDuration(stagedAnimationByPlan.getDuration()+stagePlanAnimation.getDuration());

        int [] pixelPositionStart = new int[] {startDirectionPointPx[0], startDirectionPointPx[1]};
        int [] pixelPositionEnd = new int[] {endDirectionPointPx[0], endDirectionPointPx[1]};

        double minX =0;
        double minY = 0;
        double maxX = animationRaster.getImageWidth();
        double maxY = animationRaster.getImageHeight();
        pixelPositionEnd[1] = (int) maxY - pixelPositionEnd[1];
        pixelPositionStart[1] =(int) maxY - pixelPositionStart[1];
        Mat init = calculateInitInterpolationDirection(startDirectionPointPx, endDirectionPointPx);
        Mat semanticMap = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        if (pixelPositionStart[0] == pixelPositionEnd[0]) {
            for (int i = 0; i < semanticMap.cols(); i++) {
                for (int j = 0; j < semanticMap.rows(); j++) {

                    double distance = Math.abs(j-pixelPositionStart[1]);
                    semanticMap.put(j, i, distance);
                }
            }
        }
        else if (pixelPositionStart[1] == pixelPositionEnd[1]) {
            for (int i = 0; i < semanticMap.cols(); i++) {
                for (int j = 0; j < semanticMap.rows(); j++) {
                    double distance = Math.abs(i-pixelPositionStart[0]);
                    semanticMap.put(j, i, distance);
                }
            }
        }
        else {
            double a = -(((double) (pixelPositionEnd[0] - pixelPositionStart[0])) / ((double) (pixelPositionEnd[1] - pixelPositionStart[1])));
            double b = pixelPositionStart[1] - a * pixelPositionStart[0];


            for (int i = 0; i < semanticMap.cols(); i++) {
                for (int j = 0; j < semanticMap.rows(); j++) {

                    double distance = Math.abs(a * i - j + b) / Math.sqrt(1 + a * a);
                    semanticMap.put(j, i, distance);
                }
            }
        }

        CVUtilities.flipMat(semanticMap);
        Mat zeroes = new Mat(semanticMap.size(), CvType.CV_32F);
        init = CVUtilities.bitwiseNot(init);
        semanticMap.copyTo(zeroes, init);


        Mat maskedSemanticMap = Mat.zeros(semanticMap.size(), CvType.CV_32F);
        zeroes.copyTo(maskedSemanticMap,maskMat);


        double[] minMax = CVUtilities.getMinMaxMat(maskedSemanticMap, maskMat);
        Core.subtract(maskedSemanticMap, new Scalar(minMax[0]), maskedSemanticMap, maskMat);
        Mat planEnd = maskedSemanticMap.clone();
        double newMax = (minMax[1]-minMax[0]);
        if (blendFactor !=0) {
            Core.add(planEnd, new Scalar(blendFactor), planEnd);

            Core.divide(maskedSemanticMap, new Scalar(newMax  + blendFactor), maskedSemanticMap);
            Core.divide(planEnd, new Scalar(newMax  + blendFactor), planEnd);
        }
        else {
            Core.divide(maskedSemanticMap, new Scalar(newMax), maskedSemanticMap);
            Core.divide(planEnd, new Scalar(newMax), planEnd);
        }

        List<Mat> times = new ArrayList<>();
        times.add(maskedSemanticMap);
        times.add(planEnd.clone());
        Mat stagePlan = new Mat();
        Core.merge(times, stagePlan);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setPlanMat(stagePlan);
        Mat maskStage = Mat.zeros(maskMat.size(), CvType.CV_8U);
        Mat ones = Mat.ones(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);
        ones.copyTo(maskStage,  maskMat);
        stagedAnimationByPlan.getCurrentAnimation().getAnimation().setMaskMat(maskMat);
        updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }



    public Mat calculateInitInterpolationDirection(int [] startDirectionPx, int[] endDirectionPx) {

        Mat mask = Mat.zeros(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_8U);

        int [] pixelPositionStart = new int[] {startDirectionPx[0], startDirectionPx[1]};
        int [] pixelPositionEnd = new int[] {endDirectionPx[0], endDirectionPx[1]};

        //orthogonal line equation. y = ax+b
        double minX =0;
        double minY = 0;
        double maxX = animationRaster.getImageWidth();
        double maxY = animationRaster.getImageHeight();
        pixelPositionEnd[1] = (int) maxY - pixelPositionEnd[1];
        pixelPositionStart[1] =(int) maxY - pixelPositionStart[1];
        double a = -((double)(pixelPositionEnd[0]-pixelPositionStart[0]))/((double)(pixelPositionEnd[1]-pixelPositionStart[1]));
        //a = 1/a;
        double b = pixelPositionStart[1] - a*pixelPositionStart[0];
        Double x1 = null;
        Double x2 = null;
        Double y1 = null;
        Double y2 = null;

        //http://stackoverflow.com/questions/1585525/how-to-find-the-intersection-point-between-a-line-and-a-rectangle

        // -----1-----
        // |         |
        // 2         4
        // |____3____|

        //System.out.println("pixel position start "+pixelPositionStart[0]+":"+pixelPositionStart[1]);
        //System.out.println("pixel position end "+pixelPositionEnd[0]+":"+pixelPositionEnd[1]);
        double y = a * (minX - pixelPositionStart[0]) + pixelPositionStart[1];
        if (y > minY && y < maxY) {
            x1 = minX;
            y1 = y;
        }

        y = a * (maxX - pixelPositionStart[0]) + pixelPositionStart[1];
        if (y > minY && y < maxY) {
            if (x1 != null) {
                x2 = maxX;
                y2 = y;
            }
            else {
                x1 = maxX;
                y1 = y;
            }
        }

        double x = (minY - pixelPositionStart[1]) / a + pixelPositionStart[0];
        if (x > minX && x < maxX) {
            if (x1 == null) {
                x1 = x;
                y1 = minY;
            }
            else {
                x2 = x;
                y2 = minY;
            }
        }

        x = (maxY - pixelPositionStart[1]) / a + pixelPositionStart[0];
        if (x > minX && x < maxX) {
            if (x1 == null) {
                x1 = x;
                y1 = maxY;
            }
            else {
                x2 = x;
                y2 = maxY;
            }
        }

        Point corner = new Point();
        List<Point> polygonPoints = new ArrayList<>();

        if (x1 == minX && x2 ==maxX || x1==maxX && x2==minX) {
            //System.out.println("case 2!");
            polygonPoints.add(new Point(minX,  maxY - minY));
            polygonPoints.add(new Point(maxX, maxY - minY));
            polygonPoints.add(new Point(maxX,maxY - (x1<x2?y2:y1)));
            polygonPoints.add(new Point(minX,maxY - (x1<x2?y1:y2)));
            //CVUtilities.fillPolygon(polygonPoints,mask,255);
            CVUtilities.drawPolygon(mask,polygonPoints,255);
            //CVUtilities.writeImage(mask, "filledpolygon.png");
            if (pixelPositionEnd[1]<pixelPositionStart[1]) {
                mask = CVUtilities.bitwiseNot(mask);
            }

        }
        else if (y1==minY && y2==maxY || y2==minY && y1==maxY) {
            polygonPoints.add(new Point(minX,  maxY - minY));
            polygonPoints.add(new Point(minX, maxY - maxY));
            polygonPoints.add(new Point((y2>y1?x2:x1),maxY - maxY));
            polygonPoints.add(new Point((y2>y1?x1:x2),maxY - minY));
            CVUtilities.drawPolygon(mask,polygonPoints,255);
            if (pixelPositionEnd[0]<pixelPositionStart[0]){
                mask = CVUtilities.bitwiseNot(mask);
            }
        }
        else {
            boolean flipped = false;
            if (x1 == minX && y2 == minY || y1 == minY && x2 == minX) {
                corner.set(new double[]{minX, maxY - minY});
            } else if (x1 == maxX && y2 == minY || x2 == maxX && y1 == minY) {
                corner.set(new double[]{maxX, maxY - minY});
            } else if (x1 == minX && y2 == maxY || x2 == minX && y1 == maxY) {
                corner.set(new double[]{minX, maxY - maxY});

            } else if (x1 == maxX && y2 == maxY || x2 == maxX && y1 == maxY) {
                corner.set(new double[]{maxX, maxY - maxY});
            }
            polygonPoints.add(corner);
            polygonPoints.add(new Point(x1, maxY - y1));
            polygonPoints.add(new Point(x2, maxY - y2));


            CVUtilities.fillPolygon(polygonPoints, mask, 255);
            if (Utils.distance(corner,new Point(pixelPositionEnd[0], maxY-pixelPositionEnd[1]))<Utils.distance(corner, new Point(pixelPositionStart[0], maxY-pixelPositionStart[1]))) {
                mask = CVUtilities.bitwiseNot(mask);
            }
        }

        return mask;

    }





    public void updateCompleteAnimationPlan(StagedAnimationByPlan stagedAnimationByPlan) {
        List<Mat> times = new ArrayList<>();
        Mat animationPlan = new Mat();
        times.clear();
        Mat startTimes = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        Mat endTimes = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        startTimes.setTo(new Scalar(stagedAnimationByPlan.getDuration()));
        endTimes.setTo(new Scalar(stagedAnimationByPlan.getDuration()));
        times.add(startTimes);
        times.add(endTimes);
        List<Mat> addeds = new ArrayList<>();
        List<Mat> stagePlans = new ArrayList<>();
        float duration =0;
        for (int i=0; i<stagedAnimationByPlan.getStages().size(); i++) {
            StagePlanAnimation stage = (StagePlanAnimation)stagedAnimationByPlan.getStages().get(i);
            Mat added = stage.getPlanMat().clone();
            Core.split(added, addeds);
            Core.multiply(addeds.get(0), new Scalar(stage.getDuration()), addeds.get(0));
            Core.multiply(addeds.get(1), new Scalar(stage.getDuration()), addeds.get(1));
            Core.add(addeds.get(0), new Scalar(duration),addeds.get(0));
            Core.add(addeds.get(1), new Scalar(duration),addeds.get(1));
            addeds.get(0).copyTo(startTimes, stage.getMaskMat());
            addeds.get(1).copyTo(endTimes, stage.getMaskMat());
            duration+=stage.getDuration();


        }
            Core.divide(startTimes, new Scalar(stagedAnimationByPlan.getDuration()), startTimes);
            Core.divide(endTimes, new Scalar(stagedAnimationByPlan.getDuration()), endTimes);
        Core.merge(times, animationPlan);
        if (((StagedAnimationByPlan)stagedAnimationByPlan).getPlanTexture() == null) {
            Texture planTexture = new Texture(animationRaster, animationPlan);
            stagedAnimationByPlan.setPlanTexture(planTexture);
        }
        else {
            stagedAnimationByPlan.getPlanTexture().setData(animationPlan);
        }
        CVUtilities.flipMat(animationPlan);
        canvas.pushEvent(new CanvasNewTextureMatEvent(stagedAnimationByPlan.getPlanTexture(), animationPlan, CanvasEvent.NEW_TEXTURE_FROM_MAT));
        if (stagedAnimationByPlan.getEqualizedAnimation()!=null) {
            stagedAnimationByPlan.setInitTexture(stagedAnimationByPlan.getEqualizedAnimation().getEndTexture());
        }
//        }
    }

    public void updateCompleteAnimationPlanTimed(StagedAnimationByPlan stagedAnimationByPlan) {
        List<Mat> times = new ArrayList<>();
        Mat animationPlan = new Mat();
        times.clear();
        Mat startTimes = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        Mat endTimes = new Mat(animationRaster.getImageHeight(), animationRaster.getImageWidth(), CvType.CV_32F);
        startTimes.setTo(new Scalar(stagedAnimationByPlan.getDuration()));
        endTimes.setTo(new Scalar(stagedAnimationByPlan.getDuration()));
        times.add(startTimes);
        times.add(endTimes);
        List<Mat> addeds = new ArrayList<>();
        List<Integer> levels = new ArrayList<>(stagedAnimationByPlan.getTimedStages().keySet());
        Collections.sort(levels);
        for (int i =levels.size()-1; i>=0; i--) {
            List<TimedStagePlanAnimation> timedStages = stagedAnimationByPlan.getTimedStages().get(levels.get(i));

            for (int j=timedStages.size()-1; j>=0; j--) {
                Mat added = timedStages.get(j).getAnimation().getPlanMat().clone();
                Core.split(added, addeds);
                Core.multiply(addeds.get(0), new Scalar(timedStages.get(j).getAnimation().getDuration()), addeds.get(0));
                Core.multiply(addeds.get(1), new Scalar(timedStages.get(j).getAnimation().getDuration()), addeds.get(1));
                Core.add(addeds.get(0), new Scalar(timedStages.get(j).getStartTime()),addeds.get(0));
                Core.add(addeds.get(1), new Scalar(timedStages.get(j).getStartTime()),addeds.get(1));
                addeds.get(0).copyTo(startTimes, timedStages.get(j).getAnimation().getMaskMat());
                addeds.get(1).copyTo(endTimes, timedStages.get(j).getAnimation().getMaskMat());
            }
        }

        Core.divide(startTimes, new Scalar(stagedAnimationByPlan.getDuration()), startTimes);
        Core.divide(endTimes, new Scalar(stagedAnimationByPlan.getDuration()), endTimes);
        Core.merge(times, animationPlan);

        if (((StagedAnimationByPlan)stagedAnimationByPlan).getPlanTexture() == null) {
            Texture planTexture = new Texture(animationRaster, animationPlan);
            stagedAnimationByPlan.setPlanTexture(planTexture);
        }
        else {
            stagedAnimationByPlan.getPlanTexture().setData(animationPlan);
        }
        CVUtilities.flipMat(animationPlan);
        canvas.pushEvent(new CanvasNewTextureMatEvent(stagedAnimationByPlan.getPlanTexture(), animationPlan, CanvasEvent.NEW_TEXTURE_FROM_MAT));
        if (stagedAnimationByPlan.getEqualizedAnimation()!=null) {
            stagedAnimationByPlan.setInitTexture(stagedAnimationByPlan.getEqualizedAnimation().getEndTexture());
        }

    }


    public Mat getAnimationPlanWithDurations (StagedAnimationByPlan stagedAnimationByPlan) {
        Mat result = stagedAnimationByPlan.getPlanTexture().getData().clone();
        Core.multiply(result,new Scalar(stagedAnimationByPlan.getDuration(),stagedAnimationByPlan.getDuration(),0),result);
        List<Mat> bands = new ArrayList<>();
        Core.split(result, bands);
        Mat thirdBand = new Mat(result.size(), CvType.CV_32F);
        bands.add(thirdBand);
        Mat result3bands = new Mat();
        Core.merge(bands, result3bands);
        return result3bands;

    }

    public Mat getAnimationPlanWithDuration() {
        return getAnimationPlanWithDurations((StagedAnimationByPlan)currentStagedAnimation);
    }

    public void addEqualizedStagedAnimationByPlan(int index, int clusterNumber) {
        Mat matchedmat = CVUtilities.matchColorsFast((((StagedAnimationByPlan)currentStagedAnimation).getInitTexture()).getData(),(((StagedAnimationByPlan)currentStagedAnimation).getEndTexture()).getData(), clusterNumber);
        Texture matchedTexture = new Texture(((StagedAnimationByPlan)currentStagedAnimation).getInitTexture());
        canvas.pushEvent(new CanvasNewTextureMatEvent(matchedTexture, matchedmat, CanvasEvent.NEW_TEXTURE_FROM_MAT));
        StagedAnimationByPlan eqAnimation = new StagedAnimationByPlan(((StagedAnimationByPlan)stagedAnimations.get(index)).getInitTexture(),matchedTexture);
        eqAnimation.setStagedAnimationByPlanType(StagedAnimationByPlanType.EQ);
        stagedAnimations.add(index, eqAnimation);
        createBlendAnimationPlan(eqAnimation,0);
        eqAnimation.getStages().get(0).setType(AbstractAnimation.Type.EQ);
        ((StagedAnimationByPlan) currentStagedAnimation).setEqualizedAnimation(eqAnimation);


    }

    public void updateCompleteAnimationPlan() {
        updateCompleteAnimationPlan((StagedAnimationByPlan)currentStagedAnimation);
    }


    public void createAnimationRaster(RasterGeoElement rasterGeoElement){
        animationRaster = new RasterGeoElement(rasterGeoElement);
        animationLayer.addElement(animationRaster);
        canvas.pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, animationRaster, animationLayer));

    }

    public RasterGeoElement getAnimationRaster() {
        return animationRaster;
    }

    public void setInitTexture(RasterGeoElement initRaster) {
       setInitTexture(initRaster.getTexture());
    }

    public void setInitTexture(Texture initTexture) {
        ((StagedAnimationByPlan)currentStagedAnimation).setInitTexture(initTexture);
        animationRaster.setTexture(initTexture);
    }

    public void setEndTexture(RasterGeoElement endRaster) {
       setEndTexture(endRaster.getTexture());
    }

    public void setEndTexture(Texture endTexture) {
        ((StagedAnimationByPlan)currentStagedAnimation).setEndTexture(endTexture);
    }


    public void deleteAnimation(AbstractAnimation animation) {
        if (((StagedAnimationByPlan)currentStagedAnimation).containsStage((StagePlanAnimation)animation)) {
            ((StagedAnimationByPlan) currentStagedAnimation).deleteTimedStage((StagePlanAnimation) animation);
            if (((StagedAnimationByPlan)currentStagedAnimation).getStages().size()==0) {
                deleteStagedAnimation(currentStagedAnimation);
            }
        }
        else {
            stagedAnimations.remove(((StagedAnimationByPlan) currentStagedAnimation).getEqualizedAnimation());
            ((StagedAnimationByPlan) currentStagedAnimation).setInitTexture(((StagedAnimationByPlan) ((StagedAnimationByPlan) currentStagedAnimation).getEqualizedAnimation()).getInitTexture());
            ((StagedAnimationByPlan) currentStagedAnimation).setEqualizedAnimation(null);

        }
        if (currentStagedAnimation!= null) {
            updateCompleteAnimationPlan((StagedAnimationByPlan) currentStagedAnimation);
        }
    }

    public void deleteStagedAnimation(AbstractStagedAnimation abstractStagedAnimation) {
        //System.out.println("delete staged animation!");
        stagedAnimations.remove(abstractStagedAnimation);
        if (currentStagedAnimation == abstractStagedAnimation) {
            currentStagedAnimation = null;
        }
    }

    public void replaceStage(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation oldStage, StagePlanAnimation newStage) {
       stagedAnimationByPlan.replaceStage(oldStage, newStage);
    }

    public void startAnimation() {
        for (AbstractStagedAnimation stagedAnimationByPlan : stagedAnimations) {
            stagedAnimations.add(stagedAnimationByPlan);
        }
    }

    public AbstractStagedAnimation nextStagedAnimation() {
        currentStagedAnimation = animationQueue.poll();
        return currentStagedAnimation;
    }

    public void setAnimationRasterVisible(boolean visible) {
        layerManager.getRenderingFeatures().get(animationLayer).get(animationRaster).setVisible(false);
    }

    public void exportAnimation(String fileName) {
        List<AbstractStagedAnimation> stagedAnimationsCloned = new ArrayList<>();
        stagedAnimationsCloned.addAll(stagedAnimations);
        CanvasStagedAnimationsEvent event = new CanvasStagedAnimationsEvent(CanvasEvent.DRAW_STAGED_ANIMATIONS, stagedAnimationsCloned, animationRaster, animationLayer);
        event.addParam(Constants.EXPORT_PATH, fileName);
        canvas.pushEvent(event);
    }




    public void exportProject (String path, String beforePath, String afterPath) {
        System.out.println("PATH "+path);
        if (FilenameUtils.getExtension(path).equals("baia")) {
            path = path.substring(0,path.length()-5);
        }
        boolean success = (new File(path)).mkdirs();
        if (success) {
            try {
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                Document doc = docBuilder.newDocument();
                for (int i=0; i<stagedAnimations.size(); i++) {
                    (new File(path+"/"+i)).mkdirs();

                    StagedAnimationByPlan stagedAnimationByPlan = (StagedAnimationByPlan)stagedAnimations.get(i);
                    File beforeImageFile = new File(beforePath);
                    File beforeImageFileCopy = new File(path+"/"+i+"/"+beforeImageFile.getName());
                    File afterImageFile = new File(afterPath);
                    File afterImageFileCopy = new File(path+"/"+i+"/"+afterImageFile.getName());
                    FileUtils.copyFile(beforeImageFile, beforeImageFileCopy);
                    FileUtils.copyFile(afterImageFile, afterImageFileCopy);

                    Element stagedAnimation = doc.createElement(Constants.STAGED_ANIMATION_TAG);
                    doc.appendChild(stagedAnimation);

                    Element beforeImage = doc.createElement(Constants.BEFORE_IMAGE_TAG);
                    stagedAnimation.appendChild(beforeImage);
                    beforeImage.appendChild(doc.createTextNode(i+"/"+beforeImageFile.getName()));

                    Element afterImage = doc.createElement(Constants.AFTER_IMAGE_TAG);
                    stagedAnimation.appendChild(afterImage);
                    afterImage.appendChild(doc.createTextNode(i+"/"+afterImageFile.getName()));


                    for (int j : stagedAnimationByPlan.getTimedStages().keySet()) {
                        (new File(path+"/"+i+"/"+j)).mkdirs();
                        Element level = doc.createElement(Constants.LEVEL_TAG);
                        stagedAnimation.appendChild(level);
                        Attr levelValue = doc.createAttribute("value");
                        levelValue.setValue(j+"");
                        level.setAttributeNode(levelValue);

                        for (int k=0; k<stagedAnimationByPlan.getTimedStages().get(j).size(); k++) {
                            (new File(path+"/"+i+"/"+j+"/"+k)).mkdirs();

                            Element stage = doc.createElement(Constants.STAGE_TAG);
                            level.appendChild(stage);


                            TimedStagePlanAnimation timedStagePlanAnimation = stagedAnimationByPlan.getTimedStages().get(j).get(k);
                            timedStagePlanAnimation.getAnimation().writeMasks(path+"/"+i+"/"+j+"/"+k);
                            TiledImage planImage = CVUtilities.matToJAI(timedStagePlanAnimation.getAnimation().getAnimationPlanToSave());
                            JAI.create("filestore",planImage,new File(path+"/"+i+"/"+j+"/"+k+"/plan.tiff").toPath().toString(),"TIFF");

                            Utils.appendDOMChildValue(doc, stage, Constants.START_TIME_TAG,timedStagePlanAnimation.getStartTime()+"" );
                            Utils.appendDOMChildValue(doc, stage, Constants.END_TIME_TAG, timedStagePlanAnimation.getEndTime()+"");

                            for (AnimationParameter animationParameter: timedStagePlanAnimation.getAnimation().getValuesToSave()) {
                                Element child = Utils.appendDOMChildValue(doc, stage, animationParameter.getName(), animationParameter.getValue());
                                if (animationParameter.getAttributes().size()>0) {
                                    for (AnimationParameter attribute : animationParameter.getAttributes()) {
                                        Attr attr = doc.createAttribute(attribute.getName());
                                        attr.setValue(attribute.getValue());
                                        child.setAttributeNode(attr);
                                    }
                                }
                            }


                        }
                    }

                    StreamResult result =  new StreamResult(new File(path+"/schema.xml"));
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    DOMSource source = new DOMSource(doc);
                    transformer.transform(source, result);
                    if (!FilenameUtils.getExtension(path).equals("baia")) {
                        Utils.zipIt(Utils.generateFileList(new File(path), path), new File(path).getAbsolutePath() + ".baia", path);
                    } else {
                        path = path.substring(0,path.length()-5);
                        Utils.zipIt(Utils.generateFileList(new File(path), path), new File(path).getAbsolutePath(), path);
                    }
                    FileUtils.deleteDirectory(new File(path));
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            catch (TransformerConfigurationException e) {
                e.printStackTrace();
            }
            catch (TransformerException e) {
                e.printStackTrace();
            }
            catch(IOException e) {
                e.printStackTrace();
            }

        }
    }






    public float getTotalDuration() {
        float totalTime = 0;
        for (AbstractStagedAnimation stagedAnimation : stagedAnimations) {
            if (!((StagedAnimationByPlan)stagedAnimation).getStagedAnimationByPlanType().equals(StagedAnimationByPlanType.EQ)) {
                totalTime += stagedAnimation.getDuration();
            }
        }
        return totalTime;
    }

    public void reset() {
        animationLayer.getElements().clear();
        layerManager.getRenderingFeatures().get(animationLayer).clear();
        animationRaster = null;
        stagedAnimations.clear();
        currentStagedAnimation = null;
    }








}
