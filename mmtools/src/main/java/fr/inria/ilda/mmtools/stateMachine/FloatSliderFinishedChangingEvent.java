/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import java.awt.*;

/**
 * Created by mjlobo on 08/06/16.
 */
public class FloatSliderFinishedChangingEvent extends FloatSliderChangeEvent {
    public FloatSliderFinishedChangingEvent(Component target, float value) {
        super(target, value);
    }
}
