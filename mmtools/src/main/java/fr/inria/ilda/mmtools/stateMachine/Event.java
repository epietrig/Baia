/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
/**
 * Created by mjlobo on 22/03/15.
 */
package fr.inria.ilda.mmtools.stateMachine;

public class Event  {

    Object source = null;
    String  eventName;

    public Event() {

    }

    public Event(String eventName) {
        this.eventName = eventName;
    }

    public static void attachTo( StateMachine stateMachine, Object target ) {
    	// Attaches the listener of this event to a specified `target` from a specified `stateMachine`.
    	// See MouseEvent for an example.
    }

    public static void detachFrom( StateMachine stateMachine, Object target ) {
    	// Detaches the listener of this event from a specified `target` from a specified `stateMachine`.
    }

    public String getName() {
        return this.getClass().getName();
    }

    public Object getSource() {
        return source;
    }

    public String getEventName() {
        return eventName;
    }

}
