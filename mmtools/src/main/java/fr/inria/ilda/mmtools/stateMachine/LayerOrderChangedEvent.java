/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;



/**
 * Created by mjlobo on 05/10/15.
 */
public class LayerOrderChangedEvent extends Event {
    String movedLayerName;
    String destinationLayerName;

    public LayerOrderChangedEvent (String movedLayerName, String destinationLayerName) {
        this.movedLayerName = movedLayerName;
        this.destinationLayerName = destinationLayerName;
    }

    public String getMovedLayerName() {return movedLayerName;}

    public String getDestinationLayerName() {return destinationLayerName;}
}
