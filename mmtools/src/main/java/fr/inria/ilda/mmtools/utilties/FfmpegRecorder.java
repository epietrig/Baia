/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.utilties;

import fr.inria.ilda.mmtools.gl.MapGLCanvas;
//import io.humble.video.*;
//import io.humble.video.awt.MediaPictureConverter;
//import io.humble.video.awt.MediaPictureConverterFactory;
import org.apache.commons.io.FileUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by mjlobo on 04/10/16.
 */
public class FfmpegRecorder {

    public static void recordMovie(String filename, int duration, List<BufferedImage> images, MapGLCanvas canvas) {
//        String formatname = "MP4";
//        final Muxer muxer = Muxer.make(filename, null, formatname);
//        System.out.println("muxer format "+muxer.getFormat());
//        final MuxerFormat format = muxer.getFormat();
//        final Codec codec;
////        if (codecname != null) {
////            codec = Codec.findEncodingCodecByName(codecname);
////        } else {
////            codec = Codec.findEncodingCodec(format.getDefaultVideoCodecId());
////        }
//
//        codec = Codec.findEncodingCodec(format.getDefaultVideoCodecId());
//        System.out.println("codec "+codec);
//        final Rational framerate = Rational.make(1, (int)(((float)duration)/images.size()));
//
//        /**
//         * Now that we know what codec, we need to create an encoder
//         */
//        Encoder encoder = Encoder.make(codec);
//        encoder.setWidth(images.get(0).getWidth());
//        encoder.setHeight(images.get(0).getHeight());
//        // We are going to use 420P as the format because that's what most video formats these days use
//        final PixelFormat.Type pixelformat = PixelFormat.Type.PIX_FMT_YUV420P;
//        encoder.setPixelFormat(pixelformat);
//        encoder.setTimeBase(framerate);
//
//        /** An annoynace of some formats is that they need global (rather than per-stream) headers,
//         * and in that case you have to tell the encoder. And since Encoders are decoupled from
//         * Muxers, there is no easy way to know this beyond
//         */
//        if (format.getFlag(MuxerFormat.Flag.GLOBAL_HEADER))
//            encoder.setFlag(Encoder.Flag.FLAG_GLOBAL_HEADER, true);
//
//        /** Open the encoder. */
//        encoder.open(null, null);
//
//
//        /** Add this stream to the muxer. */
//        muxer.addNewStream(encoder);
//
//        /** And open the muxer for business. */
//        try {
//            muxer.open(null, null);
//            /** Next, we need to make sure we have the right MediaPicture format objects
//             * to encode data with. Java (and most on-screen graphics programs) use some
//             * variant of Red-Green-Blue image encoding (a.k.a. RGB or BGR). Most video
//             * codecs use some variant of YCrCb formatting. So we're going to have to
//             * convert. To do that, we'll introduce a MediaPictureConverter object later. object.
//             */
//            MediaPictureConverter converter = null;
//            final MediaPicture picture = MediaPicture.make(
//                    encoder.getWidth(),
//                    encoder.getHeight(),
//                    pixelformat);
//            picture.setTimeBase(framerate);
//
//            final MediaPacket packet = MediaPacket.make();
//            for (int i = 0; i < images.size(); i++) {
//                /** Make the screen capture && convert image to TYPE_3BYTE_BGR */
//                final BufferedImage image = convertToType(images.get(i), BufferedImage.TYPE_3BYTE_BGR);
//
//                /** This is LIKELY not in YUV420P format, so we're going to convert it using some handy utilities. */
//                if (converter == null)
//                    converter = MediaPictureConverterFactory.createConverter(image, picture);
//                converter.toPicture(picture, image, i);
//
//                do {
//                    encoder.encode(packet, picture);
//                    if (packet.isComplete())
//                        muxer.write(packet, false);
//                } while (packet.isComplete());
//
//                do {
//                    encoder.encode(packet, null);
//                    if (packet.isComplete())
//                        muxer.write(packet,  false);
//                } while (packet.isComplete());
//
//                /** Finally, let's clean up after ourselves. */
//                muxer.close();
//
//
//            }

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
////
//        try {
//            Runtime.getRuntime().exec("ffmpeg -r 1 -pattern_type glob -i '/temp/*.png'  -c:v libx264 -vf fps=25 -pix_fmt yuv420p out.mp4");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }



        double framerate = (((float)images.size())/(float)duration);
        try {
        Process p = new ProcessBuilder(
                "ffmpeg", // or "ffmepg"
                "-framerate", ""+framerate,
                //"-pattern_type", "glob",
                "-i", Constants.tempPath+"/export%d.png",
                "-c:v", "libx264",
                "-r", "30", // input format
                "-pix_fmt", "yuv420p",
                "-y",
                "-vf", "scale=trunc(iw/2)*2:trunc(ih/2)*2",
               // force overwrite
                filename).redirectError(ProcessBuilder.Redirect.INHERIT).start();
            p.waitFor();
    }
        catch (Exception e) {e.printStackTrace();}

        canvas.throwFinishedExportingMovie();

        try {
            FileUtils.cleanDirectory(new File(Constants.tempPath));
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public static BufferedImage convertToType(BufferedImage sourceImage,
                                              int targetType)
    {
        BufferedImage image;

        // if the source image is already the target type, return the source image

        if (sourceImage.getType() == targetType)
            image = sourceImage;

            // otherwise create a new image of the target type and draw the new
            // image

        else
        {
            image = new BufferedImage(sourceImage.getWidth(),
                    sourceImage.getHeight(), targetType);
            image.getGraphics().drawImage(sourceImage, 0, 0, null);
        }

        return image;
    }
}
