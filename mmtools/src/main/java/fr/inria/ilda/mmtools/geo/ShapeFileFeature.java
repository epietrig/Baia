/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import fr.inria.ilda.mmtools.geo.VectorGeoElement;
import org.opengis.feature.simple.SimpleFeature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 22/06/15.
 */
public class ShapeFileFeature {

    List<VectorGeoElement> vectorGeoElements;
    SimpleFeature feature;
    HashMap<ShapeFileAttribute, Object> attributesValues;
    HashMap<ShapeFileNumericAttribute, Double> numericAttributeHashMap;
    HashMap<ShapeFileStringAttribute, String> stringAttributeHashMap;

    public ShapeFileFeature(SimpleFeature feature) {
        this.feature = feature;
        vectorGeoElements = new ArrayList<>();
        attributesValues = new HashMap<>();
        numericAttributeHashMap = new HashMap<>();
        stringAttributeHashMap = new HashMap<>();
    }

    public void addVectorGeoElement(VectorGeoElement vectorGeoElement) {
        vectorGeoElements.add(vectorGeoElement);
    }

    public List <VectorGeoElement> getVectorGeoElements() {
        return vectorGeoElements;
    }

    public void addAttribute (ShapeFileAttribute attribute) {
        if (attribute instanceof ShapeFileStringAttribute) {
            stringAttributeHashMap.put((ShapeFileStringAttribute)attribute, (String)(feature.getAttribute(attribute.getTypeName())));
        }
        else if (attribute instanceof ShapeFileNumericAttribute) {
            double d=0;
            try {
                //System.out.println("attribute name" + attribute.getTypeName().toString());
                if (feature.getAttribute(attribute.getTypeName().toString()) !=null) {
                    d = Double.parseDouble(feature.getAttribute(attribute.getTypeName()).toString());
                }
            } catch(NumberFormatException nfe) {
                return;
            }
            numericAttributeHashMap.put((ShapeFileNumericAttribute)attribute, d);
        }
        attributesValues.put(attribute, feature.getAttribute(attribute.getTypeName()));
    }

    public SimpleFeature getFeature() {
        return feature;
    }

    public Double getShapeFileNumericAttributeValue(ShapeFileNumericAttribute attribute) {
        //System.out.println("Attribute name" +attribute.getLocalName());
        //System.out.println("Attribute value "+numericAttributeHashMap.get(attribute));
        if (numericAttributeHashMap.containsKey(attribute)) {
            return numericAttributeHashMap.get(attribute);
        }
        return null;
    }

    public String getShapeFileStringAttributeValue(ShapeFileStringAttribute attribute) {
        return stringAttributeHashMap.get(attribute);
    }

    public HashMap<ShapeFileNumericAttribute, Double> getNumericAttributeHashMap() {
        return numericAttributeHashMap;
    }

    public HashMap<ShapeFileStringAttribute, String> getStringAttributeHashMap() {
        return stringAttributeHashMap;
    }


}
