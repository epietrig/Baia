/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.magicWand;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by mjlobo on 28/10/15.
 */
public class FloodFillFacade {

    public static final int NULL_RANGE = 0;
    public static final int FIXED_RANGE = 1;
    public static final int FLOATING_RANGE = 2;
    private boolean colored = true;
    private boolean masked = true;
    private int range = FIXED_RANGE;
    //private int range = FLOATING_RANGE;
    //private int range = NULL_RANGE;
    private Random random = new Random();
    private int connectivity = 4;
    private int newMaskVal = 255;
    private int lowerDiff = 40;
    private int upperDiff = 40;

    public Mat fill(Mat image, int x, int y, int upperDiff, int lowerDiff) {
        this.lowerDiff = lowerDiff;
        this.upperDiff = upperDiff;
        Mat mask = new Mat(image.rows()+2, image.cols()+2, CvType.CV_8UC1,new Scalar(0));
        Point seedPoint = new Point(x,y);
        int b = random.nextInt(256);
        int g = random.nextInt(256);
        int r = random.nextInt(256);
        Rect rect = new Rect();
        boolean colored = mask.channels()>2 ? true : false;

        Scalar newVal = colored ? new Scalar(b, g, r) : new
                Scalar(r*0.299 + g*0.587 + b*0.114);
        Scalar lowerDifference = new Scalar(lowerDiff,lowerDiff,lowerDiff);
        Scalar upperDifference = new Scalar(upperDiff,upperDiff,upperDiff);
        if(range == NULL_RANGE){
            lowerDifference = new Scalar (0,0,0);
            upperDifference = new Scalar (0,0,0);
        }
        int flags = connectivity + (newMaskVal << 8) +
                (range == FIXED_RANGE ? Imgproc.FLOODFILL_FIXED_RANGE : 0);
        flags = flags | Imgproc.FLOODFILL_MASK_ONLY;
        int area = 0;
        if(masked){
            area = Imgproc.floodFill(image, mask, seedPoint, newVal, rect,
                    lowerDifference, upperDifference, flags);
        } else{
            area = Imgproc.floodFill(image, new Mat(), seedPoint, newVal,
                    rect, lowerDifference, upperDifference, flags);
        }

        return mask;
        }





    }
