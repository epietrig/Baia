/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by mjlobo on 27/02/15.
 */
public class ShaderProgram {
    int programId;
    Hashtable<String, Integer> uniforms;
    Hashtable<Integer, String> uniformMethods;
    Hashtable<String, Integer> attributes;
    Hashtable<Integer, String> attributesMethods;
    List<String> samplers;


    public ShaderProgram (int programId) {
        this.programId = programId;
        uniforms = new Hashtable<>();
        uniformMethods = new Hashtable<>();
        attributes = new Hashtable<>();
        attributesMethods = new Hashtable<>();
        samplers = new ArrayList<>();
    }

    public void addUniform(int uniformId, String uniformName) {
        uniforms.put(uniformName, uniformId);
    }

    public void addUniform(String uniformName) {
        uniforms.put(uniformName, 0);
    }

    public void setUniformId (String uniformName, int uniformId) {
        uniforms.put(uniformName,uniformId);
    }

    public void addAttribute(int attributeid, String attributeName) {
        attributes.put(attributeName, attributeid);
    }

    public void addAttribute (String attributeName) {
        attributes.put(attributeName, 0);
    }

    public void setAttributeId (String attributeName, int attributeId) {
        attributes.put(attributeName, attributeId);
    }

    public Hashtable<Integer, String> getUniformsMethods() {
        return uniformMethods;
    }

    public Hashtable<Integer, String> getAttributesMethods() {
        return attributesMethods;
    }

    public int getProgramId () {
        return programId;
    }

    public int getUniform (String uniformName) {
        if (uniforms.get(uniformName)!= null && uniforms!=null) {
            return uniforms.get(uniformName);
        }
        else {
            return 0;
        }
    }

    public int getAttribute (String attributeName) {
        return attributes.get(attributeName);
    }

    public Hashtable<String, Integer> getUniforms() {
        return uniforms;
    }

    public Hashtable<String, Integer> getAttributes() {
        return attributes;
    }

    public void addSampler(String samplerName) {
        uniforms.put(samplerName,0);
        samplers.add(samplerName);

    }

    public List<String> getSamplers() {
        return samplers;
    }


}
