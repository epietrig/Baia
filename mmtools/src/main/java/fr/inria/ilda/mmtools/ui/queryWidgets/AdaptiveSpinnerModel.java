/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui.queryWidgets;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class AdaptiveSpinnerModel { 

	protected LinkedList<Double> values;
	protected Double value;
	protected double minStep;
	protected ArrayList<ChangeListener> listeners;
	
	public AdaptiveSpinnerModel(LinkedList<Double> values) {
		this.values = new LinkedList<Double>(values);
		Collections.sort(this.values);
		this.minStep = getMinimalStep();
		this.listeners = new ArrayList<ChangeListener>();
		this.value = 0.0;
	}
	
	public void addChangeListener(ChangeListener listener) {
		listeners.add(listener);
	}

	public void removeChangeListener(ChangeListener listener) {listeners.remove(listener);}
	
	protected void fireChangeEvent() {
		ChangeEvent event = new ChangeEvent(this);
		for (Iterator<ChangeListener> iterator = listeners.iterator(); iterator.hasNext();) {
			ChangeListener changeListener = iterator.next();
			changeListener.stateChanged(event);
		}
	}
	
	public double getMinimalStep() {
		double minStep = Double.MAX_VALUE;
		Double previous = values.get(0);
		for(int i = 1; i < values.size(); i++) {
			Double current = values.get(i);
			minStep = Math.min(current-previous, minStep);
		}
		return minStep;
	}
	
//	@Override
//	public Number getStepSize() {
//		return minStep;
//	}
	
	public Double getMinValue() {
		return values.getFirst();
	}
	
	public Double getMaxValue() {
		return values.getLast();
	}
	
	public Double getPreviousValue() {
		for(int i = 0; i < values.size(); i++) {
			Double v = values.get(i);
			if(v >= value) {
				if(i-1 >= 0) {
					return values.get(i-1);
				} else {
					return values.get(0);
				}
			}
		}
		if(values.size() >= 2) {
			return values.get(values.size()-2);
		} else {
			return value;
		}
	}
	
	public Double getValue() {
		return value;
	}

	public Double getNextValue() {
		for(int i = 0; i < values.size(); i++) {
			Double v = values.get(i);
			if(v > value) {
				if(i < values.size()) {
					return v;
				}
			}
		}
		return value;
	}
	
	public void setValue(Object value) {
		double d = 0;
		try {
			d = Double.parseDouble(""+value);
		} catch(NumberFormatException nfe) {
			return;
		}
		this.value = d;
		//fireChangeEvent();
	}
	
	public String getFormat() {
		String f = "#.";
		for(int i = 0; i < getDecimalsCount(getMinimalStep()); i++) {
			f += "#";
		}
		return f;
	}
	
	private static int getDecimalsCount(double number){
	    //convert the number to a string
	    String strNumber = ""+number;
	    int index = strNumber.indexOf('.');
	    if(index == -1) {
	    	index = strNumber.indexOf(',');
	    }
	    int decimalCount = 0;
	    for(int i = strNumber.length()-1; i >= index; i--) {
	    	if(strNumber.charAt(i) != '0') {
	    		decimalCount = i - index;
	    		break;
	    	}
	    }
	    return decimalCount;
	}

	public LinkedList<Double> getValues() {
		return values;
	}

	public void setValues(LinkedList<Double> values) {
		this.values = values;
	}
	
}
