/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

//import fr.inria.ilda.ml.queryWidgets.QueryWidget;
import org.opengis.feature.type.Name;

/**
 * Created by mjlobo on 22/06/15.
 */
public abstract class ShapeFileAttribute {

    protected Name typeName;
    protected String localName;


    public ShapeFileAttribute(Name typeName, String localName) {
        super();
        this.typeName = typeName;
        this.localName = localName;
    }
    public Name getTypeName() {
        return typeName;
    }
    public void setTypeName(Name typeName) {
        this.typeName = typeName;
    }
    public String getLocalName() {
        return localName;
    }
    public void setLocalName(String localName) {
        this.localName = localName;
    }
    public abstract void update(Object o);



}
