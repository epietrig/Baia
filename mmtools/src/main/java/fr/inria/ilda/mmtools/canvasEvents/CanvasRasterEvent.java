/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

import fr.inria.ilda.mmtools.filters.TextureFilter;
import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.MNTRasterGeoElement;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;

/**
 * Created by mjlobo on 22/07/15.
 */
public class CanvasRasterEvent extends CanvasEvent{

    RasterGeoElement rasterGeoElement;
    Layer layer;
    TextureFilter textureFilter;
    public CanvasRasterEvent(String event, RasterGeoElement mntRasterGeoElement, Layer layer) {
        super(event);
        this.rasterGeoElement = mntRasterGeoElement;
        this.layer = layer;
        this.textureFilter = textureFilter;
    }

    public RasterGeoElement getMntRasterGeoElement() {
        return rasterGeoElement;
    }

    public Layer getLayer() {
        return layer;
    }

    public TextureFilter getTextureFilter() {
        return textureFilter;
    }
}
