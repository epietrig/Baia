/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

import fr.inria.ilda.mmtools.gl.Texture;
import org.opencv.core.Mat;

/**
 * Created by mjlobo on 27/02/16.
 */
public class CanvasNewTextureMatEvent extends CanvasEvent {
    Texture texture;
    Mat mat;

    public CanvasNewTextureMatEvent(Texture texture, Mat mat, String event) {
        super(event);
        this.texture = texture;
        this.mat = mat;

    }

    public Texture getTexture() {
        return  texture;
    }

    public Mat getMat() {
        return mat;
    }
}
