/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.utilties.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Pack200;

/**
 * Created by mjlobo on 20/06/16.
 */
public class StageDirectionPlanAnimation extends StagePlanAnimation{
    List<int[]> startPoints;
    List<int[]> endPoints;


    public StageDirectionPlanAnimation(Type type, float duration, int[] startPoint, int[] endPoint) {
        super(type, duration);
        startPoints = new ArrayList<>();
        endPoints = new ArrayList<>();
        startPoints.add(startPoint);
        endPoints.add(endPoint);
    }

    public StageDirectionPlanAnimation(Type type, float duration, int[] startPoint, int[] endPoint, boolean blurBorder, int blurWidth) {
        super(type, duration, blurBorder, blurWidth);
        startPoints = new ArrayList<>();
        endPoints = new ArrayList<>();
        startPoints.add(startPoint);
        endPoints.add(endPoint);

    }

    public StageDirectionPlanAnimation(Type type) {
        super(type);
        startPoints = new ArrayList<>();
        endPoints  = new ArrayList<>();

    }

    public int[] getFirstStartPoint() {
        return startPoints.get(0);
    }

    public int[] getFirstEndPoint() {
        return endPoints.get(0);
    }

    public void setFirstStartPoint(int[] firstStartPoint) {
        System.out.println("setting first start point! "+firstStartPoint[0]+" "+firstStartPoint[1]);
        if (startPoints.size()==0) {
            startPoints.add(firstStartPoint);
        }
        else {
            startPoints.set(0, firstStartPoint);
        }
    }

    public void setFirstEndPoint(int[] firstEndPoint) {
        System.out.println("setting first end point ! "+firstEndPoint[0]+" "+firstEndPoint[1]);
        if (endPoints.size() == 0) {
           endPoints.add(firstEndPoint);
        }
        else {
            endPoints.set(0, firstEndPoint);
        }
    }

    public boolean isComplete() {
        if (startPoints.size()>0 && endPoints.size() >0) {
            if (startPoints.get(0) != null && endPoints.get(0) != null) {
                return true;
            } else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public List<AnimationParameter> getValuesToSave() {
        List<AnimationParameter> valuesToSave = super.getValuesToSave();
        AnimationParameter firstPointParameter = new AnimationParameter(Constants.FIRST_POINT_TAG);
        firstPointParameter.addAttribute(Constants.X_TAG, startPoints.get(0)[0]+"");
        firstPointParameter.addAttribute(Constants.Y_TAG, startPoints.get(0)[1]+"");
        AnimationParameter endPointParameter = new AnimationParameter(Constants.END_POINT_TAG);
        endPointParameter.addAttribute(Constants.X_TAG, endPoints.get(0)[0]+"");
        endPointParameter.addAttribute(Constants.Y_TAG, endPoints.get(0)[1]+"");
        valuesToSave.add(firstPointParameter);
        valuesToSave.add(endPointParameter);
        return valuesToSave;
    }

    public List<String> getValuesToRetrieve() {
        java.util.List<String> valuestoRetrieve = super.getValuesToRetrieve();
        valuestoRetrieve.add(Constants.FIRST_POINT_TAG);
        valuestoRetrieve.add(Constants.END_POINT_TAG);
        return valuestoRetrieve;
    }


}
