/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.morphingAlgorithm;

/**
 * Created by mjlobo on 14/01/2017.
 */
public class Corner {
    Double[] coord;
    double convex;
    int index;
    EigenValues eigenValues;
    double featureVariation;
    double featureSideVariation;
    double featureSize;
    double varROL;
    double varROR;
    double pR;
    double pL;

    public Corner (Double[] coord, double convex, int index) {
        this.coord = coord;
        this.convex = convex;
        this.index = index;

    }

    public Corner (Corner clone) {
        this.coord = clone.getCoord().clone();
        this.convex = clone.getConvex();
        this.index = clone.getIndex();
        this.eigenValues = new EigenValues(clone.getEigenValues());
        this.featureVariation = clone.getFeatureVariation();
        this.featureSideVariation = clone.getFeatureSideVariation();
        this.featureSize = clone.getFeatureSize();
        this.varROL = clone.getVarROL();
        this.varROR = clone.getVarROR();
        this.pR = clone.getpR();
        this.pL = clone.getpL();
    }

    public Double[] getCoord() {
        return coord;
    }

    public double getX() {
        return coord[0];
    }

    public double getY() {
        return coord[1];
    }

    public double getConvex() {
        return convex;
    }

    public int getIndex() {
        return index;
    }

    public void setEigenValues(EigenValues eigenValues) {
        this.eigenValues = eigenValues;
    }

    public EigenValues getEigenValues() {
        return eigenValues;
    }

    public void setFeatureVariation(double featureVariation) {
        this.featureVariation = featureVariation;
    }

    public void setFeatureSideVariation(double featureSideVariation) {
        this.featureSideVariation = featureSideVariation;
    }

    public void setFeatureSize(double featureSize) {
        this.featureSize = featureSize;
    }

    public double getFeatureVariation() {
        return featureVariation;
    }

    public double getFeatureSideVariation() {
        return featureSideVariation;
    }

    public double getFeatureSize() {
        return featureSize;
    }

    public void setVarROL(double varROL) {
        this.varROL = varROL;
    }

    public void setVarROR(double varROR) {
        this.varROR = varROR;
    }

    public double getVarROL() {
        return varROL;
    }

    public double getVarROR() {
        return varROR;
    }

    public double getpR() {
        return pR;
    }

    public void setpR(double pR) {
        this.pR = pR;
    }

    public double getpL() {
        return  pL;
    }

    public void setpL(double pL) {
        this.pL = pL;
    }



}
