/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.apache.commons.io.FileUtils;
import org.opencv.core.Mat;

/**
 * Created by mjlobo on 10/09/16.
 */
public class StageTexturedPlanAnimation extends StagePlanAnimation {
    Mat importedMat;
    boolean dec;
    String importedPath; //used for saving project only

    public StageTexturedPlanAnimation(Type type, float duration, Mat planMat, float noValue, boolean dec) {
        super(type, duration);
        this.dec = dec;
        if (noValue>0) {
            this.importedMat = CVUtilities.thresholdToZeroInverted(planMat,noValue-1);
        }
        else {
            this.importedMat = CVUtilities.thresholdToZero(planMat,noValue+1);
        }
    }

    public StageTexturedPlanAnimation(Type type, float duration, boolean blurBorder, int blurWidth, Mat planMat, float noValue, boolean dec) {
       this (type, duration, blurBorder, blurWidth, planMat, noValue, dec, "");

    }

    public StageTexturedPlanAnimation(Type type, float duration, boolean blurBorder, int blurWidth, Mat planMat, float noValue, boolean dec, String importedPath) {
        super(type, duration, blurBorder, blurWidth);
        this.dec = dec;
        if (noValue>0) {
            this.importedMat = CVUtilities.thresholdToZeroInverted(planMat,noValue-1);
        }
        else {
            this.importedMat = CVUtilities.thresholdToZero(planMat,noValue+1);
        }
        this.importedPath = importedPath;

    }

    public StageTexturedPlanAnimation (Type type) {
        super(type);
    }

    public Mat getImportedMat() {
        return importedMat;
    }

    public boolean isDec() {
        return dec;
    }

    public void setDec(boolean dec) {
        this.dec = dec;
    }

    public void setImportedPath(String importedPath) {
        this.importedPath = importedPath;
    }

    public void setImportedMat (Mat importedMat) {
        this.importedMat = importedMat;
    }

    public String getImportedPath() {
        return importedPath;
    }

    public boolean isComplete() {
        if (importedMat!=null) {
            return true;
        }
        else {
            return false;
        }
    }

    public List<AnimationParameter> getValuesToSave() {
        List<AnimationParameter> valuesToSave = super.getValuesToSave();
        valuesToSave.add(new AnimationParameter(Constants.IMPORTED_PLAN_TAG, Constants.IMPORTED_PLAN_NAME));
        valuesToSave.add(new AnimationParameter(Constants.DEC_TAG,""+(dec? 1:0)));
        return valuesToSave;
    }

    public List<String> getValuesToRetrieve() {
        java.util.List<String> valuestoRetrieve = super.getValuesToRetrieve();
        valuestoRetrieve.add(Constants.IMPORTED_PLAN_TAG);
        valuestoRetrieve.add(Constants.DEC_TAG);
        return valuestoRetrieve;
    }

    public void writeMasks(String path) {
        super.writeMasks(path);
        File importedFile = new File(importedPath);
        File impotedFileCopy = new File(path+"/"+Constants.IMPORTED_PLAN_NAME);
        try {
            FileUtils.copyFile(importedFile, impotedFileCopy);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
