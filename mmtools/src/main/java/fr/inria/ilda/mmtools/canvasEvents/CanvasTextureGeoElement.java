/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.RGBRasterGeoElement;

/**
 * Created by mjlobo on 09/02/16.
 */
public class CanvasTextureGeoElement extends CanvasEvent {

    GeoElement geoElement;
    RGBRasterGeoElement rgbRasterGeoElement;
    Layer layer;
    String compMethod;

    public CanvasTextureGeoElement(String event, GeoElement geoElement, RGBRasterGeoElement rgbRasterGeoElement, Layer layer, String compMethod) {
        super(event);
        this.geoElement = geoElement;
        this.rgbRasterGeoElement = rgbRasterGeoElement;
        this.layer = layer;
        this.compMethod = compMethod;
    }

    public GeoElement getGeoElement() {
        return geoElement;
    }

    public RGBRasterGeoElement getRgbRasterGeoElement() {
        return rgbRasterGeoElement;
    }

    public Layer getLayer() {
        return layer;
    }

    public String getCompMethod() {
        return compMethod;
    }
}
