/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

import java.awt.*;

/**
 * Created by mjlobo on 04/11/15.
 */
public class ButtonEvent extends Event{
    Component target;
    String action;

    public ButtonEvent(Component target, String action) {
        this.target = target;
        this.action = action;
    }

    public Component getTarget() {
        return target;
    }

    public String getAction() {
        return action;
    }


}
