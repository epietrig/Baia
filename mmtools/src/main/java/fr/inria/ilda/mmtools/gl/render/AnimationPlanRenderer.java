/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.animation.AbstractStagedAnimation;
import fr.inria.ilda.mmtools.animation.StagePlanAnimation;
import fr.inria.ilda.mmtools.animation.StagedAnimationByPlan;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3d;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 01/06/16.
 */
public class AnimationPlanRenderer extends AbstractAnimationRenderer{

    ShaderProgram program;

    public AnimationPlanRenderer(ShaderProgram program) {
        this.program = program;
    }

    public void render (AbstractStagedAnimation stagedAnimation, GLAutoDrawable drawable, Camera camera, float tic, LayerManager layerManager,
                        RasterGeoElement animationRaster) {
        GL4 gl = (GL4) drawable.getGL();

        int vertexAttributeId = program.getAttribute(ShaderConstants.POSITION);
        int textureAttributeId = program.getAttribute(ShaderConstants.TEXTURE_POSITION);
        StagedAnimationByPlan stagedAnimationByPlan = (StagedAnimationByPlan) stagedAnimation;
        if (stagedAnimationByPlan.getInitTexture() != null && stagedAnimationByPlan.getEndTexture() != null) {
            if (((StagePlanAnimation) stagedAnimationByPlan.getStagePlanTimeAnimationAtTime(tic/1000f))!=null && ((StagePlanAnimation) stagedAnimationByPlan.getStagePlanTimeAnimationAtTime(tic/1000f)).isComplete()) {
                for (ShapeGL shape : animationRaster.getShapes()) {
                    if (!shape.getOfseted() && shape instanceof PolygonGL && shape.getVisible()) {
                        gl.glUseProgram(program.getProgramId());
                        gl.glUniformMatrix4fv(program.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
                        gl.glUniformMatrix4fv(program.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
                        gl.glUniform1f(program.getUniform(ShaderConstants.DURATION), stagedAnimationByPlan.getDuration() * 1000);
                        gl.glUniform1f(program.getUniform("tic"), tic);

                        BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                        BufferGL textureBuffer = shape.getBuffer(BufferGL.Type.TEXTURE);
                        if (vertexBuffer != null) {
                            gl.glEnableVertexAttribArray(vertexAttributeId);
                            gl.glBindBuffer(vertexBuffer.getTarget(), layerManager.getBufferVbo(vertexBuffer));
                            gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                            gl.glEnableVertexAttribArray(textureAttributeId);
                            gl.glBindBuffer(textureBuffer.getTarget(), layerManager.getBufferVbo(textureBuffer));
                            gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                            BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                            gl.glBindBuffer(indexBuffer.getTarget(), layerManager.getBufferVbo(indexBuffer));
                            gl.glActiveTexture(gl.GL_TEXTURE0);
                            gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(stagedAnimationByPlan.getInitTexture()));
                            gl.glActiveTexture(gl.GL_TEXTURE1);
                            gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(stagedAnimationByPlan.getEndTexture()));
                            gl.glActiveTexture(gl.GL_TEXTURE2);
                            gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(stagedAnimationByPlan.getPlanTexture()));
                            gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                        }


                    }
                }
            }
        }
    }

    public void render (AbstractStagedAnimation stagedAnimation, GLAutoDrawable drawable, Camera camera, Vector3d eye, Vector3d center, double left, double right, double bottom,
                        double top, float tic, LayerManager layerManager, RasterGeoElement animationRaster) {
        GL4 gl = (GL4) drawable.getGL();

        int vertexAttributeId = program.getAttribute(ShaderConstants.POSITION);
        int textureAttributeId = program.getAttribute(ShaderConstants.TEXTURE_POSITION);
        StagedAnimationByPlan stagedAnimationByPlan = (StagedAnimationByPlan) stagedAnimation;
        if (stagedAnimationByPlan.getInitTexture() != null && stagedAnimationByPlan.getEndTexture() != null) {
            if (((StagePlanAnimation) stagedAnimationByPlan.getStagePlanTimeAnimationAtTime(tic/1000f)).isComplete()) {
                for (ShapeGL shape : animationRaster.getShapes()) {
                    if (!shape.getOfseted() && shape instanceof PolygonGL && shape.getVisible()) {
                        gl.glUseProgram(program.getProgramId());
                        gl.glUniformMatrix4fv(program.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(eye, center), 0);
                        gl.glUniformMatrix4fv(program.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(left, right, bottom, top), 0);
                        gl.glUniform1f(program.getUniform(ShaderConstants.DURATION), stagedAnimationByPlan.getDuration() * 1000);
                        gl.glUniform1f(program.getUniform("tic"), tic);

                        BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                        BufferGL textureBuffer = shape.getBuffer(BufferGL.Type.TEXTURE);
                        if (vertexBuffer != null) {
                            gl.glEnableVertexAttribArray(vertexAttributeId);
                            gl.glBindBuffer(vertexBuffer.getTarget(), layerManager.getBufferVbo(vertexBuffer));
                            gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                            gl.glEnableVertexAttribArray(textureAttributeId);
                            gl.glBindBuffer(textureBuffer.getTarget(), layerManager.getBufferVbo(textureBuffer));
                            gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                            BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                            gl.glBindBuffer(indexBuffer.getTarget(), layerManager.getBufferVbo(indexBuffer));
                            gl.glActiveTexture(gl.GL_TEXTURE0);
                            gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(stagedAnimationByPlan.getInitTexture()));
                            gl.glActiveTexture(gl.GL_TEXTURE1);
                            gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(stagedAnimationByPlan.getEndTexture()));
                            gl.glActiveTexture(gl.GL_TEXTURE2);
                            gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(stagedAnimationByPlan.getPlanTexture()));
                            gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                        }


                    }
                }
            }
        }
    }
}
