/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui;


import fr.inria.ilda.mmtools.stateMachine.ModeButtonPressedEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.mmtools.utilties.Constants;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 30/07/15.
 */
public class ModeToolBar extends JToolBar{

    protected static Font FONT = new Font(null, Font.PLAIN, 16);
    List<String> modes;
    int iconWidth = 16;
    int iconHeight = 16;

    HashMap<String, JButton> modeButtons = new HashMap<>();
    public ModeToolBar(List<String> modes) {
        setBorder(new EmptyBorder(2,0,2,0));
        this.modes = modes;
        addButtons();
    }

    public ModeToolBar(List<String> modes, int iconWidth, int iconHeight) {
        this.iconHeight = iconHeight;
        this.iconWidth = iconWidth;
        setBorder(new EmptyBorder(2,0,2,0));
        this.modes = modes;
        addButtons();
    }





    public void addButtons() {
        for (String mode : modes) {
            if (mode.equals(Constants.SEPARATOR)) {
                addSeparator();
            }
            else {
                JButton button = null;
                File image = new File("icons/" + mode + ".png");
                if (image.exists()) {
                    button = new JButton(new ImageIcon(new ImageIcon("icons/" + mode + ".png").getImage().getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)));
                    //button.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
                    button.setMargin(new Insets(3, 5, 3, 5));
                } else {
                    button = new JButton(mode);
                }
                button.setActionCommand(mode);
                button.setFont(FONT);
                add(button);
                modeButtons.put(mode, button);
                //addSeparator();
            }

        }


    }

    public void setOnlyButtonSelected(String selectedMode) {
        for (String mode: modeButtons.keySet()) {
            if (mode.equals(selectedMode)) {
                modeButtons.get(mode).setSelected(true);
            }
            else {
                modeButtons.get(mode).setSelected(false);
            }
        }
    }

    public void selectButton(String buttonName) {
        for (String mode: modeButtons.keySet()) {
            if (mode.equals(buttonName)) {
                modeButtons.get(mode).setSelected(true);
            }
        }
    }

    public void addListeners(final StateMachine stateMachine) {
        for (final JButton button: modeButtons.values()) {
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    stateMachine.handleEvent(new ModeButtonPressedEvent(button, button.getActionCommand(),ModeToolBar.this));
                }
            });
        }


    }

    public void disableAllButtonsButOne(String modeToKeep) {
        for (String mode: modeButtons.keySet()) {
            if (mode.equals(modeToKeep)) {
                modeButtons.get(mode).setEnabled(true);
            }
            else {
                modeButtons.get(mode).setEnabled(false);
            }
        }
    }

    public void enableAll() {
        for (String mode: modeButtons.keySet()) {
            modeButtons.get(mode).setEnabled(true);
        }
    }

    public void enableButton(String buttonText, boolean enabled) {
        for (String mode: modeButtons.keySet()) {
            if (mode.equals(buttonText)) {
                modeButtons.get(mode).setEnabled(enabled);
            }
        }
    }

    public void enableAllButtons(boolean enabled) {
        for (String mode: modeButtons.keySet()) {
            modeButtons.get(mode).setEnabled(enabled);

        }
    }

}
