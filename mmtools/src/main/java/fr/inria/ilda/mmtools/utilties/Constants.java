/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.utilties;

import fr.inria.ilda.mmtools.animation.AbstractAnimation;

import java.util.HashMap;

/**
 * Created by mjlobo on 01/12/15.
 */
public class Constants {

    public static int  MAX_TEXTURE_SIZE = 8192;
    static int LBP_radius = 1;
    static int LBP_p = 8;
    public static double LPB_threshold = 0.2;
    public static String LBP_DESCRIPTOR = "LBP";
    public static String LBP_HUE = "LBP hue";
    public static String COMPARISON_THRESHOLD = "comparison threshold";
    //public static enum ImageModelName {CIELAB, HSV}
    public static String INIT_ANIMATION_RASTER = "init_animation_raster";
    public static String END_ANIMATION_RASTER = "end_animation_raster";
    public static String ANIMATION_ENDED = "animationended";
    public static String COLOR_MODEL = "colorModel";
    public static String EQ_ANIMATION_BUTTON = "addequalizedaniamtion";
    public static String MASK_ANIMATION_BUTTON = "maskedanimation";
    public static String PLAY_ANIMATION = "playanimation";
    public static String ERODE_TRANSITION = "Erode";
    public static String DILATE_TRANSITION = "Dilate";
    public static String BLEND_TRANSITION = "Blend";
    public static String DIRECTION_TRANSITION = "Direction";
    public static String DEFORM_TRANSITION = "Deform";
    public static String RADIAL_TRANSITION = "Radial";
    public static String TRANSITION_COMBOBOX = "transitionComboBox";
    public static String ADD_MASK_ANIMATION_BUTTON = "addmaskanimation";
    public static String PLAY_ANIMATION_STEP = "playanimationstep";
    public static String SEMANTIC_MAP_CB = "semanticmapcb";
    public static String DELETE_ANIMATION = "deleteanimation";
    public static String RADIAL_SEMANTIC = "Radial";
    public static String LINEAR_INTERPOLATION = "Linear";
    public static String SIGMOID_INTERPOLATION = "Sigmoid";
    public static String SEPARATOR = "Separator";
    public static enum ColorModel {RGB, BGR};
    public static String EXPORT_PATH = "export_path";

    public static String STAGED_ANIMATION_TAG = "staged_animation";
    public static String STAGE_TAG = "stage";
    public static String START_TIME_TAG = "start_time";
    public static String END_TIME_TAG = "end_time";
    public static String TYPE_TAG = "type";
    public static String ANIMATION_PLAN_TAG = "animation_plan";
    public static String INIT_MASK_TAG = "init_mask";
    public static String END_MASK_TAG = "end_mask";
    public static String LEVEL_TAG = "level";
    public static String LEVEL_VALUE_TAG = "value";
    public static String BEFORE_IMAGE_TAG = "before";
    public static String AFTER_IMAGE_TAG= "after";
    public static String MASK_TAG = "mask";
    public static String CENTER_TAG = "center";
    public static String DIRECTION_TAG = "direction";
    public static String FIRST_POINT_TAG = "first_point";
    public static String END_POINT_TAG = "end_point";
    public static String X_TAG = "x";
    public static String Y_TAG = "y";
    public static String BLUR_BORDER_TAG = "blur_border";
    public static String IMPORTED_PLAN_TAG = "imported_plan";
    public static String DEC_TAG= "dec";

    public static String BEFORE_MASK_FILE_NAME = "beforemask.png";
    public static String AFTER_MASK_FILE_NAME = "afteremask.png";
    public static String PLAN_FILE_NAME = "plan.tiff";
    public static String MASK_FILE_NAME = "mask.png";
    public static String IMPORTED_PLAN_NAME = "imported.tiff";


    public static String tempPath= "temp";
    /**
     * @author The Elite Gentleman
     *
     */
    public enum ImageModelName {
        CIELAB("CieLab"),
        HSV("HSV")
        ;

        private final String text;

        /**
         * @param text
         */
        private ImageModelName(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }

    //Extensions
    public static enum EXT {TIF, ECW, JPG, SHP};

}
