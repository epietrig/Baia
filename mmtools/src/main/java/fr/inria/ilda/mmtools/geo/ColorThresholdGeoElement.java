/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

/**
 * Created by mjlobo on 25/05/16.
 */
import com.vividsolutions.jts.geom.Geometry;
import fr.inria.ilda.mmtools.geo.DrawnGeoElement;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.gl.Texture;

import javax.vecmath.Vector3f;
import java.awt.image.Raster;

/**
 * Created by mjlobo on 25/05/16.
 */
public class ColorThresholdGeoElement extends DrawnGeoElement{
    double threshold;
    Vector3f compareColor;
    Texture textureToThreshold;;
    String name;
    Texture maskedTexture;
    Vector3f maskColor;
    boolean changedMaskTexture = false;

    public ColorThresholdGeoElement(Geometry geometry, double threshold, Vector3f compareColor, Texture textureToThreshold, String name) {
        super(geometry);
        this.threshold = threshold;
        this.compareColor = compareColor;
        this.textureToThreshold = textureToThreshold;
        this.renderingType = ShaderManager.RenderedElementTypes.COLOR_DIFF;
        this.name = name;
    }

    public ColorThresholdGeoElement(Geometry geometry, double threshold, Vector3f compareColor, Texture textureToThreshold, String name, Vector3f maskColor) {
        super(geometry);
        this.threshold = threshold;
        this.compareColor = compareColor;
        this.textureToThreshold = textureToThreshold;
        this.renderingType = ShaderManager.RenderedElementTypes.COLOR_DIFF;
        this.name = name;
        this.maskColor = maskColor;
    }

    public ColorThresholdGeoElement(Geometry geometry, double threshold, Vector3f compareColor, String name, Vector3f maskColor, Texture maskedTexture) {
        super(geometry);
        this.threshold = threshold;
        this.compareColor = compareColor;
        this.maskColor = maskColor;
        this.maskedTexture = maskedTexture;
        this.name = name;
    }

    public ColorThresholdGeoElement(Geometry geometry, double threshold, Vector3f compareColor, Texture textureToThreshold, String name, Vector3f maskColor, Texture maskedTexture) {
        super(geometry);
        this.threshold = threshold;
        this.compareColor = compareColor;
        this.textureToThreshold = textureToThreshold;
        this.renderingType = ShaderManager.RenderedElementTypes.COLOR_DIFF;
        this.name = name;
        this.maskColor = maskColor;
        this.maskedTexture = maskedTexture;
    }



    public double getThreshold() {
        return threshold;
    }

    public Vector3f getCompareColor() {
        return compareColor;
    }

    public Texture getTextureToThreshold() {
        return textureToThreshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public void setCompareColor(Vector3f compareColor) {
        this.compareColor = compareColor;
    }

    public void setTextureToThreshold(Texture textureToThreshold) {
        this.textureToThreshold = textureToThreshold;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMaskedTexture(Texture maskedTexture) {
        this.maskedTexture = maskedTexture;
    }

    public Texture getMaskedTexture() {
        return maskedTexture;
    }

    public Vector3f getMaskColor() {
        return maskColor;
    }

    public boolean isChangedMaskTexture() {
        return changedMaskTexture;
    }

    public void setChangedMaskTexture(boolean changedMaskTexture) {
        this.changedMaskTexture = changedMaskTexture;
    }

}
