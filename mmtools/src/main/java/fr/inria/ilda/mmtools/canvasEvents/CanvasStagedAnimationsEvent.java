/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

import fr.inria.ilda.mmtools.animation.AbstractStagedAnimation;
import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 22/09/16.
 */
public class CanvasStagedAnimationsEvent extends CanvasEvent {
    List<AbstractStagedAnimation> animations;
    RasterGeoElement animationRaster;
    Layer animationLayer;
    HashMap<String, String> params = new HashMap<>();

    public CanvasStagedAnimationsEvent(String event, List<AbstractStagedAnimation> animations, RasterGeoElement animationRaster, Layer animationLayer) {
        super(event);
        this.animations = animations;
        this.animationRaster = animationRaster;
        this.animationLayer = animationLayer;
    }

    public List<AbstractStagedAnimation> getAnimations() {
        return animations;
    }

    public RasterGeoElement getAnimationRaster() {
        return animationRaster;
    }

    public Layer getAnimationLayer() {
        return animationLayer;
    }

    public void addParam(String paramName, String paramValue) {
        params.put(paramName, paramValue);
    }

    public String getParam(String paramName) {
        return params.get(paramName);
    }

}
