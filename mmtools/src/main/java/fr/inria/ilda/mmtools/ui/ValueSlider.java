/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui;

import fr.inria.ilda.mmtools.stateMachine.SliderChangeEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Created by mjlobo on 25/05/16.
 */
public class ValueSlider extends JPanel {
    JSlider slider;
    JLabel label;
    JLabel valueLabel;
    int min;
    int max;
    int value;
    String text;

    public ValueSlider(int min, int max, int value, String text, String name) {
        this.max = max;
        this.min = min;
        this.value = value;
        this.text = text;
        label = new JLabel(text);
        valueLabel = new JLabel(""+value);
        slider = new JSlider(min, max, value);
        slider.setMajorTickSpacing(max);
        //slider.setMinorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        add(label);
        add(valueLabel);
        add(slider);

        setName(name);
    }

    public void setValue(int value) {
        this.value = value;
        slider.setValue(value);
        repaint();
    }

    public int getValue() {
        return value;
    }

    public void addListeners(final StateMachine stateMachine) {
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                valueLabel.setText(""+((JSlider)e.getSource()).getValue());
                setValue(((JSlider)e.getSource()).getValue());
                if (!((JSlider)e.getSource()).getValueIsAdjusting()) {
                    stateMachine.handleEvent(new SliderChangeEvent(ValueSlider.this, value));
                }
            }
        });

    }
}
