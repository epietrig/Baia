/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.stateMachine;

/**
 * Created by mjlobo on 11/03/16.
 */
public class MaskAnimationParamChangedEvent extends Event {
    String interpolationType;
    String colorModel;
    int thresholdValue;

    public MaskAnimationParamChangedEvent(String interpolationType, String colorModel, int thresholdValue) {
        this.interpolationType = interpolationType;
        this.colorModel = colorModel;
        this.thresholdValue = thresholdValue;
    }

    public String getInterpolationType() {
        return interpolationType;
    }

    public String getColorModel() {
        return colorModel;
    }

    public int getThresholdValue() {
        return thresholdValue;
    }
}
