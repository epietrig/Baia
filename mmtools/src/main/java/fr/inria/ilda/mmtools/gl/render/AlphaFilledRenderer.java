/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

/**
 * Created by mjlobo on 26/04/16.
 */
public class AlphaFilledRenderer extends AbstractGeoRenderer {
    AbstractLineStripRenderer lineRenderer;
    AbstractPolygonGLRenderer polygonRenderer;
    AbstractPolygonGLRenderer holeRenderer;
    Texture holeTexture;

    public AlphaFilledRenderer(AbstractLineStripRenderer lineRenderer, AbstractPolygonGLRenderer polygonRenderer, AbstractPolygonGLRenderer holeRenderer, Texture holeTexture) {
        this.lineRenderer = lineRenderer;
        this.polygonRenderer = polygonRenderer;
        this.holeRenderer = holeRenderer;
        this.holeTexture = holeTexture;
    }
    @Override
    public void render(GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager) {
        //ge.printCoordinates();
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    if (((PolygonGL) shape).getIsHole()) {
                      holeRenderer.render(shape,drawable,camera,null, null, layerManager);
                    }
                    else {
                        //System.out.println("ge.getfillcolor() "+ge.getFillColor());
                        polygonRenderer.render(shape,drawable,camera, ge.getBlurredTexture(), ge.getFillColor(), layerManager);
                    }
                }
                else if (shape instanceof LineStripGL) {
                    lineRenderer.render(shape,drawable, camera, layerManager);
                }
            }
        }
    }
}
