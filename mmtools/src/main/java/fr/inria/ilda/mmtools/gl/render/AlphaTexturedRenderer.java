/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.DrawnGeoElement;
import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

/**
 * Created by mjlobo on 25/04/16.
 */
public class AlphaTexturedRenderer extends AbstractGeoRenderer {

    AbstractPolygonGLRenderer polygonRenderer;
    AbstractLineStripRenderer lineStripRenderer;
    AbstractPolygonGLRenderer holeRenderer;
    Texture holeTexture;


    public AlphaTexturedRenderer(AbstractPolygonGLRenderer polygonRenderer, AbstractLineStripRenderer lineStripRenderer, AbstractPolygonGLRenderer holeRenderer, Texture holeTexture) {
        this.polygonRenderer = polygonRenderer;
        this.lineStripRenderer = lineStripRenderer;
        this.holeRenderer = holeRenderer;
        this.holeTexture = holeTexture;
    }

    public AlphaTexturedRenderer(AbstractPolygonGLRenderer polygonRenderer, AbstractLineStripRenderer lineStripRenderer) {
        this.polygonRenderer = polygonRenderer;
        this.lineStripRenderer = lineStripRenderer;
    }

    @Override
    public void render(GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager) {
        for (ShapeGL shape : ge.getShapes()) {
            if (shape.getVisible() && !shape.getOnlyInFirst()) {
                if (shape instanceof PolygonGL) {
                    if (((PolygonGL) shape).getIsHole()) {
                            ((PolygonGL) shape).setTexture(holeTexture);
                            holeRenderer.render(shape,drawable,camera, null, null, layerManager);
                    }
                    else{
                        polygonRenderer.render(shape,drawable,camera,ge.getBlurredTexture(), ge.getFillColor(), layerManager);
                    }
                }
                else if (shape instanceof LineStripGL) {
                    lineStripRenderer.render(shape, drawable, camera, layerManager);
                }
            }
        }
    }

    public void setHoleTexture (Texture holeTexture) {
        this.holeTexture = holeTexture;
    }

    public void setHoleRenderer(AbstractPolygonGLRenderer holeRenderer) {
        this.holeRenderer = holeRenderer;
    }
}
