/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.canvasEvents;

/**
 * Created by mjlobo on 01/07/15.
 */
public abstract class CanvasEvent {

    public static final String POINT_ADDED = "pointAdded";
    public static final  String DRAWN_FINISHED = "drawnFinished";
    public static final  String DRAWN_CREATED = "drawnCreated";
    public static final  String DRAWN_MOVED = "drawnMoved";
    public static final String ELEMENT_MODIFIED = "elementModified";
    public static final String DRAWN_MOVED_CHANGE = "drawnMovedChange";
    public static final String VECTOR_SELECTION_UPDATED = "vectorSelectionUpdated";
    public static final String POINT_ALIGN_ADDED = "pointAlignAdded";
    public static final String NEW_MNT_RASTERGEOELEMENT = "newMNTRasterGeoELement";
    public static final String TRANSLUCENT_VECTOR_SELECTION_CHANGED = "textureVectorSelectionChanged";
    public static final String TRANSLUCENT_MOVED = "translucentMoved";
    public static final String TRANSLUCENT_BORDER_ENABLED = "translucentBorderEnabled";
    public static final String TRANSLUCENT_LAYER_ENABLED = "translucentLayerEnabled";
    public static final String GEOELEMENT_CREATED = "geoElementCreated";
    public static final String MNT_UPDATED = "mntUpdated";
    public static final String RGB_UPDATED = "rgbUpdated";
    public static final String TEXTURE_ELEMENT = "textureElement";
    public static final String MASK_UPDATED = "maskupdated";
    public static final String NEW_TEXTURE_FROM_MAT = "newtexturefromamt";
    public static final String NEW_TEXTURE_LIST_FROM_MATS = "newtexturelistfromats";
    public static final String DRAW_MASKS = "draw_masks";
    public static final String TEXTURE_MAT_EDITED = "texture_mat_edited";
    public static final String DRAW_STAGED_ANIMATIONS = "draw_staged_animations";
    public static final String NEW_LAYER = "new_layer";
    public static final String NEW_RASTER = "new_raster";
    public static final String FINISHED_LOADING_RASTERS = "finished_loading_rasters";
    public static final String FINISHED_EXPORTING_MOVIE = "finished_exporting_movie";

    String event;

    public  String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public CanvasEvent (String event) {
        this.event = event;
    }

}
