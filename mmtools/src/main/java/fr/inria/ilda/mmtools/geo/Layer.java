/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import com.vividsolutions.jts.geom.Geometry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mjlobo on 09/02/15.
 */
public abstract class Layer {

    ArrayList<GeoElement> elements;
    public boolean isVisible;

    public abstract ArrayList<GeoElement> getElements();

    private int vao=-1;

    HashSet<GeoElement> elementsToRender;
    HashSet<GeoElement> translucentElements;

    public Layer(boolean isVisible) {
        this.isVisible = isVisible;
        elements = new ArrayList<GeoElement>();
        elementsToRender = new HashSet<>();
        translucentElements = new HashSet<>();
    }

    public void setVao(int vao) {
        this.vao = vao;
    }

    public int getVao() {
        return vao;
    }

    public void setVisibility (boolean isVisible) {
        this.isVisible = isVisible;
    }

    public boolean getVisibility() {
        return isVisible;
    }

    public List <GeoElement> getElementsContainedInGeometry (Geometry geometry) {
        isVisible = true;
        List<GeoElement> result = new ArrayList<>();
        for (GeoElement g : elements) {
            if (geometry.contains(g.getGeometry())) {
                result.add(g);
            }
        }
        return result;
    }

    public HashSet <GeoElement> getElementsIntersectingGeometry (Geometry geometry) {
        isVisible = true;
        HashSet<GeoElement> result = new HashSet<>();
        for (GeoElement g : elements) {
            if (geometry.intersects(g.getGeometry())) {
                result.add(g);
            }
        }
        return result;
    }

    public HashSet <GeoElement> getElementsIntersectingGeometry (Geometry geometry, List<GeoElement> elementsToConsider) {
        isVisible = true;
        HashSet<GeoElement> result = new HashSet<>();
        for (GeoElement g : elementsToConsider) {
            if (geometry.intersects(g.getGeometry())) {
                result.add(g);
            }
        }
        return result;
    }

    public List<Object> getElementsPerfectlyContained(Geometry geometry) {
        isVisible = true;
        List<GeoElement> result = new ArrayList<>();
        HashMap<GeoElement, GeoElement> intersections= new HashMap<>();
        for (int i=0; i<elements.size(); i++) {
            GeoElement g = elements.get(i);
            if (geometry.intersects(g.getGeometry())) {
                if (geometry.contains(g.getGeometry())) {
                    result.add(g);
                    //System.out.println("Geometry contained");
                }
                else {
                    g.getGeometry().buffer(0);
                    geometry.buffer(0);
                    VectorGeoElement intersection = new VectorGeoElement(g.getGeometry().intersection(geometry), ((VectorGeoElement) g).getTexture());
                    intersections.put(g,intersection);
                }
            }
        }

        List<Object> listResult = new ArrayList<Object>();
        listResult.add(result);
        listResult.add(intersections);
        return listResult;
    }

    public void setVisibility(boolean isVisible, boolean propagate) {
        setVisibility(isVisible);
        if (propagate) {
            for (GeoElement geoElement:elements) {
                geoElement.setVisible(isVisible);
            }
        }
    }

    public void addTranslucentElement(GeoElement element) {
        //elements.add(element);
        translucentElements.add(element);
        elementsToRender.add(element);
    }

    public void setElementVisible(GeoElement ge) {
        ge.setVisible(true);
        elementsToRender.add(ge);
    }

    public void addElement(GeoElement ge) {
        elements.add(ge);
    }

    public void removeElement(GeoElement ge) {
        elements.remove(ge);
    }

    public HashSet<GeoElement> getElementsToRender() {
        return elementsToRender;
    }

    public HashSet<GeoElement> getTranslucentElements() {
        return translucentElements;
    }
}
