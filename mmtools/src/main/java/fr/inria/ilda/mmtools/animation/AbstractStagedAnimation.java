/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

import fr.inria.ilda.mmtools.geo.Layer;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.utilties.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 01/06/16.
 */
public class AbstractStagedAnimation {

    Layer animationLayer;
    ShaderManager.RenderedElementTypes renderingType;
    List<AbstractAnimation> stages;
    float duration = 2.0f;


    public Layer getAnimationLayer() {
        return animationLayer;
    }

    public ShaderManager.RenderedElementTypes getRenderingType() {
        return renderingType;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public float getDuration() {
        return duration;
    }

    public Pair<Float,AbstractAnimation> getStagePlanTimeAnimationAndTimeAtTime(float time) {
        float accumulatedTime = 0.0f;
        if (time>=getDuration()) {
            return new Pair<>(duration-stages.get(stages.size()-1).getDuration(),stages.get(stages.size()-1));
        }
        for (AbstractAnimation animation : stages) {
            if (accumulatedTime<= time && time<=animation.getDuration()+accumulatedTime) {
                return new Pair<>(accumulatedTime, animation);
            }
            else {
                accumulatedTime += animation.getDuration();
            }
        }
        return null;

    }

    public int getIndexOfStage(AbstractAnimation stage) {
        for (int i=0; i<stages.size(); i++) {
            if (stages.get(i).equals(stage)) {
                return i;
            }
        }
        return -1;
    }

    public List<AbstractAnimation> getStages() {
        return stages;
    }

    public float getAnimationStartTime(int indexOfAnimation) {
        float accumulatedTime = 0;
        for (int i=0; i<indexOfAnimation; i++) {
            accumulatedTime += stages.get(i).getDuration();
        }

        return accumulatedTime;
    }

    public void replaceStage(AbstractAnimation oldAnimation, AbstractAnimation newAnimation) {
        int oldIndex = ((ArrayList)stages).indexOf(oldAnimation);
        stages.remove(oldIndex);
        ((ArrayList) stages).add(oldIndex, newAnimation);
    }


}
