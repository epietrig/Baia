/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.filters;

import fr.inria.ilda.mmtools.gl.FrameBufferObject;
import fr.inria.ilda.mmtools.gl.PolygonGL;

import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 08/09/15.
 */
public class TwoPassFilter {
    private FrameBufferObject fbo1;
    private FrameBufferObject fbo2;
    private FrameBufferObject readFbo;
    PolygonGL quad;
    private int vaoID;

    public TwoPassFilter(FrameBufferObject fbo1, FrameBufferObject fbo2, int vaoID) {
        this.fbo1 = fbo1;
        this.fbo2 = fbo2;
        this.vaoID = vaoID;
    }

    public void calculateQuad (Vector3d eye, double right, double left, double bottom, double up) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{eye.x+left, eye.y+bottom});
        coordinates.add(new double[]{eye.x+right, eye.y+bottom});
        coordinates.add(new double[]{eye.x+left, eye.y+up});
        coordinates.add(new double[]{eye.x+right, eye.y+up});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad = new PolygonGL(coordinates, indexes,fbo1.getTexture());


    }

    public void refreshQuadCoordinates (Vector3d eye, double right, double left, double bottom, double up) {
        List<double[]> coordinates = new ArrayList<>();
        coordinates.add(new double[]{eye.x+left, eye.y+bottom});
        coordinates.add(new double[]{eye.x+right, eye.y+bottom});
        coordinates.add(new double[]{eye.x+left, eye.y+up});
        coordinates.add(new double[]{eye.x+right, eye.y+up});

        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(1);
        indexes.add(0);
        indexes.add(3);
        indexes.add(0);
        indexes.add(2);

        quad.setCoordinates(coordinates);
        quad.setIndexes(indexes);
        quad.refreshCoordinates();
        quad.updateBuffers();

    }

    public PolygonGL getQuad() {
        return quad;
    }

}
