/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

/**
 * Created by mjlobo on 29/05/15.
 */
public class FrameBufferObject {
    private int vboiD;
    private Texture texture;

    public FrameBufferObject(int vboiD, Texture texture) {
        this.vboiD = vboiD;
        this.texture = texture;
    }

    public int getVboiD() {
        return vboiD;
    }

    public Texture getTexture() {
        return texture;
    }

    public void refreshTexture(double width, double height, double[] lowerCorner, double pixels_per_unit, double[] pixelsPerUnitArray) {
        texture.setWidth(width);
        texture.setHeight(height);
        texture.setLowerCorner(lowerCorner);
        texture.setPixels_per_unit(pixels_per_unit);
        texture.setPixels_per_unit_array(pixelsPerUnitArray);
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

}
