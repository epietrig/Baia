/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;
import com.jogamp.opengl.GL;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
//import fr.inria.ilda.ml.MapLayerViewer;
import poly2Tri.Triangulation;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by mjlobo on 08/03/15.
 */
public class PolygonGL extends ShapeGL {
    BufferGL vertexBufferGL;
    BufferGL textureBufferGL;
    BufferGL indexesBufferGL;
    float[] texCoords;
    protected Texture texture;
    protected boolean isComplete;
    protected Geometry parentGeometry;
    protected int textureID;
    FloatBuffer texBuffer;
    protected FloatBuffer blurredTexCoords;
    protected Texture blurredTexture;
    Geometry geometry;
    boolean isHole = false;




    public PolygonGL(List<double[]> coordinates, ArrayList<Integer> indexes) {
       this(coordinates, indexes, null , 0.0f);
    }

    public PolygonGL(List<double[]> coordinates) {
       this(coordinates, 0.0f);
    }

    public PolygonGL(List<double[]> coordinates, float z) {
        this(coordinates, null ,z);

    }

    public PolygonGL(Geometry parentGeometry) {
        this.parentGeometry = parentGeometry;
        coordinates.clear();



    }

    public PolygonGL(List<double[]> coordinates, ArrayList<Integer> indexes, Texture texture) {
        this(coordinates, indexes, texture, 0.0f);
    }

    public PolygonGL(List<double[]> coordinates, ArrayList<Integer> indexes, Texture texture, float z) {
        this.z = z;
        this.texture = texture;
        this.coordinates = (List<double[]>)((ArrayList<double[]>)coordinates).clone();
        this.indexes = indexes;
        buffers = new Hashtable<>();
        updateBuffers();
    }

    public PolygonGL(List<double[]> coordinates, Texture texture, float z) {
        this.z = z;
        this.texture = texture;
        this.coordinates = new ArrayList<>();
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        buffers = new Hashtable<>();
        indexes = new ArrayList<>();
        drawCompletePolygon();
        updateBuffers();

    }

    public PolygonGL(List<double[]> coordinates, Texture texture) {
        this(coordinates, texture, 0.0f);
    }

    public PolygonGL(PolygonGL polygonGL) {
        coordinates = new ArrayList<>();
        indexes = new ArrayList<>();
        for (double[] coord : polygonGL.getCoordinates()) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        for (Integer index : polygonGL.getIndexes()) {
            indexes.add(index);
        }
        buffers = new Hashtable<>();
        offseted = polygonGL.getOfseted();
        onlyInFirst = polygonGL.getOnlyInFirst();
        this.texture = polygonGL.getTexture();
        this.isHole = polygonGL.getIsHole();
        updateBuffers();
    }




    public void updateBuffers() {
        if (buffers.size() == 0) {
            vertexBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.VERTEX, BufferGL.DataType.FLOAT, 3);
            textureBufferGL = new BufferGL(GL.GL_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.TEXTURE, BufferGL.DataType.FLOAT, 2);
            indexesBufferGL = new BufferGL(GL.GL_ELEMENT_ARRAY_BUFFER, GL.GL_STATIC_DRAW, BufferGL.Type.INDEX, BufferGL.DataType.INT, 1);
        }
        buffers.put(vertexBufferGL, getVertices());
        updateTextureBuffer();
        buffers.put(indexesBufferGL, getIndex());
    }

    public void updateTextureBuffer() {
        if (blurredTexture != null) {
            if (blurredTexCoords == null) {
                calculateTextureCoords(blurredTexture);
                buffers.put(textureBufferGL, getBlurredTexCoords());
            }

        } else {
            if (texBuffer == null) {
                buffers.put(textureBufferGL, getTextureCoords());
            }
            else {
                buffers.put(textureBufferGL, texBuffer);
            }
        }
    }

    public FloatBuffer getTextureCoords() {
        float X=(float)texture.getLowerCorner()[0];
        float Y=(float)texture.getLowerCorner()[1];
        texCoords = new float[2*vertices.length/3];
        int j=0;
        for(int i=0; i<texCoords.length; i++){
            texCoords[i] = (float)((vertices[j]-X)/texture.getWidth());
            texCoords[i+1]=(float)((vertices[j+1]-Y)/texture.getHeight());
            i++;
            j+=3;
        }
        FloatBuffer texBuffer = FloatBuffer.wrap(texCoords);
        return texBuffer;

    }

    public FloatBuffer getBlurredTexCoords() {
        return blurredTexCoords;
    }

    public void calculateTextureCoords(Texture texture) {
        float X=(float)texture.getLowerCorner()[0];
        float Y=(float)texture.getLowerCorner()[1];
        texCoords = new float[2*vertices.length/3];
        int j=0;
        for(int i=0; i<texCoords.length; i++){
            texCoords[i] = (float)((vertices[j]-X)/texture.getWidth());
            texCoords[i+1]=(float)((vertices[j+1]-Y)/texture.getHeight());
            i++;
            j+=3;
        }
        blurredTexCoords= FloatBuffer.wrap(texCoords);
    }

    public void setTextureID(int textureID) {
        this.textureID = textureID;
        texture.setID(textureID);
    }

    public int getTextureID() {
        return textureID;
    }

    public void calculatePolygon(){

        if (!isCCW()) {
            Collections.reverse(coordinates);
        }

        coordinates.remove(coordinates.size()-1);
        double [][] verticesArray = new double[coordinates.size()][2];
        for (int i=0; i<coordinates.size(); i++){
            verticesArray[i][0] = coordinates.get(i)[0];
            verticesArray[i][1] = coordinates.get(i)[1];
        }
        ArrayList triangles = new ArrayList();
        int [] numVerticesinContures = new int []{coordinates.size()};
        Triangulation.debug =true;
        try {
            triangles = Triangulation.triangulate(1, numVerticesinContures, verticesArray);
        }
        catch(Exception e) {
            e.printStackTrace();
            //System.out.println("problem! ");
            printCoordinates();
        }
        ArrayList t;
        indexes = new ArrayList<Integer>();
        for (int i=0; i<triangles.size(); i++){
            t = (ArrayList)triangles.get(i);
            for (int j=0; j<3; j++) {
                indexes.add((int)((ArrayList)t).get(j));
            }
        }

    }

    public int getIndexOfCoordinate(double[] coord, float[][] vertices) {
        for (int i=0; i<coordinates.size(); i++) {
            if (vertices[i][0] == coord[0] && vertices[i][1]==coord[1]) {
                return i;
            }
        }
        return -1;
    }


    public void drawCompletePolygon(){

        Coordinate[] coordsJTS = new Coordinate[coordinates.size()+1];
        ArrayList <Coordinate> intersections = new ArrayList<>();
        for(int i=0; i<coordinates.size();i++) {
            coordsJTS[i] = new Coordinate(coordinates.get(i)[0], coordinates.get(i)[1]);
        }
        coordsJTS[coordsJTS.length-1] = new Coordinate(coordsJTS[0].x, coordsJTS[0].y);
        GeometryFactory geometryFactory = new GeometryFactory();
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordsJTS);
        Polygon polygon = new Polygon(new LinearRing(coordinateArraySequence,geometryFactory),null,geometryFactory);
        if (!polygon.isSimple()) {
            Geometry simplified = polygon.buffer(0);
            coordinates.clear();
            for (Coordinate coord : simplified.getCoordinates()) {
                coordinates.add(new double[]{coord.x, coord.y});
            }
            if (!isCCW()) {
                Collections.reverse(coordinates);
            }


        }
        calculatePolygon();
        isComplete = true;
    }

    public void calculateGeometry () {
        GeometryFactory geometryFactory = new GeometryFactory();
        Coordinate[] coordinatesArray = new Coordinate[coordinates.size()+1];
        for (int i=0; i<coordinatesArray.length-1; i++) {
            coordinatesArray[i] = new Coordinate(coordinates.get(i)[0], coordinates.get(i)[1]);
        }
        coordinatesArray[coordinatesArray.length-1]=coordinatesArray[0];
        CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinatesArray);
        LinearRing linearRing = new LinearRing(coordinateArraySequence, geometryFactory);
        geometry = new Polygon(linearRing, null, geometryFactory);
        geometry.buffer(0);
    }



    public boolean getIsComplete(){
        return isComplete;
    }

    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public boolean isDrawable () {
        if(coordinates.size()<3) {
            return false;
        }
        else {
            return true;
        }

    }

    public void offsetCoordinates() {
        super.offsetCoordinates();
        texCoords = new float[0];
        updateBuffers();
    }



    public ShaderManager.RenderedElementTypes getType() {
        return ShaderManager.RenderedElementTypes.TEXTURED;
    }


    public void addBuffer (int target, int usage, BufferGL.Type type, BufferGL.DataType datatype, int size, Buffer buffer) {
        BufferGL bufferGL = new BufferGL(target, usage, type, datatype, size);
        buffers.put(bufferGL,buffer);
    }

    public void updateBuffer(BufferGL bufferGL, Buffer buffer) {
        buffers.remove(bufferGL);
        buffers.put(bufferGL, buffer);
    }


    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public Texture getTexture() {
        return texture;
    }

    public void refreshCoordinates(List<double[]> coordinates) {
        super.refreshCoordinates();
        this.coordinates.clear();
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }


        drawCompletePolygon();
        updateBuffers();
    }

    public void refreshCoordinatesValues(List<double[]> coordinates) {
        super.refreshCoordinates();
        this.coordinates.clear();
        for (double[] coord : coordinates) {
            this.coordinates.add(new double[] {coord[0], coord[1]});
        }
        updateBuffers();

    }



    public void setTexCoords(ArrayList<float[]> texList) {
        texCoords = new float[texList.size()*2];
        int j=0;
        for (int i=0; i<texCoords.length; i++) {
            texCoords[i] = texList.get(j)[0];
            texCoords[i+1]=texList.get(j)[1];
            i++;
            j++;
        }
        texBuffer = FloatBuffer.wrap(texCoords);

    }

    public Texture getBlurredTexture() {
        return blurredTexture;
    }

    public void setBlurredTexture(Texture blurredTexture) {
        this.blurredTexture = blurredTexture;
        blurredTexCoords = null;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setIsHole(boolean isHole) {
        this.isHole = isHole;
    }

    public boolean getIsHole() {
        return isHole;
    }


}
