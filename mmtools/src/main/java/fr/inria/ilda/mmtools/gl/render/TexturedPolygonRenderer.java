/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.gl.*;

import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 25/04/16.
 */
public class TexturedPolygonRenderer extends AbstractPolygonGLRenderer {

    public TexturedPolygonRenderer(ShaderProgram program) {
        super(program);
    }

    @Override
    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Texture secondaryTexture, Vector3f color, LayerManager layerManager) {
        render(shapeGL, drawable, camera, ((PolygonGL)shapeGL).getTexture(), shapeGL.getAlpha(),false, camera.getEye(), camera.getCenter(), camera.getLeft(), camera.getRight(),camera.getBottom(), camera.getTop());
    }

    public void render(ShapeGL shapeGL, GLAutoDrawable drawable, Camera camera, Texture texture, float alpha, boolean docked, Vector3d eye, Vector3d center, double left, double right, double bottom, double top) {
        GL4 gl = (GL4)drawable.getGL();
        PolygonGL polygon = (PolygonGL)shapeGL;
        ShaderProgram textureProgram = program;
        int vertexAttributeId = textureProgram.getAttribute(ShaderConstants.POSITION);

        gl.glUseProgram(textureProgram.getProgramId());
        gl.glUniformMatrix4fv(textureProgram.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(eye, center), 0);
        System.out.println("textureProgram.getUniform(ShaderConstants.PROJ_MATRIX) "+textureProgram.getUniform(ShaderConstants.PROJ_MATRIX));
        gl.glUniformMatrix4fv(textureProgram.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(left, right, bottom, top), 0);
        gl.glUniform2f(textureProgram.getUniform("textureDimension"), (float)(texture.getWidth()), (float)(texture.getHeight()));
        gl.glUniform2f(textureProgram.getUniform("offset"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        if (docked) {
            gl.glUniform2f(textureProgram.getUniform("offsetTexture"),0.0f,0.0f);
        }
        else {
            gl.glUniform2f(textureProgram.getUniform("offsetTexture"), (float)polygon.getOffset()[0], (float)polygon.getOffset()[1]);
        }
        gl.glUniform2f(textureProgram.getUniform("textureLowerCorner"), (float)(texture.getLowerCorner()[0]), (float)(texture.getLowerCorner()[1]));
        gl.glUniform1f(textureProgram.getUniform("alpha"), alpha);

        BufferGL vertexBuffer = polygon.getBuffer(BufferGL.Type.VERTEX);
        if (vertexBuffer != null) {
            gl.glEnableVertexAttribArray(vertexAttributeId);
            gl.glBindBuffer(vertexBuffer.getTarget(), vertexBuffer.getBuffer());
            gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
            BufferGL indexBuffer = polygon.getBuffer(BufferGL.Type.INDEX);
            gl.glBindBuffer(indexBuffer.getTarget(), indexBuffer.getBuffer());
            gl.glActiveTexture(gl.GL_TEXTURE0);
            gl.glBindTexture(gl.GL_TEXTURE_2D, texture.getID());
            gl.glDrawElements(GL_TRIANGLES, polygon.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
        }
    }
}
