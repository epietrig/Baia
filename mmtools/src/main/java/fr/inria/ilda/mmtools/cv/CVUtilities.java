/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.cv;


import fr.inria.ilda.mmtools.geo.RGBRasterGeoElement;
import fr.inria.ilda.mmtools.geo.Tile;
import fr.inria.ilda.mmtools.magicWand.FloodFillFacade;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

import javax.media.jai.*;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.image.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.List;


/**
 * Created by mjlobo on 27/10/15.
 */

public class CVUtilities {

    public CVUtilities() {}

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        }

    public static Mat bufferedImageToMat(BufferedImage bufferedImage) {
        Mat mat = new Mat();
        if (bufferedImage.getRaster().getDataBuffer() instanceof DataBufferByte) {
            byte[] pixels = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
            mat = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8UC3);
            mat.put(0, 0, pixels);
        }
        else if (bufferedImage.getRaster().getDataBuffer() instanceof DataBufferInt) {
            int[] pixels = ((DataBufferInt)bufferedImage.getRaster().getDataBuffer()).getData();
            mat = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8U);
            mat.put(0, 0, pixels);
        }else if (bufferedImage.getRaster().getDataBuffer() instanceof java.awt.image.DataBufferFloat) {
            float[] pixels = ((java.awt.image.DataBufferFloat)bufferedImage.getRaster().getDataBuffer()).getData();
            mat = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_32FC3);
            mat.put(0,0,pixels);
            return mat;
        }


        List<Mat> rgbChannels = new ArrayList<>();
        Core.split(mat,rgbChannels);
        Collections.swap(rgbChannels,0,2);
        Core.merge(rgbChannels, mat);
        Core.flip(mat,mat,0);
        return mat;
    }

    public static Mat matFromFloatArray(float [] floatArray, int width, int height) {
        Mat mat = new Mat(height, width, CvType.CV_32F);
        mat.put(0,0,floatArray);
        flipMat(mat);
        Core.MinMaxLocResult minMaxLocResult = Core.minMaxLoc(mat);
        Mat sobel = sobelFilter(mat);
        return mat;
    }

    public static TiledImage matToJAI(Mat mat) {
        long count = mat.total()*mat.channels();
        List<Mat> mats = new ArrayList<>();
        Core.split(mat, mats);
        float[] floatBuffer = new float[(int)count];
        mat.get(0,0,floatBuffer);
        javax.media.jai.DataBufferFloat dbuffer = new javax.media.jai.DataBufferFloat(floatBuffer, mat.width()*mat.height()*3);

        int[] bandOffsets = new int[3];
        bandOffsets[0] = 0;
        bandOffsets[1] = 1;
        bandOffsets[2] = 2;
        PixelInterleavedSampleModel sm = new PixelInterleavedSampleModel(DataBuffer.TYPE_FLOAT, mat.width(), mat.height(), 3, 3*mat.width(), bandOffsets);


        float[] pixel = new float[1];
        ColorModel colorModel = PlanarImage.createColorModel(sm);
        WritableRaster raster = RasterFactory.createWritableRaster(sm,dbuffer, new java.awt.Point(0,0));
        TiledImage tiledImage = new TiledImage(0,0,mat.width(),mat.height(),0,0, sm,colorModel);
        tiledImage.setData(raster);
        JAI.create("filestore",tiledImage,"floatpattern.tif","TIFF");

        return tiledImage;


    }


    public static Mat dataBufferToMat(ByteBuffer buffer, int width, int height) {
        byte[] pixels = buffer.array();
        Mat mat = new Mat(height, width, CvType.CV_8UC4);
        mat.put(0,0,pixels);
        return  mat;
    }

    public static Mat renderedImageToMat (RenderedImage renderedImage) {
        int width = renderedImage.getWidth();
        int height = renderedImage.getHeight();
        byte[] pixels = ((DataBufferByte)renderedImage.getData().getDataBuffer()).getData();
        Mat mat = new Mat(height, width, CvType.CV_8UC4);
        mat.put(0,0,pixels);
        return mat;
    }

    public static BufferedImage mattoBufferedImage(Mat m){
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if ( m.channels() == 3 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        else if (m.channels()==4) {
            type = BufferedImage.TYPE_4BYTE_ABGR;
        }
        int bufferSize = m.channels()*m.cols()*m.rows();
        byte [] b = new byte[bufferSize];
        m.get(0,0,b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;

    }

    public static byte[] matToByteArrayBGR(Mat m) {
        int bufferSize = m.channels()*m.cols()*m.rows();
        Mat newMat = new Mat();
        List<Mat> rgbChannels = new ArrayList<>();
        Core.split(m,rgbChannels);
        Collections.swap(rgbChannels,0,2);
        Core.merge(rgbChannels, newMat);
        Core.flip(newMat,newMat,0);
        byte [] b = new byte[bufferSize];
        newMat.get(0, 0, b);
        return b;
    }

    public static byte[] matOneBandToByteArray(Mat m) {
        int bufferSize = m.channels()*m.cols()*m.rows();
        Mat newMat = new Mat();
        Core.flip(m,newMat,0);
        byte [] b = new byte[bufferSize];
        newMat.get(0, 0, b);
        return b;
    }

    public static byte[] matToByteArray(Mat m) {
        int bufferSize = m.channels()*m.cols()*m.rows();
        Mat newMat = new Mat();
        List<Mat> rgbChannels = new ArrayList<>();
        Core.split(m, rgbChannels);

        Core.flip(m,newMat,0);
        byte [] b = new byte[bufferSize];
        newMat.get(0, 0, b);
        return b;
    }

    public static float[] mat3CFToByteArray(Mat m) {
        int bufferSize = m.channels()*m.cols()*m.rows();
        Mat newMat = new Mat();
        Core.flip(m,newMat,0);
        float [] b = new float[bufferSize];
        newMat.get(0, 0, b);
        return b;
    }

    public static float[] bandToByteArray(Mat m) {
        int bufferSize = m.channels()*m.cols()*m.rows();
        Mat newMat = m.clone();
        float [] b = new float[bufferSize];
        newMat.get(0, 0, b);
        Mat testMat = new Mat(m.rows(), m.cols(), CvType.CV_32F);
        testMat.put(0,0,b);
        return b;
    }

    public static Mat grayScale (BufferedImage bufferedImage) {
        Mat image = bufferedImageToMat(bufferedImage);
        Mat grayImage = new Mat();
        Imgproc.cvtColor(image, grayImage, Imgproc.COLOR_RGBA2GRAY);
        return grayImage;

    }


    public static Mat boxFilter (BufferedImage bufferedImage) {
        Mat image = bufferedImageToMat(bufferedImage);
        Mat boxFilterImage = new Mat();
        Imgproc.boxFilter(image,boxFilterImage,-1,new Size(5,5));
        return boxFilterImage;
    }

    public static Mat floodFill (Mat mat, int x, int y, int upperDiff, int lowerDiff) {
        FloodFillFacade facade = new FloodFillFacade();
        return  facade.fill(mat, x, y, upperDiff, lowerDiff).submat(1,mat.rows()+1,1, mat.cols()+1);
    }

    public static Mat maskByDistance (Mat mat, Vector3f value, double threshDistance) {
        Mat dist = mat.clone();
        dist.setTo(new Scalar(value.x, value.y, value.z));
        Core.subtract(mat, dist, dist);
        Core.multiply(dist,dist,dist);
        List<Mat> channels = new ArrayList<>();
        Core.split(dist, channels);
        Mat result = new Mat(mat.size(), CvType.CV_32F);
        for (Mat channel: channels) {
            Core.add(channel, result, result);
        }
        Core.sqrt(result, result);
        Mat mask = new Mat(result.size(), CvType.CV_8U);
        Imgproc.threshold(result, mask, threshDistance, 1, Imgproc.THRESH_BINARY_INV);
        mask.convertTo(mask, CvType.CV_8U,255);

        return mask;
    }
    public static Mat grayScale (Mat image) {
        Mat grayImage = new Mat();
        if (image.type() == CvType.CV_8UC3) {
            Imgproc.cvtColor(image, grayImage, Imgproc.COLOR_RGB2GRAY);
            return grayImage;
        }
        else {
            return image;
        }
    }

    public static Mat RGBtoCIELab(Mat image) {
        Mat cielab = new Mat(image.rows(), image.cols(), CvType.CV_32FC3);
        if (image.type() == CvType.CV_8UC3) {
            Mat converted = new Mat();
            image.convertTo(converted, CvType.CV_32FC3,1.0/255.5);
            Imgproc.cvtColor(converted, cielab, Imgproc.COLOR_RGB2Lab);
            return cielab;

        }
        else {
            return image;
        }
    }

    public static Mat RGBtoHSV(Mat image) {
        Mat hsv = new Mat(image.rows(), image.cols(), CvType.CV_32FC3);
        if (image.type() == CvType.CV_8UC3) {
            Mat converted = new Mat();
            image.convertTo(converted, CvType.CV_32FC3,1.0/255.5);
            Imgproc.cvtColor(converted, hsv, Imgproc.COLOR_RGB2HSV);
            return hsv;
        }
        else {
            return image;
        }
    }

    public static List<MatOfPoint> findContours(Mat image) {
        List<MatOfPoint> points = new ArrayList<>();
        Imgproc.findContours(image,points,new Mat(),Imgproc.RETR_CCOMP,Imgproc.CHAIN_APPROX_NONE);
        return points;

    }

    public static Mat drawContours(Mat image, List<MatOfPoint> contours, int width) {
        Imgproc.drawContours(image, contours,0,new Scalar(255,0,0),width);
        return image;
    }

    public static Mat drawContours(Mat image, List<MatOfPoint> contours, int width, double[] color, double minArea) {
        for (int i=0; i<contours.size(); i++) {
            if (Imgproc.contourArea(contours.get(i))>minArea) {
                Imgproc.drawContours(image, contours, i, new Scalar(color), width);
            }
        }
        return image;
    }


    public static Mat cannyEdgeDetector (Mat image) {
        Mat result = new Mat();
        Imgproc.Canny(image, result, 300, 600, 5, true);
        return result;
    }

    public static Mat calculateDiff (Mat image1, Mat image2) {
        Mat result = new Mat();
        Core.bitwise_xor(image1, image2, result);
        return result;
    }

    public static void blobDetection (Mat image) {
        Mat greyImage = grayScale(image);
        FeatureDetector blobDetector = FeatureDetector.create(FeatureDetector.SIMPLEBLOB);
        MatOfKeyPoint keyPoints = new MatOfKeyPoint();
        blobDetector.detect(greyImage,keyPoints);
        Mat outImage = new Mat();
        Features2d.drawKeypoints(greyImage, keyPoints, outImage);
        Imgcodecs.imwrite("blob.png", outImage);

    }

    public static HashMap<Color, float[]> calculateHist (Mat image) {
        Mat redHist = new Mat();
        Mat greenHist = new Mat();
        Mat blueHist = new Mat();
        List<Mat> colorMats = new ArrayList<>();
        Core.split(image, colorMats);
        int histSizeNum = 256;
        MatOfInt mHistSize = new MatOfInt(histSizeNum);
        MatOfFloat mRanges = new MatOfFloat(0,256f);
        Mat mask = new Mat();
        //Mat hist = new Mat();

        if (image.channels()>3) {
            Imgproc.calcHist(Arrays.asList(colorMats.get(1)), new MatOfInt(0), mask, blueHist, mHistSize, mRanges);
            Core.normalize(blueHist, blueHist, 0, 1, Core.NORM_MINMAX, -1, new Mat());
            Imgproc.calcHist(Arrays.asList(colorMats.get(2)), new MatOfInt(0), mask, greenHist, mHistSize, mRanges);
            Core.normalize(greenHist, greenHist, 0, 1, Core.NORM_MINMAX, -1, new Mat());
            Imgproc.calcHist(Arrays.asList(colorMats.get(3)), new MatOfInt(0), mask, redHist, mHistSize, mRanges);
            Core.normalize(redHist, redHist, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        }
        else {
            //System.out.println("Hist = 3");
            Imgproc.calcHist(Arrays.asList(colorMats.get(0)), new MatOfInt(0), mask, blueHist, mHistSize, mRanges);
            Core.normalize(blueHist, blueHist, 0, 1, Core.NORM_MINMAX, -1, new Mat());
            Imgproc.calcHist(Arrays.asList(colorMats.get(1)), new MatOfInt(0), mask, greenHist, mHistSize, mRanges);
            Core.normalize(greenHist, greenHist, 0, 1, Core.NORM_MINMAX, -1, new Mat());
            Imgproc.calcHist(Arrays.asList(colorMats.get(2)), new MatOfInt(0), mask, redHist, mHistSize, mRanges);
            Core.normalize(redHist, redHist, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        }
        float[] redData = new float[histSizeNum];
        float[] blueData = new float[histSizeNum];
        float[] greenData = new float[histSizeNum];
        blueHist.get(0,0,blueData);
        redHist.get(0,0,redData);
        greenHist.get(0, 0, greenData);

        HashMap<Color, float[]> colorBands = new HashMap<>();
        colorBands.put(Color.BLUE,blueData);
        colorBands.put(Color.GREEN, greenData);
        colorBands.put(Color.RED, redData);
        return colorBands;
    }

    public static Mat equalizeIntensity (Mat image) {
        if (image.channels()==3) {
            Mat hsv = new Mat();
            Imgproc.cvtColor(image,hsv,Imgproc.COLOR_RGB2HSV);
            List <Mat> channels = new ArrayList<>();
            Core.split(hsv, channels);
            Imgproc.equalizeHist(channels.get(2), channels.get(2));

            Mat result = new Mat();
            Core.merge(channels,hsv);
            Imgproc.cvtColor(hsv, result, Imgproc.COLOR_HSV2RGB);


            return result;
        }
        else if (image.channels() == 1) {
            Imgproc.equalizeHist(image, image);
            return  image;
        }

        return new Mat();
    }

    public static Mat claheEqualization (Mat image) {
        CLAHE clahe =Imgproc.createCLAHE();
        Mat labImage = new Mat();
        Imgproc.cvtColor(image, labImage, Imgproc.COLOR_RGB2Lab);
        List<Mat> bands = new ArrayList<>();
        Core.split(labImage,bands);
        clahe.setClipLimit(4);
        Mat result = new Mat();
        clahe.apply(bands.get(0), bands.get(0));
        Core.merge(bands, result);

        Mat claheImage = new Mat();
        Imgproc.cvtColor(result, claheImage, Imgproc.COLOR_Lab2RGB);

        Mat greyClahe = grayScale(claheImage);
        Mat blurred = new Mat();
        Imgproc.blur(greyClahe, blurred, new Size(3,3));


        return claheImage;
    }

    public static Mat cornerDetector (Mat image) {
        Mat grey = grayScale(image);
        Mat outImage = new Mat();
        Imgproc.cornerHarris(grey, outImage, 2, 3, 0.04);
        return outImage;
    }

    public static Mat siftDetector(Mat image) {
        FeatureDetector siftDetector = FeatureDetector.create(FeatureDetector.SIFT);
        MatOfKeyPoint keyPoints = new MatOfKeyPoint();
        Mat greyImage = grayScale(image);
        siftDetector.detect(greyImage,keyPoints);
        Mat outImage = new Mat();
        Features2d.drawKeypoints(greyImage, keyPoints, outImage);
        return image;

    }

    public static Mat otsuThreshold (Mat image) {
        Mat outImage = new Mat();
        Mat greyImage = grayScale(image);
        Imgproc.threshold(greyImage,outImage,0,255, Imgproc.THRESH_BINARY_INV+ Imgproc.THRESH_OTSU);
        return outImage;
    }


    public static Mat watershedAlgorithm2(Mat image) {
        Mat kernel = new MatOfFloat(1,1,1,1,-8,1,1,1,1);

        // do the laplacian filtering as it is
        // well, we need to convert everything in something more deeper then CV_8U
        // because the kernel has some negative values,
        // and we can expect in general to have a Laplacian image with negative values
        // BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
        // so the possible negative number will be truncated
        Mat imgLaplacian = new Mat();
        Mat sharp = image; // copy source image to another temporary one
        Imgproc.filter2D(sharp, imgLaplacian, CvType.CV_32F, kernel);
        image.convertTo(sharp, CvType.CV_32F);
        Mat imgResult = new Mat();
        Core.subtract(sharp,imgLaplacian,imgResult);

        // convert back to 8bits gray scale
        imgResult.convertTo(imgResult, CvType.CV_8UC3);
        imgLaplacian.convertTo(imgLaplacian, CvType.CV_8UC3);

        image = imgResult; // copy back
        // Create binary image from source image
        Mat bw = new Mat();
        Imgproc.cvtColor(image, bw, Imgproc.COLOR_RGB2GRAY);
        Imgproc.threshold(bw, bw, 40, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Imgcodecs.imwrite("binary.png", bw);

        // Perform the distance transform algorithm
        Mat dist = new Mat();
        Imgproc.distanceTransform(bw, dist, Imgproc.DIST_L2, 5);
        Imgcodecs.imwrite("disttransform1.png", dist);
        // Normalize the distance image for range = {0.0, 1.0}
        // so we can visualize and threshold it
        Core.normalize(dist, dist, 0, 1., Core.NORM_MINMAX);
        Imgcodecs.imwrite("disttransform.png", dist);

        // Threshold to obtain the peaks
        // This will be the markers for the foreground objects
        Imgproc.threshold(dist, dist, .4, 1., Imgproc.THRESH_BINARY);

        // Dilate a bit the dist image
        Mat kernel1 = new Mat(3,3,CvType.CV_8UC1,new Scalar(1));
        Imgproc.dilate(dist, dist, kernel1);
        Imgcodecs.imwrite("Peaks.png", dist);

        // Create the CV_8U version of the distance image
        // It is needed for findContours()
        Mat dist_8u = new Mat();
        dist.convertTo(dist_8u, CvType.CV_8U);

        // Find total markers
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(dist_8u, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        // Create the marker image for the watershed algorithm
        Mat markers = new Mat(dist.size(), CvType.CV_32SC1, new Scalar(0));

        // Draw the foreground markers
        for (int i = 0; i < contours.size(); i++)
            Imgproc.drawContours(markers, contours, i, new Scalar(i + 1), -1);
        Imgcodecs.imwrite("contoursandmarkers.png", markers);

        // Perform the watershed algorithm
        Imgproc.watershed(image, markers);

        Mat mark = new Mat(3,3,CvType.CV_8UC1,new Scalar(0));
        markers.convertTo(mark, CvType.CV_8UC1);
        Core.bitwise_not(mark, mark);

        Mat dst = new Mat(markers.size(), CvType.CV_8UC3, new Scalar(0));
        List<double[]> colors = new ArrayList<>();
        for (int i=0; i<contours.size(); i++) {
            double r = Math.random()*255;
            double g = Math.random()*255;
            double b = Math.random()*255;
            colors.add(new double[] {r,g,b});
        }

        for (int i = 0; i < markers.rows(); i++)
        {
            for (int j = 0; j < markers.cols(); j++)
            {
                int index = (int)markers.get(i,j)[0];
                if (index > 0 && index <= contours.size()) {
                    dst.put(i,j,colors.get(index-1));
                }
                else{
                    dst.put(i,j,0,0,0);
                }
            }
        }


        Imgcodecs.imwrite("watershedimage.png", dst);

        return new Mat();

    }

    static public List<Tile> tileImage (RGBRasterGeoElement rgbRasterGeoElement, int maxTileSize) {
        Mat image = rgbRasterGeoElement.getMat();
        List<Tile> tiles = new ArrayList<>();
        if (image.width() < maxTileSize && image.height() < maxTileSize) {
            return null;
        }
        else {
            System.out.println("rows "+image.rows());
            System.out.println("cols "+image.cols());
            int nTilesHeight = image.rows()/maxTileSize;
            double nTilesWidthDouble = image.width()/maxTileSize;
            //
            int nTilesWidth = image.cols()/maxTileSize;
            double nTilesHeightDouble = image.height()/maxTileSize;
            System.out.println("nTilesHeight " + nTilesHeight +"double "+nTilesHeightDouble);
            System.out.println("nTilesWidth "+nTilesWidth +"double "+nTilesWidthDouble);
            int lastTileWidth = image.width()%maxTileSize;
            double lastTileWidthReal = lastTileWidth/rgbRasterGeoElement.getPixels_per_unit();
            System.out.println("lastTileWidth "+lastTileWidth);
            int lastTileHeight = image.height()%maxTileSize;
            double lastTileHeightReal = lastTileHeight/rgbRasterGeoElement.getPixels_per_unit();
            System.out.println("lastTileHeight "+lastTileHeight);

            double upperCornerX = rgbRasterGeoElement.getLowerCorner()[0];
            double upperCornerY = rgbRasterGeoElement.getLowerCorner()[1] + rgbRasterGeoElement.getRealHeight();
            double tileRealSize = maxTileSize/rgbRasterGeoElement.getPixels_per_unit();
            System.out.println("rgb real width "+rgbRasterGeoElement.getRealWidth());
            System.out.println("rgb x "+rgbRasterGeoElement.getX());
            System.out.println("rgb y "+rgbRasterGeoElement.getY());
            Rect roi;
            for (int i=0; i<nTilesWidth; i++) {
                for (int j=0; j<nTilesHeight; j++) {
                    roi = new Rect(i*maxTileSize, j*maxTileSize, maxTileSize, maxTileSize);
                    System.out.println("i*maxTileSize "+i*maxTileSize);
                    System.out.println("j*maxTileSize "+j*maxTileSize);
                    System.out.println("tile real size "+tileRealSize);

                    Mat tileMat = new Mat(image,roi);
                    Tile newTile = new Tile(upperCornerX+i*tileRealSize+tileRealSize/2, upperCornerY-j*tileRealSize-tileRealSize/2,maxTileSize, maxTileSize, tileMat,tileRealSize, tileRealSize, rgbRasterGeoElement.getPixels_per_unit());
                    tiles.add(newTile);
                    Imgcodecs.imwrite("tile_"+i+"_"+j+".png",tileMat);
                }
            }
            for (int i=0; i<nTilesWidth; i++) {
                roi = new Rect(i*maxTileSize, nTilesHeight*maxTileSize, maxTileSize, lastTileHeight);
                Mat tileMat = new Mat(image, roi);
                Tile newTile = new Tile(upperCornerX+i*tileRealSize+tileRealSize/2,upperCornerY-nTilesHeight*tileRealSize-lastTileHeightReal/2,maxTileSize, lastTileHeight,tileMat, tileRealSize, lastTileHeight/rgbRasterGeoElement.getPixels_per_unit(), rgbRasterGeoElement.getPixels_per_unit());
                tiles.add(newTile);
                //Imgcodecs.imwrite("tile_"+i+"_"+nTilesHeight+".png", tileMat);
            }
            for (int j=0; j<nTilesHeight; j++){
                roi = new Rect(nTilesWidth*maxTileSize, j*maxTileSize, lastTileWidth, maxTileSize);
                Mat tileMat = new Mat(image, roi);
                Tile newTile = new Tile(upperCornerX+nTilesWidth*tileRealSize+lastTileWidthReal/2, upperCornerY-j*tileRealSize-tileRealSize/2, lastTileWidth, maxTileSize, tileMat, lastTileWidth/rgbRasterGeoElement.getPixels_per_unit(), tileRealSize, rgbRasterGeoElement.getPixels_per_unit());
                tiles.add(newTile);
                //Imgcodecs.imwrite("tile_"+nTilesWidth+"_"+j+".png", tileMat);
            }

            //corner
            Rect cornerRoi = new Rect(nTilesWidth*maxTileSize, nTilesHeight*maxTileSize, lastTileWidth, lastTileHeight);
            Mat cornerMat = new Mat(image, cornerRoi);
            Tile newTile = new Tile(upperCornerX+nTilesWidth*tileRealSize+lastTileWidthReal/2,upperCornerY-nTilesHeight*tileRealSize-lastTileHeightReal/2, lastTileWidth, lastTileHeight, cornerMat, lastTileWidth/rgbRasterGeoElement.getPixels_per_unit(), lastTileHeight/rgbRasterGeoElement.getPixels_per_unit(),rgbRasterGeoElement.getPixels_per_unit());
            tiles.add(newTile);
            //Imgcodecs.imwrite("cornerMat.png",cornerMat);


            return tiles;

        }




    }

    //Code c++ from https://github.com/bytefish/opencv/blob/master/lbp/lbp.cpp
    static public Mat getELBP (Mat src, int radius, int neighbors) {
        neighbors = Math.max(Math.min(neighbors, 31),1);
        Mat dst = Mat.zeros(src.rows()-2*radius, src.cols()-2*radius, CvType.CV_32SC1);
        for (int n=0; n<neighbors; n++) {
            //sample points
            double x = (radius * Math.cos(2*Math.PI*n/neighbors));
            double y = (radius*-Math.sin(2*Math.PI*n/neighbors));
            //relative indices
            int fx = (int)Math.floor(x);
            int fy = (int)Math.floor(y);
            int cx = (int)Math.ceil(x);
            int cy = (int)Math.ceil(y);
            //fractional part
            double ty = y - fy;
            double tx = x-fx;
            //set interpolation wieghts
            double w1 = (1-tx) * (1-ty);
            double w2 = tx * (1-ty);
            double w3 = (1-tx) * ty;
            double w4 = tx * ty;
            //iterate through your data
            for (int i = radius; i<src.rows()-radius; i++) {
                for (int j=radius; j<src.cols()-radius; j++) {
                    double t = w1 * src.get(i+fy, j+fx)[0] + w2*src.get(i+fy, j+cx)[0] + w3*src.get(i+cy, j+fx)[0] + w4*src.get(i+cy, j+cx)[0];
                    if (t>src.get(i,j)[0]) {
                        double newValue = dst.get(i-radius,j-radius)[0];
                        dst.put(i - radius, j - radius,(int)newValue+1<<n);
                        System.out.println("new value "+((int)newValue+1<<n));
                    } else {
                        double newValue = dst.get(i-radius,j-radius)[0];
                        dst.put(i - radius, j - radius,(int)newValue+0<<n);
                    }
                }
            }


        }

        return dst;

    }

    static public Mat getHueLBP(Mat src, int radius, int p) {

        List<Mat> colorMats = new ArrayList<>();
        Core.split(src, colorMats);
        System.out.println("number of bands "+colorMats.size());
        colorMats.remove(0);
        Mat hue = new Mat();
        Imgproc.cvtColor(src, hue, Imgproc.COLOR_BGR2HSV);
        List<Mat> hsvList = new ArrayList<>();
        Core.split(hue,hsvList);
        return getELBPBand(hsvList.get(0),radius,p);
    }

    static public Mat getELBPBand (Mat src, int radius, int p) {
        p = Math.max(Math.min(p, 31),1);
        Mat dst = Mat.zeros(src.rows()-2*radius, src.cols()-2*radius, CvType.CV_32F);
        for (int i = radius; i<src.rows()-radius; i++) {
            for (int j = radius; j < src.cols() - radius; j++) {
                int sum = 0;
                double xfirst = radius * Math.cos(0);
                double yfirst = radius * Math.sin(0);
                double gfirst = bilinearInterpolation(xfirst, yfirst, i, j, src);
                double xlast = radius * Math.cos(2*Math.PI*(p-1)/p);
                double ylast = (radius*-Math.sin(2*Math.PI*(p-1)/p));
                double glast = bilinearInterpolation(xlast, ylast, i, j, src);
                double gc = src.get(i,j)[0];
                for (int n=0; n<p; n++) {
                    double x = (radius * Math.cos(2*Math.PI*n/p));
                    double y = (radius*-Math.sin(2*Math.PI*n/p));
                    //relative indices
                    double gp = bilinearInterpolation(x, y, i, j, src);

                    if (n>0) {
                        sum+=Math.abs(sign(gp-gc)-sign(glast-gc));
                    }
                    if (gp>src.get(i,j)[0]) {
                        double newValue = dst.get(i-radius,j-radius)[0];
                        dst.put(i - radius, j - radius,(int)newValue+(1<<n));
                        //System.out.println("new value "+((int)newValue+(1<<n)));
                    } else {
                        double newValue = dst.get(i-radius,j-radius)[0];
                        //System.out.println("new value "+((int)newValue+(0<<n)));
                        dst.put(i - radius, j - radius,(int)newValue+(0<<n));
                    }
                }
                sum += Math.abs(sign(glast-gc)-sign(gfirst-gc));
                if (sum>2) {
                    dst.put(i-radius, j-radius,p+1);
                }
                //System.out.println("sum "+sum);
            }
        }
        double min; double max;
        Core.MinMaxLocResult result = Core.minMaxLoc(dst);
        min = result.minVal;
        max = result.maxVal;
        Mat adjMap = new Mat(dst.rows(), dst.cols(), CvType.CV_32F);
        Core.convertScaleAbs(dst,adjMap,255/max,0);
        return dst;

    }


    static int sign (double a) {
        if (a>=0) return 1;
        else return 0;
    }

    static double bilinearInterpolation (double x, double y, int i, int j, Mat src) {
        int fx = (int)Math.floor(x);
        int fy = (int)Math.floor(y);
        int cx = (int)Math.ceil(x);
        int cy = (int)Math.ceil(y);
        //fractional part
        double ty = y - fy;
        double tx = x-fx;
        //set interpolation wieghts
        double w1 = (1-tx) * (1-ty);
        double w2 = tx * (1-ty);
        double w3 = (1-tx) * ty;
        double w4 = tx * ty;
        double gp = w1 * src.get(i+fy, j+fx)[0] + w2*src.get(i+fy, j+cx)[0] + w3*src.get(i+cy, j+fx)[0] + w4*src.get(i+cy, j+cx)[0];
        return gp;
    }

    public static double compareHistCHI (Mat hist1, Mat hist2) {
        Core.normalize(hist1, hist1, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        Core.normalize(hist2, hist2, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        return Imgproc.compareHist(hist1, hist2,Imgproc.CV_COMP_CHISQR);
    }

    static public Mat getELBP2 (Mat src, int radius, int p) {
        Mat grayImage = new Mat();
        Imgproc.cvtColor(src, grayImage, Imgproc.COLOR_RGB2GRAY);
        Imgcodecs.imwrite("grey.png",grayImage);
        src = grayImage;
        return getELBPBand(src, radius, p);
    }

    public static void writeImage(Mat mat, String path) {
        System.out.println("write image");
        Imgcodecs.imwrite(path,mat);
    }

    public static void flipMat (Mat mat) {
        Core.flip(mat,mat, 0);
    }

    public static Mat calculateBandHistMat(Mat image, double minVal, double maxVal) {
        Mat mask = new Mat();
        Mat hist = new Mat();
        //System.out.println("max value "+maxVal + "min value "+minVal);
        double histSizeNum = maxVal;

        MatOfInt mHistSize = new MatOfInt((int)histSizeNum);
        MatOfFloat mRanges = new MatOfFloat((float)minVal,(float)histSizeNum);
        Imgproc.calcHist(Arrays.asList(image), new MatOfInt(0), mask, hist, mHistSize, mRanges);
        return hist;
    }

    public static Mat calculateBandHistMat(Mat image) {
        Mat mask = new Mat();
        Mat hist = new Mat();
        Core.MinMaxLocResult result = Core.minMaxLoc(image);
        //System.out.println("max value "+result.maxVal + "min value "+result.minVal);
        double histSizeNum = result.maxVal;

        MatOfInt mHistSize = new MatOfInt((int)histSizeNum);
        MatOfFloat mRanges = new MatOfFloat((float)result.minVal,(float)histSizeNum);
        Imgproc.calcHist(Arrays.asList(image), new MatOfInt(0), mask, hist, mHistSize, mRanges);
        return hist;
    }

    public static Mat inRange(Vector3f startColor, Vector3f endColor, Mat image) {
        Mat result = new Mat();
        Core.inRange(image, new Scalar(startColor.x, startColor.y, startColor.z), new Scalar(endColor.x, endColor.y, endColor.z), result);
        Imgcodecs.imwrite("inRange.png", result);
        return result;
    }

    public static Mat inRange(double minValue, double maxValue, Mat image) {
        Mat result = new Mat();
        Core.inRange(image, new Scalar(minValue), new Scalar(maxValue), result);
        Imgcodecs.imwrite("inRange.png", result);
        return result;
    }

    public static Mat calculateDeltaE(Mat image, Vector3f baseColor) {
        Mat result = new Mat(image.rows(), image.cols(), CvType.CV_32F);
        Mat compare = new Mat(image.rows(), image.cols(),CvType.CV_32FC3,new Scalar(baseColor.x, baseColor.y, baseColor.z));
        Mat compareRGB = new Mat();
        Imgproc.cvtColor(compare,compareRGB,Imgproc.COLOR_Lab2RGB);
        //writeImage(compareRGB, "basecolor.png");
        Core.subtract(image, compare, result);
        //writeImage(result,"diff.png");
        Mat pow = Mat.zeros(image.rows(), image.cols(), CvType.CV_32FC3);
        Core.pow(result,2,pow);
        //writeImage(pow,"result.png");
        Mat sum = Mat.zeros(image.rows(), image.cols(), CvType.CV_32FC3);
        Mat sumTot = Mat.zeros(image.rows(), image.cols(), CvType.CV_32FC1);
        List<Mat> matsPow = new ArrayList<>();
        Core.split(pow,matsPow);
        Core.add(matsPow.get(0), matsPow.get(1), sum);
        Core.add(matsPow.get(2),sum,sumTot);
        //writeImage(sumTot, "sumtot.png");
        Mat norm = new Mat();
        Core.normalize(sum,norm,0,255,Core.NORM_MINMAX);
        //writeImage(norm, "sumtotnorm.png");
        return sumTot;
    }

    public static Mat matchHistogramsHSV (Mat baseImage, Mat targetImage) {
        Mat baseImageHSV = new Mat();
        Mat targetImageHSV = new Mat();
        //writeImage(baseImage, "baseimage.png");
        if (baseImage.channels()>3) {
            baseImageHSV = RGBtoHSV(baseImage);
        }
        else {
            Imgproc.cvtColor(baseImage, baseImageHSV, Imgproc.COLOR_RGB2HSV);
        }
        if (targetImage.channels()>3) {
            targetImageHSV = RGBtoHSV(targetImage);
        }
        else {
            Imgproc.cvtColor(targetImage, targetImageHSV, Imgproc.COLOR_RGB2HSV);
        }
        List<float[]> baseCdfs = calculateHistCDFHSV(baseImageHSV);
        List<float[]> targetCdfs = calculateHistCDFHSV(targetImageHSV);
        int [] lookUpTableH = createLookUpTable(baseCdfs.get(0), targetCdfs.get(0));
        int[] lookUpTableS = createLookUpTable(baseCdfs.get(1), targetCdfs.get(1));
        List<Mat> baseChannels = new ArrayList<>();
        Core.split(baseImageHSV, baseChannels);

        for (int i = 0; i<baseChannels.get(0).rows(); i++) {
            for (int j=0; j<baseChannels.get(0).cols(); j++) {
                Double value = baseChannels.get(0).get(i,j)[0];
                baseChannels.get(0).put(i,j,lookUpTableH[value.intValue()]);
            }
        }

        for (int i = 0; i<baseChannels.get(1).rows(); i++) {
            for (int j=0; j<baseChannels.get(1).cols(); j++) {
                Double value = baseChannels.get(1).get(i,j)[0];
                baseChannels.get(1).put(i,j,lookUpTableS[value.intValue()]);
            }
        }
        Mat result = new Mat();
        Core.merge(baseChannels, baseImageHSV);
        Imgproc.cvtColor(baseImageHSV, result, Imgproc.COLOR_HSV2RGB);
        //writeImage(targetImage, "targetimage.png");
        //writeImage(result, "baseimagematched.png");
        return result;
    }

    public static List <float[]> calculateHistCDFHSV (Mat image) {
        //Mat hsv = ABGRtoHSV(image);
        List <float[]> hists = calculateHistHSV(image);
        List<float[]> cdfs = calculateHistCDF(hists, image);
        return cdfs;
    }

    public static List <float[]> calculateHistHSV (Mat image) {
        List<Mat> channels = new ArrayList<>();
        Core.split(image, channels);
//        int histSizeH = 30;
//        int histSizeS = 32;
        int histSizeH = 180;
        int histSizeS = 256;
        MatOfInt mHistSizeH = new MatOfInt(histSizeH);
        MatOfInt mHistSizeS = new MatOfInt(histSizeS);
        MatOfFloat mRangesH = new MatOfFloat(0,180);
        MatOfFloat mRangesS = new MatOfFloat(0,256);
        Mat mask = new Mat();

        Mat hHist = new Mat();
        Mat sHist = new Mat();

        Imgproc.calcHist(Arrays.asList(channels.get(0)), new MatOfInt(0), mask, hHist, mHistSizeH, mRangesH);
        Imgproc.calcHist(Arrays.asList(channels.get(1)), new MatOfInt(0), mask, sHist, mHistSizeS, mRangesS);

        float[] hData = new float[histSizeH];
        float[] sData = new float[histSizeS];

        hHist.get(0,0,hData);
        sHist.get(0,0,sData);

        ArrayList<float[]> result = new ArrayList<>();
        result.add(hData);
        result.add(sData);

        return result;


    }

    public static List<float[]> calculateHistFloat(Mat image) {
        List<Mat> channels = new ArrayList<>();
        Core.split(image, channels);
        List<Double> mins = new ArrayList<>();
        List<Double> maxs = new ArrayList<>();
        double[] minmax;
        for (Mat channel : channels) {
            minmax = getMinMaxMat(channel);
            mins.add(minmax[0]);
            maxs.add(minmax[1]);
        }
        MatOfInt sizeMat;
        MatOfFloat range;
        Mat result;
        int size;
        float[] data;
        List<float[]> histResult= new ArrayList<>();
        for ( int i=0; i<channels.size(); i++) {
            size = maxs.get(i).intValue()-mins.get(i).intValue();
            sizeMat = new MatOfInt(size);
            range = new MatOfFloat(mins.get(i).floatValue(), maxs.get(i).floatValue());
            result = new Mat();
            Imgproc.calcHist(Arrays.asList(channels.get(0)), new MatOfInt(0), new Mat(),result, sizeMat, range);
            data = new float[size];
            result.get(0,0,data);
            histResult.add(data);
        }
        return histResult;
    }

    public static float[] cumulativeCountCut(float lowCut, float upCut, Mat image) {
        double[] minMax = getMinMaxMat(image);
        List<float[]> histCDF = calculateHistCDF(calculateHistFloat(image), image);
        int i=0;
        while (histCDF.get(0)[i]<lowCut) {
            i++;
        }
        float newMin = i+(float)minMax[0];
        i=histCDF.get(0).length-1;
        while (histCDF.get(0)[i]>upCut) {
            i--;
        }
        float newMax = i+(float)minMax[0];
        return new float[]{newMin, newMax};
    }

    //One channel mat
    public static void minMaxThresholdFloat(float min, float max, Mat image) {
        for (int i=0; i<image.rows(); i++) {
            for (int j=0; j<image.cols(); j++) {
                if (image.get(i,j)[0]<min) {
                    image.put(i,j,min);
                }
                if (image.get(i,j)[0]>max) {
                    image.put(i,j,max);
                }
            }
        }
        double[] minMax = getMinMaxMat(image);
    }

    public static List<float[]> calculateHistCDF (List<float[]> hists, Mat image) {
        List<float[]> result = new ArrayList<>();
        for (float[] hist : hists) {
            float[] cdf = new float[hist.length];
            cdf[0] = hist[0]/(image.cols()*image.rows());
            for (int i=1; i<hist.length; i++) {
                cdf[i] = cdf[i-1] + hist[i]/(image.cols()*image.rows());
            }
            result.add(cdf);
        }
        return  result;
    }

    public static int[] createLookUpTable (float[] baseHist, float[] targetHist) {
        int [] result = new int[baseHist.length];
        for (int j = 0; j<baseHist.length; j++) {
            float baseValue = baseHist[j];
            float min = Float.MAX_VALUE;
            int minIndex = -1;
            for (int i=0; i<targetHist.length; i++) {
                float targetValue = targetHist[i];
                if (Math.abs(targetValue-baseValue)<min) {
                    //System.out.println("Math.abs(targetValue-baseValue)<min" + Math.abs(targetValue-baseValue));
                    min = Math.abs(targetValue-baseValue);
                    minIndex = i;

                }
            }
            result[j] = minIndex;

        }

        return result;
    }

    public Mat difference (Mat m1, Mat m2) {
        Mat result = new Mat();
        Core.subtract(m1, m2, result);
        return result;
    }

    public static boolean isZero(Mat mat) {
        if (Core.countNonZero(mat)<1) {
            return true;
        }
        else {
            return false;
        }
    }

    public static Mat erode (Mat mat) {
        int erosionSize =1;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, new  Size(2*erosionSize + 1, 2*erosionSize+1));
        Mat result = new Mat();
        Imgproc.erode(mat,result, element);
        return result;
    }

    public static Mat dilate (Mat mat) {
        int dilationSize =1;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, new  Size(2*dilationSize + 1, 2*dilationSize+1));
        Mat result = new Mat();
        Imgproc.dilate(mat,result, element);
        return result;
    }

    public static Mat dilate(Mat mat, int iterations) {
        Mat result = new Mat();
        result = dilate(mat);
        for (int i=0; i<iterations-1; i++) {
            result = dilate(result);
        }
        return result;
    }

    public static Mat or(Mat m1, Mat m2) {
        Mat result = new Mat();
        Core.bitwise_or(m1, m2, result);
        return result;
    }

    public static Mat or(Mat m1, Mat m2, Mat mask) {
        Mat result = new Mat();
        Core.bitwise_or(m1, m2, result, mask);
        return result;
    }


    public static Mat and(Mat m1, Mat m2) {
        Mat result = new Mat();
        Core.bitwise_and(m1, m2, result);
        return result;
    }

    public static Mat drawRect(int[] p1, int[] p2, int color, Mat mat) {
        Imgproc.rectangle(mat,new Point(p1[0], p1[1]),new Point(p2[0], p2[1]),new Scalar(color),-1);
        return mat;
    }

    public static Mat drawLine(int[] p1, int[] p2, int color, Mat mat) {
        Imgproc.line(mat, new Point(p1[0], p1[1]), new Point(p2[0], p2[1]), new Scalar(color), 2);
        return mat;
    }

    public static Mat drawPoint(int[] p1, int color, Mat mat) {
        Imgproc.circle(mat,new Point(p1[0], p1[1]),5, new Scalar(255),-1);
        return mat;
    }

    public static Mat fillPolygon (List<Point> points, Mat mat, int color) {
        MatOfPoint matOfPoint = new MatOfPoint();
        matOfPoint.fromList(points);
        Imgproc.fillConvexPoly(mat,matOfPoint,new Scalar(color));
        return mat;
    }

    public static Mat bitwiseNot(Mat src) {
        Mat result = new Mat();
        Core.bitwise_not(src, result);
        return result;
    }

    public static Mat blur(Mat src) {
        Mat result = new Mat();
        Imgproc.blur(src, result, new Size(3,3) );
        return result;
    }

    public static Mat add (Mat m1, Mat m2) {
        Mat result = new Mat();
        Core.add(m1, m2, result);
        return result;
    }

    public static Mat substract(Mat m1, Mat m2) {
        Mat result = new Mat();
        Core.subtract(m1, m2, result);
        return result;
    }

    public static double[] getMinMaxMat (Mat mat) {
        Core.MinMaxLocResult minMaxLocResult = Core.minMaxLoc(mat);
        return new double[] {minMaxLocResult.minVal, minMaxLocResult.maxVal};
    }

    public static double[] getMinMaxMat(Mat mat, Mat mask) {
        Core.MinMaxLocResult minMaxLocResult = Core.minMaxLoc(mat,mask);
        return new double[] {minMaxLocResult.minVal, minMaxLocResult.maxVal};
    }

    //k = number of clusters
    public static Mat matchColorsFast(Mat target, Mat source, int k) {
        Mat sourceCieLab = RGBtoCIELab(source);
        Mat targetCieLab = RGBtoCIELab(target);



        List<Cluster> sourceClusters = clusterMat(sourceCieLab, k);
        List<Cluster> targetClusters = clusterMat(targetCieLab,k);

        Mat transfered = transfer(targetCieLab, sourceClusters, targetClusters);
        Imgproc.cvtColor(transfered,transfered,Imgproc.COLOR_Lab2RGB);
        transfered.convertTo(transfered, CvType.CV_8UC3,255);
        //writeImage(transfered, "transferedclusters.png");
        return  transfered;

    }

    public static List<Cluster> clusterMat (Mat cutout, int k) {
        Mat beforeTransfer = new Mat();
        Imgproc.cvtColor(cutout,beforeTransfer,Imgproc.COLOR_Lab2RGB);
        beforeTransfer.convertTo(beforeTransfer, CvType.CV_8UC3,255);
        //writeImage(beforeTransfer, cutout+"beforetransferclusters.png");
        List <Cluster> clusters = new ArrayList<>();
        Mat samples = cutout.reshape(1, cutout.cols() * cutout.rows());
        Mat samples32f = new Mat();
        samples.convertTo(samples32f, CvType.CV_32F);
        Mat labels = new Mat();
        TermCriteria criteria = new TermCriteria(TermCriteria.COUNT, 100, 2);
        Mat centers = new Mat();
        Core.kmeans(samples32f, k, labels, criteria, 3, Core.KMEANS_PP_CENTERS, centers);
        centers.reshape(3);
        HashMap<Integer, Mat> masks = new HashMap<>();
        List<HashMap<Integer, List<Float>>> clusterArrays = new ArrayList<>();
        for (int i=0; i<cutout.channels(); i++) {
            clusterArrays.add(new HashMap<Integer, List<Float>>());
        }
        for (int i=0; i<centers.rows(); i++) {
            double [] center = new double[3];
            for (int j=0; j<centers.cols(); j++) {
                //System.out.println("center i "+i +" j "+j+ ":"+centers.get(i,j)[0]);
                center[j] = centers.get(i,j)[0];
            }
            clusters.add(new Cluster(center,i,cutout.channels()));
            masks.put(i, Mat.zeros(cutout.size(), CvType.CV_32F));
        }
        int rows = 0;
        for(int y = 0; y < cutout.rows(); y++) {
            for (int x = 0; x < cutout.cols(); x++) {
                int label = (int)labels.get(rows,0)[0];
                masks.get(label).put(y,x,1.0f);
                for (int i=0; i<clusterArrays.size(); i++) {
                    if (!clusterArrays.get(i).containsKey(label)) {
                        clusterArrays.get(i).put(label, new ArrayList<Float>());
                    }
                    clusterArrays.get(i).get(label).add((float)cutout.get(y,x)[i]);
                }
                rows++;
            }
        }

        List<Mat> meanMats;
        List<HashMap<Integer, MatOfDouble>> means = new ArrayList<>();
        List<HashMap<Integer, MatOfDouble>> stdDevs = new ArrayList<>();
        for (int i =0; i<cutout.channels(); i++) {
            means.add(new HashMap<Integer, MatOfDouble>());
            stdDevs.add(new HashMap<Integer, MatOfDouble>());
        }
        for (int i = 0; i<clusters.size();i++) {
            meanMats = new ArrayList<>();
            for (int j=0; j<cutout.channels(); j++) {
                meanMats.add(new Mat(clusterArrays.get(j).get(i).size(),1, CvType.CV_32F));

                for (int l=0; l<clusterArrays.get(j).get(i).size(); l++) {
                    meanMats.get(j).put(l,0,clusterArrays.get(j).get(i).get(l).doubleValue());

                }
                MatOfDouble matMean= new MatOfDouble();
                MatOfDouble matStdDev = new MatOfDouble();
                Core.meanStdDev(meanMats.get(j),matMean, matStdDev);
                clusters.get(i).getMeans()[j] = matMean.get(0,0)[0];
                clusters.get(i).getStddevs()[j] = matStdDev.get(0,0)[0];
                means.get(j).put(i, matMean);
                stdDevs.get(j).put(i, matStdDev);
            }
            clusters.get(i).setMask(masks.get(i));

        }


        for (Cluster cluster: clusters) {
            Mat maskConverted = new Mat();
            cluster.getMask().convertTo(maskConverted, CvType.CV_8UC1,255);
            //writeImage(maskConverted, cluster.getMask()+"maskcluster"+cluster.getId()+".png");
        }

        return clusters;


    }


    public static Mat transfer(Mat sourceMap, List<Cluster> sourceClusters, List<Cluster> tarClusters) {
        HashMap<Cluster, Cluster> matched = new HashMap<>();
        for (Cluster tarCluster: tarClusters) {
            double min = Double.MAX_VALUE;
            Cluster minCluster=null;
            for (Cluster srcCluster: sourceClusters) {
                double dist = tarCluster.distance(srcCluster);
                if (dist<min) {
                    min = dist;
                    minCluster = srcCluster;
                }
                matched.put(tarCluster, minCluster);
            }
        }
        List<double[]> clusterScaled = new ArrayList<>();
        List<double[]> distances = new ArrayList<>();
        double[] clusterDistance = new double[sourceClusters.size()];
        double weight;
        double totaldistance=0;
        double otherdistance = 0;
        Mat transfer = new Mat(sourceMap.size(), sourceMap.type());
        double [] newValue = new double[sourceMap.channels()];
        for (int i=0; i<sourceMap.channels(); i++) {
            clusterScaled.add(new double[sourceClusters.size()]);
            distances.add(new double[sourceClusters.size()]);
        }
        for (int i=0; i<sourceMap.rows(); i++) {
            for (int j=0; j<sourceMap.cols(); j++) {
                double[] pixel = sourceMap.get(i,j);
                for (int cluster =0; cluster<tarClusters.size(); cluster++) {
                    Cluster srcCluster = matched.get(tarClusters.get(cluster));
                    for (int channel=0; channel<sourceMap.channels(); channel++) {
                        distances.get(channel)[cluster] = Math.pow(tarClusters.get(cluster).getStddevs()[channel]/tarClusters.get(cluster).distance(pixel[channel], channel),1);
                        clusterScaled.get(channel)[cluster] = pixel[channel]-tarClusters.get(cluster).getMeans()[channel];
                        clusterScaled.get(channel)[cluster] *= srcCluster.getStddevs()[channel]/tarClusters.get(cluster).getStddevs()[channel];
                        clusterScaled.get(channel)[cluster] += srcCluster.getMeans()[channel];

                    }
                    clusterDistance[cluster] = sourceClusters.get(cluster).distance(pixel);
                }

                for (int channel=0; channel<sourceMap.channels(); channel++) {
                    totaldistance = 0;
                    newValue[channel]=0;
                    for (int cluster=0; cluster<tarClusters.size(); cluster++) {
                        totaldistance+=distances.get(channel)[cluster];
                    }
                    for (int cluster=0; cluster<tarClusters.size(); cluster++) {
                        if (tarClusters.size()==1) {
                            if (tarClusters.get(cluster).getMask().get(i,j)[0]==1) {
                                newValue[channel] = clusterScaled.get(channel)[cluster];
                            }
                        }

                        else {
                            newValue[channel] += (clusterScaled.get(channel)[cluster] * distances.get(channel)[cluster])/totaldistance;
                        }

                    }
                }

                transfer.put(i,j,newValue);

            }
        }
        return transfer;
    }

    public static List<Mat> cluster(Mat cutout, int k) {
        Mat samples = cutout.reshape(1, cutout.cols() * cutout.rows());
        Mat samples32f = new Mat();
        samples.convertTo(samples32f, CvType.CV_32F);

        Mat labels = new Mat();
        TermCriteria criteria = new TermCriteria(TermCriteria.COUNT, 100, 1);
        Mat centers = new Mat();
        Core.kmeans(samples32f, k, labels, criteria, 1, Core.KMEANS_PP_CENTERS, centers);
        return showClusters(cutout, labels, centers);
    }

    private static List<Mat> showClusters (Mat cutout, Mat labels, Mat centers) {
        centers.reshape(3);

        List<Mat> clusters = new ArrayList<Mat>();
        List<HashMap<Integer, List<Integer>>> clusterArrays = new ArrayList<>();
        for (int i=0; i<cutout.channels(); i++) {
            clusterArrays.add(new HashMap<Integer, List<Integer>>());
        }
        for(int i = 0; i < centers.rows(); i++) {
            clusters.add(Mat.zeros(cutout.size(), cutout.type()));
        }

        Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
        for(int i = 0; i < centers.rows(); i++) counts.put(i, 0);

        int rows = 0;
        for(int y = 0; y < cutout.rows(); y++) {
            for(int x = 0; x < cutout.cols(); x++) {
                int label = (int)labels.get(rows, 0)[0];
                int r = (int)centers.get(label, 2)[0];
                int g = (int)centers.get(label, 1)[0];
                int b = (int)centers.get(label, 0)[0];
                counts.put(label, counts.get(label) + 1);
                clusters.get(label).put(y, x, b, g, r);

                for (int i=0; i<clusterArrays.size(); i++) {
                    if (!clusterArrays.get(i).containsKey(label)) {
                        clusterArrays.get(i).put(label, new ArrayList<Integer>());
                    }
                    clusterArrays.get(i).get(label).add((int)cutout.get(y,x)[i]);
                }
                rows++;
            }
        }
        List<Mat> meanMats;
        List<HashMap<Integer, MatOfDouble>> means = new ArrayList<>();
        List<HashMap<Integer, MatOfDouble>> stdDevs = new ArrayList<>();
        for (int i =0; i<cutout.channels(); i++) {
            means.add(new HashMap<Integer, MatOfDouble>());
            stdDevs.add(new HashMap<Integer, MatOfDouble>());
        }
        for (int i = 0; i<counts.size();i++) {
            meanMats = new ArrayList<>();
            for (int j=0; j<cutout.channels(); j++) {
                meanMats.add(new Mat(clusterArrays.get(j).get(i).size(),1, CvType.CV_32F));
                for (int k=0; k<clusterArrays.get(j).get(i).size(); k++) {
                    meanMats.get(j).put(k,0,clusterArrays.get(j).get(i).get(k).doubleValue());

                }

                MatOfDouble matMean= new MatOfDouble();
                MatOfDouble matStdDev = new MatOfDouble();
                Core.meanStdDev(meanMats.get(j),matMean, matStdDev);
                means.get(j).put(i, matMean);
                stdDevs.get(j).put(i, matStdDev);
            }

        }
        return clusters;
    }


    public static Mat mapGreyToRGB(Mat greyImage, Mat rgbImage) {
        writeImage(greyImage, "greyImage.png");
        writeImage(rgbImage, "rgbImage.png");
        int gridWidth = 200;
        int gridHeight = 200;
        Mat labImage = RGBtoCIELab(rgbImage);
        List<Mat> channels = new ArrayList<>();
        Core.split(labImage, channels);
        MatOfDouble sourceMeanMat = new MatOfDouble();
        MatOfDouble sourceStdDevMat = new MatOfDouble();
        MatOfDouble targetMeanMat = new MatOfDouble();
        MatOfDouble targetStdDevMat = new MatOfDouble();
        Core.meanStdDev(channels.get(0),sourceMeanMat, sourceStdDevMat);
        Core.meanStdDev(greyImage, targetMeanMat, targetStdDevMat);
        double sourceMean = sourceMeanMat.get(0,0)[0];
        double sourceStdDev = sourceStdDevMat.get(0,0)[0];
        double targetMean = targetMeanMat.get(0,0)[0];
        double targetStdDev = targetStdDevMat.get(0,0)[0];
        Mat mapped = greyImage.clone();
        Core.subtract(mapped, new Scalar(targetMean),mapped);
        Core.multiply(mapped, new Scalar(sourceStdDev/targetStdDev), mapped);
        Core.add(mapped, new Scalar(sourceMean), mapped);
        List <SamplePixel> samples = new ArrayList<>();
        int neighbSize = 5;
        Mat neighbourhood = new Mat(neighbSize, neighbSize, CvType.CV_32F);
        Mat result = new Mat(greyImage.size(), labImage.type());
        MatOfDouble neigbStdDev = new MatOfDouble();
        for (int i=0; i<gridWidth; i++) {
            for (int j=0; j<gridHeight; j++) {
                int row = rgbImage.rows()/gridHeight*i;
                int col = rgbImage.cols()/gridWidth*j;
                for (int ni = - neighbSize/2; ni<neighbSize/2+1; ni++) {
                    for (int nj=-neighbSize/2; nj<neighbSize/2+1; nj++) {
                        neighbourhood.put(ni+neighbSize/2,nj+neighbSize/2,labImage.get(ni,nj)[0]);
                    }
                }
                Core.meanStdDev(neighbourhood,new MatOfDouble(), neigbStdDev);
                samples.add(new SamplePixel(row, col, labImage.get(row,col),neigbStdDev.get(0,0)[0]));
            }
        }
        //System.out.println("finished calculating samples");
        double minValue = Double.MAX_VALUE;
        int minIndex = -1;
        for (int i=0; i<greyImage.rows(); i++) {
            for (int j=0; j<greyImage.cols(); j++) {
                for (int ni = - neighbSize/2; ni<neighbSize/2+1; ni++) {
                    for (int nj=-neighbSize/2; nj<neighbSize/2+1; nj++) {
                        neighbourhood.put(ni+neighbSize/2,nj+neighbSize/2,labImage.get(ni,nj)[0]);
                    }
                }
                //System.out.println("i: "+i + " j:"+j);
                Core.meanStdDev(neighbourhood,new MatOfDouble(), neigbStdDev);
                for (int s=0; s<samples.size(); s++) {
                    if (samples.get(s).getStdDev()*0.5+samples.get(s).getValues()[0]<minValue) {
                        minValue = samples.get(s).getStdDev()*0.5+samples.get(s).getValues()[0];
                        minIndex = s;
                    }
                }
                result.put(i,j,samples.get(minIndex).getValues());
            }
        }
        Imgproc.cvtColor(result,result,Imgproc.COLOR_Lab2RGB);
        result.convertTo(result, CvType.CV_8UC3,255);
        writeImage(result, "transferedgrey.png");

        return null;



    }

    public static Mat makeMaxMin(Mat mat, Mat mask) {
        Mat result = new Mat();
        Core.MinMaxLocResult minxmaxmask = Core.minMaxLoc(mask);
        Core.multiply(mat, new Scalar(-1),result);
        Core.MinMaxLocResult minMaxLocResult = Core.minMaxLoc(mat);
        Core.add(result, new Scalar(minMaxLocResult.maxVal),result);
        Mat maskedResult = Mat.zeros(mask.size(), result.type());
        result.copyTo(maskedResult, mask);
        return result;
    }

    public static Mat makeMaxMin(Mat mat) {
        //Mat mask = Mat.ones(mat.size(), mat.type());
        Mat mask = new Mat(mat.size(), CvType.CV_8U);
        mask.setTo(new Scalar(255));
        return makeMaxMin(mat, mask);
    }

    public static Mat sobelFilter (Mat mat) {
        Mat sobelResult = new Mat();
        Imgproc.Sobel(mat,sobelResult,CvType.CV_32F,1,0);
        Imgproc.Sobel(sobelResult,sobelResult,CvType.CV_32F,0,1);
        return sobelResult;
    }

    public static Mat drawPolygon(Mat img, List<Point> points, Vector3f color) {
        List<MatOfPoint> matOfPointsList = new ArrayList<>();
        MatOfPoint matOfPoints = new MatOfPoint();
        matOfPoints.fromList(points);
        matOfPointsList.add(matOfPoints);
        //writeImage(img, "drawnmaskscvbefore.png");
        Imgproc.fillPoly(img,matOfPointsList,new Scalar(color.x,color.y,color.z));
        //Imgproc.rectangle(img, new Point(100,100), new Point(200,200), new Scalar(255,255,255));
       // writeImage(img, "drawnmaskscvresult.png");
        return img;
    }

    public static Mat drawPolygon(Mat img, List<Point> points, int color) {
        List<MatOfPoint> matOfPointsList = new ArrayList<>();
        MatOfPoint matOfPoints = new MatOfPoint();
        matOfPoints.fromList(points);
        matOfPointsList.add(matOfPoints);
        //writeImage(img, "drawnmaskscvbefore.png");

        Imgproc.fillPoly(img,matOfPointsList,new Scalar(color));
        //Imgproc.rectangle(img, new Point(100,100), new Point(200,200), new Scalar(255,255,255));
        //writeImage(img, "drawnmaskscvresult.png");
        return img;
    }

    public static Mat loadImageFromFile (String path) {
        Mat result = Imgcodecs.imread(path);
        return result;
    }


    public static Mat thresholdToZero(Mat toThreshold, float thresholdValue) {
        Mat result = new Mat();
        Imgproc.threshold(toThreshold, result, thresholdValue, thresholdValue, Imgproc.THRESH_TOZERO);
        return result;
    }

    public static Mat thresholdToZeroInverted(Mat toThreshold, float thresholdValue) {
        Mat result = new Mat();
        Imgproc.threshold(toThreshold, result, thresholdValue, thresholdValue, Imgproc.THRESH_TOZERO_INV);
        return result;
    }

    public static List<Double[]> getCountour(Mat image, int index) {
        List<Double[]> contourPoints = new ArrayList<>();
        Mat grayImage1 = new Mat();
        if (image.channels()>=3) {
            Imgproc.cvtColor(image, grayImage1, Imgproc.COLOR_RGB2GRAY);
        } else {
            grayImage1 = image;
        }
        grayImage1.convertTo(grayImage1, CvType.CV_8UC1);
        Mat outImage = new Mat();
        List<MatOfPoint> contours = findContours(grayImage1);
        if (contours.size()>0) {
            Mat contour = contours.get(index);
            int length = contour.rows();
            for (int i=0; i<length; i++) {
                contourPoints.add(new Double[]{contour.get(i,0)[0], contour.get(i,0)[1]});
            }

        }
        Mat clone = image.clone();
        Imgproc.drawContours(clone,contours,0,new Scalar(255,0,0));
        Imgcodecs.imwrite("contours"+image+".png", clone);
        return contourPoints;
    }

    public static List<MatOfPoint> findContoursExternal(Mat image) {
        List<MatOfPoint> points = new ArrayList<>();
        Imgproc.findContours(image,points,new Mat(),Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE);
        return points;

    }

    public static Mat drawPolygonMask(List<int[]> coordinates, int rows, int cols) {
        Mat result = Mat.zeros(rows, cols, CvType.CV_8U);
        List<Point> points = new ArrayList<>();
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        for (int i = 0; i < coordinates.size(); i++) {
            //newPoints.add(new Double[]{stageMorphVectorAnimation.getStartBoundary().get(i)[0] * (1 - t) + stageMorphVectorAnimation.getEndBoundary().get(i)[0] * t, stageMorphVectorAnimation.getStartBoundary().get(i)[1] * (1 - t) + stageMorphVectorAnimation.getEndBoundary().get(i)[1] * t});
            points.add(new Point(coordinates.get(i)[0], coordinates.get(i)[1]));
        }
        Point[] pointsArray = new Point[points.size()];
        for (int i=0; i<points.size(); i++) {
            pointsArray[i] = points.get(i);
        }
        MatOfPoint matOfPoint = new MatOfPoint(pointsArray);
        matOfPoints.add(matOfPoint);
        Imgproc.fillPoly(result,matOfPoints,new Scalar(255));
        //Imgcodecs.imwrite("maskcontour.png", result);
        return result;


    }



















}
