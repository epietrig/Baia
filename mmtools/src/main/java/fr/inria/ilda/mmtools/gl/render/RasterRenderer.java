/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import fr.inria.ilda.mmtools.geo.GeoElement;
import fr.inria.ilda.mmtools.geo.LayerManager;
import fr.inria.ilda.mmtools.geo.RGBRasterGeoElement;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.gl.*;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

/**
 * Created by mjlobo on 25/04/16.
 */
public class RasterRenderer extends AbstractGeoRenderer {

    ShaderProgram program;

    public RasterRenderer(ShaderProgram program) {
       this.program = program;
    }

    @Override
    public void render(GeoElement ge, GLAutoDrawable drawable, Camera camera, LayerManager layerManager) {
            GL4 gl = (GL4) drawable.getGL();
            RasterGeoElement rasterGeoElement = (RasterGeoElement)ge;
            ShaderProgram raster = program;
            int vertexAttributeId = raster.getAttribute(ShaderConstants.POSITION);
            int textureAttributeId = raster.getAttribute(ShaderConstants.TEXTURE_POSITION);
            for (ShapeGL shape : ge.getShapes()) {

                if (!shape.getOfseted() && shape instanceof PolygonGL && shape.getVisible()) {
                    gl.glUseProgram(raster.getProgramId());
                    gl.glUniformMatrix4fv(raster.getUniform(ShaderConstants.MV_MATRIX), 1, false, camera.lookAt(), 0);
                    gl.glUniformMatrix4fv(raster.getUniform(ShaderConstants.PROJ_MATRIX), 1, false, camera.getProjectionMatrix(), 0);
                    gl.glUniform1f(raster.getUniform("alpha"), ((RasterGeoElement) ge).getAlpha());

                    BufferGL vertexBuffer = shape.getBuffer(BufferGL.Type.VERTEX);
                    BufferGL textureBuffer = shape.getBuffer(BufferGL.Type.TEXTURE);
                    BufferGL indexBuffer = shape.getBuffer(BufferGL.Type.INDEX);
                    if (vertexBuffer != null && layerManager.getBufferVbo(vertexBuffer)!=null && layerManager.getBufferVbo(indexBuffer) != null && layerManager.getBufferVbo(textureBuffer)!=null) {
                        gl.glEnableVertexAttribArray(vertexAttributeId);
                        gl.glBindBuffer(vertexBuffer.getTarget(), layerManager.getBufferVbo(vertexBuffer));
                        gl.glVertexAttribPointer(vertexAttributeId, vertexBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        gl.glEnableVertexAttribArray(textureAttributeId);
                        gl.glBindBuffer(textureBuffer.getTarget(), layerManager.getBufferVbo(textureBuffer));
                        gl.glVertexAttribPointer(textureAttributeId, textureBuffer.getSize(), GL.GL_FLOAT, false, 0, 0);
                        gl.glBindBuffer(indexBuffer.getTarget(), layerManager.getBufferVbo(indexBuffer));
                        gl.glActiveTexture(gl.GL_TEXTURE0);
                        gl.glBindTexture(gl.GL_TEXTURE_2D, layerManager.getTextureId(((PolygonGL) shape).getTexture()));
                        gl.glDrawElements(GL_TRIANGLES, shape.getIndex().array().length, gl.GL_UNSIGNED_INT, 0);
                    }


                }
            }
        }

    //}
}
