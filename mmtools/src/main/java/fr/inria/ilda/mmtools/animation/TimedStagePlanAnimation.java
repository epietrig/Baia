/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.animation;

/**
 * Created by mjlobo on 23/01/2017.
 */
public class TimedStagePlanAnimation {
    float startTime;
    float endTime;
    StagePlanAnimation animation;
    int level;

    public TimedStagePlanAnimation (float startTime, float endTime, StagePlanAnimation animation, int level) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.animation = animation;
        this.level = level;
    }

    public float getEndTime() {
        return startTime + animation.getDuration();
    }

    public float getStartTime() {
        return startTime;
    }

    public StagePlanAnimation getAnimation() {
        return animation;
    }

    public int getLevel() {
        return level;
    }

    public void setAnimation(StagePlanAnimation stagePlanAnimation) {
        this.animation = stagePlanAnimation;
    }

    public void setStartTime(float startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(float endTime) {
        this.endTime = endTime;
    }

    public void setLevel (int level) {
        this.level = level;
    }


}
