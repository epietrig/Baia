/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.gl;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.sun.media.jai.rmi.HashtableState;

import java.io.InputStream;
import java.util.Hashtable;

/**
 * Created by mjlobo on 20/04/16.
 */
public class ShaderManager {
    public enum RenderedElementTypes {TEXTURED, LINE, POINTS, DRAWN, SIMPLE, BLUR, RASTER, ALTITUDE, LINE_STRIP, FILLED, FILLED_TRANSLUCENT, TRACE, SIGMOID, COLOR_DIFF, SIMPLE_ANIMATION, MASK_ANIMATION, BACKGROUND_ANIMATION, DILATE, MASK_EROSION, MASK_DILATE, EROSION, DIRECTION, SEMANTIC_INC, SEMANTIC_DEC, SEMANTIC_DEC_MASK, SEMANTIC_INC_MASK, ANIMATION_PLAN, MASK};
    GL4 gl;
    Hashtable<RenderedElementTypes, ShaderProgram> programs;
    public ShaderManager(GL4 gl) {
        this.gl = gl;
        programs = new Hashtable<>();
    }

    public void initializeShaders (GLAutoDrawable drawable) {
        GL3 gl = (GL3) drawable.getGL();

        programs = new Hashtable<>();
        ShaderProgram textured = new ShaderProgram(GLUtils.createShaderPrograms(drawable,"shaders/basic_vertex.glsl", "shaders/basic_fragment.glsl"));
        textured.addSampler(ShaderConstants.SAMPLER);
        textured.addUniform(ShaderConstants.MV_MATRIX);
        textured.addAttribute(ShaderConstants.POSITION);
        textured.addAttribute(ShaderConstants.TEXTURE_POSITION);
        textured.addUniform(ShaderConstants.OFFSET);
        textured.addUniform(ShaderConstants.TEXTURE_DIMENSION);
        textured.addUniform(ShaderConstants.ALPHA);
        textured.addUniform(ShaderConstants.TEXTURE_LOWER_CORNER);
        textured.addUniform(ShaderConstants.OFFSET_TEXTURE);
        System.out.println("Program id in initialize shaders "+textured.getProgramId());
        programs.put(RenderedElementTypes.TEXTURED, textured);

        ShaderProgram lines = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/line_vertex.glsl", "shaders/line_fragment.glsl"));
        lines.addUniform(ShaderConstants.MV_MATRIX);
        lines.addUniform(ShaderConstants.PROJ_MATRIX);
        lines.addAttribute(ShaderConstants.POSITION);
        lines.addUniform(ShaderConstants.LINE_WIDTH);
        lines.addAttribute(ShaderConstants.NORMAL);
        lines.addUniform(ShaderConstants.OFFSET);
        programs.put(RenderedElementTypes.LINE, lines);

        ShaderProgram points = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/points_vertex.glsl", "shaders/points_fragment.glsl"));
        points.addUniform(ShaderConstants.MV_MATRIX);
        points.addUniform(ShaderConstants.PROJ_MATRIX);
        points.addAttribute(ShaderConstants.POSITION);
        points.addUniform(ShaderConstants.COLOR);
        programs.put(RenderedElementTypes.POINTS, points);

        ShaderProgram drawn = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/drawn_vertex.glsl", "shaders/drawn_fragment.glsl"));
        drawn.addSampler(ShaderConstants.SAMPLER);
        drawn.addUniform(ShaderConstants.MV_MATRIX);
        drawn.addUniform(ShaderConstants.PROJ_MATRIX);
        drawn.addUniform(ShaderConstants.OFFSET);
        drawn.addAttribute(ShaderConstants.POSITION);
        drawn.addAttribute(ShaderConstants.TEXTURE_POSITION);
        drawn.addUniform(ShaderConstants.TEXTURE_DIMENSION);
        drawn.addSampler(ShaderConstants.BLURRED_SAMPLER);
        drawn.addUniform(ShaderConstants.ALPHA);
        //change in shader!!
        drawn.addUniform(ShaderConstants.TEXTURE_LOWER_CORNER);
        programs.put(RenderedElementTypes.DRAWN, drawn);


        System.out.println("Initializing simple program..");
        ShaderProgram simpleTwoColor = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/simple_vertex.glsl", "shaders/simple_fragment.glsl"));
        simpleTwoColor.addUniform(ShaderConstants.MV_MATRIX);
        simpleTwoColor.addUniform(ShaderConstants.PROJ_MATRIX);
        simpleTwoColor.addUniform(ShaderConstants.OFFSET);
        simpleTwoColor.addAttribute(ShaderConstants.POSITION);
        programs.put(RenderedElementTypes.SIMPLE, simpleTwoColor);

        System.out.println("Initializing blur shader");
        ShaderProgram blurShader = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/blur_vertex.glsl", "shaders/blur_fragment.glsl"));
        blurShader.addUniform(ShaderConstants.MV_MATRIX);
        blurShader.addUniform(ShaderConstants.PROJ_MATRIX);
        blurShader.addUniform(ShaderConstants.BLUR_MULTIPLY_VEC);
        blurShader.addUniform(ShaderConstants.PIXEL_SIZE);
        blurShader.addUniform(ShaderConstants.BLUR_RADIUS);
        blurShader.addSampler(ShaderConstants.SAMPLER);
        blurShader.addAttribute(ShaderConstants.POSITION);
        blurShader.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.BLUR, blurShader);

        System.out.println("Initializing raster shader..");
        ShaderProgram rasterShader = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/raster_vertex.glsl", "shaders/raster_fragment.glsl"));
        System.out.println("raster program id "+rasterShader.getProgramId());
        rasterShader.addUniform(ShaderConstants.MV_MATRIX);
        rasterShader.addUniform(ShaderConstants.PROJ_MATRIX);
        rasterShader.addSampler(ShaderConstants.SAMPLER);
        rasterShader.addAttribute(ShaderConstants.POSITION);
        rasterShader.addAttribute(ShaderConstants.TEXTURE_POSITION);
        rasterShader.addUniform("alpha");
        programs.put(RenderedElementTypes.RASTER, rasterShader);

        System.out.println("Initializing altitude shader...");
        ShaderProgram altitudeShader = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/altitude_vertex.glsl","shaders/altitude_fragment.glsl"));
        altitudeShader.addUniform(ShaderConstants.MV_MATRIX);
        altitudeShader.addUniform( ShaderConstants.PROJ_MATRIX);
        altitudeShader.addUniform( ShaderConstants.SAMPLER);
        altitudeShader.addUniform( ShaderConstants.START_COLOR_RGB);
        altitudeShader.addUniform( ShaderConstants.END_COLOR_RGB);
        altitudeShader.addUniform( ShaderConstants.MIN);
        altitudeShader.addUniform( ShaderConstants.MAX);
        altitudeShader.addAttribute(ShaderConstants.POSITION);
        altitudeShader.addAttribute( ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.ALTITUDE, altitudeShader);

        System.out.println("Initializing line strip shader...");
        ShaderProgram linesStripShader = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/line_strip_vertex.glsl", "shaders/line_strip_fragment.glsl", "shaders/line_strip_geometry.glsl"));
        linesStripShader.addUniform(ShaderConstants.MV_MATRIX);
        linesStripShader.addUniform( ShaderConstants.PROJ_MATRIX);
        linesStripShader.addUniform( "THICKNESS");
        linesStripShader.addUniform( "WIN_SCALE");
        linesStripShader.addAttribute(ShaderConstants.POSITION);
        linesStripShader.addUniform(ShaderConstants.OFFSET);
        linesStripShader.addUniform( ShaderConstants.COLOR);
        linesStripShader.addUniform( ShaderConstants.ALPHA);
        programs.put(RenderedElementTypes.LINE_STRIP, linesStripShader);

        ShaderProgram fillColorShader = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/color_fill_vertex.glsl","shaders/color_fill_fragment.glsl"));
        fillColorShader.addUniform( ShaderConstants.MV_MATRIX);
        fillColorShader.addUniform( ShaderConstants.PROJ_MATRIX);
        fillColorShader.addAttribute(ShaderConstants.POSITION);
        fillColorShader.addUniform(ShaderConstants.OFFSET);
        fillColorShader.addUniform( ShaderConstants.COLOR);
        fillColorShader.addUniform( ShaderConstants.ALPHA);
        programs.put(RenderedElementTypes.FILLED, fillColorShader);

        ShaderProgram fillTranslucentColorShader = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/translucent_color_vertex.glsl","shaders/translucent_color_fragment.glsl"));
        fillTranslucentColorShader.addUniform( ShaderConstants.MV_MATRIX);
        fillTranslucentColorShader.addUniform( ShaderConstants.PROJ_MATRIX);
        fillTranslucentColorShader.addAttribute( ShaderConstants.POSITION);
        fillTranslucentColorShader.addAttribute( ShaderConstants.TEXTURE_POSITION);
        fillTranslucentColorShader.addUniform(ShaderConstants.OFFSET);
        fillTranslucentColorShader.addUniform( ShaderConstants.COLOR);
        fillTranslucentColorShader.addUniform( ShaderConstants.ALPHA);;
        fillTranslucentColorShader.addSampler( ShaderConstants.BLURRED_SAMPLER);
        programs.put(RenderedElementTypes.FILLED_TRANSLUCENT, fillTranslucentColorShader);


        System.out.println("initializing trace shader...");
        ShaderProgram traceProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/trace_vertex.glsl", "shaders/trace_fragment.glsl"));
        traceProgram.addUniform( ShaderConstants.MV_MATRIX);
        traceProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        traceProgram.addAttribute( ShaderConstants.POSITION);
        traceProgram.addUniform(ShaderConstants.OFFSET);
        traceProgram.addUniform( ShaderConstants.TEXTURE_DIMENSION);
        traceProgram.addUniform( ShaderConstants.TEXTURE_LOWER_CORNER);
        traceProgram.addSampler( ShaderConstants.SAMPLER);
        traceProgram.addUniform( ShaderConstants.COLOR);
        traceProgram.addUniform( ShaderConstants.ALPHA);
        programs.put(RenderedElementTypes.TRACE, traceProgram);

        System.out.println("initializing sigmoid interpolation shader..");
        ShaderProgram sigmoidProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/altitude_vertex.glsl", "shaders/sigmoid_fragment.glsl"));
        sigmoidProgram.addUniform( ShaderConstants.MV_MATRIX);
        sigmoidProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        sigmoidProgram.addSampler( ShaderConstants.SAMPLER);
        sigmoidProgram.addUniform( ShaderConstants.START_COLOR_RGB);
        sigmoidProgram.addUniform( ShaderConstants.END_COLOR_RGB);
        sigmoidProgram.addAttribute( ShaderConstants.POSITION);
        sigmoidProgram.addAttribute( ShaderConstants.TEXTURE_POSITION);
        sigmoidProgram.addUniform( ShaderConstants.MIN);
        sigmoidProgram.addUniform( ShaderConstants.MAX);
        programs.put(RenderedElementTypes.SIGMOID, sigmoidProgram);

        System.out.println("Initializing color diff shader..");
        ShaderProgram colordiff = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/colordiff_vertex.glsl", "shaders/colordiff_fragment.glsl"));
        colordiff.addUniform(ShaderConstants.MV_MATRIX);
        colordiff.addUniform(ShaderConstants.PROJ_MATRIX);
        colordiff.addSampler( ShaderConstants.SAMPLER);
        colordiff.addAttribute(ShaderConstants.POSITION);
        colordiff.addAttribute(ShaderConstants.TEXTURE_POSITION);
        colordiff.addUniform(ShaderConstants.ALPHA);
        colordiff.addUniform(ShaderConstants.COMPARE_COLOR);
        colordiff.addUniform(ShaderConstants.THRESHOLD);
        colordiff.addSampler(ShaderConstants.MASK);
        programs.put(RenderedElementTypes.COLOR_DIFF, colordiff);

        System.out.println("Initializing animation shader..");
        ShaderProgram animationProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/simple_animation_vertex.glsl", "shaders/simple_animation_fragment.glsl"));
        animationProgram.addUniform( ShaderConstants.MV_MATRIX);
        animationProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        animationProgram.addSampler( ShaderConstants.START_TEXTURE);
        animationProgram.addSampler( ShaderConstants.END_TEXTURE);
        animationProgram.addAttribute( ShaderConstants.POSITION);
        animationProgram.addAttribute( ShaderConstants.TEXTURE_POSITION);
        animationProgram.addUniform( ShaderConstants.DURATION);
        animationProgram.addUniform( ShaderConstants.TIC);
        programs.put(RenderedElementTypes.SIMPLE_ANIMATION, animationProgram);

        System.out.println("Initializing mask animation shader..");
        ShaderProgram maskAnimationProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/mask_animation_vertex.glsl", "shaders/mask_animation_fragment.glsl"));
        maskAnimationProgram.addUniform( ShaderConstants.MV_MATRIX);
        maskAnimationProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        maskAnimationProgram.addSampler( ShaderConstants.START_TEXTURE);
        maskAnimationProgram.addSampler( ShaderConstants.END_TEXTURE);
        maskAnimationProgram.addSampler( ShaderConstants.MASK_TEXTURE);
        maskAnimationProgram.addSampler( ShaderConstants.BG_TEXTURE);
        maskAnimationProgram.addAttribute( ShaderConstants.POSITION);
        maskAnimationProgram.addAttribute( ShaderConstants.TEXTURE_POSITION);
        maskAnimationProgram.addUniform( ShaderConstants.DURATION);
        maskAnimationProgram.addUniform( ShaderConstants.TIC);
        programs.put(RenderedElementTypes.MASK_ANIMATION, maskAnimationProgram);

        System.out.println("Initializing background animation shader..");
        ShaderProgram backgroundAnimationProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/background_animation_vertex.glsl", "shaders/background_animation_fragment.glsl"));
        backgroundAnimationProgram.addUniform( ShaderConstants.MV_MATRIX);
        backgroundAnimationProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        backgroundAnimationProgram.addSampler( ShaderConstants.START_TEXTURE);
        backgroundAnimationProgram.addSampler( ShaderConstants.END_TEXTURE);
        backgroundAnimationProgram.addSampler( ShaderConstants.MASK_TEXTURE);
        backgroundAnimationProgram.addSampler( ShaderConstants.IN_MASK_TEXTURE);
        backgroundAnimationProgram.addAttribute(ShaderConstants.POSITION);
        backgroundAnimationProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        backgroundAnimationProgram.addUniform( ShaderConstants.DURATION);
        backgroundAnimationProgram.addUniform( ShaderConstants.TIC);
        programs.put(RenderedElementTypes.BACKGROUND_ANIMATION, backgroundAnimationProgram);

        System.out.println("Initliazing dilation shader...");
        ShaderProgram dilateProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/dilate_vertex.glsl", "shaders/dilate_fragment.glsl"));
        dilateProgram.addUniform( ShaderConstants.MV_MATRIX);
        dilateProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        dilateProgram.addUniform( ShaderConstants.INIT_MASK);
        dilateProgram.addUniform( ShaderConstants.END_MASK);
        dilateProgram.addUniform( ShaderConstants.PIXEL_WIDTH);
        dilateProgram.addUniform( ShaderConstants.PIXEL_HEIGHT);
        dilateProgram.addAttribute(ShaderConstants.POSITION);
        dilateProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.DILATE, dilateProgram);

        System.out.println("Initliazing erosion shader...");
        ShaderProgram erosionProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/erosion_vertex.glsl", "shaders/erosion_fragment.glsl"));
        erosionProgram.addUniform( ShaderConstants.MV_MATRIX);
        erosionProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        erosionProgram.addSampler( ShaderConstants.INIT_MASK);
        erosionProgram.addSampler( ShaderConstants.END_MASK);
        erosionProgram.addUniform( ShaderConstants.PIXEL_WIDTH);
        erosionProgram.addUniform( ShaderConstants.PIXEL_HEIGHT);
        erosionProgram.addAttribute(ShaderConstants.POSITION);
        erosionProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.EROSION, erosionProgram);

        System.out.println("Initliazing mask erode shader...");
        ShaderProgram maskErodeProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/mask_erosion_vertex.glsl", "shaders/mask_erosion_fragment.glsl"));
        maskErodeProgram.addUniform( ShaderConstants.MV_MATRIX);
        maskErodeProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        maskErodeProgram.addSampler( ShaderConstants.MASK_TEXTURE);
        maskErodeProgram.addSampler( ShaderConstants.IN_MASK_TEXTURE);
        maskErodeProgram.addSampler( ShaderConstants.BG_TEXTURE);
        maskErodeProgram.addUniform( ShaderConstants.DURATION);
        maskErodeProgram.addUniform( ShaderConstants.TIC);
        maskErodeProgram.addSampler( ShaderConstants.END_MASK);
        maskErodeProgram.addAttribute(ShaderConstants.POSITION);
        maskErodeProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.MASK_EROSION, maskErodeProgram);

        System.out.println("Initliazing mask dilate shader...");
        ShaderProgram maskDilateProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/mask_dilation_vertex.glsl", "shaders/mask_dilation_fragment.glsl"));
        maskDilateProgram.addUniform( ShaderConstants.MV_MATRIX);
        maskDilateProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        maskDilateProgram.addSampler( ShaderConstants.MASK_TEXTURE);
        maskDilateProgram.addSampler( ShaderConstants.IN_MASK_TEXTURE);
        maskDilateProgram.addSampler( ShaderConstants.BG_TEXTURE);
        maskDilateProgram.addUniform( ShaderConstants.DURATION);
        maskDilateProgram.addUniform( ShaderConstants.TIC);
        maskDilateProgram.addSampler( ShaderConstants.END_MASK);
        maskDilateProgram.addSampler( ShaderConstants.INIT_MASK);
        maskDilateProgram.addAttribute(ShaderConstants.POSITION);
        maskDilateProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.MASK_DILATE, maskDilateProgram);
        //programs.put(RenderedElementTypes.BACKGROUND_ANIMATION, backgroundAnimationProgram);

        System.out.println("Initliazing mask direction shader...");
        ShaderProgram maskDirectionProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/mask_direction_vertex.glsl", "shaders/mask_direction_fragment.glsl"));
        maskDirectionProgram.addUniform( ShaderConstants.MV_MATRIX);
        maskDirectionProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        maskDirectionProgram.addSampler( ShaderConstants.MASK_TEXTURE);
        maskDirectionProgram.addSampler( ShaderConstants.IN_MASK_TEXTURE);
        maskDirectionProgram.addSampler( ShaderConstants.BG_TEXTURE);
        maskDirectionProgram.addUniform( ShaderConstants.DURATION);
        maskDirectionProgram.addUniform( ShaderConstants.TIC);
        maskDirectionProgram.addAttribute(ShaderConstants.POSITION);
        maskDirectionProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.DIRECTION, maskDirectionProgram);

        System.out.println("Initializing semantic increasing animation shader...");
        ShaderProgram semanticIncAnimationProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/semantic_animation_vertex.glsl", "shaders/semantic_animation_inc_fragment.glsl"));
        semanticIncAnimationProgram.addUniform( ShaderConstants.MV_MATRIX);
        semanticIncAnimationProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        semanticIncAnimationProgram.addUniform( ShaderConstants.INIT_TEXTURE);
        semanticIncAnimationProgram.addUniform( ShaderConstants.END_TEXTURE);
        semanticIncAnimationProgram.addSampler( ShaderConstants.SEMANTIC_TEXTURE);
        semanticIncAnimationProgram.addUniform( ShaderConstants.DURATION);
        semanticIncAnimationProgram.addUniform( ShaderConstants.TIC);
        semanticIncAnimationProgram.addUniform( ShaderConstants.MIN_VALUE);
        semanticIncAnimationProgram.addUniform( "maxValue");
        semanticIncAnimationProgram.addAttribute(ShaderConstants.POSITION);
        semanticIncAnimationProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.SEMANTIC_INC, semanticIncAnimationProgram);

        System.out.println("Initializing semantic decreasing animation shader...");
        ShaderProgram semanticDecAnimationProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/semantic_animation_vertex.glsl", "shaders/semantic_animation_dec_fragment.glsl"));
        semanticDecAnimationProgram.addUniform( ShaderConstants.MV_MATRIX);
        semanticDecAnimationProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        semanticDecAnimationProgram.addSampler( ShaderConstants.INIT_TEXTURE);
        semanticDecAnimationProgram.addSampler( ShaderConstants.END_TEXTURE);
        semanticDecAnimationProgram.addSampler( ShaderConstants.SEMANTIC_TEXTURE);
        semanticDecAnimationProgram.addUniform( ShaderConstants.DURATION);
        semanticDecAnimationProgram.addUniform( ShaderConstants.TIC);
        semanticDecAnimationProgram.addUniform( ShaderConstants.MIN_VALUE);
        semanticDecAnimationProgram.addUniform( ShaderConstants.MAX_VALUE);
        semanticDecAnimationProgram.addAttribute(ShaderConstants.POSITION);
        semanticDecAnimationProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.SEMANTIC_DEC, semanticDecAnimationProgram);

        System.out.println("Initializing semantic drecresagin mask animation shader...");
        ShaderProgram semanticDecAnimationMaskProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/semantic_animation_vertex.glsl", "shaders/semantic_animation_dec_mask_fragment.glsl"));
        semanticDecAnimationMaskProgram.addUniform( ShaderConstants.MV_MATRIX);
        semanticDecAnimationMaskProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        semanticDecAnimationMaskProgram.addSampler( ShaderConstants.INIT_TEXTURE);
        semanticDecAnimationMaskProgram.addSampler( ShaderConstants.END_TEXTURE);
        semanticDecAnimationMaskProgram.addSampler( ShaderConstants.SEMANTIC_TEXTURE);
        semanticDecAnimationMaskProgram.addUniform( ShaderConstants.DURATION);
        semanticDecAnimationMaskProgram.addUniform( ShaderConstants.TIC);
        semanticDecAnimationMaskProgram.addUniform( ShaderConstants.MIN_VALUE);
        semanticDecAnimationMaskProgram.addUniform( ShaderConstants.MAX_VALUE);
        semanticDecAnimationMaskProgram.addUniform( ShaderConstants.MASK);
        semanticDecAnimationMaskProgram.addAttribute( ShaderConstants.POSITION);
        semanticDecAnimationMaskProgram.addAttribute( ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.SEMANTIC_DEC_MASK, semanticDecAnimationMaskProgram);

        System.out.println("Initializing semantic increasing mask animation shader...");
        ShaderProgram semanticIncAnimationMaskProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/semantic_animation_vertex.glsl", "shaders/semantic_animation_inc_mask_fragment.glsl"));
        semanticIncAnimationMaskProgram.addUniform( ShaderConstants.MV_MATRIX);
        semanticIncAnimationMaskProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        semanticIncAnimationMaskProgram.addSampler( ShaderConstants.BACKGROUND_TEXTURE);
        semanticIncAnimationMaskProgram.addSampler( ShaderConstants.IN_MASK_TEXTURE);
        semanticIncAnimationMaskProgram.addSampler( ShaderConstants.SEMANTIC_TEXTURE);
        semanticIncAnimationMaskProgram.addUniform( ShaderConstants.DURATION);
        semanticIncAnimationMaskProgram.addUniform( ShaderConstants.TIC);
        semanticIncAnimationMaskProgram.addUniform( ShaderConstants.MIN_VALUE);
        semanticIncAnimationMaskProgram.addUniform( ShaderConstants.MAX_VALUE);
        semanticIncAnimationMaskProgram.addSampler( ShaderConstants.INIT_MASK);
        semanticIncAnimationMaskProgram.addSampler( ShaderConstants.END_MASK);
        semanticIncAnimationMaskProgram.addAttribute( ShaderConstants.POSITION);
        semanticIncAnimationMaskProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.SEMANTIC_INC_MASK, semanticIncAnimationMaskProgram);

        System.out.println("initializing animation plan shader...");
        ShaderProgram animationPlanProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/animation_plan_vertex.glsl", "shaders/animation_plan_fragment.glsl"));
        animationPlanProgram.addUniform( ShaderConstants.MV_MATRIX);
        animationPlanProgram.addUniform( ShaderConstants.PROJ_MATRIX);
        animationPlanProgram.addSampler(ShaderConstants.START_TEXTURE);
        animationPlanProgram.addSampler(ShaderConstants.END_TEXTURE);
        animationPlanProgram.addAttribute( ShaderConstants.POSITION);
        animationPlanProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        animationPlanProgram.addUniform( ShaderConstants.DURATION);
        animationPlanProgram.addUniform( ShaderConstants.TIC);
        animationPlanProgram.addSampler("planTexture");
        programs.put(RenderedElementTypes.ANIMATION_PLAN, animationPlanProgram);

        System.out.println("Initializing mask shader...");
        ShaderProgram maskProgram = new ShaderProgram(GLUtils.createShaderPrograms(drawable, "shaders/colordiff_vertex.glsl", "shaders/mask_fragment.glsl"));
        maskProgram.addUniform(ShaderConstants.MV_MATRIX);
        maskProgram.addUniform(ShaderConstants.PROJ_MATRIX);
        maskProgram.addSampler(ShaderConstants.SAMPLER);
        maskProgram.addUniform(ShaderConstants.ALPHA);
        maskProgram.addUniform(ShaderConstants.COLOR);
        maskProgram.addAttribute( ShaderConstants.POSITION);
        maskProgram.addAttribute(ShaderConstants.TEXTURE_POSITION);
        programs.put(RenderedElementTypes.MASK, maskProgram);

        for (ShaderProgram shaderProgram: programs.values()) {
            for (String uniformName : shaderProgram.getUniforms().keySet()) {
                shaderProgram.setUniformId(uniformName, gl.glGetUniformLocation(shaderProgram.getProgramId(), uniformName));
            }
            for (String attributeName: shaderProgram.getAttributes().keySet()) {
                shaderProgram.setAttributeId(attributeName, gl.glGetAttribLocation(shaderProgram.getProgramId(), attributeName));
            }
        }


    }

    public void addUniformToProgram(ShaderProgram shaderProgram, String uniformName) {
        shaderProgram.addUniform(gl.glGetUniformLocation(shaderProgram.getProgramId(), uniformName), uniformName);
    }

    public void addAttributeToProgram (ShaderProgram shaderProgram, String attributeName) {
        shaderProgram.addAttribute(gl.glGetAttribLocation(shaderProgram.getProgramId(), attributeName), attributeName);
    }

    public Hashtable<RenderedElementTypes,ShaderProgram> getPrograms() {
        return programs;
    }

}
