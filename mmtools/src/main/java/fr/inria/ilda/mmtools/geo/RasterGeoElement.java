/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.geo;

import com.vividsolutions.jts.geom.util.AffineTransformation;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.gl.PolygonGL;
import fr.inria.ilda.mmtools.gl.ShapeGL;
import fr.inria.ilda.mmtools.gl.Texture;
import org.geotools.coverage.grid.GridCoverage2D;
import org.opencv.core.Mat;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.util.ArrayList;

//import javax.media.opengl.GL;

/**
 * Created by mjlobo on 06/02/15.
 */
public class RasterGeoElement extends GeoElement {
    //pixel height and width
    int imageWidth;
    int imageHeight;
    //image heigh and width in geographical coordinates
    double realWidth;
    double realHeight;
    double [] lowerCorner;
    double pixels_per_unit;
    ArrayList<float[]> texCoords;
    String name;
    String path;
    float alpha=1.0f;
    Mat mat;
    double[] pixels_per_unit_array;
    Texture texture;


    PolygonGL polygon;
    CoordinateReferenceSystem coordinateReferenceSystem;


    public RasterGeoElement (GridCoverage2D coverage) {
        renderingType = ShaderManager.RenderedElementTypes.RASTER;
        fillColor = DEFAULT_FILL_COLOR;
        shapes = new ArrayList<>();
        texCoords = new ArrayList<>();
        imageWidth = coverage.getRenderedImage().getWidth();
        imageHeight = coverage.getRenderedImage().getHeight();
        double [] upperCorner = coverage.getEnvelope2D().getUpperCorner().getCoordinate();
        upperCorner[0] =Math.round(upperCorner[0]);
        upperCorner[1] =Math.round(upperCorner[1]);

        lowerCorner = coverage.getEnvelope2D().getLowerCorner().getCoordinate();
        lowerCorner[0] = Math.round(lowerCorner[0]);
        lowerCorner[1] = Math.round(lowerCorner[1]);

        realWidth = (upperCorner[0]-lowerCorner[0]);
        realHeight = (upperCorner[1]-lowerCorner[1]);


        x = (lowerCorner[0]+realWidth/2);
        y = (lowerCorner[1]+realHeight/2);



        calculateArrays();

        pixels_per_unit = imageWidth/realWidth;
        pixels_per_unit_array = new double[2];
        pixels_per_unit_array[0] = pixels_per_unit;
        pixels_per_unit_array[1] = imageHeight/realHeight;
        polygon = new PolygonGL(coordinates, indexesList, new Texture(this));
        polygon.setTexCoords(texCoords);
        shapes.add(polygon);
        calculateGeometry();
        coordinateReferenceSystem = coverage.getCoordinateReferenceSystem();


    }

    public RasterGeoElement (Mat mat) {
        renderingType = ShaderManager.RenderedElementTypes.TEXTURED;
        fillColor = DEFAULT_FILL_COLOR;
        shapes = new ArrayList<>();
        texCoords = new ArrayList<>();

        imageHeight = mat.rows();
        imageWidth = mat.cols();

        lowerCorner = new double[] {0,0};


        realWidth = imageWidth;
        realHeight = imageHeight;
        x = (lowerCorner[0]+realWidth/2);
        y = (lowerCorner[1]+realHeight/2);
        calculateArrays();
        pixels_per_unit = imageWidth/realWidth;
        pixels_per_unit_array = new double[2];
        pixels_per_unit_array[0] = pixels_per_unit;
        pixels_per_unit_array[1] = imageHeight/realHeight;
        polygon = new PolygonGL(coordinates, indexesList, new Texture(this));
        polygon.setTexCoords(texCoords);
        shapes.add(polygon);
        calculateGeometry();


    }

    public RasterGeoElement (Mat mat, ShaderManager.RenderedElementTypes renderingType) {
        this.renderingType = renderingType;
        fillColor = DEFAULT_FILL_COLOR;
        shapes = new ArrayList<>();
        texCoords = new ArrayList<>();

        imageHeight = mat.rows();
        imageWidth = mat.cols();

        lowerCorner = new double[] {0,0};


        realWidth = imageWidth;
        realHeight = imageHeight;
        x = (lowerCorner[0]+realWidth/2);
        y = (lowerCorner[1]+realHeight/2);
        calculateArrays();
        pixels_per_unit = imageWidth/realWidth;
        pixels_per_unit_array = new double[2];
        pixels_per_unit_array[0] = pixels_per_unit;
        pixels_per_unit_array[1] = imageHeight/realHeight;
        polygon = new PolygonGL(coordinates, indexesList, new Texture(this));
        polygon.setTexCoords(texCoords);
        shapes.add(polygon);
        calculateGeometry();


    }

    public RasterGeoElement (RasterGeoElement otherRaster) {
        renderingType = ShaderManager.RenderedElementTypes.RASTER;
        fillColor = DEFAULT_FILL_COLOR;
        shapes = new ArrayList<>();
        texCoords = new ArrayList<>();

        imageHeight = otherRaster.getImageHeight();
        imageWidth = otherRaster.getImageWidth();

        lowerCorner = new double[] {otherRaster.getLowerCorner()[0],otherRaster.getLowerCorner()[1]};


        realWidth = otherRaster.getRealWidth();
        realHeight = otherRaster.getRealHeight();
        x = (lowerCorner[0]+realWidth/2);
        y = (lowerCorner[1]+realHeight/2);
        calculateArrays();
        pixels_per_unit = imageWidth/realWidth;
        pixels_per_unit_array = new double[2];
        pixels_per_unit_array[0] = pixels_per_unit;
        pixels_per_unit_array[1] = imageHeight/realHeight;
        polygon = new PolygonGL(coordinates, indexesList, new Texture(this));
        //polygon = new PolygonGL(coordinates, indexesList, otherRaster.getTexture());
        polygon.setTexCoords(texCoords);
        shapes.add(polygon);
        calculateGeometry();
        setTexture(otherRaster.getTexture());
        coordinateReferenceSystem = otherRaster.getCoordinateReferenceSystem();
    }



    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }



    private void calculateArrays() {
        coordinates = new ArrayList<>();
        coordinates.add(new double[] {x+realWidth/2,y+realHeight/2});
        texCoords.add(new float[] {1.0f,1.0f});
        coordinates.add(new double[] {x-realWidth/2,y+realHeight/2});
        texCoords.add(new float[] {0.0f,1.0f});
        coordinates.add(new double[]{x-realWidth/2,y-realHeight/2});
        texCoords.add(new float[] {0.0f,0.0f});
        coordinates.add(new double[] {x+realWidth/2,y-realHeight/2});
        texCoords.add(new float[] {1.0f,0.0f});

        indexesList = new ArrayList<>();
        indexesList.add(0);
        indexesList.add(1);
        indexesList.add(2);
        indexesList.add(0);
        indexesList.add(2);
        indexesList.add(3);
    }

    public void setTextureID(int textureId) {
        polygon.setTextureID(textureId);
        //System.out.println("raster texture id "+polygon.getTextureID());
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
        polygon.setTexture(texture);
        texture.print();
    }



//    public Texture getTexture() {
//        return texture;
//    }

    public double getRealWidth() {
        //System.out.println("real width " + realWidth);
        return realWidth;
    }

    public double getRealHeight() {
        return realHeight;
    }

    public double[] getLowerCorner() {
        return  lowerCorner;
    }

    public double getPixels_per_unit() {
        return pixels_per_unit;
    }

    public Texture getTexture() {
        return polygon.getTexture();
    }


    @Override
    public void bufferPolygon(double bufferOffset) {

    }



    public void transformCoordinates(AffineTransformation affineTransformation) {
        //originalCoordinates = (ArrayList<double[]>)(coordinates.clone());
        super.copyInOriginalCoordinates();
        System.out.println("before");
        polygon.printCoordinates();
        super.transformCoordinates(affineTransformation);
        polygon.refreshCoordinates(coordinates);
        System.out.println("realWidth before"+realWidth);
        System.out.println("realHeight before"+realHeight);
        updateAttributes();
        System.out.println("realWidth "+realWidth);
        System.out.println("realHeight "+realHeight);
        System.out.println("pixels_per_unit "+pixels_per_unit);
        System.out.println("lowerCorner "+lowerCorner[0]+":"+lowerCorner[1]);
        updateTexture();

        System.out.println("after");
        polygon.printCoordinates();
    }

    void updateTexture () {
        polygon.getTexture().setHeight(realHeight);
        polygon.getTexture().setWidth(realWidth);
        polygon.getTexture().setLowerCorner(lowerCorner);
        polygon.getTexture().setPixels_per_unit(pixels_per_unit);
    }

    void updateAttributes () {
        realWidth = coordinates.get(0)[0]-coordinates.get(1)[0];
        realHeight = coordinates.get(1)[1]-coordinates.get(2)[1];
        lowerCorner = coordinates.get(2);
        pixels_per_unit = imageWidth/realWidth;
        x = lowerCorner[0]+realWidth/2;
        y = lowerCorner[1]+realHeight/2;
    }

    public void revertTransform() {
        polygon.printCoordinates();
        polygon.refreshCoordinates(originalCoordinates);
        polygon.printCoordinates();
        updateAttributes();
        updateTexture();
        calculateGeometry();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
        for (ShapeGL shape: shapes) {
            if (shape instanceof PolygonGL) {
                shape.setAlpha(alpha);
            }
        }
    }

    public float getAlpha() {
        return alpha;
    }

    public int[] getCoordinateInPx(double[] coord) {
        int x = (int) Math.round((coord[0] - lowerCorner[0])*pixels_per_unit_array[0]);
        int y = (int) Math.round((realHeight-coord[1] + lowerCorner[1])*pixels_per_unit_array[1]);

        return new int[]{x,y};
    }

    public double[] pxToCoordinate(int [] pixelCoords) {
        double x = pixelCoords[0]/pixels_per_unit_array[0]+lowerCorner[0];
        double y = realHeight+lowerCorner[1]-pixelCoords[1]/pixels_per_unit_array[1];
        return new double[] {x,y};
    }

    public PolygonGL getPolygon() {
        return polygon;
    }

    public Mat getMat() {
        return mat;
    }

    public double[] getPixels_per_unit_array() {
        return pixels_per_unit_array;
    }

    public CoordinateReferenceSystem getCoordinateReferenceSystem() {
        return  coordinateReferenceSystem;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }








}

