/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.mmtools.ui.queryWidgets;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;
import java.util.LinkedList;

public class AdaptiveSpinner extends JComponent {

	protected JFormattedTextField textfield;
	protected JButton plusButton;
	protected JButton minusButton;
	protected AdaptiveSpinnerModel model;
	
	public AdaptiveSpinner(AdaptiveSpinnerModel model) {
		super();
		this.textfield = new JFormattedTextField(new DecimalFormat(model.getFormat()));
		this.textfield.setMargin(new java.awt.Insets(1, 1, 1, 1));
		this.model = model;
		this.plusButton = new JButton(new ImageIcon("icons/up_arrow.png"));
		this.plusButton.setMargin(new java.awt.Insets(1, 1, 1, 1));
		this.minusButton = new JButton(new ImageIcon("icons/down_arrow.png"));
		this.minusButton.setMargin(new java.awt.Insets(1, 1, 1, 1));
		GridBagConstraints gbc = new GridBagConstraints();
		setLayout(new GridBagLayout());
		gbc.fill=GridBagConstraints.HORIZONTAL;
		gbc.anchor=GridBagConstraints.WEST;
		Utils.buildConstraints(gbc, 0, 0, 1, 1, 1, 1);
		add(this.textfield, gbc);
		gbc.fill=GridBagConstraints.NONE;
		gbc.anchor=GridBagConstraints.WEST;
		Utils.buildConstraints(gbc, 1, 0, 1, 1, 0, 0);
		add(this.plusButton, gbc);
		Utils.buildConstraints(gbc, 2, 0, 1, 1, 0, 0);
		add(this.minusButton, gbc);
		AdaptiveSpinner.this.textfield.setText(""+AdaptiveSpinner.this.model.getValue());
		this.textfield.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateModelFromTextField();
			}
		});
		this.textfield.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent e) {
				//System.out.println("hereeeeeeee focus lost");
				updateModelFromTextField();
			}
			public void focusGained(FocusEvent e) { }
		});
		this.textfield.setColumns(8);
		this.plusButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//updateModelFromTextField();
				AdaptiveSpinner.this.model.setValue(AdaptiveSpinner.this.model.getNextValue());
				AdaptiveSpinner.this.model.fireChangeEvent();
				//System.out.println("Adaptive spinner "+this);
			}
		});
		this.minusButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//updateModelFromTextField();
				AdaptiveSpinner.this.model.setValue(AdaptiveSpinner.this.model.getPreviousValue());
				AdaptiveSpinner.this.model.fireChangeEvent();

			}
		});
		this.model.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				//System.out.println("Adaptive spinner in state changed"+this);
				//System.out.println("Old text "+AdaptiveSpinner.this.textfield.getText());
				//System.out.println("Here in state changed! "+ AdaptiveSpinner.this.model.getValue());
				if (!AdaptiveSpinner.this.textfield.getText().equals(""+AdaptiveSpinner.this.model.getValue())) {
					//System.out.println("setting different value in text field!");
				}
				AdaptiveSpinner.this.textfield.setText(""+AdaptiveSpinner.this.model.getValue());

			}
		});
	}
	
	public Double getValue() {
		return model.getValue();
	}
	
	protected void setValue(Double value) {
		model.setValue(value);
		AdaptiveSpinner.this.textfield.setText(""+AdaptiveSpinner.this.model.getValue());
	}
	
	protected void updateModelFromTextField() {
		//System.out.println("updateModelFromTextField()");
		double d = 0;
		try {
			d = Double.parseDouble(this.textfield.getText());
		} catch(NumberFormatException nfe) {
			return;
		}
		if (AdaptiveSpinner.this.model.getValue() != d) {
			AdaptiveSpinner.this.model.setValue(d);
			AdaptiveSpinner.this.model.fireChangeEvent();
		}

	}

	public static void main(String[] args) {
		LinkedList<Double> values = new LinkedList<Double>();
		values.add(0.234);
		values.add(0.237);
		values.add(4.234);
		values.add(2.010);
		AdaptiveSpinnerModel spinnerModel = new AdaptiveSpinnerModel(values);
		AdaptiveSpinner adaptiveSpinner = new AdaptiveSpinner(spinnerModel);
		JFrame frame = new JFrame();
		frame.getContentPane().add(adaptiveSpinner);
		frame.pack();
		frame.setVisible(true);
		System.out.println(0.237-0.234);
	}

	public AdaptiveSpinnerModel getModel() {
		return model;
	}

	public void setModel(AdaptiveSpinnerModel model) {
		this.model = model;
	}

	public void setEnabled(boolean enabled) {
		if (enabled) {
			textfield.setEnabled(true);
			minusButton.setEnabled(true);
			plusButton.setEnabled(true);
		}
		else {
			textfield.setEnabled(false);
			minusButton.setEnabled(false);
			plusButton.setEnabled(false);
		}
	}

}
