/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410 core

in vec2 tc;
uniform float radius;
out vec4 fragColor;
uniform sampler2D s;
uniform sampler2D blurred;
uniform vec2 p1;
uniform vec2 p2;
uniform float alpha;
in vec2 texCoordFBO;

void main()
{

    fragColor = texture(blurred, texCoordFBO);
    fragColor.a = fragColor.r*alpha;
    fragColor.rgb = texture(s,tc).rgb;
    fragColor.a = fragColor.a*texture(s,tc).a;


}
