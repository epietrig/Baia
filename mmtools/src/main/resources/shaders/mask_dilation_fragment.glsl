/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

uniform sampler2D inMaskTexture;
uniform sampler2D bgTexture;
uniform sampler2D maskTexture;
uniform sampler2D endMask;
uniform sampler2D initMask;
out vec4 fragColor;
in vec2 tc;
in float tstep;


void main() {


     vec4 newColor = vec4(0.0,0.0,0.0,1.0);
     vec3 inMaskextureRGB = texture(inMaskTexture, tc).rgb;
     vec3 maskRGB = texture(maskTexture, tc).rgb;
     vec3 bgTextureRGB = texture(bgTexture,tc).rgb;
     vec3 endMaskRGB = texture(endMask,tc).rgb;
     vec3 initMaskRGB = texture(initMask, tc).rgb;


      newColor.r = (initMaskRGB.r*((1-tstep)*inMaskextureRGB.r+tstep*bgTextureRGB.r)+(1-initMaskRGB.r)*(maskRGB.r*bgTextureRGB.r+(1-maskRGB.r)*inMaskextureRGB.r)) * endMaskRGB.r+(1-endMaskRGB.r)*bgTextureRGB.r;
      newColor.g = (initMaskRGB.r*((1-tstep)*inMaskextureRGB.g+tstep*bgTextureRGB.g)+(1-initMaskRGB.r)*(maskRGB.r*bgTextureRGB.g+(1-maskRGB.r)*inMaskextureRGB.g)) * endMaskRGB.r+(1-endMaskRGB.r)*bgTextureRGB.g;
      newColor.b = (initMaskRGB.r*((1-tstep)*inMaskextureRGB.b+tstep*bgTextureRGB.b)+(1-initMaskRGB.r)*(maskRGB.r*bgTextureRGB.b+(1-maskRGB.r)*inMaskextureRGB.b)) * endMaskRGB.r+(1-endMaskRGB.r)*bgTextureRGB.b;
     newColor.a = 1.0;
     fragColor = newColor;


}
