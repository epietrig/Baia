/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

uniform sampler2D inMaskTexture;
uniform sampler2D bgTexture;
uniform sampler2D maskTexture;
uniform sampler2D endMask;
out vec4 fragColor;
in vec2 tc;
in float tstep;


void main() {


     vec4 newColor = vec4(0.0,0.0,0.0,1.0);
     vec3 inMaskTextureRGB = texture(inMaskTexture, tc).rgb;
     vec3 maskRGB = texture(maskTexture, tc).rgb;
     vec3 bgTextureRGB = texture(bgTexture,tc).rgb;
     vec3 endMaskRGB = texture(endMask,tc).rgb;
     newColor.r = (endMaskRGB.r*((1-tstep)*inMaskTextureRGB.r+tstep*bgTextureRGB.r)+(1-endMaskRGB.r)*inMaskTextureRGB.r) * maskRGB.r+(1-maskRGB.r)*bgTextureRGB.r;
     newColor.g = (endMaskRGB.r*((1-tstep)*inMaskTextureRGB.g+tstep*bgTextureRGB.g)+(1-endMaskRGB.r)*inMaskTextureRGB.g) * maskRGB.r+(1-maskRGB.r)*bgTextureRGB.g;
     newColor.b = (endMaskRGB.r*((1-tstep)*inMaskTextureRGB.b+tstep*bgTextureRGB.b)+(1-endMaskRGB.r)*inMaskTextureRGB.b) * maskRGB.r+(1-maskRGB.r)*bgTextureRGB.b;
     newColor.a = 1.0;
     fragColor = newColor;

}