/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

in vec2 tc;
out vec4 fragColor;
uniform sampler2D s;
uniform sampler2D mask;
uniform float alpha;
uniform vec3 compareColor;
uniform float threshold;

void main()
{
   fragColor = vec4(1.0,1.0,1.0,1.0);
   vec3 rgb = texture(s,tc).rgb;
   float dist = distance(rgb,compareColor);
   float distToMask = distance(fragColor.rgb,compareColor);
   float a=alpha;
   if (dist>threshold) {
    a = 0.0f;
   }
   if (texture(mask,tc).r == 1.0) {
        a = alpha;
   }
   fragColor.rgb = vec3(0.84,0.13,0.27);
   fragColor.a = a;

}