/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

in vec2 tc;
out vec4 fragColor;
uniform sampler2D init_mask;
uniform sampler2D end_mask;
uniform float pixel_width;
uniform float pixel_height;


void main()
{
    vec4 sampler[9];
    vec4 minValue = vec4(1.0);
    vec2 offset;

    for (int i = -1; i < 2; i++)
   {
       for (int j=-1; j<2; j++) {
         offset = vec2(i*pixel_width, j*pixel_height);
         sampler[i] = texture(init_mask, tc+offset);
         minValue = min(sampler[i], minValue);
     }
   }

        vec3 end_maskRGB = texture(end_mask,tc).rgb;
        vec4 finalColor = minValue;
        if (end_maskRGB == vec3(1.0)) {
            finalColor = vec4(1.0);
        }

      fragColor = finalColor;


}
