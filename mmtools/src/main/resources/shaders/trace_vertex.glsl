/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 330 core

in vec4 position;
in vec2 texPos;
out vec2 tc;
uniform mat4 mv_matrix;
uniform mat4 proj_matrix;
uniform vec2 offset;
uniform vec2 textureDimension;
uniform vec2 textureLowerCorner;

void main() {
    vec4 newpos = vec4(position.x + offset.x, position.y +offset.y, position.z,1.0);
    gl_Position = proj_matrix * mv_matrix * newpos;
    tc.x = (newpos.x-textureLowerCorner.x)/textureDimension.x;
    tc.y = (newpos.y-textureLowerCorner.y)/textureDimension.y;
}