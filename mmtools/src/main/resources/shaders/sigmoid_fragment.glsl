/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

in vec2 tc;
out vec4 fragColor;
uniform sampler2D s;
in vec4 color;
uniform vec3 startColorRGB;
uniform vec3 endColorRGB;
#define M_PI 3.1415926535897932384626433832795
uniform float min;
uniform float max;

float applySigmoid (float x) {

    float y = 1/(1+exp(-10*x+5));
    return y;
}

vec3 interpolateRGB(float color) {
     vec3 newColor;
     newColor.r = (1-color)*startColorRGB.r+color*endColorRGB.r;
     newColor.g = (1-color)*startColorRGB.g+color*endColorRGB.g;
     newColor.b = (1-color)*startColorRGB.b+color*endColorRGB.b;
     newColor.r = applySigmoid(newColor.r);
     newColor.g = applySigmoid(newColor.g);
     newColor.b = applySigmoid(newColor.b);

     return newColor;
}



void main() {
    vec3 textureColor = texture(s,tc).rgb;
    float color = (textureColor.r-min)/(max-min);
    fragColor.rgb = interpolateRGB(color);
    fragColor.a = 1.0;
}