/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

uniform sampler2D initTexture;
uniform sampler2D endTexture;
uniform sampler2D semanticTexture;
uniform float minValue;
uniform float maxValue;
out vec4 fragColor;
in vec2 tc;
in float tstep;



void main() {


     vec4 newColor = vec4(0.0,0.0,0.0,1.0);
     vec3 initTextureRGB = texture(initTexture, tc).rgb;
     vec3 endTextureRGB = texture(endTexture, tc).rgb;
     float semanticValue = texture(semanticTexture, tc).r;
     float threshold = minValue + (maxValue-minValue)*(1-tstep);
     if (semanticValue<threshold) {
        newColor.rgb = initTextureRGB;
     }
     else {
        newColor.rgb = endTextureRGB;
     }

     fragColor = newColor;

}
