/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 330 core


out vec4 fragColor;
uniform vec3 color;


void main() {
    fragColor = vec4(color.x,color.y,color.z,1.0);

//mapbox gl
 float u_blur = 0.2;
 float dist = length(gl_PointCoord - 0.5);
 float t = smoothstep(0.5, 0.5 - u_blur, dist);
 vec4 alphacolor = vec4(color.x, color.y, color.z,1.0);
 fragColor = alphacolor * t;

}