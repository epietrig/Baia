/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410 core

layout (triangles) in;
layout (triangle_strip) out;
layout (max_vertices = 3) out;
in vec2 tc[];
out vec2 texCoords;
in vec2 texCoordFBO[];
out vec2 texCoordsFBO;
out vec3 dist;
//from paper: http://orbit.dtu.dk/fedora/objects/orbit:55459/datastreams/file_3735323/content

void main(void)
{
    vec2 v0 = gl_in[2].gl_Position.xy - gl_in[1].gl_Position.xy;
    vec2 v1 = gl_in[2].gl_Position.xy - gl_in[0].gl_Position.xy;
    vec2 v2 = gl_in[1].gl_Position.xy - gl_in[0].gl_Position.xy;
    float area = abs(v1.x*v2.y - v1.y*v2.x);


        gl_Position = gl_in[0].gl_Position;
        texCoords = tc[0];
        dist = vec3(area/length(v0),0,0);
        texCoordsFBO = texCoordFBO[0];
        EmitVertex();

         gl_Position = gl_in[1].gl_Position;
         texCoords = tc[1];
         dist = vec3(0,area/length(v1),0);
         texCoordsFBO = texCoordFBO[1];
         EmitVertex();

         gl_Position = gl_in[2].gl_Position;
         texCoords = tc[2];
         dist = vec3(0,0,area/length(v2));
         texCoordsFBO = texCoordFBO[2];
         EmitVertex();

    EndPrimitive();



}