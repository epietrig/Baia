/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

uniform sampler2D startTexture;
uniform sampler2D endTexture;
uniform sampler2D bgTexture;
uniform sampler2D maskTexture;
in float tstep;
out vec4 fragColor;
in vec2 tc;


void main() {


     vec4 newColor = vec4(0.0,0.0,0.0,1.0);
     vec3 startTextureRGB = texture(startTexture, tc).rgb;
     vec3 endTextureRGB = texture(endTexture, tc).rgb;
     vec3 maskRGB = texture(maskTexture, tc).rgb;
     vec3 bgTextureRGB = texture(bgTexture,tc).rgb;
     newColor.r = ((1-tstep)*startTextureRGB.r+tstep*endTextureRGB.r) * maskRGB.r+(1-maskRGB.r)*bgTextureRGB.r;
     newColor.g = ((1-tstep)*startTextureRGB.g+tstep*endTextureRGB.g) * maskRGB.r+(1-maskRGB.r)*bgTextureRGB.g;
     newColor.b = ((1-tstep)*startTextureRGB.b+tstep*endTextureRGB.b) * maskRGB.r+(1-maskRGB.r)*bgTextureRGB.b;
     newColor.a = 1.0;
     fragColor = newColor;




}
