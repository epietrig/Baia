/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

out vec4 color;
uniform sampler2D s;
in vec2 tc;
uniform vec2 blurMultiplyVec;
uniform float pixel_size;
uniform float radius;


float pi = 3.14159265f;
float numBlurPixelsPerSide = 4.0f;


void main() {

  // Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
  //http://callumhay.blogspot.fr/2010/09/gaussian-blur-shader-glsl.html
  float sigma = 4.0;
  float blurSize = pixel_size;
  vec3 incrementalGaussian;
  incrementalGaussian.x = 1.0f / (sqrt(2.0f * pi) * sigma);
  incrementalGaussian.y = exp(-0.5f / (sigma * sigma));
  incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;

  vec4 avgValue = vec4(0.0f, 0.0f, 0.0f, 0.0f);
  float coefficientSum = 0.0f;

  // Take the central sample first...
  avgValue += texture(s,tc) * incrementalGaussian.x;
  coefficientSum += incrementalGaussian.x;
  incrementalGaussian.xy *= incrementalGaussian.yz;

  // Go through the remaining 8 vertical samples (4 on each side of the center)
  for (float i = 1.0f; i <= numBlurPixelsPerSide; i++) {
    avgValue += texture(s, tc - i * blurSize *
                          blurMultiplyVec) * incrementalGaussian.x;
    avgValue += texture(s, tc + i * blurSize *
                          blurMultiplyVec) * incrementalGaussian.x;
    coefficientSum += 2 * incrementalGaussian.x;
    incrementalGaussian.xy *= incrementalGaussian.yz;
  }

  color = avgValue / coefficientSum;
}
