/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

uniform sampler2D backgroundTexture;
uniform sampler2D inMaskTexture;
uniform sampler2D initMask;
uniform sampler2D endMask;
uniform sampler2D semanticTexture;
uniform float minValue;
uniform float maxValue;
out vec4 fragColor;
in vec2 tc;
in float tstep;

void main() {

    float semanticValue = texture(semanticTexture, tc).r;
    float threshold = minValue + (maxValue-minValue)*
    tstep;
    vec3 backgroundTextureRGB = texture(backgroundTexture, tc).rgb;
    vec3 inMaskTextureRGB = texture(inMaskTexture, tc).rgb;
    vec4 newColor = vec4(0.0,0.0,0.0,1.0);
    float intersection = texture(initMask, tc).r*texture(endMask, tc).r;
    if (semanticValue<=threshold) {
         newColor.rgb = backgroundTextureRGB;

    }
    else {
        if (semanticValue==maxValue) {
            newColor.rgb = ((1-tstep)*inMaskTextureRGB+tstep*backgroundTextureRGB);
        }
        else {
            newColor.rgb = inMaskTextureRGB;
        }

    }

    fragColor = newColor;

}
