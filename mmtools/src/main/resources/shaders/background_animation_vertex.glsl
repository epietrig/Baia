/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

in vec4 position;
in vec2 texPos;
out vec2 tc;
uniform mat4 mv_matrix;
uniform mat4 proj_matrix;
uniform float duration;
uniform float tic;
out float tstep;

void main() {

    gl_Position = proj_matrix * mv_matrix * position;
    tc = texPos;
    tstep = tic/duration;
}

