/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410

uniform sampler2D startTexture;
uniform sampler2D endTexture;
uniform sampler2D planTexture;
in float tstep;
out vec4 fragColor;
in vec2 tc;

void main() {

     vec4 newColor = vec4(0.0,0.0,0.0,1.0);
     vec3 initTextureRGB = texture(startTexture, tc).rgb;
     vec3 endTextureRGB = texture(endTexture, tc).rgb;
     vec2 planValue = texture(planTexture, tc).rg;

     if (tstep<planValue.r) {
        newColor.rgb = initTextureRGB;
     }
     else {
        if (tstep<planValue.g) {
            newColor.rgb = (tstep-planValue.r)/(planValue.g-planValue.r)*initTextureRGB + (1-(tstep-planValue.r)/(planValue.g-planValue.r)*endTextureRGB);
            newColor.rgb = (tstep-planValue.r)/(planValue.g-planValue.r)*endTextureRGB + (1-(tstep-planValue.r)/(planValue.g-planValue.r))*initTextureRGB;
        }
        else {
            newColor.rgb = endTextureRGB;
        }
     }

     fragColor = newColor;

}
