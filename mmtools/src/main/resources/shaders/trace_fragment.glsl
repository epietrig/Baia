/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
#version 410


in vec2 tc;
out vec4 fragColor;
uniform sampler2D s;
uniform float alpha;
uniform vec3 color;


void main()
{
    float r = max(fragColor.r, 1.0);
    vec4 texColor = texture(s,tc);
    fragColor = vec4(color,alpha*texColor.r);

}

