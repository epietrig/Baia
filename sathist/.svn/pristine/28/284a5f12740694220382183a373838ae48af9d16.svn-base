/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;


import fr.inria.ilda.mmtools.animation.*;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.geo.RasterGeoElement;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.mmtools.utilties.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.imageio.ImageIO;
import javax.tools.JavaCompiler;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.geom.Arc2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 24/03/2017.
 */
public class ProjectLoader {

    SatHistViewer satHistViewer;

    HashMap<String, LoadingAction> loadingActions;
    public ProjectLoader(SatHistViewer satHistViewer) {
        this.satHistViewer = satHistViewer;
        loadingActions = new HashMap<String, LoadingAction>();
        loadingActions.put(Constants.ANIMATION_PLAN_TAG, setPlanMatAction);
        loadingActions.put(Constants.END_MASK_TAG, setEndMaskMatAction);
        loadingActions.put(Constants.MASK_TAG, setMaskMatAction);
        loadingActions.put(Constants.INIT_MASK_TAG, setInitMaskMatAction);
        loadingActions.put(Constants.CENTER_TAG, setRadialCenter);
        loadingActions.put(Constants.FIRST_POINT_TAG, setInitPointDirection);
        loadingActions.put(Constants.END_POINT_TAG, setEndPointDirection);
        loadingActions.put(Constants.BLUR_BORDER_TAG, setBlurBorder);
        loadingActions.put(Constants.DEC_TAG, setDecValueAction);
        loadingActions.put(Constants.IMPORTED_PLAN_TAG, setImportedMatAndPath);

    }

    static String extractedPath = "temp/extracted";
    public  void openProject (String path) {
        Utils.unZipIt(path, extractedPath);
        String extractedAbsolutePath = System.getProperty("user.dir")+"/"+extractedPath+"/";
        try {
            File fXmlFile = new File(extractedPath+"/schema.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();


            NodeList stagedAnimationsList = doc.getElementsByTagName(Constants.STAGED_ANIMATION_TAG);


            for (int temp = 0; temp < stagedAnimationsList.getLength(); temp++) {

                Node stagedAnimationNode = stagedAnimationsList.item(temp);


                if (stagedAnimationNode.getNodeType() == Node.ELEMENT_NODE) {
                    HashMap<Integer, List<TimedStagePlanAnimation>> timedStagesList = new HashMap<>();

                    Element stagedAnimationElement = (Element) stagedAnimationNode;

                    RasterGeoElement beforeRaster = satHistViewer.addNewRaster(extractedAbsolutePath+stagedAnimationElement.getElementsByTagName(Constants.BEFORE_IMAGE_TAG).item(0).getTextContent(),false,"before.jpg", Constants.EXT.JPG, false);
                    RasterGeoElement afterRaster = satHistViewer.addNewRaster(extractedAbsolutePath+stagedAnimationElement.getElementsByTagName(Constants.AFTER_IMAGE_TAG).item(0).getTextContent(),false,"after.jpg", Constants.EXT.JPG, false);
                    satHistViewer.updateRasterCombos();
                    NodeList levelList = stagedAnimationElement.getElementsByTagName(Constants.LEVEL_TAG);
                    for (int levelCount =0; levelCount<levelList.getLength(); levelCount++) {
                        if (levelList.item(levelCount).getNodeType() == Node.ELEMENT_NODE) {
                            Element levelElement = (Element)levelList.item(levelCount);
                            int level = Integer.valueOf(levelElement.getAttribute(Constants.LEVEL_VALUE_TAG));
                            NodeList stagesList = levelElement.getElementsByTagName(Constants.STAGE_TAG);
                            List<TimedStagePlanAnimation> timedStages = new ArrayList<>();
                            for (int stageCount = 0; stageCount < stagesList.getLength(); stageCount++) {
                                if (stagesList.item(stageCount).getNodeType() == Node.ELEMENT_NODE) {
                                    Element stageElement = (Element)stagesList.item(stageCount);
                                    float startTime = Float.valueOf(stageElement.getElementsByTagName(Constants.START_TIME_TAG).item(0).getTextContent());
                                    float endTime = Float.valueOf(stageElement.getElementsByTagName(Constants.END_TIME_TAG).item(0).getTextContent());
                                    String pathToFiles =  extractedAbsolutePath + "/"+temp+"/"+levelCount+"/"+stageCount+"/";
                                    StagePlanAnimation animation = createAnimationByType(stageElement.getElementsByTagName(Constants.TYPE_TAG).item(0).getTextContent());
                                    for (String tag : animation.getValuesToRetrieve()) {
                                        if (stageElement.getElementsByTagName(tag).item(0)!=null) {
                                            loadingActions.get(tag).perform(animation, stageElement.getElementsByTagName(tag).item(0).getTextContent(), pathToFiles, (Element)stageElement.getElementsByTagName(tag).item(0));
                                        }
                                        else {
                                            //loadingActions.get(tag).perform(animation, "", pathToFiles, stageElement.get(tag));
                                        }
                                    }
                                    animation.setDuration(endTime-startTime);
                                    TimedStagePlanAnimation timedStage = new TimedStagePlanAnimation(startTime, endTime, animation, level);
                                    timedStages.add(timedStage);

                                }
                            }
                            timedStagesList.put(level, timedStages);
                        }


                    }
                    StagedAnimationByPlan stagedAnimationByPlan = new StagedAnimationByPlan(beforeRaster, afterRaster, timedStagesList);
                    satHistViewer.addNewStagedAnimation(stagedAnimationByPlan);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDurationInAnimation(StagePlanAnimation stagePlanAnimation, float duration) {
        stagePlanAnimation.setDuration(duration);
    }

    public void setMaskMat (StagePlanAnimation stagePlanAnimation, String pathToMask) {
        Mat loaded = CVUtilities.loadImageFromFile(pathToMask);
        List<Mat> splitLoaded = new ArrayList<>();
        Core.split(loaded, splitLoaded);
        splitLoaded.get(0).convertTo(splitLoaded.get(0), CvType.CV_8U);
        stagePlanAnimation.setMaskMat(splitLoaded.get(0));
    }

    public void setPlanMat (StagePlanAnimation stagePlanAnimation, String path) {
        Mat animationPlan = null;
        File f = new File (path);
        try{
            BufferedImage img = ImageIO.read(f);
            animationPlan = CVUtilities.bufferedImageToMat(img);
        }catch (Exception e){
            e.printStackTrace();
        }
        stagePlanAnimation.setPlanMat(animationPlan);
    }

    public void setEndMaskMat(StagePlanAnimation stagePlanAnimation, String path) {
        StageMorphAnimation stageMorphAnimation = (StageMorphAnimation)stagePlanAnimation;
        Mat loaded = CVUtilities.loadImageFromFile(path);
        List<Mat> splitLoaded = new ArrayList<>();
        Core.split(loaded, splitLoaded);
        splitLoaded.get(0).convertTo(splitLoaded.get(0), CvType.CV_8U);
        stageMorphAnimation.setEndMask(splitLoaded.get(0));
    }

    public void setInitMaskMat(StagePlanAnimation stagePlanAnimation, String path) {
        Mat loaded = CVUtilities.loadImageFromFile(path);
        List<Mat> splitLoaded = new ArrayList<>();
        Core.split(loaded, splitLoaded);
        splitLoaded.get(0).convertTo(splitLoaded.get(0), CvType.CV_8U);
        stagePlanAnimation.setInitMask(splitLoaded.get(0));
    }

    public void setRadialCenter(StagePlanAnimation stagePlanAnimation, Element centerElement) {
        StageRadialPlanAnimation stageRadialPlanAnimation = (StageRadialPlanAnimation)stagePlanAnimation;
        int x = Integer.valueOf(centerElement.getAttribute(Constants.X_TAG));
        int y = Integer.valueOf(centerElement.getAttribute(Constants.Y_TAG));
        stageRadialPlanAnimation.setCenterCoords(new int[] {x,y});
    }

    public void setInitPoint(StagePlanAnimation stagePlanAnimation, Element initPointElement) {
        StageDirectionPlanAnimation stageDirectionPlanAnimation = (StageDirectionPlanAnimation)stagePlanAnimation;
        int x =  Integer.valueOf(initPointElement.getAttribute(Constants.X_TAG));
        int y = Integer.valueOf(initPointElement.getAttribute(Constants.Y_TAG));
        stageDirectionPlanAnimation.setFirstStartPoint(new int[] {x,y});
    }

    public void setEndPoint(StagePlanAnimation stagePlanAnimation, Element endPointElement) {
        StageDirectionPlanAnimation stageDirectionPlanAnimation = (StageDirectionPlanAnimation)stagePlanAnimation;
        int x =  Integer.valueOf(endPointElement.getAttribute(Constants.X_TAG));
        int y = Integer.valueOf(endPointElement.getAttribute(Constants.Y_TAG));
        stageDirectionPlanAnimation.setFirstEndPoint(new int[] {x,y});
    }


    public LoadingAction setMaskMatAction = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            setMaskMat(stagePlanAnimation, path+value);
        }
    };

    public LoadingAction setPlanMatAction = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            setPlanMat(stagePlanAnimation, path+value);
        }
    };

    public LoadingAction setEndMaskMatAction = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            setEndMaskMat(stagePlanAnimation, path+value);
        }
    };

    public LoadingAction setInitMaskMatAction = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            setInitMaskMat(stagePlanAnimation, path+value);
        }
    };

    public LoadingAction setRadialCenter = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            setRadialCenter (stagePlanAnimation, element);
        }
    };

    public LoadingAction setInitPointDirection = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            setInitPoint(stagePlanAnimation, element);
        }
    };

    public LoadingAction setEndPointDirection = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            setEndPoint(stagePlanAnimation, element);
        }
    };

    public LoadingAction setBlurBorder = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            int blurWidth = Integer.valueOf(value);
            stagePlanAnimation.setBlurWidth(blurWidth);
            if (blurWidth!=0) {
                stagePlanAnimation.setBlurBorder(true);
            }
        }
    };

    public LoadingAction setDecValueAction = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            StageTexturedPlanAnimation stageTexturedPlanAnimation = (StageTexturedPlanAnimation)stagePlanAnimation;
            int decValue = Integer.valueOf(value);
            if (decValue == 1) {
                stageTexturedPlanAnimation.setDec(true);
            }
            else {
                stageTexturedPlanAnimation.setDec(false);
            }

        }
    };

    public LoadingAction setImportedMatAndPath = new LoadingAction() {
        @Override
        public void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element) {
            StageTexturedPlanAnimation stageTexturedPlanAnimation = (StageTexturedPlanAnimation)stagePlanAnimation;
            RasterGeoElement mntRaster = satHistViewer.addNewRaster(path+value, false, value, Constants.EXT.TIF, false);
            stageTexturedPlanAnimation.setImportedMat(mntRaster.getMat());
            stageTexturedPlanAnimation.setImportedPath(value+path);
        }
    };

    public StagePlanAnimation createAnimationByType(String type) {
        AbstractAnimation.Type animationType = AbstractAnimation.Type.valueOf(type);
        StagePlanAnimation animation = null;
        switch (animationType) {
            case BLEND:
                animation = new StagePlanAnimation(AbstractAnimation.Type.BLEND);
                break;
            case RADIAL_IN:
                animation = new StageRadialPlanAnimation(AbstractAnimation.Type.RADIAL_IN);
                break;
            case RADIAL_OUT:
                animation = new StageRadialPlanAnimation(AbstractAnimation.Type.RADIAL_OUT);
                break;
            case DILATE:
                animation = new StageMorphAnimation(AbstractAnimation.Type.DILATE);
                break;
            case ERODE:
                animation = new StageMorphAnimation(AbstractAnimation.Type.ERODE);
                break;
            case DEFORM:
                animation = new StageMorphAnimation(AbstractAnimation.Type.DEFORM);
                break;
            case DIRECTION:
                animation = new StageDirectionPlanAnimation(AbstractAnimation.Type.DIRECTION);
                break;
            case VECTOR_MORPH:
                animation = new StageMorphVectorAnimation(AbstractAnimation.Type.VECTOR_MORPH);
                break;
            case SEMANTIC:
                animation = new StageTexturedPlanAnimation(AbstractAnimation.Type.SEMANTIC);
                break;

        }
        return animation;
    }




}
