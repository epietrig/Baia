#!/bin/sh
# example on macOS, with OpenCV provided through Homebrew
java -Djava.library.path="/usr/local/Cellar/opencv/3.4.2/share/OpenCV/java"  -Djogl.debug.DebugGL -Xmx8192M -Xms2048M -jar target/sathist-1.0-SNAPSHOT.jar "$@"
