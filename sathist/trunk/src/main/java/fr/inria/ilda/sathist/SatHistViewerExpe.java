/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.sathist.ui.LoadingDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.HashMap;

/**
 * Created by mjlobo on 10/04/2017.
 */
public class SatHistViewerExpe extends SatHistViewer {

    static String LOG_ANIM_FILE_EXT = ".baia";
    static String LOG_TIME_FILE_EXT = ".csv";
    static String LOG_DIR = "logs/real";
    static final String OUTPUT_CSV_SEP = "\t";

    int currentTrial=-1;
    int firstTrial=0;
    String[] datasets;
    String datasetPath;
    int nParticipant;

    static String BEFORE_RASTER_NAME = "before.jpg";
    static String AFTER_RASTER_NAME = "after.jpg";

    LoadingDialog loadingTrialDialog;

    HashMap<String, Boolean> canvasLoaded;

    long startTime;
    long endTime;

    File tlogFile;
    BufferedWriter bwt;

    String nextText = "Next";
    String doneText = "Done";


    public SatHistViewerExpe(ShOptions options) {
        super(options);
        canvasLoaded = new HashMap<>();
        canvasLoaded.put(BEFORE_CANVAS, false);
        canvasLoaded.put(AFTER_CANVAS, false);
        canvasLoaded.put(PREVIEW_CANVAS, false);
        if (options.export == "") {
            firstTrial = options.firstTrial;

            currentTrial = options.firstTrial;
            datasetPath = options.datasets;
            //datasets = new String[9];
            loadingTrialDialog = new LoadingDialog("Loading trial, please wait", mainFrame);
            loadTrials(options.trials);
            nextTrial();
            nextTrialButton.setVisible(true);
            nextTrialButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (nextTrialButton.getText() == doneText) {
                        doneTrial();
                    } else {
                        nextTrial();
                    }
                }
            });
            startTrialButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    startTime = System.nanoTime();
                    enableInterface();
                }
            });
            backTrialButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    backTrial();
                }
            });
            backTrialButton.setEnabled(false);
            startTrialButton.setVisible(true);
            initLogTrial(nParticipant);
        }
        else {
            exportResults(options.export);
        }
//        HashMap<String, String> test = new HashMap<>();
//        test.put("hello",null);
//        System.out.println(test.get("bye"));




    }

    public void loadTrials(String tf) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(tf);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line = br.readLine();
            String[] aux = line.split(",");
            datasets = new String[aux.length];
            for (int i=0; i<aux.length; i++) {
                datasets[i] = aux[i];
                System.out.println("dataset "+datasets[i]);
            }
            fis.close();

            String [] path = tf.split("/");
            if(path.length>0) {
                String name [] = path[path.length-1].split("\\.");
                if(name.length>0)
                {
                    nParticipant = Integer.parseInt(name[0].split("_")[1]);
                    if (!(0<=nParticipant && nParticipant<=15)) {
                        System.out.println("Wrong name File");
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    static File initLogFile(String fileName, String dirName, String ext){
        String outputFile = dirName + File.separator + fileName+ext;
        System.out.println("outputfile "+outputFile);
        File file = new File(outputFile);
        int i = 0;
        while (file.exists()){
            i++;
            file = new File(outputFile.substring(0,outputFile.length()-ext.length()) + "-" + i);
        }
        return file;
    }

    void initLogTrial(int subjectID) {
        tlogFile = initLogFile(subjectID + "_trials", LOG_DIR, LOG_TIME_FILE_EXT);
        try {
            bwt = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tlogFile), "UTF-8"));
            try {
                // trial column headers
                bwt.write("SID" + OUTPUT_CSV_SEP +
                        "Dataset" + OUTPUT_CSV_SEP +
                        "Time" + OUTPUT_CSV_SEP
                );
                bwt.newLine();
                bwt.flush();
            }
            catch (IOException ex){ex.printStackTrace();}

        }
        catch(IOException ex){ex.printStackTrace();}
    }

    void writeTrial(int subjectID, String dataset, String time) {
        try {
            String change = dataset.split("_")[0];
            bwt.write(subjectID + OUTPUT_CSV_SEP +
                    dataset + OUTPUT_CSV_SEP +
                    time + OUTPUT_CSV_SEP
            );
            bwt.newLine();
            bwt.flush();

        }
        catch (IOException ex){ex.printStackTrace();}
    }



    public void nextTrial() {
        if (currentTrial > firstTrial) {
            File logFile = initLogFile(nParticipant + "_" + datasets[currentTrial], LOG_DIR, LOG_ANIM_FILE_EXT);
            saveProject(logFile.getAbsolutePath());
            writeTrial(nParticipant, datasets[currentTrial], (System.nanoTime() - startTime) + "");
            backTrialButton.setEnabled(true);
        }

        if (currentTrial < datasets.length-1) {
            currentTrial++;
            loadingTrialDialog.setVisible(true);
            loadingTrialDialog.setLocationRelativeTo(mainFrame);
            deleteAllRasters();
            String path = datasetPath + "/" + datasets[currentTrial];
            System.out.println("current trial " + currentTrial);
            //if (currentTrial<2) {
            addNewRaster(path + "/" + BEFORE_RASTER_NAME, false, BEFORE_RASTER_NAME, Constants.EXT.JPG, false);
            addNewRaster(path + "/" + AFTER_RASTER_NAME, false, AFTER_RASTER_NAME, Constants.EXT.JPG, true);
            updateRasterCombos();
            //}
            disableInterface();
        } else {
            JOptionPane.showMessageDialog(mainFrame, "You Finished. Thank you!");
            mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING));
        }
    }

    public void backTrial() {
        if (currentTrial >0) {
            currentTrial--;
            loadingTrialDialog.setVisible(true);
            loadingTrialDialog.setLocationRelativeTo(mainFrame);
            deleteAllRasters();
            String path = datasetPath + "/" + datasets[currentTrial];
            System.out.println("current trial " + currentTrial);
            //if (currentTrial<2) {
            addNewRaster(path + "/" + BEFORE_RASTER_NAME, false, BEFORE_RASTER_NAME, Constants.EXT.JPG, false);
            addNewRaster(path + "/" + AFTER_RASTER_NAME, false, AFTER_RASTER_NAME, Constants.EXT.JPG, true);
            updateRasterCombos();
            //}
            disableInterface();
        } else {
            backTrialButton.setEnabled(false);
        }
    }

    public void doneTrial() {
        endTime = System.nanoTime();
        nextTrialButton.setText(nextText);
    }


    @Override
    public  void finishedLoadingRasters(MapGLCanvas canvas) {
        System.out.println("Finished loading rasters! " + canvas.getName());
        canvasLoaded.put(canvas.getName(), true);
        int sumTrue = 0;
        for (String canvasName : canvasLoaded.keySet()) {
            if (canvasLoaded.get(canvasName)) {
                sumTrue++;
            }
        }
        System.out.println("sum true "+sumTrue);
        if (sumTrue == 3) {
            loadingTrialDialog.setVisible(false);
            for (String canvasName : canvasLoaded.keySet()) {
                canvasLoaded.put(canvasName, false);
            }


        }
    }

    public void disableInterface() {
        addStagedAnimationButton.setEnabled(false);
        animationControlPanel.setEnabled(false);
        nextTrialButton.setEnabled(false);
        nextTrialButton.setText(doneText);
        beforeRasterCombo.setEnabled(false);
        afterRasterCombo.setEnabled(false);
        startTrialButton.setEnabled(true);
        setUpdateButtonEnabled(false);
    }

    public void enableInterface() {
        addStagedAnimationButton.setEnabled(true);
        animationControlPanel.setEnabled(true);
        nextTrialButton.setEnabled(true);
        startTrialButton.setEnabled(false);
    }

    public void exportResults(String exportPath) {
        File file = new File(exportPath);
        loadProject(file.getPath());
        System.out.println("PATH! "+file.getPath().substring(0,file.getPath().length()-5));
        previewController.exportAnimation(file.getPath().substring(0,file.getPath().length()-5)+".mov");
        //previewController.exportAnimation(file.getPath().substring(0,file.getPath().length()-5));
    }
        //}



}
