/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.animation.AbstractAnimation;
import fr.inria.ilda.mmtools.animation.StagePlanAnimation;
import fr.inria.ilda.mmtools.animation.StagedAnimationByPlan;
import fr.inria.ilda.mmtools.animation.TimedStagePlanAnimation;
import fr.inria.ilda.mmtools.stateMachine.*;
import fr.inria.ilda.mmtools.ui.FloatSlider;
import fr.inria.ilda.mmtools.utilties.Pair;
import fr.inria.ilda.sathist.ui.StagedAnimationByPlanView;
import fr.inria.ilda.sathist.ui.TimeLineTick;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 14/06/16.
 */
public class TimeLineController {

    JPanel stages;
    JLayeredPane layeredStagesAndSlider;
    JPanel view;
    JScrollPane scrollPane;
    List<StagedAnimationByPlanController> stagedAnimationByPlanControllers;
    double startTime = 0;
    double currentTime = 0;
    int[] startPoint;
    int height;
    FloatSlider timeSlider;
    int pixelsPerSecond = 75;
    SatHistViewer satHistViewer;
    StagePlanController selectedStagePlanController;
    StagedAnimationByPlanController selectedStagedAnimationByPlan;
    List<StagePlanController> selectedStagePlanControllerList;

    int minimumSliderWidth = 0;
    JPanel sliderPanel;
    int timeSliderHeight;
    TimeLineTick timeLineTick;
    DragSource ds;
    String name;

    static int STAGES_X = 15;
    static int SLIDER_HEIGHT = 30;
    static int SLIDER_WIDTH = 10;
    static int SLIDER_Y = 7;
    static int TITLE_EMPTY_BORDER_HEIGHT = 5;
    static int STAGES_HEIGHT_DIFF = SLIDER_HEIGHT + TITLE_EMPTY_BORDER_HEIGHT + 4;
    static int RIGHT_OFFSET = 10;


    public TimeLineController(int width, int height, SatHistViewer satHistViewer, String name) {

        this.satHistViewer = satHistViewer;
        this.name = name;
        stagedAnimationByPlanControllers = new ArrayList<>();
        selectedStagePlanControllerList = new ArrayList<>();

        view = new JPanel();
        view.setLayout(new BorderLayout());

        layeredStagesAndSlider = new JLayeredPane();
        layeredStagesAndSlider.setLayout(null);
        layeredStagesAndSlider.setPreferredSize(new Dimension(width, height));
        layeredStagesAndSlider.setMinimumSize(new Dimension(width, height));
        layeredStagesAndSlider.setOpaque(true);
        layeredStagesAndSlider.setBackground(UIManager.getColor ( "Panel.background" ));
        TitledBorder lineBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Timeline");
        lineBorder.setTitleJustification(TitledBorder.CENTER);
        Border emptyBorder = BorderFactory.createEmptyBorder(0,0,TITLE_EMPTY_BORDER_HEIGHT,0);
        layeredStagesAndSlider.setBorder(BorderFactory.createCompoundBorder(emptyBorder,lineBorder));

        scrollPane = new JScrollPane();
        scrollPane.getViewport().add(layeredStagesAndSlider);
        scrollPane.setBorder(null);
        scrollPane.removeMouseWheelListener(scrollPane.getMouseWheelListeners()[0]);
        view.add(scrollPane, BorderLayout.CENTER);


        stages = new JPanel();
        stages.setLayout(new GridBagLayout());
        stages.setBorder(new EmptyBorder(0,0,2,0));
        stages.setBounds(STAGES_X,SLIDER_HEIGHT,width-10, height-STAGES_HEIGHT_DIFF);
        layeredStagesAndSlider.add(stages,new Integer(0));


        sliderPanel = new JPanel();
        sliderPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));


        timeSlider = new FloatSlider((float)startTime, (float)minimumSliderWidth/pixelsPerSecond, (float)currentTime, SatHistConstants.TIMELINE_SLIDER);
        timeSlider.setPreferredSize(new Dimension(minimumSliderWidth, SLIDER_HEIGHT));
        timeSlider.setMinimumSize(new Dimension(minimumSliderWidth, SLIDER_HEIGHT));

        sliderPanel.add(timeSlider);
        sliderPanel.setBounds(0,7,width, SLIDER_HEIGHT);
        sliderPanel.setOpaque(false);
        layeredStagesAndSlider.add(sliderPanel,new Integer(2));

        timeLineTick = new TimeLineTick(height,15);
        timeLineTick.setPreferredSize(new Dimension(width, height));
        timeLineTick.setMinimumSize(new Dimension(width, height));
        timeLineTick.setBounds(0,height/10+7,width, (int)(0.75*height));
        timeLineTick.setVisible(false);
        layeredStagesAndSlider.add(timeLineTick,new Integer(1));

        startPoint = new int[2];
        this.height = height;


    }



    public void addStagedAnimationByPlan(StagedAnimationByPlan stagedAnimationByPlan, StateMachine stateMachine) {
        StagedAnimationByPlanController stagedAnimationByPlanController = new StagedAnimationByPlanController(stagedAnimationByPlan, this, pixelsPerSecond, stateMachine, 4*height/5);
        stagedAnimationByPlanControllers.add(stagedAnimationByPlanController);
        if (!timeLineTick.isVisible()) {
            timeLineTick.setVisible(true);
        }
        sliderPanel.revalidate();
        sliderPanel.repaint();
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = stagedAnimationByPlanControllers.size()-1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.PAGE_START;
        c.weightx = 1;
        c.weighty = 1;
        stages.add(stagedAnimationByPlanController.getStagedAnimationByPlanView(),c);


        stages.revalidate();
        float duration = 0;
        for (int i=0; i<stagedAnimationByPlanControllers.size(); i++){
            duration+=stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getDurationTimed();
        }

        if (selectedStagedAnimationByPlan!=null) {
            selectedStagedAnimationByPlan.unselect();
        }
        selectedStagedAnimationByPlan = stagedAnimationByPlanController;
        selectedStagedAnimationByPlan.select();

        updateDuration();


    }

    public void addEqualizedAnimation(TimedStagePlanAnimation equalizedStagePlanAnimation, StateMachine stateMachine) {
       getSelectedStagedAnimationByPlan().addStagePlan(equalizedStagePlanAnimation, stateMachine, 0);
        getSelectedStagedAnimationByPlan().setEqualizedController(equalizedStagePlanAnimation);
        updateDuration();
    }

    public void addDefaultStage(TimedStagePlanAnimation animation, StateMachine stateMachine) {
        selectedStagedAnimationByPlan.addStagePlan(animation, stateMachine);
        updateDuration();
    }

    public void updateTimeSlider () {
        float duration = 0;
        for (int i=0; i<stagedAnimationByPlanControllers.size(); i++){
            duration+=stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getDurationTimed();
        }
        if (duration>0) {
            timeSlider.setEndVal((duration * pixelsPerSecond) / pixelsPerSecond);
            timeSlider.setPreferredSize(new Dimension((int) (duration * pixelsPerSecond) + 30, SLIDER_HEIGHT));
            timeSlider.setMinimumSize(new Dimension((int) (duration * pixelsPerSecond) + 30, SLIDER_HEIGHT));
        }
        else {
            timeSlider.setPreferredSize(new Dimension(minimumSliderWidth,SLIDER_HEIGHT));
            timeSlider.setMinimumSize(new Dimension(minimumSliderWidth,SLIDER_HEIGHT));
            timeLineTick.setVisible(false);

        }
        timeSlider.repaint();
        timeSlider.revalidate();
    }


    public void updateDuration() {
        satHistViewer.animationDurationChanged();
        updateView();

    }

    public void updateView() {
        float duration = 0;
        int maxLevel = 0;
        int maxHeight = 0;
        for (int i=0; i<stagedAnimationByPlanControllers.size(); i++){
            duration+=stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getDurationTimed();
            if (stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getMaxLevel() >= maxLevel) {
                maxLevel = stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getMaxLevel();
                maxHeight = stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlanView().getCurrentHeight();
            }
        }
        layeredStagesAndSlider.setPreferredSize(new Dimension((int)(duration*pixelsPerSecond)+STAGES_X+RIGHT_OFFSET, maxHeight+STAGES_HEIGHT_DIFF+SLIDER_Y+2));
        layeredStagesAndSlider.revalidate();
        layeredStagesAndSlider.repaint();
        stages.setBounds(STAGES_X,SLIDER_HEIGHT,(int)(duration*pixelsPerSecond), maxHeight+2);
        sliderPanel.setBounds(0,SLIDER_Y,(int)(duration*pixelsPerSecond+STAGES_X+SLIDER_WIDTH), SLIDER_HEIGHT);
        timeLineTick.setBounds(0,SLIDER_HEIGHT,(int)(duration*pixelsPerSecond+STAGES_X+SLIDER_WIDTH/2), layeredStagesAndSlider.getPreferredSize().height-STAGES_HEIGHT_DIFF);
        updateTimeSlider();
        stages.revalidate();
        stages.repaint();
        sliderPanel.revalidate();
        sliderPanel.repaint();
        view.revalidate();
    }

    public void updateDuration(float newDuration) {
        //System.out.println("NEW DURATION "+newDuration);
        selectedStagedAnimationByPlan.modifyStagePlanDuration(selectedStagePlanController, newDuration);
        float duration = 0;
        for (int i=0; i<stagedAnimationByPlanControllers.size(); i++){
            //duration+=stagedAnimationByPlanControllers.get(i).getDuration();
            duration+=stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getDurationTimed();
        }
        //System.out.println("timeline duration "+duration);
       // selectedStagedAnimationByPlan.updateDuration();
        updateTimeSlider();
        updateView();
        stages.revalidate();
        stages.repaint();
        layeredStagesAndSlider.revalidate();
        layeredStagesAndSlider.repaint();
        sliderPanel.revalidate();
        sliderPanel.repaint();
    }

    public void deleteAnimation(AbstractAnimation animation) {
        StagePlanAnimation stagePlanAnimation = (StagePlanAnimation)animation;
        selectedStagedAnimationByPlan.removeStagePlan(stagePlanAnimation);
        if (selectedStagedAnimationByPlan.getDuration()==0) {
            deleteStagedAnimation(selectedStagedAnimationByPlan);
        }
        float duration = 0;
        if (stagedAnimationByPlanControllers.size()==0) {
            timeSlider.setPreferredSize(new Dimension(minimumSliderWidth,timeSliderHeight));
            timeSlider.setMinimumSize(new Dimension(minimumSliderWidth,timeSliderHeight));
            timeLineTick.setVisible(false);
        }
//        else {
//            for (int i = 0; i < stagedAnimationByPlanControllers.size(); i++) {
//                duration += stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getDurationTimed();
//            }
//            timeSlider.setEndVal((duration*pixelsPerSecond)/pixelsPerSecond);
//            timeSlider.setPreferredSize(new Dimension((int)(duration*pixelsPerSecond)+30,timeSliderHeight));
//            timeSlider.setMinimumSize(new Dimension((int)(duration*pixelsPerSecond)+30,timeSliderHeight));
//        }
//        timeSlider.repaint();
//        timeSlider.revalidate();
        updateTimeSlider();
        updateView();
    }

    public void deleteStagedAnimation(StagedAnimationByPlanController stagedAnimationByPlanController) {
        //System.out.println("delete staged animation in timeline controller!");
        stagedAnimationByPlanControllers.remove(stagedAnimationByPlanController);
        selectedStagedAnimationByPlan = null;
        selectedStagePlanController = null;
        stages.remove(stagedAnimationByPlanController.getStagedAnimationByPlanView());
        updateView();

//        stages.revalidate();
//        stages.repaint();
//        float duration = 0;
//        if (stagedAnimationByPlanControllers.size()==0) {
//            timeSlider.setPreferredSize(new Dimension(minimumSliderWidth,timeSliderHeight));
//            timeSlider.setMinimumSize(new Dimension(minimumSliderWidth,timeSliderHeight));
//            timeLineTick.setVisible(false);
//        }
//        else {
//            for (int i = 0; i < stagedAnimationByPlanControllers.size(); i++) {
//                duration += stagedAnimationByPlanControllers.get(i).getStagedAnimationByPlan().getDurationTimed();
//            }
//            timeSlider.setEndVal((duration*pixelsPerSecond)/pixelsPerSecond);
//            timeSlider.setPreferredSize(new Dimension((int)(duration*pixelsPerSecond)+30,timeSliderHeight));
//            timeSlider.setMinimumSize(new Dimension((int)(duration*pixelsPerSecond)+30,timeSliderHeight));
//        }
//        timeSlider.repaint();
//        timeSlider.revalidate();
    }



    int oldPosition = 0;
    float oldSliderPosition=0;
    public void updateCurrentTime(float v) {
        //timeLine.setCurrentTime(v);
        currentTime = v;
        //System.out.println("v " + v);
        if (v!=oldSliderPosition) {
            timeSlider.setValue(v);
            oldSliderPosition =v;
        }

        //timeLineTick.setPosX(15+(int)(v*pixelsPerSecond)+2*stagedAnimationByPlanControllers.size());
        //timeLineTick.setPosX(15+(int)(v*pixelsPerSecond)+2*stagedAnimationByPlanControllers.size());

        if (15+(int)(v*pixelsPerSecond)!=oldPosition) {
            timeLineTick.setPosX(15 + (int) (v * pixelsPerSecond));
            oldPosition = 15 + (int) (v * pixelsPerSecond);

        }
    }


    public void updateAnimation(StagePlanController stagePlanController) {
        //timeLine.updateAnimationView(stagePlanController.getStagePlanView(),(int)stagePlanController.getAnimation().getDuration()*timeLine.getPixelsPerSecond());
        //stagePlanController.updateAnimationDuration();
    }

    public void addListeners(StateMachine stateMachine) {

        timeSlider.addListeners(stateMachine);
        timeSlider.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                satHistViewer.startPlayAnimation();
            }
        });

    }




    public StagedAnimationByPlanController selectStagedAnimationByPlan(int x, int y) {
        StagedAnimationByPlanController newSelectedStagedAnimationByPlanController = null;
        for (StagedAnimationByPlanController stagedAnimationByPlanController: stagedAnimationByPlanControllers) {
                    if (stagedAnimationByPlanController.getStagedAnimationByPlanView().getBounds().contains(x, y)) {
                        newSelectedStagedAnimationByPlanController = stagedAnimationByPlanController;
                        stagedAnimationByPlanController.select();
                    }
                }
        if (newSelectedStagedAnimationByPlanController!= selectedStagedAnimationByPlan) {
            if (selectedStagedAnimationByPlan!=null) {
                selectedStagedAnimationByPlan.unselect();
            }
            selectedStagedAnimationByPlan = newSelectedStagedAnimationByPlanController;
        }
        return selectedStagedAnimationByPlan;
    }

    public Pair<StagedAnimationByPlanController, StagePlanController> getSelected(int x, int y) {
        StagedAnimationByPlanController newSelectedStagedAnimationByPlanController = null;
        StagePlanController newStagePlanControllerSelected = null;

        for (StagedAnimationByPlanController stagedAnimationByPlanController: stagedAnimationByPlanControllers) {
            Point relativePoint = SwingUtilities.convertPoint(layeredStagesAndSlider,x,y,stagedAnimationByPlanController.getStagedAnimationByPlanView());
            if (stagedAnimationByPlanController.getStagedAnimationByPlanView().contains(relativePoint)) {
                newSelectedStagedAnimationByPlanController = stagedAnimationByPlanController;
                newStagePlanControllerSelected = stagedAnimationByPlanController.getStagePlanControllerAt(x,y);
                if (newStagePlanControllerSelected != null) {
                    stagedAnimationByPlanController.selectStagePlanController(newStagePlanControllerSelected);

                }
                stagedAnimationByPlanController.select();
            }
        }
        if (newSelectedStagedAnimationByPlanController == null) {
            if (selectedStagePlanController != null) {
                selectedStagePlanController.getStagePlanView().unSelect();
                selectedStagePlanControllerList.clear();
            }
        }
        if (newSelectedStagedAnimationByPlanController!= selectedStagedAnimationByPlan) {
            if (selectedStagedAnimationByPlan!=null) {
                selectedStagedAnimationByPlan.unselect();
                for (StagePlanController stagePlanController : selectedStagePlanControllerList) {
                    stagePlanController.getStagePlanView().unSelect();
                }
                selectedStagePlanControllerList.clear();
            }
            else {
                for (StagePlanController stagePlanController : selectedStagePlanControllerList) {
                    stagePlanController.getStagePlanView().unSelect();
                }
                selectedStagePlanControllerList.clear();
            }
            selectedStagedAnimationByPlan = newSelectedStagedAnimationByPlanController;
        }
        if (newStagePlanControllerSelected!=null) {
            List<StagePlanController> toRemove = new ArrayList<>();
            if (!selectedStagePlanControllerList.contains(newStagePlanControllerSelected)) {
                selectedStagePlanControllerList.add(newStagePlanControllerSelected);
            }
            for (int i=0; i<selectedStagePlanControllerList.size(); i++) {
                if (selectedStagePlanControllerList.get(i)!=newStagePlanControllerSelected) {
                    selectedStagePlanControllerList.get(i).getStagePlanView().unSelect();
                    toRemove.add(selectedStagePlanControllerList.get(i));
                }
            }
            selectedStagePlanControllerList.removeAll(toRemove);
            selectedStagePlanController = newStagePlanControllerSelected;
        }
        else {
            if (selectedStagePlanController != null)  {
                selectedStagePlanController.getStagePlanView().unSelect();
            }
            selectedStagePlanController = null;
            selectedStagePlanControllerList.clear();
        }
        return new Pair<>(selectedStagedAnimationByPlan, selectedStagePlanController);

    }

//



    public void moveStagePlan(int x, int y) {
        selectedStagedAnimationByPlan.offsetStagePlan(selectedStagePlanController, x-startPoint[0],
                y-startPoint[1]);
        int newWidth = 0;
        int newHeight = 0;
        for (StagedAnimationByPlanController stagedAnimationByPlanController : stagedAnimationByPlanControllers) {
            newWidth+= stagedAnimationByPlanController.getStagedAnimationByPlanView().getPreferredSize().getWidth();
        }
        for (StagedAnimationByPlanController stagedAnimationByPlanController : stagedAnimationByPlanControllers) {
            if (stagedAnimationByPlanController.getStagedAnimationByPlanView().getPreferredSize().getHeight()>
                    newHeight) {
                newHeight = (int)stagedAnimationByPlanController.getStagedAnimationByPlanView().getPreferredSize().getHeight();
            }
        }

        layeredStagesAndSlider.setPreferredSize(new Dimension(newWidth+STAGES_X+RIGHT_OFFSET, layeredStagesAndSlider.getHeight()));
        //layeredStagesAndSlider.setPreferredSize(new Dimension(newWidth+STAGES_X+RIGHT_OFFSET,newHeight));
        layeredStagesAndSlider.revalidate();
        layeredStagesAndSlider.repaint();
        stages.setBounds(STAGES_X,SLIDER_HEIGHT,newWidth, stages.getHeight());
        sliderPanel.setBounds(0,SLIDER_Y,newWidth+STAGES_X+SLIDER_WIDTH, SLIDER_HEIGHT);

    }

    public void startMoving(int x, int y) {
        startPoint[0] = x;
        startPoint[1] = y;
        selectedStagedAnimationByPlan.fillAuxTimes();
    }

    public void finishMoving() {
        selectedStagedAnimationByPlan.finishMoving(selectedStagePlanController);
//        updateTimeSlider();
        updateDuration();
        layeredStagesAndSlider.revalidate();
    }


    public JPanel getView() {
        return view;
    }

    public JLayeredPane getLayeredStagesAndSlider() {
        return layeredStagesAndSlider;
    }

    public JPanel getStagesPanel() {
        return stages;
    }

    public SatHistViewer getSatHistViewer() {
        return satHistViewer;
    }



    public double getCurrentTime() {
        return currentTime;
    }


    public StagePlanController getStagePlanControllerAt (Point p) {
        for (StagedAnimationByPlanController stagedAnimationByPlanController: stagedAnimationByPlanControllers) {
            Point relativePoint = SwingUtilities.convertPoint(scrollPane, p.x, p.y, stagedAnimationByPlanController.getStagedAnimationByPlanView());
            if (stagedAnimationByPlanController.getStagedAnimationByPlanView().contains(relativePoint)) {
                return stagedAnimationByPlanController.getStagePlanControllerAt(p.x,p.y);
            }
        }
//        }
        return null;
    }




    public void checkStagedAnimationCompleteness() {
        selectedStagedAnimationByPlan.checkCompleteness();
    }

    public StagedAnimationByPlanController getSelectedStagedAnimationByPlan() {
        return selectedStagedAnimationByPlan;
    }

    public String getName() {
        return name;
    }

    public StagedAnimationByPlanController getStagedAnimationControllerByView(StagedAnimationByPlanView view) {
        for (StagedAnimationByPlanController controller : stagedAnimationByPlanControllers) {
            if (controller.getStagedAnimationByPlanView().equals(view)) {
                return controller;
            }
        }
        return null;
    }


    public void updateStagedAnimationTitle(String canvasName, String newTitle) {
        selectedStagedAnimationByPlan.updateTitle(canvasName, newTitle);
    }


    public List<StagePlanController> getSelectedStagePlanControllerList() {
        return selectedStagePlanControllerList;
    }


    public StagePlanController getSelectedStagePlanController() {
        return selectedStagePlanController;
    }

    public void reset() {
        stagedAnimationByPlanControllers.clear();
        stages.removeAll();
        stages.repaint();
        updateTimeSlider();
    }



    public void resize () {
        stages.revalidate();
        stages.repaint();
        sliderPanel.revalidate();
        sliderPanel.repaint();

    }



}
