/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.animation.StagePlanAnimation;
import org.w3c.dom.Element;


/**
 * Created by mjlobo on 26/03/2017.
 */
public abstract class LoadingAction {
    public abstract void perform(StagePlanAnimation stagePlanAnimation, String value, String path, Element element);
}
