/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;



import fr.inria.ilda.mmtools.gl.Texture;

import javax.swing.*;

/**
 * Created by mjlobo on 17/02/2017.
 */
public class UpdateAnimationWorker extends SwingWorker {

    SatHistViewer satHistViewer;
    Texture initTexture;
    Texture endTexture;
    long initTime;

    public UpdateAnimationWorker(SatHistViewer satHistViewer, Texture initTexture, Texture endTexture) {
        this.satHistViewer = satHistViewer;
        this.initTexture = initTexture;
        this.endTexture = endTexture;
    }

    @Override
    protected Object doInBackground() throws Exception {
        initTime = System.currentTimeMillis();
        satHistViewer.updateAnimationInPreview(initTexture, endTexture);
        return null;
    }

    protected void done() {
        satHistViewer.finishUpdatingAnimation();
    }
}
