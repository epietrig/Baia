/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.sathist.SatHistConstants;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by mjlobo on 14/02/2017.
 */
public class LoadingDialog extends JDialog {
    JLabel messageLabel;
    JLabel waitLabel;
    JLabel loadingLabel;
    ImageIcon loadingIcon;
    String text = "Loading..";
    String iconName = "loading2.gif";

    public LoadingDialog(String message, JFrame frame) {
        super(frame);
        JPanel mainPanel = new JPanel();
        this.text = message;
        messageLabel = new JLabel(message);
        waitLabel = new JLabel(SatHistConstants.WAIT_MESSAGE);
        ImageIcon image = new ImageIcon(SatHistConstants.ICONS_PATH+"/"+iconName);
        loadingLabel = new JLabel(new ImageIcon(image.getImage().getScaledInstance(30,30,0)));
        //getContentPane().setLayout(new FlowLayout());
        //getContentPane().add(loadingLabel);
        //getContentPane().add(messageLabel);
        //mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
        JPanel loadingGifPanel = new JPanel();
        loadingGifPanel.add(loadingLabel);

        JPanel messagePanel = new JPanel();
        messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.PAGE_AXIS));
        messagePanel.add(messageLabel);
        messagePanel.add(waitLabel);

//        mainPanel.add(loadingLabel);
//        mainPanel.add(messageLabel);


        mainPanel.add(loadingGifPanel);
        mainPanel.add(messagePanel);




        mainPanel.setBorder(new EmptyBorder(5,5,5,5));
        add(mainPanel);
        pack();
        setVisible(false);
        //setVisible(true);
        //setVisible(false);

    }

    public LoadingDialog(JFrame frame) {
        this("Loading..." ,frame);
    }

    public void update(String text) {
        this.text = text;
        messageLabel.setText(text);
        revalidate();
        pack();
    }
}
