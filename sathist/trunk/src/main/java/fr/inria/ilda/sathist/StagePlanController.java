/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.animation.TimedStagePlanAnimation;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.sathist.ui.StagePlanViewLayout;

import javax.swing.*;

/**
 * Created by mjlobo on 14/06/16.
 */
public class StagePlanController {
    TimedStagePlanAnimation animation;
    StagePlanViewLayout stagePlanView;
    StagedAnimationByPlanController controller;
    double pixelsPerSecond;

    public StagePlanController (TimedStagePlanAnimation animation, int height, double pixelsPerSecond, StagedAnimationByPlanController controller, StateMachine stateMachine, JComponent toPropagate) {
        this.animation = animation;
        this.pixelsPerSecond = pixelsPerSecond;
        stagePlanView = new StagePlanViewLayout(this, height, (int)pixelsPerSecond, checkCompleteness());
        stagePlanView.addListeners(stateMachine, toPropagate);
        this.controller = controller;
    }

    public StagePlanController (TimedStagePlanAnimation animation, int height, double pixelsPerSecond, StagedAnimationByPlanController controller, StateMachine stateMachine, boolean complete, JComponent toPropagate) {
        this.animation = animation;
        this.pixelsPerSecond = pixelsPerSecond;
        stagePlanView = new StagePlanViewLayout(this, height, (int)pixelsPerSecond, complete);
        stagePlanView.addListeners(stateMachine, toPropagate);
        this.controller = controller;
    }

    public void updateAnimationDuration(float d) {
        animation.getAnimation().setDuration(d);
        stagePlanView.update(pixelsPerSecond);

    }

    public void updateView() {
        stagePlanView.update(checkCompleteness(), pixelsPerSecond);
    }

    public void updateView(boolean complete) {

        stagePlanView.update(complete, pixelsPerSecond);
    }

    public boolean checkCompleteness() {
        return animation.getAnimation().isComplete();
    }


    public void setAnimation(TimedStagePlanAnimation animation) {
        this.animation = animation;
    }



    public TimedStagePlanAnimation getTimedAnimation() {
        return animation;
    }

    public StagePlanViewLayout getStagePlanView() {
        return stagePlanView;
    }
}
