/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import javax.xml.crypto.Data;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Created by mjlobo on 30/08/16.
 */
public class TransferableStagePlanView implements Transferable {

    protected static DataFlavor intFlavor = new DataFlavor(Integer.class, "Integer");
    protected static DataFlavor stageFlavor = new DataFlavor(StagePlanController.class, "Stage plan controller");
    protected static DataFlavor[] supportedFlavors = {DataFlavor.stringFlavor, intFlavor};
    StagePlanController stagePlanController;
    Integer index;

    public TransferableStagePlanView(StagePlanController stagePlanController, Integer index) {
        this.stagePlanController = stagePlanController;
        this.index = index;
    }


    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return supportedFlavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        if (flavor.equals(DataFlavor.stringFlavor) ||  flavor.equals(intFlavor) || flavor.equals(stageFlavor)) {
            return true;
        }
        return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if (flavor.equals(DataFlavor.stringFlavor)) {
            return stagePlanController.getStagePlanView().toString();
        }
        if (flavor.equals(intFlavor)) {
            return index;
        }
        if (flavor.equals(stageFlavor)) {
            return stagePlanController;
        }
        else
            throw new UnsupportedFlavorException(flavor);
    }
}
