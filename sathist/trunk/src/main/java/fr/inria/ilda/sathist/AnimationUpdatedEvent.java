/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.stateMachine.Event;


/**
 * Created by mjlobo on 19/04/2017.
 */
public class AnimationUpdatedEvent extends Event {
    String action;
    boolean changed;

    public AnimationUpdatedEvent(String action, boolean changed) {
        this.action = action;
        this.changed = changed;
    }

    public String getAction() {
        return action;
    }

    public boolean getChanged() {
        return changed;
    }
}
