/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import java.awt.*;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceAdapter;

/**
 * Created by mjlobo on 30/08/16.
 */
public class StagePlanViewTransfer extends DragSourceAdapter implements DragGestureListener {
    TimeLineController timeLineController;

    public StagePlanViewTransfer (TimeLineController timeLineController) {
        this.timeLineController = timeLineController;
    }
    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        Cursor cursor = DragSource.DefaultCopyDrop;
        StagePlanController controller = timeLineController.getSelectedStagePlanController();
        if (controller!=null) {
            //TransferableStagePlanView transferableStagePlanView = new TransferableStagePlanView(controller, timeLineController.getIndexOfStage(controller));
            TransferableStagePlanView transferableStagePlanView = new TransferableStagePlanView(controller, 0);

            dge.startDrag(cursor, transferableStagePlanView);
        }
    }
}
