/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.mmtools.stateMachine.ComboBoxEvent;
import fr.inria.ilda.mmtools.stateMachine.MaskAnimationParamChangedEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.mmtools.ui.FloatSlider;
import fr.inria.ilda.mmtools.ui.StringComboPanel;
import fr.inria.ilda.mmtools.ui.ValueSlider;
import fr.inria.ilda.mmtools.utilties.Constants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Created by mjlobo on 25/05/16.
 */
public class MaskOptions extends JPanel {
    FloatSlider diffSlider;
    JComboBox colorModelCB;
    float minColor = 0;
    float maxColor = 100;
    JPanel diffSliderPanel = new JPanel();
    StringComboPanel transitionCB;
    JButton maskAddButton;
    static Dimension colorModelCBDimension = new Dimension(200,20);

    public MaskOptions(float diff, String name) {
        String [] colorModels = new String[] {Constants.ImageModelName.CIELAB.toString(), Constants.ImageModelName.HSV.toString()};
        colorModelCB = new JComboBox(colorModels);
        colorModelCB.setName(Constants.COLOR_MODEL);
        diffSlider = new FloatSlider(minColor,maxColor,diff, name);
        java.util.List<String> transitionNames = new ArrayList<>();
        transitionNames.add(Constants.BLEND_TRANSITION);
        transitionNames.add(Constants.DILATE_TRANSITION);
        transitionNames.add(Constants.ERODE_TRANSITION);
        transitionNames.add(Constants.DIRECTION_TRANSITION);
        transitionNames.add(Constants.RADIAL_SEMANTIC);
        transitionCB = new StringComboPanel("Transition", transitionNames, Constants.TRANSITION_COMBOBOX);
        maskAddButton = new JButton("Add");
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        diffSliderPanel = new JPanel();
        diffSliderPanel.setLayout(new BoxLayout(diffSliderPanel, BoxLayout.X_AXIS));
        JLabel toleranceLabel = new JLabel("Tolerance:");
        colorModelCB.setPreferredSize(colorModelCBDimension);
        colorModelCB.setMaximumSize(colorModelCBDimension);
        diffSliderPanel.add(colorModelCB);
        diffSliderPanel.add(Box.createRigidArea(new Dimension(20,20)));
        diffSliderPanel.add(toleranceLabel);
        diffSliderPanel.add(diffSlider);
        add(diffSliderPanel);
        setBorder(BorderFactory.createRaisedBevelBorder());
    }

    public void addListeners(final StateMachine stateMachine) {
        diffSlider.addListeners(stateMachine);
        colorModelCB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new ComboBoxEvent((String)colorModelCB.getSelectedItem(),colorModelCB));
            }
        });
        transitionCB.addListeners(stateMachine);
    }

    public JComboBox getTransitionCB() {
        return transitionCB.getComboBox();
    }

    public JComboBox getColorModelCB() {
        return colorModelCB;
    }

    public void updateSliderValue(float newValue) {
        if (newValue != diffSlider.getFloatValue()) {
            diffSlider.setValue(newValue);
        }
    }

    public void updateColorModel (String colorModel) {
        if (!((String)colorModelCB.getSelectedItem()).equals(colorModel)) {
            colorModelCB.setSelectedItem(colorModel);
        }
    }


}
