/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.stateMachine.Event;

import java.awt.*;

/**
 * Created by mjlobo on 08/09/16.
 */
public class StagedAnimationViewEvent extends Event {
    Component target;
    String action;

    public StagedAnimationViewEvent(Component target, String action) {
        this.target = target;
        this.action = action;
    }

    public Component getTarget() {
        return target;
    }

    public String getAction() {
        return action;
    }
}
