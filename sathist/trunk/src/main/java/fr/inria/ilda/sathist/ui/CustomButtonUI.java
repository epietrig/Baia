/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.sathist.SatHistConstants;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

/**
 * Created by mjlobo on 24/04/2017.
 */
public class CustomButtonUI extends BasicButtonUI {
    int BUTTON_HEIGHT = 20;
    @Override
    public void paint(Graphics g, JComponent c) {
        AbstractButton button = (AbstractButton) c;
        Graphics2D g2d = (Graphics2D) g;
        final int buttonWidth = button.getWidth();
        final int buttonHeight = button.getHeight();
//        if (button.getModel().isRollover()) {
//            // Rollover
//            GradientPaint gp = new GradientPaint(0, 0, Color.green, 0, BUTTON_HEIGHT * 0.6f, Color.red, true);
//            g2d.setPaint(gp);
//        } else if (button.isEnabled()) {
//            // Enabled
//            //GradientPaint gp = new Paint(0, 0, Color.red, 0, BUTTON_HEIGHT * 0.6f, Color.gray, true);
//            g2d.setPaint(gp);
//        } else {
//            // Disabled
//            GradientPaint gp = new GradientPaint(0, 0, Color.black, 0, BUTTON_HEIGHT * 0.6f, Color.blue, true);
//            g2d.setPaint(gp);
//        }

        g2d.setColor(Color.LIGHT_GRAY);
        g2d.fillRoundRect(2, 2, buttonWidth-4, buttonHeight-4,2,2);
        g2d.setColor(SatHistConstants.changedColor);
        g2d.fillRoundRect(3, 3, buttonWidth-6, buttonHeight-6,3,3);
        super.paint(g, button);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.GRAY);
        g2d.setStroke(new BasicStroke(1.2f));
        g2d.draw(new RoundRectangle2D.Double(1, 1, (buttonWidth - 3),
                (buttonHeight - 3), 12, 8));
        //g2d.setStroke(new BasicStroke(1.5f));
        //g2d.drawLine(4, buttonHeight - 3, buttonWidth - 4, buttonHeight - 3);

        g2d.dispose();
    }
}
