/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.mmtools.animation.StagePlanAnimation;
import fr.inria.ilda.mmtools.stateMachine.*;
import fr.inria.ilda.mmtools.ui.StringComboPanel;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.sathist.SatHistConstants;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.metal.MetalButtonUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 02/09/16.
 */
public class AnimationAddPanel extends JPanel{

    JButton addButton;
    JButton addEqualizedButton;
    JButton saveButton;

    StringComboPanel transitionCB;
    JComboBox mntComboBox;

    JCheckBox blurredCheckbox;
    JTextField blurredBorderWidthTextArea;

    JPanel addPanel;
    JPanel bluredBorderPanel;
    JCheckBox invertImported;
    JPanel invertedPanel;
    ButtonUI enabledButtonUI;
    ButtonUI disabledButtonUI;

    public AnimationAddPanel() {
        this(null);
    }

    int defaultBorder = 10;

    public AnimationAddPanel(List<String> mntNames) {

        addButton = new JButton("Update");
        disabledButtonUI = addButton.getUI();
        enabledButtonUI = new CustomButtonUI();
        addButton.setActionCommand(SatHistConstants.ADD_ANIMATION_ACTION);


        addEqualizedButton = new JButton("Add Equalization Step");
        addEqualizedButton.setActionCommand(SatHistConstants.ADD_EQ_ANIMATION_ACTION);

        saveButton = new JButton("Save");
        saveButton.setActionCommand(SatHistConstants.SAVE_ANIMATION_PLAN);

        List<String> transitions = new ArrayList<>();
        transitions.add(Constants.BLEND_TRANSITION);
        transitions.add(SatHistConstants.ERODE_NAME);
        transitions.add(SatHistConstants.DILATE_NAME);
        transitions.add(Constants.DIRECTION_TRANSITION);
        //transitions.add(Constants.RADIAL_TRANSITION);
        transitions.add(SatHistConstants.RADIAL_IN_TRANSITION);
        transitions.add(SatHistConstants.RADIAL_OUT_TRANSITION);
        transitions.add(SatHistConstants.IMPORTED_TRANSITION);
        transitions.add(Constants.DEFORM_TRANSITION);
        transitions.add(SatHistConstants.MORPH_VECTOR_TRANSITION);
        transitionCB = new StringComboPanel("Transition", transitions, SatHistConstants.TRANSITION_CB);

        bluredBorderPanel = new JPanel();
        blurredCheckbox = new JCheckBox("Blurred Border");
        blurredCheckbox.setName(SatHistConstants.BLURRED_CHECKBOX);
        bluredBorderPanel.add(blurredCheckbox);
        blurredBorderWidthTextArea = new JTextField(2);
        blurredBorderWidthTextArea.setText("10"+SatHistConstants.BLEND_PIXELS);
        blurredBorderWidthTextArea.setName(SatHistConstants.BLURRED_TEXT_AREA);
        bluredBorderPanel.add(blurredBorderWidthTextArea);
        bluredBorderPanel.add(new JLabel("px"));
        enableBlurredBorderPanel(true);

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy =0;
        c.gridx = 0;
        add(transitionCB, c);

        if (mntNames!=null) {
            invertedPanel = new JPanel();
            mntComboBox = new JComboBox();
            mntComboBox.setName(SatHistConstants.MNT_CB);
            for (String mntname : mntNames) {
                mntComboBox.addItem(mntname);
            }

            invertedPanel.add(mntComboBox);
            mntComboBox.setVisible(false);

            invertImported = new JCheckBox("Invert");
            invertImported.setName(SatHistConstants.INVERT_IMPORTED_CHECKBOX);
            invertedPanel.add(invertImported);
            invertImported.setVisible(false);

            c.gridx++;
            add(invertedPanel, c);
        }

        c.gridx++;
        add(bluredBorderPanel, c);

        c.gridx++;
        add(new JPanel(),c);

        c.gridx++;
        c.anchor = GridBagConstraints.WEST;
        add(addButton, c);


    }

    public void update(String transitionName, int blurWidth, boolean isBorderBlur) {
        transitionCB.update(transitionName);
        //System.out.println("blur width! "+(""+blurWidth));
        if (blurWidth==0) {
            blurredBorderWidthTextArea.setText(""+defaultBorder);
        } else {
            blurredBorderWidthTextArea.setText("" + blurWidth);
        }
        blurredCheckbox.setSelected(isBorderBlur);
        setUpdateButtonEnabled(false);

    }

    public void enableBlurredBorderPanel(boolean enabled) {
        for (Component component: bluredBorderPanel.getComponents()) {
            component.setEnabled(enabled);
        }
    }

    public void addListenersAnimationSelection(final StateMachine stateMachine) {
        addListenersToButton(addButton, stateMachine);
        addListenersToButton(addEqualizedButton, stateMachine);
        addListenersToButton(saveButton, stateMachine);

    }

    public void addListenersCanvas(final StateMachine stateMachine) {
        transitionCB.addListeners(stateMachine);
        mntComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new ComboBoxEvent((String)mntComboBox.getSelectedItem(), mntComboBox));
            }
        });
        blurredCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new CheckBoxEvent(blurredCheckbox.isSelected(), blurredCheckbox.getName()));
            }
        });
        blurredBorderWidthTextArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new TextFieldEvent(blurredBorderWidthTextArea, blurredBorderWidthTextArea.getText()));
            }
        });
        invertImported.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new CheckBoxEvent(invertImported.isSelected(), invertImported.getName()));
            }
        });
        blurredBorderWidthTextArea.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                //System.out.println("document listener! "+blurredBorderWidthTextArea.getText());
                stateMachine.handleEvent(new TextFieldEvent(blurredBorderWidthTextArea, blurredBorderWidthTextArea.getText()));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                //System.out.println("document listener! "+blurredBorderWidthTextArea.getText());
                stateMachine.handleEvent(new TextFieldEvent(blurredBorderWidthTextArea, blurredBorderWidthTextArea.getText()));
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                //System.out.println("document listener! "+blurredBorderWidthTextArea.getText());
                stateMachine.handleEvent(new TextFieldEvent(blurredBorderWidthTextArea, blurredBorderWidthTextArea.getText()));
            }
        });
    }

    public void addListenersToButton(final JButton button, final StateMachine stateMachine ) {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("animation add panel button pressed!");
                stateMachine.handleEvent(new ButtonEvent(button, button.getActionCommand()));
            }
        });



    }

    public void setMntComboBoxVisible(boolean visible) {
        mntComboBox.setVisible(visible);
        invertImported.setVisible(visible);
    }

    public String getSelectedMntInComboBox() {
        return (String)mntComboBox.getSelectedItem();
    }

    public void setAllEnabled(boolean enabled) {
        for (Component component : transitionCB.getComponents()) {
            component.setEnabled(enabled);
        }
        enableBlurredBorderPanel(enabled);
        if (!enabled) {
            setUpdateButtonEnabled(enabled);

        }
        setMntComboBoxVisible(false);
    }

    public void reset() {
        transitionCB.getComboBox().setSelectedIndex(-1);
        blurredCheckbox.setSelected(false);
        blurredBorderWidthTextArea.setText(defaultBorder+"");
    }

    public void setUpdateButtonEnabled(boolean enabled) {

        addButton.setEnabled(enabled);
        if (enabled) {
            addButton.setUI(enabledButtonUI);
        }
        else {
            addButton.setUI(disabledButtonUI);
        }
    }

    public void updateMNT(List<String> mntNames) {
        mntComboBox.removeAllItems();
        for (String name : mntNames) {
            mntComboBox.addItem(name);
        }
    }






}
