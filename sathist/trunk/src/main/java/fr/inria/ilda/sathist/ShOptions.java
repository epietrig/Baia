/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by mjlobo on 04/12/14.
 */
public class ShOptions {
    @Option(name = "-nofs", aliases = {"--nofullscreen"}, usage = "disable full-screen")
    public boolean nofullscreen = false;

    @Option(name = "-retina", aliases = {"--isRetina"}, usage = "use for retina displays")
    public boolean isRetina = false;

//    @Option(name="-sld", aliases = {"--styledescriptor"}, usage = "Style descriptor layer file")
//    public String sld="";

//    @Option(name="-rasters", aliases = {"--rasterImages"}, usage = "folder containing raster layers")
//    public String rasterPath= null;

    @Option (name="-martinique", aliases = {"--martinique"}, usage="load martinique maps")
    public boolean martinique = false;

    @Option (name="-martinique_complete", aliases = {"--martinique complete"}, usage="load martinique maps with more vector files")
    public boolean martinique_complete=false;

    @Option (name="-nepal", aliases = {"--nepal"}, usage="load nepal maps")
    public boolean nepal = false;

    @Option(name="-lanalhue", aliases ={"--lanalhue"}, usage= "load lanalhue maps")
    public boolean lanalhue = false;

    @Option(name="-trekking", aliases ={"--trekking"}, usage= "load trekking maps")
    public boolean trekking = false;

    @Option(name="-tsunami", aliases = {"--tsunami"}, usage="load tsunami maps")
    public boolean tsunami = false;

    @Option(name="-difference", aliases={"--difference"}, usage="chose difference for martinique maps")
    public String difference = "";

    @Option(name="-scan", aliases={"--scan"}, usage="scenario with different scans")
    public boolean scan = false;

    @Option(name="-martinique_full", aliases=("--mcomplete"), usage="load complete martinique maps")
    public boolean martiniqueFull = false;

    @Option(name="-folder", aliases=("--folder"), usage="load all files in folder")
    public String folder="";

    @Option(name="-project", aliases=("--project"), usage="load project")
    public String project = "";

    @Option(name="-participant", aliases="--p", usage="number of participant")
    public String participant="0";

    @Option(name="-trial", aliases="--t", usage="trial of participant")
    public String trial="0";


    //expe options
    @Option(name = "-t", aliases = {"--first-trial"}, usage = "first trial")
    public int firstTrial = -1;

    @Option(name = "-datasets", aliases = {"--datasets-path"}, usage = "datasets")
    public String datasets = "";

    @Option(name = "-trials", aliases = {"--trials-path"}, usage = "datasets")
    public String trials = "";

    @Option(name="-expe", aliases ={"--experiment"}, usage= "is experiment")
    public boolean isExpe = false;

    @Option(name="-export", aliases = {"--export-folder"}, usage= "folder to export")
    public String export = "";






    @Argument
    List<String> arguments = new ArrayList<String>();

}
