/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * Created by mjlobo on 12/09/16.
 */
public class InsideBorderPanel extends JPanel{
    int lineWidth;
    Color lineColor;
    int lineAlpha;
    int width;
    int height;

    public InsideBorderPanel(int lineWidth, Color lineColor, int lineAlpha, int witdh, int height) {
        this.lineWidth = lineWidth;
        this.lineColor = lineColor;
        this.lineAlpha = lineAlpha;
        this.width = witdh;
        this.height = height;
        //setBorder(new LineBorder(Color.GRAY));
        repaint();
    }

    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g = (Graphics2D) graphics;
        Stroke oldStroke = g.getStroke();
        Color oldColor = g.getColor();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setStroke(new BasicStroke(lineWidth));
        g.setColor(lineColor);
        g.drawRect(0,0,getWidth()-1, getHeight()-1);
        g.setStroke(oldStroke);
        g.setColor(oldColor);
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
        repaint();
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
        repaint();
    }

}
