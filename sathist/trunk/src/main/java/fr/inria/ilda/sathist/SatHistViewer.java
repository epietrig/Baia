/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.FPSAnimator;
import fr.inria.ilda.mmtools.animation.*;
import fr.inria.ilda.mmtools.canvasEvents.*;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.gl.MapGLCanvasListener;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.morphingAlgorithm.MorphingAlgorithm;
import fr.inria.ilda.mmtools.morphingAlgorithm.NotEnoughCornersExeption;
import fr.inria.ilda.mmtools.stateMachine.ButtonEvent;
import fr.inria.ilda.mmtools.stateMachine.MenuActionEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.mmtools.ui.ModeToolBar;
import fr.inria.ilda.mmtools.ui.PopUpMenu;
import fr.inria.ilda.mmtools.ui.StringComboPanel;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.mmtools.utilties.Pair;
import fr.inria.ilda.sathist.ui.*;
import org.apache.commons.io.FilenameUtils;
import org.lwjgl.Sys;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.TiledImage;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.List;
import org.opencv.videoio.VideoWriter;

/**
 * Created by mjlobo on 21/04/16.
 */
public class SatHistViewer implements MapGLCanvasListener, StaticImageCanvasControllerListener{

    List<ImageCanvasController> imageCanvasControllers;
    StaticImageCanvasController beforeController;
    StaticImageCanvasController afterController;
    AnimatedImageCanvasController previewController;

    LayerManager beforeLayerManager;
    public final JFrame mainFrame = new JFrame();
    public final JFrame previewFrame = new JFrame();
    StateMachine stateMachine;
    double borderForOffsetPolygons;
    RasterGeoElement beforeRaster;
    RasterGeoElement afterRaster;
    BoundingBox sceneBounds;
    double scene_pixels_per_unit;
    double maxAlphaRadius = 20;
    static int SCREEN_WIDTH =  1280;
    static int SCREEN_HEIGHT =  1160;
    java.util.List rasterNames;

    static String BEFORE_CANVAS = "before_canvas";
    static String AFTER_CANVAS = "after_canvas";
    static String PREVIEW_CANVAS = "preview_canvas";

    //Modes
    static String NAVIGATION_MODE = "navigationMode";
    static String MAGIC_WAND ="magicWand";
    static String SEPARATOR = "Separator";
    static String MAGIC_WAND_ALL = "magicWandAll";
    static String ANIMATION = "animation";
    static String BOTH = "both";
    static String ONLY_ONE = "onlyOne";
    java.util.List<String> modes;
    java.util.List<String> selectionModes;
    ModeToolBar modeToolBar;
    ModeToolBar selectionModesToolBar;
    PopUpMenu popupMenu;
    JFileChooser fileChooser;
    JFileChooser folderChooser;
    TimeLineController timeLineController;
    AnimationControlPanel animationControlPanel;
    AnimationAddPanel animationAddPanel;
    StringComboPanel beforeRasterCombo;
    StringComboPanel afterRasterCombo;
    DefaultComboBoxModel<String> rasterComboModel;
    JPanel animationControlsPanel;
    JButton nextTrialButton;
    JButton startTrialButton;
    JButton backTrialButton;
    String selectedTransition = Constants.BLEND_TRANSITION;
    String selectedMnt;
    boolean invertedImported;
    JSplitPane splitPane;

    String selectionMode = ONLY_ONE;
    List<StaticImageCanvasController> selectedStatics;
    List<RasterLayer> rasterLayers;

    int canvasProportionH = SCREEN_HEIGHT/3;
    int timeLineProportionH = SCREEN_HEIGHT/9;
    int animationControlsH = 45;
    int modetoolbarProportionH=50;
    int canasplusmodetoolbarProportionH = modetoolbarProportionH+canvasProportionH;

    double[] startPoint;
    double[] endPoint;

    List<String> mntNames;

    Mat copiedMask;

    JMenuBar mainMenuBar;
    JMenu actionsMenu;

    JButton addStagedAnimationButton;

    String timeLineName = "timeLine";
    StateMachine animationSelectionStateMachine;
    PopUpMenu timeLinePopUpMenu;
    DurationDialog durationDialog;
    LoadingDialog loadingDialog;

    HashMap<String, Cursor> cursors = new HashMap<>();
    int cursorSize = 26;

    String participant;
    String trial;

    boolean isRetina;

     enum CHANGES {TYPE, MASK, BLURRED, BLURRED_WIDTH, DEC, IMPORTED}
    List<CHANGES> changes = new ArrayList<>();

    Action displayPreviewWindow = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            displayPreviewWindow();
        }
    };

    Action exportMovieAndScreenshot = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            exportVideoAndScreenShot();
        }
    };


    static {
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }


    public SatHistViewer(ShOptions options) {
        imageCanvasControllers = new ArrayList<>();
        rasterLayers = new ArrayList<>();
        beforeController = new StaticImageCanvasController(BEFORE_CANVAS);
        beforeController.addListener(this);
        afterController = new StaticImageCanvasController(AFTER_CANVAS);
        afterController.addListener(this);
        imageCanvasControllers.add(beforeController);
        imageCanvasControllers.add(afterController);
        selectedStatics = new ArrayList<>();
        mntNames = new ArrayList<>();

        beforeLayerManager = new LayerManager();
        modes = new ArrayList<>();
        rasterNames = new ArrayList();
        selectionModes = new ArrayList<>();
        modes.add(NAVIGATION_MODE);
        modes.add(StateMachineBuilder.lasso);
        modes.add(StateMachineBuilder.lassoDelete);
        modes.add(MAGIC_WAND);
        modes.add(MAGIC_WAND_ALL);
        modes.add(SEPARATOR);
        selectionModes.add(ONLY_ONE);
        selectionModes.add(BOTH);
        List<String> popUpActions = new ArrayList<>();
        popUpActions.add(SatHistConstants.UNDO_LAST_ACTION);
        popUpActions.add(SatHistConstants.NEGATE_MASK_ACTION);
        popUpActions.add(SatHistConstants.COPY_MASK);
        popUpActions.add(SatHistConstants.PASTE_MASK);
        popUpActions.add(SatHistConstants.REPLICATE_MASK);
        popUpActions.add(SatHistConstants.LOAD_MASK);
        popUpActions.add(SatHistConstants.SAVE_MASK);
        popUpActions.add(SatHistConstants.SELECT_CONTOUR);
        popUpActions.add(SatHistConstants.EDIT_DIRECTION);
        popUpActions.add(SatHistConstants.EDIT_RADIAL_CENTER);

        popupMenu = new PopUpMenu(popUpActions);

        List<String> timeLinePopUpActions = new ArrayList<>();
        timeLinePopUpActions.add(SatHistConstants.ADD_STAGED_ANIMATION_ACTION);
        timeLinePopUpActions.add(SatHistConstants.ADD_STAGE_ACTION);
        timeLinePopUpActions.add(SatHistConstants.ADD_EQ_ANIMATION_ACTION);


        timeLinePopUpMenu = new PopUpMenu(timeLinePopUpActions);

        fileChooser = new JFileChooser();
        folderChooser = new JFileChooser();
        folderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);


        if (!options.folder.equals("")) {
            File folder = new File(options.folder);
            File[] listOfFiles = folder.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    String ext = FilenameUtils.getExtension(listOfFiles[i].getAbsolutePath());
                    Constants.EXT extEnum = null;
                    if (ext.equals("shp") || ext.equals("SHP")) {
                        initVectorLayers(false, listOfFiles[i].getAbsolutePath(), listOfFiles[i].getName());
                    } else if (ext.equals("tif") || (ext.equals("TIF") || (ext.equals("tiff") || (ext.equals("TIFF"))))) {
                        extEnum = Constants.EXT.TIF;
                    } else if (ext.equals("ecw") || (ext.equals("ECW"))) {
                        extEnum = Constants.EXT.ECW;
                    } else if (ext.equals("jpg") || ext.equals("JPG")) {
                        extEnum = Constants.EXT.JPG;
                    }
                    if (extEnum == Constants.EXT.TIF || extEnum == Constants.EXT.JPG || extEnum == Constants.EXT.ECW) {
                        initRasterLayers(listOfFiles[i].getAbsolutePath(), false, listOfFiles[i].getName(), extEnum);
                    }
                } else if (listOfFiles[i].isDirectory()) {
                    //System.out.println("Directory " + listOfFiles[i].getName());
                }
            }


        }
        participant = options.participant;
        trial = options.trial;
        createCursors();
        this.isRetina = options.isRetina;
        initGL(options.isRetina);
        if (rasterNames.size()>1) {
            addStagedAnimationButton.setEnabled(true);
        }
        //System.out.println("RETINA  "+Utils.hasRetinaDisplay());
        //initGL(Utils.hasRetinaDisplay());
        initStateMachine();
        if (!options.project.equals("")) {
            File file = new File(options.project);
            loadProject(file.getPath());
        }


    }

    private Layer initVectorLayers(boolean isVisible, String path, String name) {
        return beforeLayerManager.initVectorLayers(isVisible, path, name,beforeRaster.getTexture(),borderForOffsetPolygons,sceneBounds);
    }

    private Layer initRasterLayers(String tiffName, boolean isVisible, String name) {
        return initRasterLayers(tiffName, isVisible, name, Constants.EXT.TIF);
    }
    protected Layer initRasterLayers (String tiffName, boolean isVisible, String name, Constants.EXT ext) {
        RasterLayer rasterLayer = null;
        rasterLayer = (RasterLayer) LayerManager.initRasterLayers(tiffName, isVisible, name, ext);
        rasterLayers.add(rasterLayer);
        RasterGeoElement  rasterGeoElement = (RasterGeoElement)rasterLayer.getElements().get(0);
        if (rasterGeoElement instanceof MNTRasterGeoElement) {
            mntNames.add(rasterGeoElement.getName());
        }
        for (ImageCanvasController imageCanvasController : imageCanvasControllers) {
            imageCanvasController.getLayerManager().addLayer(rasterLayer, false,0);
        }
        if (beforeRaster == null) {
            beforeRaster = rasterGeoElement;
        }
        else {
            if (afterRaster == null) {
                afterRaster = rasterGeoElement;
            }
        }

        if (sceneBounds == null) {
            sceneBounds = new BoundingBox(rasterGeoElement.getRealWidth(), rasterGeoElement.getRealHeight(), rasterGeoElement.getLowerCorner());
        }
        else {
            sceneBounds.union(new BoundingBox(rasterGeoElement.getRealWidth(), rasterGeoElement.getRealHeight(), rasterGeoElement.getLowerCorner()).getGeometry());
        }

        if (scene_pixels_per_unit == -1) {
            scene_pixels_per_unit = rasterGeoElement.getPixels_per_unit();
        }
        else if (scene_pixels_per_unit < rasterGeoElement.getPixels_per_unit()) {
            scene_pixels_per_unit = rasterGeoElement.getPixels_per_unit();
        }

        borderForOffsetPolygons= maxAlphaRadius/scene_pixels_per_unit;
        rasterNames.add(rasterGeoElement.getName());
        return rasterLayer;
    }

    public String getRasterPath(String rasterName) {
        for (Layer layer : rasterLayers) {
            RasterLayer rasterLayer = (RasterLayer)layer;
            if (rasterLayer.getName().equals(rasterName)) {
                RasterGeoElement rasterGeoElement = (RasterGeoElement)rasterLayer.getElements().get(0);
                return rasterGeoElement.getPath();
            }
        }
        return null;
    }


    protected RasterGeoElement addNewRaster(String tifName, boolean isVisible, String name, Constants.EXT ext, boolean throwEvent ) {
        Layer rasterLayer = initRasterLayers(tifName, isVisible, name, ext);
        if (rasterLayer.getElements().get(0) instanceof RGBRasterGeoElement) {
            for (ImageCanvasController imageCanvasController : imageCanvasControllers) {
                imageCanvasController.newRaster((RasterGeoElement) rasterLayer.getElements().get(0), rasterLayer);
                initCameraParameters(imageCanvasController.getCanvasPanel().getCanvas(), isRetina);
                if (imageCanvasController != previewController && throwEvent) {
                    imageCanvasController.getCanvasPanel().getCanvas().pushEvent(new CanvasEventBasic(CanvasEvent.FINISHED_LOADING_RASTERS));
                }
                if (imageCanvasController == previewController && previewController.getAnimationManager().getAnimationRaster() != null) {
                    imageCanvasController.getCanvasPanel().getCanvas().pushEvent(new CanvasEventBasic(CanvasEvent.FINISHED_LOADING_RASTERS));
                }
            }
            if (previewController.getAnimationManager().getAnimationRaster() == null) {
                previewController.createAnimationRaster((RasterGeoElement) rasterLayer.getElements().get(0));
                if (throwEvent) {
                    previewController.getCanvasPanel().getCanvas().pushEvent(new CanvasEventBasic(CanvasEvent.FINISHED_LOADING_RASTERS));
                }
            }
        }
        return (RasterGeoElement)rasterLayer.getElements().get(0);
    }

    public void createCursors () {
        cursors.put(StateMachineBuilder.navigationMode, Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon(new ImageIcon("icons/cursor/navigate_cursor.png").getImage().getScaledInstance(cursorSize, cursorSize, Image.SCALE_SMOOTH)).getImage(),
                new Point(0,0),"navigation cursor"));
        cursors.put(StateMachineBuilder.lasso, Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon(new ImageIcon("icons/cursor/lasso.png").getImage().getScaledInstance(cursorSize, cursorSize, Image.SCALE_SMOOTH)).getImage(),
                new Point(0,0),"lasso cursor"));
        cursors.put(StateMachineBuilder.lassoDelete, Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon(new ImageIcon("icons/cursor/lassodelete.png").getImage().getScaledInstance(cursorSize, cursorSize, Image.SCALE_SMOOTH)).getImage(),
                new Point(0,0),"lasso delete cursor"));
        cursors.put(StateMachineBuilder.magicWand, Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon(new ImageIcon("icons/cursor/magicwand.png").getImage().getScaledInstance(cursorSize, cursorSize, Image.SCALE_SMOOTH)).getImage(),
                new Point(0,0),"magic wand cursor"));
        cursors.put(StateMachineBuilder.magicWandAll, Toolkit.getDefaultToolkit().createCustomCursor(
                new ImageIcon(new ImageIcon("icons/cursor/magicwandall.png").getImage().getScaledInstance(cursorSize, cursorSize, Image.SCALE_SMOOTH)).getImage(),
                new Point(0,0),"magic wand all cursor"));
        cursors.put(SatHistConstants.DEFAULT_CURSOR, Cursor.getDefaultCursor());

    }public void setCursor(String mode) {
        for (ImageCanvasController controller : imageCanvasControllers) {
            if (mode.equals(StateMachineBuilder.navigationMode)) {
                controller.setCursor(cursors.get(mode));
                //mainFrame.setCursor(cursors.get(mode));
            }
            else {
                if (controller instanceof StaticImageCanvasController) {
                    controller.setCursor(cursors.get(mode));
                    //mainFrame.setCursor(cursors.get(mode));
                }
            }
        }
    }

    public HashMap<String, Cursor> getCursors() {
        return cursors;
    }

    void initGL(boolean isRetina) {
        final GLCapabilities glCapabilities = new GLCapabilities(GLProfile.get(GLProfile.GL4));
        glCapabilities.setStencilBits(8);

        durationDialog = new DurationDialog(mainFrame);


        actionsMenu = new JMenu("File");
        actionsMenu.add(new JMenuItem(SatHistConstants.OPEN_MENU_TEXT));
        actionsMenu.add(new JMenuItem(SatHistConstants.SAVE_MENU_TEXT));
        actionsMenu.add(new JMenuItem(SatHistConstants.EXPORT_MENU_TEXT));
        actionsMenu.add(new JMenuItem(SatHistConstants.SAVE_PROJECT_MENU_TEXT));
        actionsMenu.add(new JMenuItem(SatHistConstants.OPEN_PROJECT_MENU_TEXT));
        actionsMenu.add(new JMenuItem(SatHistConstants.QUIT_APPLICATION_MENU_TEXT));
        mainMenuBar = new JMenuBar();
        mainMenuBar.add(actionsMenu);

        JPanel canvasesPanel = new JPanel();



        beforeRasterCombo = new StringComboPanel("Start",rasterNames, BEFORE_CANVAS);
        afterRasterCombo = new StringComboPanel("End", rasterNames, AFTER_CANVAS);
        JPanel beforeAfterPanel = new JPanel();
        animationControlsPanel = new JPanel();
        final JPanel animationPreview = new JPanel();
        timeLineController = new TimeLineController(SCREEN_WIDTH, timeLineProportionH, this, timeLineName);
        animationPreview.setLayout(new BoxLayout(animationPreview, BoxLayout.LINE_AXIS));


        animationControlsPanel.setPreferredSize(new Dimension(SCREEN_WIDTH, animationControlsH));

        animationControlsPanel.setMinimumSize(new Dimension(SCREEN_WIDTH, animationControlsH));
        animationControlPanel = new AnimationControlPanel();
        animationControlsPanel.setLayout(new GridLayout(1,3));
        JPanel backPanel = new JPanel();
        backPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        backTrialButton = new JButton("Back");
        backPanel.add(backTrialButton);
        animationControlsPanel.add(backPanel);
        animationControlsPanel.add(animationControlPanel);
        JPanel nextButtonPanel = new JPanel();
        nextButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        startTrialButton = new JButton("Start");
        nextTrialButton = new JButton("Next");
        nextButtonPanel.add(nextTrialButton);
        nextButtonPanel.add(startTrialButton);
        animationControlsPanel.add(nextButtonPanel);
        nextTrialButton.setVisible(false);
        startTrialButton.setVisible(false);


        animationAddPanel = new AnimationAddPanel(mntNames);
        animationAddPanel.setPreferredSize(new Dimension(SCREEN_WIDTH, animationControlsH+20));
        animationAddPanel.setMinimumSize(new Dimension(SCREEN_WIDTH, animationControlsH+20));
        animationAddPanel.setMaximumSize(new Dimension(SCREEN_WIDTH, animationControlsH+20));


        //animationPreview.setPreferredSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));
        canvasesPanel.setLayout(new BoxLayout(canvasesPanel, BoxLayout.LINE_AXIS));

        JPanel modesToolBarPanel = new JPanel();
        modesToolBarPanel.setLayout(new BoxLayout(modesToolBarPanel,BoxLayout.LINE_AXIS));
        modeToolBar = new ModeToolBar(modes,20,20);
        modeToolBar.setOnlyButtonSelected(NAVIGATION_MODE);

        selectionModesToolBar = new ModeToolBar(selectionModes,20,20);
        selectionModesToolBar.setOnlyButtonSelected(selectionMode);
        modesToolBarPanel.add(modeToolBar);
        modesToolBarPanel.add(selectionModesToolBar);
        addStagedAnimationButton = new JButton("Add Before/After Transition");
        addStagedAnimationButton.setEnabled(false);
        addStagedAnimationButton.setActionCommand(SatHistConstants.ADD_STAGED_ANIMATION_ACTION);
        modesToolBarPanel.setPreferredSize(new Dimension(SCREEN_WIDTH, modetoolbarProportionH));
        modesToolBarPanel.add(addStagedAnimationButton);
        modesToolBarPanel.setMinimumSize(new Dimension(SCREEN_WIDTH, modetoolbarProportionH));
        //modesToolBarPanel.setMaximumSize(new Dimension(SCREEN_WIDTH, modetoolbarProportionH));

        GLWindow beforeWindow = GLWindow.create(glCapabilities);
        final MapGLCanvas beforeCanvas = new MapGLCanvas(beforeController.getLayerManager(), beforeWindow,isRetina);
        //canvases.put(beforeCanvas, BEFORE_CANVAS);
        GLWindow afterWindow = GLWindow.create(glCapabilities);
        MapGLCanvas afterCanvas = new MapGLCanvas(afterController.getLayerManager(), afterWindow, isRetina);
        //canvases.put(afterCanvas, AFTER_CANVAS);
        //GLWindow previewWindow = GLWindow.create(glCapabilities);

        //MapGLCanvas previewCanvas = new MapGLCanvas(previewController.getLayerManager(), previewWindow, isRetina);
        if (beforeRaster != null) {
            previewController = new AnimatedImageCanvasController(PREVIEW_CANVAS, glCapabilities, isRetina, beforeRaster, afterRaster);
        }
        else {
            previewController = new AnimatedImageCanvasController(PREVIEW_CANVAS, glCapabilities, isRetina);
        }
        for (RasterLayer rasterLayer : rasterLayers) {
            previewController.getLayerManager().addLayer(rasterLayer, false);
        }
        imageCanvasControllers.add(previewController);
        previewController.getCanvasPanel().getCanvas().addListener(this);
        //AnimationPlanManager animationPlanManager = new AnimationPlanManager(previewController.getLayerManager(), previewCanvas);
        //canvases.put(previewCanvas, PREVIEW_CANVAS);

        ImageCanvasPanel beforePanel = new StaticImageCanvasPanel(beforeCanvas,beforeRasterCombo,BEFORE_CANVAS);
        //beforePanel.update(beforeRaster.getName());
        ImageCanvasPanel afterPanel = new StaticImageCanvasPanel(afterCanvas, afterRasterCombo, AFTER_CANVAS);
        //afterPanel.update(afterRaster.getName());


        //ImageCanvasPanel previewPanel = new AnimatedImageCanvasPanel(previewCanvas, PREVIEW_CANVAS);
        beforeController.setCanvasPanel(beforePanel);
        beforeController.getCanvasPanel().getCanvas().addListener(this);
        afterController.setCanvasPanel(afterPanel);
        afterController.getCanvasPanel().getCanvas().addListener(this);
        ((StaticImageCanvasPanel)beforeController.getCanvasPanel()).addKeyListeners(this);
        ((StaticImageCanvasPanel)afterController.getCanvasPanel()).addKeyListeners(this);
        //previewController.setCanvasPanel(previewPanel);



        //beforePanel.add(beforeCanvas, BorderLayout.CENTER);
        //afterPanel.add(afterCanvas, BorderLayout.CENTER);
        animationPreview.add(previewController.getCanvasPanel());
        //animationPreview.add(Box.createRigidArea(new Dimension(SCREEN_WIDTH/4, canvasProportionH)));


        canvasesPanel.add(beforePanel);
        canvasesPanel.add(afterPanel);
        initKeyBindings(canvasesPanel);

        beforeCanvas.setPreferredSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));

        //beforeCanvas.setMinimumSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));

        //beforeCanvas.setMaximumSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));
        afterCanvas.setPreferredSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));
        //afterCanvas.setMinimumSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));

        //afterCanvas.setMaximumSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));
        //previewController.getCanvasPanel().getCanvas().setPreferredSize(new Dimension(SCREEN_WIDTH/2, canvasProportionH));

        beforeAfterPanel.setLayout(new BorderLayout());
        beforeAfterPanel.setPreferredSize(new Dimension(SCREEN_WIDTH, canasplusmodetoolbarProportionH));
        beforeAfterPanel.setMinimumSize(new Dimension(SCREEN_WIDTH, canasplusmodetoolbarProportionH));
        //beforeAfterPanel.setMaximumSize(new Dimension(SCREEN_WIDTH, canasplusmodetoolbarProportionH));

//        initCameraParameters(afterCanvas, isRetina);
//        initCameraParameters(beforeCanvas, isRetina);
//        initCameraParameters(previewController.getCanvasPanel().getCanvas(), isRetina);

        beforeWindow.addGLEventListener(beforeCanvas);
        afterWindow.addGLEventListener(afterCanvas);

        //initCameraParameters();
        final FPSAnimator beforeAnimator = new FPSAnimator(beforeWindow, 120, true);
        final FPSAnimator afterAnimator = new FPSAnimator(afterWindow,120, true);
        final FPSAnimator previewAnimator = new FPSAnimator(previewController.getGlWindow(), 120, true);

        // Create the top-level container
        // Swing's JFrame or AWT's Frame
        //beforeAfterPanel.add(modeToolBar, BorderLayout.PAGE_START);
        beforeAfterPanel.add(modesToolBarPanel, BorderLayout.PAGE_START);
        beforeAfterPanel.add(canvasesPanel, BorderLayout.CENTER);
        //beforeAfterPanel.setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT/2));
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // Use a dedicate thread to run the stop() to ensure that the
                // animator stops before program exits.
                new Thread() {
                    @Override
                    public void run() {
                        if (beforeAnimator.isStarted()) beforeAnimator.stop();
                        if (afterAnimator.isStarted()) afterAnimator.stop();
                        if (previewAnimator.isStarted()) previewAnimator.stop();
                        System.exit(0);
                    }
                }.start();
            }
        });
        mainFrame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                timeLineController.resize();
            }
        });
        splitPane= new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setDividerSize(4);
        splitPane.setOneTouchExpandable(true);
        mainFrame.getContentPane().setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.PAGE_AXIS));
        mainFrame.add(splitPane);
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.PAGE_AXIS));
        topPanel.add(beforeAfterPanel);
        topPanel.add(animationAddPanel);
        splitPane.setTopComponent(topPanel);

        JPanel bottomPanel = new JPanel();
        //bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.PAGE_AXIS));
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.add(timeLineController.getView(), BorderLayout.CENTER);
        bottomPanel.add(animationControlPanel, BorderLayout.PAGE_END);
        splitPane.setBottomComponent(bottomPanel);
        splitPane.setResizeWeight(0.5);
//        mainFrame.getContentPane().setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.PAGE_AXIS));
//        mainFrame.add(beforeAfterPanel);
//        mainFrame.add(animationAddPanel);
//        mainFrame.add(timeLineController.getView());
//        mainFrame.add(animationControlsPanel);

        mainFrame.setJMenuBar(mainMenuBar);

        //mainFrame.add(timeLineController.getTimeLine());

        //mainFrame.add(animationPreview);
        mainFrame.setTitle("Baia");
        mainFrame.pack();

        previewFrame.add(animationPreview);
        previewFrame.setTitle("Preview");
        previewFrame.getRootPane().setBorder(new LineBorder(new Color(238,238,238), 2));

       // previewFrame.setUndecorated(true);
        previewFrame.pack();

        //mainFrame.setVisible(true);

        initCameraParameters(afterCanvas, isRetina);
        initCameraParameters(beforeCanvas, isRetina);
        //initCameraParameters(previewController.getCanvasPanel().getCanvas(), isRetina);

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    mainFrame.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
                    mainFrame.pack();
                    mainFrame.setVisible(true);

                    animationPreview.setPreferredSize(new Dimension(SCREEN_WIDTH/2,beforeCanvas.getHeight()));
                    previewFrame.setSize(new Dimension(SCREEN_WIDTH/2,beforeCanvas.getHeight()));
                    //previewFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                    //previewFrame.setUndecorated(true);
                   // previewFrame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
                    previewFrame.pack();
                    previewFrame.setVisible(true);
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        initCameraParameters(previewController.getCanvasPanel().getCanvas(), isRetina);
        beforeAnimator.start();
        afterAnimator.start();
        previewAnimator.start();
    }

    public void initKeyBindings(JPanel panel) {
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("P"),
                "pressed");
        panel.getActionMap().put("pressed", displayPreviewWindow);
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("E"), "export");
        panel.getActionMap().put("export", exportMovieAndScreenshot);
    }

    public void displayPreviewWindow() {
        previewFrame.pack();
        previewFrame.setVisible(true);
    }

    public void updateRasterCombos() {
        beforeRasterCombo.removeAllItems();
        afterRasterCombo.removeAllItems();
        for (Object rasterName : rasterNames) {
            beforeRasterCombo.addItem((String)rasterName);
            afterRasterCombo.addItem((String)rasterName);
        }
        beforeRasterCombo.revalidate();
        beforeRasterCombo.getComboBox().setSelectedIndex(-1);
        afterRasterCombo.revalidate();
        afterRasterCombo.getComboBox().setSelectedIndex(-1);
    }

    public void updateMNTNames() {
        animationAddPanel.updateMNT(mntNames);
    }



    public void initCameraParameters (MapGLCanvas canvas, boolean isRetina) {
        if (beforeRaster != null) {
            Vector3d eyeInit = new Vector3d(beforeRaster.getX(), beforeRaster.getY(), 5.0);
            Vector3d centerInit = new Vector3d(beforeRaster.getX(), beforeRaster.getY(), 0.0);

            double pixelCoef = Math.min(((double) canvas.getWidth()) / (double) beforeRaster.getImageWidth(), ((double) canvas.getHeight()) / (double) beforeRaster.getImageHeight());

            canvas.getCamera().setEye(eyeInit);
            canvas.getCamera().setCenter(centerInit);

            //canvasPanel.getCanvas().getCamera().setPixels_per_unit(new double[] {visibleRaster.getPixels_per_unit_array()[0],visibleRaster.getPixels_per_unit_array()[1]});
            if (isRetina) {
                canvas.getCamera().setPixels_per_unit(new double[]{beforeRaster.getPixels_per_unit_array()[0] * pixelCoef, beforeRaster.getPixels_per_unit_array()[1] * pixelCoef});
                canvas.getCamera().setCamera_pixels_per_unit(beforeRaster.getPixels_per_unit());
            } else {
                canvas.getCamera().setPixels_per_unit(new double[]{beforeRaster.getPixels_per_unit_array()[0] * pixelCoef, beforeRaster.getPixels_per_unit_array()[1] * pixelCoef});
                canvas.getCamera().setCamera_pixels_per_unit(beforeRaster.getPixels_per_unit());
            }
            canvas.getCamera().updateParameters(isRetina, canvas.getWidth(), canvas.getHeight(), new int[]{0, 0, 0, 0});
        }
//        canvas.getCamera().printCameraParameters();
//        //System.out.println ("pixels per unit array "+beforeRaster.getPixels_per_unit_array()[0] + " "+beforeRaster.getPixels_per_unit_array()[1]);
    }

    public void initStateMachine() {
        stateMachine = StateMachineBuilder.createStateMachine(imageCanvasControllers, this);
        //stateMachine.setState(StateMachineBuilder.navigationMode);
        stateMachine.start();
        for (ImageCanvasController controller: imageCanvasControllers) {
            controller.getCanvasPanel().addListeners(stateMachine);
        }
        modeToolBar.addListeners(stateMachine);
        selectionModesToolBar.addListeners(stateMachine);
        animationControlPanel.addListeners(stateMachine);
        animationAddPanel.addListenersCanvas(stateMachine);
        popupMenu.addListeners(stateMachine);

        timeLineController.addListeners(stateMachine);
        for (int i=0; i<actionsMenu.getItemCount(); i++) {
            final JMenuItem item = actionsMenu.getItem(i);
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    stateMachine.handleEvent(new MenuActionEvent(item, item.getText()));
                    //System.out.println("Action in PopUpMEnu "+((JMenuItem)e.getSource()).getText());
                }
            });
        }
        addStagedAnimationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                animationSelectionStateMachine.handleEvent(new ButtonEvent(addStagedAnimationButton, addStagedAnimationButton.getActionCommand()));
            }
        });

        animationSelectionStateMachine = StateMachineBuilder.createAnimationSelectionStateMachine(this, stateMachine);
        //animationSelectionStateMachine.setSource(timeLineController.getName(),timeLineController.getView());
        animationSelectionStateMachine.setSource(timeLineController.getName(),timeLineController.getLayeredStagesAndSlider());
        animationSelectionStateMachine.start();
        timeLinePopUpMenu.addListeners(animationSelectionStateMachine);
        animationAddPanel.addListenersAnimationSelection(animationSelectionStateMachine);
        durationDialog.addListeners(animationSelectionStateMachine);

//        playButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                stateMachine.handleEvent(new ButtonEvent(playButton, "PLAY"));
//
//            }
//        });
    }

    //Camera operations
    public void startPanning(int x, int y) {
        //startPointPx = new int[] {x,y};
        for (ImageCanvasController controller: imageCanvasControllers) {
            controller.startPanning(x, y);
        }
    }

    public void pan(int x, int y) {
//        for (MapGLCanvas canvas : canvases.keySet()) {
//            canvas.getCamera().moveCamera(startPointPx[0]-x,y-startPointPx[1]);
//        }
//        startPointPx[0] = x;
//        startPointPx[1] = y;
        for (ImageCanvasController controller: imageCanvasControllers) {
            //if (controller.getVisibleRaster()!=null) {
            //System.out.println("controller pan "+controller.getName());
                controller.pan(x, y);
            //}
        }

    }

    public void zoomIn(int x, int y, String canvasName) {
        ImageCanvasController targetController = getControllerByName(canvasName);
        targetController.zoomIn(x,y);
        for (ImageCanvasController controller: imageCanvasControllers) {
            if (controller != targetController) {
                controller.zoomInToCenter(new double[]{(double)targetController.getCanvasPanel().getCanvas().getCamera().getCenter().x,
                        (double)targetController.getCanvasPanel().getCanvas().getCamera().getCenter().y});
            }
        }
    }

    public void zoomOut(int x, int y, String canvasName) {
        ImageCanvasController targetController = getControllerByName(canvasName);
        targetController.zoomOut(x,y);
        for (ImageCanvasController controller: imageCanvasControllers) {
            if (controller != targetController) {
                controller.zoomOutToCenter(new double[]{(double)targetController.getCanvasPanel().getCanvas().getCamera().getCenter().x,
                        (double)targetController.getCanvasPanel().getCanvas().getCamera().getCenter().y});
            }
        }
    }

    //Animation Creation Operations

    public void enableStagedAnimationOptions(boolean enabled) {
        for (ImageCanvasController controller: imageCanvasControllers) {
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController)controller).enableStagedAnimationsOptions(enabled);
            }
        }
    }

    public void enableDeleteMasksButton(boolean enabled) {
        for (ImageCanvasController controller: imageCanvasControllers) {
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController)controller).enableDeleteMasksButton(enabled);
            }
        }
    }

    public void changeRaster (String name, String newRaster) {
        //System.out.println("new layer name "+newRaster);
        //System.out.println("layer manager name "+name);
        ImageCanvasController targetController = getControllerByName(name);
        targetController.setVisibleRaster(newRaster);
        if (newRaster != null) {
            if (name == BEFORE_CANVAS) {
                //System.out.println("before canvas!");
                previewController.setInitRaster(targetController.getVisibleRaster());
            } else if (name == AFTER_CANVAS) {
                //System.out.println("after canvas!");
                previewController.setEndRaster(targetController.getVisibleRaster());
            }
        }
        timeLineController.checkStagedAnimationCompleteness();
        timeLineController.updateStagedAnimationTitle(name, newRaster);


    }

    public ImageCanvasController getControllerByName(String name) {
        for (ImageCanvasController controller: imageCanvasControllers) {
            if (controller.getName().equals(name)) {
                return controller;
            }
        }
        return null;
    }

    public void deleteAllRasters() {
        stateMachine.setState(stateMachine.getState(StateMachineBuilder.navigationMode), true);
        blurredBorderEnabled = false;
        animationSelectionStateMachine.setState(animationSelectionStateMachine.getState(StateMachineBuilder.noSelection), true);
        rasterNames.clear();
        for (ImageCanvasController controller : imageCanvasControllers) {
            controller.reset();
        }
        beforeRaster = null;
        afterRaster = null;
        updateRasterCombos();
        timeLineController.reset();
        backwardAnimation();
        mainFrame.pack();
        copiedMask = null;
        rasterLayers.clear();


    }


    //Selection operations
    public void doMagicWandAll(String canvasName, int x, int y) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).doMagicWandAllInc(x, y);
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).doMagicWandAllInc(x, y);
                }
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //
                    setContoursVisible(true);
                }
            }
        }
    }

    public void setMagicWandThreshold(String name, float newValue) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(name);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).setMagicWandThreshold(newValue, false);
                //((StaticImageCanvasController)controller).pushMaskChangedEvent();
            }

        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).setMagicWandThreshold(newValue, true);
                }
            }
        }
    }

    public void updateMasks(String name) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(name);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).pushMaskChangedEvent();
                //((StaticImageCanvasController)controller).pushMaskChangedEvent();
            }

        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).pushMaskChangedEvent();
                }
            }
        }
    }

    public void setImageModel(JComboBox imageModelCB, String colorModelName) {
        //System.out.println("set image model in sat hist viewer!");
        ImageCanvasController selectedController = null;
        for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    if (controller.getCanvasPanel() instanceof StaticImageCanvasPanel) {
                        if (((StaticImageCanvasPanel) controller.getCanvasPanel()).getMaskOptions().getColorModelCB() == imageModelCB) {
                            selectedController = controller;
                            ((StaticImageCanvasController) controller).changeColorModel(colorModelName, false);
                        }
                    }
                }
            }
       if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController && selectedController != null && selectedController!=controller) {
                    ((StaticImageCanvasController) controller).changeColorModel(colorModelName, true);
                }
            }
        }
    }

    public void changeSelectedModeToolBar(String selectionMode, ModeToolBar modeToolBar) {
        if (modeToolBar == selectionModesToolBar) {
            this.selectionMode = selectionMode;
        }
        modeToolBar.setOnlyButtonSelected(selectionMode);
    }

    public void doMagicWand(String canvasName,int x, int y) {
    //System.out.println("Canvas name !"+canvasName);
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                //((StaticImageCanvasController) controller).doMagicWand(x, y);
                ((StaticImageCanvasController) controller).doMagicWandInc(x, y);
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //setContoursVisible(true);
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    //((StaticImageCanvasController) controller).doMagicWand(x, y);
                    ((StaticImageCanvasController) controller).doMagicWandInc(x, y);
                    if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                        setContoursVisible(true);
                    }
                }
            }
        }
    }

    public void updateMagicWand(String canvasName) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                //((StaticImageCanvasController) controller).updateMagicWand();
                ((StaticImageCanvasController) controller).updateMagicWandInc();
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //setContoursVisible(true);
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    //((StaticImageCanvasController) controller).updateMagicWand();
                    ((StaticImageCanvasController) controller).updateMagicWandInc();
                    if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                        setContoursVisible(true);
                    }
                }
            }
        }
    }

    public void updateMagicWandAll(String canvasName) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).updateMagicWandAllInc();
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //setContoursVisible(true);
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).updateMagicWandAllInc();
                    if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                        setContoursVisible(true);
                    }
                }
            }
        }
    }

    public void invertMask(String canvasName) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).invertMask();
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //setContoursVisible(true);
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).invertMask();
                    if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                        setContoursVisible(true);
                    }
                }
            }
        }
    }

    public void startDrawingLassoMask(int x, int y, String canvasName, boolean add) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).startDrawingLassoMask(x, y, add);
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).startDrawingLassoMask(x, y, add);
                }
            }
        }
    }

    public void drawLassoMask(int x, int y, String canvasName) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).drawLassoMask(x, y);

            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).drawLassoMask(x, y);
                }
            }
        }
    }

    public void finishLassoMask(String canvasName) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).finishLassoMask();
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //setContoursVisible(true);
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).finishLassoMask();
                    if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                        setContoursVisible(true);
                    }
                }
            }
        }
    }


    public void defineRadialTransitionCenter (int x, int y, String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            ((StaticImageCanvasController) controller).defineRadialCenter(x,y);
            startPoint = ((StaticImageCanvasController) controller).getStartPoint();
        }
    }

    public void addNewVectorDirection(int x, int y, String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            ((StaticImageCanvasController) controller).displayVectorDirectionDrawns(x,y);
            startPoint = ((StaticImageCanvasController) controller).getStartPoint();
        }
    }

    public void modifiyVectorDirecton (int x, int y, String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            ((StaticImageCanvasController) controller).drawSegmentDrawnGeoElement(x,y);
            endPoint =  ((StaticImageCanvasController) controller).getEndPoint();
            //startPoint = ((StaticImageCanvasController) controller).getStartPoint();
        }
    }

    public void endVectorDirection(int x, int y, String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            ((StaticImageCanvasController) controller).drawSegmentDrawnGeoElement(x,y);
            //startPoint = ((StaticImageCanvasController) controller).getStartPoint();
            endPoint =  ((StaticImageCanvasController) controller).getEndPoint();
        }

    }

    public void selectContour(int x, int y, String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            ((StaticImageCanvasController)controller).selectContour(x,y);
        }
    }

    public void deleteMasks(String canvasName, boolean throwEvent) {
        if (selectionMode.equals(ONLY_ONE)) {
            ImageCanvasController controller = getControllerByName(canvasName);
            if (controller instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) controller).deleteMasks(throwEvent);
                ((StaticImageCanvasController) controller).setMagicWandThreshold((float)SatHistConstants.defaultThreshold,true);
                ((StaticImageCanvasController) controller).deleteContourDrawns();
            }
        }
        else if (selectionMode.equals(BOTH)) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).deleteMasks(throwEvent);
                }
                ((StaticImageCanvasController) controller).setMagicWandThreshold((float)SatHistConstants.defaultThreshold,true);
                ((StaticImageCanvasController) controller).deleteContourDrawns();
            }
        }
    }

    public void deleteMasks(String canvasName) {
        deleteMasks(canvasName, true);
    }

    public void undoLastMagicWand(String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            //((StaticImageCanvasController) controller).removeLastMagicWand();
            ((StaticImageCanvasController) controller).undoLast();
            if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                //setContoursVisible(true);
                ((StaticImageCanvasController) controller).displayContour();
            }
        }
    }

    public void saveMask(String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            int returnVal = fileChooser.showSaveDialog(mainFrame);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                //System.out.println("file absolute path "+file.getAbsolutePath());
                //System.out.println("file name "+file.getName());
                //JAI.create("filestore",tiledImage,file.toPath().toString(),"TIFF");
                //CVUtilities.writeImage(previewController.getAnimationPlanToSave(), file.toPath().toString());
                ((StaticImageCanvasController) controller).saveMask(file.toPath().toString());
            }

        }
    }

    public void loadMask(String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            int returnVal = fileChooser.showOpenDialog(mainFrame);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                //System.out.println("file absolute path "+file.getAbsolutePath());
                //System.out.println("file name "+file.getName());
                //JAI.create("filestore",tiledImage,file.toPath().toString(),"TIFF");
                //CVUtilities.writeImage(previewController.getAnimationPlanToSave(), file.toPath().toString());
                Mat loaded = CVUtilities.loadImageFromFile(file.toPath().toString());
                List<Mat> splitLoaded = new ArrayList<>();
                Core.split(loaded, splitLoaded);
                splitLoaded.get(0).convertTo(splitLoaded.get(0), CvType.CV_8U);

                ((StaticImageCanvasController) controller).loadMask(splitLoaded.get(0), true);
            }

        }
    }

    public void copyMask(String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            if (((StaticImageCanvasController)controller).getMagicWandTexture() != null) {
                copiedMask = ((StaticImageCanvasController) controller).getMagicWandTexture().getData();
            }
        }
    }

    public void pasteMask(String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller instanceof StaticImageCanvasController) {
            if (copiedMask != null) {
                ((StaticImageCanvasController) controller).pasteMask(copiedMask);
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //setContoursVisible(true);
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
    }

    public void replicateMask(String canvasName) {
        ImageCanvasController controller = getControllerByName(canvasName);
        if (controller == beforeController) {
            if (beforeController.getMagicWandTexture() != null) {
                afterController.loadMask(beforeController.getMagicWandTexture().getData(), true);
                if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                    //setContoursVisible(true);
                    afterController.displayContour();
                }
            }
        }
        else if (controller == afterController) {
            if (afterController.getMagicWandTexture() != null) {
                beforeController.loadMask(afterController.getMagicWandTexture().getData(), true);
            }
            if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                //setContoursVisible(true);
                beforeController.displayContour();
            }
        }
    }



    public void displayMaskSelectedStagePlan(StagePlanAnimation stagePlanAnimation) {

        if (stagePlanAnimation.getInitMask()!=null) {
            ((StaticImageCanvasController) getControllerByName(BEFORE_CANVAS)).loadMask(stagePlanAnimation.getInitMask());
            ((StaticImageCanvasController) getControllerByName(BEFORE_CANVAS)).setMagicWandThreshold((float)SatHistConstants.defaultThreshold,true);
        } else {
            ((StaticImageCanvasController) getControllerByName(BEFORE_CANVAS)).deleteMasks();
        }
        if (stagePlanAnimation.getEndMask() != null) {
            ((StaticImageCanvasController) getControllerByName(AFTER_CANVAS)).loadMask(stagePlanAnimation.getEndMask());
            ((StaticImageCanvasController) getControllerByName(AFTER_CANVAS)).setMagicWandThreshold((float)SatHistConstants.defaultThreshold,true);
        }
        else {
            ((StaticImageCanvasController) getControllerByName(AFTER_CANVAS)).deleteMasks();
        }
        }



    public void setMNTModelsCBVisible(boolean visible) {
        animationAddPanel.setMntComboBoxVisible(visible);
        selectedMnt = animationAddPanel.getSelectedMntInComboBox();
    }




    public void setContoursVisible(boolean visible) {
        if (visible) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).displayContour();
                }
            }
        }
        else {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).deleteContourDrawns();
                }
            }
        }
    }

    public void setDirectionVisible(boolean visible) {
        if (visible) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    //((StaticImageCanvasController) controller).displayContour();
                    if (controller.getName().equals(BEFORE_CANVAS)) {
                        if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageDirectionPlanAnimation) {
                            StageDirectionPlanAnimation stageDirectionPlanAnimation = (StageDirectionPlanAnimation) timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation();
                            ((StaticImageCanvasController) controller).displayDirectionObject(stageDirectionPlanAnimation.getFirstStartPoint(), stageDirectionPlanAnimation.getFirstEndPoint());
                        }
                    }
                }
            }
        }
        else {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).deleteDirectionObject();
                }
            }
        }
    }

    public void setRadialCenterVisible(boolean visible) {
        if (visible) {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    //((StaticImageCanvasController) controller).displayContour();
                    if (controller.getName().equals(BEFORE_CANVAS)) {
                        if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageRadialPlanAnimation) {
                            StageRadialPlanAnimation stageRadialPlanAnimation = (StageRadialPlanAnimation)timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation();
                            ((StaticImageCanvasController) controller).displayRadialCenter(stageRadialPlanAnimation.getCenterCoords());
                        }
                    }
                }
            }
        }
        else {
            for (ImageCanvasController controller : imageCanvasControllers) {
                if (controller instanceof StaticImageCanvasController) {
                    ((StaticImageCanvasController) controller).deleteRadialCenter();
                }
            }
        }
    }




    public ModeToolBar getModeToolBar() {
        return modeToolBar;
    }

    public PopUpMenu getPopUpMenu() {
        return popupMenu;
    }

    public void setMaskOptionsVisible(boolean visible) {
        for (ImageCanvasController imageCanvasController: imageCanvasControllers) {
            if (imageCanvasController instanceof StaticImageCanvasController) {
                ((StaticImageCanvasController) imageCanvasController).setMaskOptionsVisible(visible);
            }
        }
    }

    public void setPopUpMenuItemsEnabled(boolean enabled) {
        for (String menu : popupMenu.getMenuItems().keySet()) {
            popupMenu.enableMenuItem(menu, enabled);
        }
    }

    //ANIMATION METHODS

    boolean blurredBorderEnabled;
    int blurredBorderWidth = SatHistConstants.BLEND_PIXELS;

    public void playAnimation() {
        previewFrame.toFront();
        if (!previewController.getCanvasPanel().getCanvas().getIsAnimating() && !previewController.getCanvasPanel().getCanvas().getPauseAnimation() ||
                previewController.getCanvasPanel().getCanvas().getIsAnimating() && previewController.getCanvasPanel().getCanvas().getPauseAnimation()  ) {
            animationControlPanel.setPlayButtonPaused();
        }
        else {
            animationControlPanel.setPlayButtonPlay();
        }
        previewController.playAnimation();
        //beforeController.pushMaskChangedEvent();

    }

    public void startPlayAnimation() {
        if (!previewController.getCanvasPanel().getCanvas().getIsAnimating()) {
            previewController.getCanvasPanel().getCanvas().startAnimating();
            if (!previewController.getCanvasPanel().getCanvas().getPauseAnimation()) {
                previewController.getCanvasPanel().getCanvas().pauseAnimation(true);
            }
        }
    }


    public void setAnimationTime(float time) {
        //System.out.println("set animation time!");
        previewController.setAnimationTime(time);
    }

    public void changeAnimationTransition(String selectedTransition) {
        this.selectedTransition = selectedTransition;
        if (!timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().getType().equals(SatHistConstants.namesTransitions.get(selectedTransition))){
            if (!changes.contains(CHANGES.TYPE)) {
                changes.add(CHANGES.TYPE);
            }
            animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, true));
        }
        else {
            if (changes.contains(CHANGES.TYPE)) {
                changes.remove(CHANGES.TYPE);
            }
            animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, false));

        }
    }

    public void setBlurredBorderEnabled(boolean blurredBorderEnabled) {
        this.blurredBorderEnabled = blurredBorderEnabled;
        if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().isBlurBorder()==blurredBorderEnabled){
            if (changes.contains(CHANGES.BLURRED)) {
                changes.remove(CHANGES.BLURRED);
            }
            animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, false));

        }
        else {
            if (!changes.contains(CHANGES.BLURRED)) {
                changes.add(CHANGES.BLURRED);
            }
            animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, true));

        }
        //System.out.println("set blurred border enabled! "+blurredBorderEnabled);
    }

    public void setInvertedImported(boolean invertedImported) {
        this.invertedImported = invertedImported;
        if (((StageTexturedPlanAnimation)timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation()).isDec()==invertedImported){
            if (changes.contains(CHANGES.DEC)) {
                changes.remove(CHANGES.DEC);
            }
            animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, false));

        }
        else {
            if (!changes.contains(CHANGES.DEC)) {
                changes.add(CHANGES.DEC);
            }
            animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, true));

        }
    }

    public void setMntSelected(String selectedMnt) {
        this.selectedMnt = selectedMnt;
        if (timeLineController.getSelectedStagePlanController()!=null) {
            if (((StageTexturedPlanAnimation) timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation()).getImportedPath() == getRasterPath(selectedMnt)) {
                if (changes.contains(CHANGES.IMPORTED)) {
                    changes.remove(CHANGES.IMPORTED);
                }
                animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, false));

            } else {
                if (!changes.contains(CHANGES.IMPORTED)) {
                    changes.add(CHANGES.IMPORTED);
                }
                animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, true));

            }
        }

    }


    public void setBlurredBorderWidth(int blurredBorderWidth) {
        this.blurredBorderWidth = blurredBorderWidth;
        if (timeLineController.getSelectedStagePlanController()!=null) {
            if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().getBlurWidth() == blurredBorderWidth || !blurredBorderEnabled) {
                if (changes.contains(CHANGES.BLURRED_WIDTH)) {
                    changes.remove(CHANGES.BLURRED_WIDTH);
                }
                animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, false));

            } else {
                if (blurredBorderEnabled) {
                    if (changes.contains(CHANGES.BLURRED_WIDTH)) {
                        changes.add(CHANGES.BLURRED_WIDTH);
                    }
                    animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, true));

                }
            }
        }
        //System.out.println("set blurred border width! "+blurredBorderWidth);
    }


    public void addNewStagedAnimation() {
        previewController.addNewStagedAnimation(0);
        //timeLineController.addStagedAnimationByPlan((StagedAnimationByPlan)previewController.getCurrentStagedAnimation(), stateMachine);
        //System.out.println("(StagedAnimationByPlan)previewController.getCurrentStagedAnimation() "+(StagedAnimationByPlan)previewController.getCurrentStagedAnimation());
        timeLineController.addStagedAnimationByPlan((StagedAnimationByPlan)previewController.getCurrentStagedAnimation(), animationSelectionStateMachine);
        if (((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture()!=null) {
            //beforeController.setVisibleRaster(stagedAnimationByPlan.getStagedAnimationByPlan().getInitTexture().getName());
            beforeController.updateRasterSelected(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture().getName());
            timeLineController.updateStagedAnimationTitle(BEFORE_CANVAS,((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture().getName());
        }
        else {
            // beforeController.setVisibleRaster(null);
            beforeController.updateRasterSelected(null);
        }
        if (((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getEndTexture()!=null) {
            //afterController.setVisibleRaster(stagedAnimationByPlan.getStagedAnimationByPlan().getEndTexture().getName());
            afterController.updateRasterSelected(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getEndTexture().getName());
        } else {
            //afterController.setVisibleRaster(null);
            afterController.updateRasterSelected(null);
        }

    }

    public void addNewStagedAnimation(StagedAnimationByPlan stagedAnimationByPlan) {
        previewController.addNewStagedAnimation(stagedAnimationByPlan);
        timeLineController.addStagedAnimationByPlan((StagedAnimationByPlan)previewController.getCurrentStagedAnimation(), animationSelectionStateMachine);
        if (((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture()!=null) {
            beforeController.updateRasterSelected(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture().getName());
            timeLineController.updateStagedAnimationTitle(BEFORE_CANVAS,((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture().getName());
        }
        else {
            beforeController.updateRasterSelected(null);
        }
        if (((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getEndTexture()!=null) {
            afterController.updateRasterSelected(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getEndTexture().getName());
        } else {
            afterController.updateRasterSelected(null);
        }
    }

    public Pair<StagedAnimationByPlanController, StagePlanController> selectStagedAnimation(int x, int y) {
        StagedAnimationByPlanController oldSelected = timeLineController.getSelectedStagedAnimationByPlan();
        Pair<StagedAnimationByPlanController, StagePlanController> selectedAnimations = timeLineController.getSelected(x, y);
        StagedAnimationByPlanController stagedAnimationByPlan = selectedAnimations.getValue1();
        if (stagedAnimationByPlan != null) {
            previewController.setCurrentStagedAnimation(stagedAnimationByPlan.getStagedAnimationByPlan());

            if (selectedAnimations.getValue2()!= null) {
                ((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).setCurrentAnimation(selectedAnimations.getValue2().getTimedAnimation());
                animationAddPanel.update(SatHistConstants.transitionNames.get(selectedAnimations.getValue2().getTimedAnimation().getAnimation().getType()),(int)Math.round(selectedAnimations.getValue2().getTimedAnimation().getAnimation().getBlurWidth()),
                        selectedAnimations.getValue2().getTimedAnimation().getAnimation().isBlurBorder());
                selectedTransition = SatHistConstants.transitionNames.get(selectedAnimations.getValue2().getTimedAnimation().getAnimation().getType());
            }


        }


        return selectedAnimations;
    }



    public void updateRasters() {
        StagedAnimationByPlanController stagedAnimationByPlan = timeLineController.getSelectedStagedAnimationByPlan();
        if (stagedAnimationByPlan!=null){
            if (stagedAnimationByPlan.getInitTexture()!=null) {
                beforeController.updateRasterSelected(stagedAnimationByPlan.getInitTexture().getName());
            }
            else {
                beforeController.updateRasterSelected(null);
            }
            if (stagedAnimationByPlan.getStagedAnimationByPlan().getEndTexture()!=null) {
                afterController.updateRasterSelected(stagedAnimationByPlan.getStagedAnimationByPlan().getEndTexture().getName());
            } else {
                afterController.updateRasterSelected(null);
            }
            previewController.setCurrentStagedAnimation(stagedAnimationByPlan.getStagedAnimationByPlan());
        }
        else {
            beforeController.updateRasterSelected(null);
            afterController.updateRasterSelected(null);
        }
    }

    public void updateMaks() {
        displayMaskSelectedStagePlan(timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation());
    }

    public void clearMasks() {
        deleteMasks(BEFORE_CANVAS, false);
        deleteMasks(AFTER_CANVAS, false);
    }

    public void updateAnimationObjects () {
        if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageDirectionPlanAnimation) {
            setDirectionVisible(true);
            popupMenu.getMenuItems().get(SatHistConstants.EDIT_DIRECTION).setEnabled(true);
        }
        else {
            setDirectionVisible(false);
            popupMenu.getMenuItems().get(SatHistConstants.EDIT_DIRECTION).setEnabled(false);
        }
        if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageRadialPlanAnimation) {
            setRadialCenterVisible(true);
            popupMenu.getMenuItems().get(SatHistConstants.EDIT_RADIAL_CENTER).setEnabled(true);
        }
        else {
            setRadialCenterVisible(false);
            popupMenu.getMenuItems().get(SatHistConstants.EDIT_RADIAL_CENTER).setEnabled(false);
        }
        if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageMorphVectorAnimation) {
            setContoursVisible(true);
            popupMenu.getMenuItems().get(SatHistConstants.SELECT_CONTOUR).setEnabled(true);
        }
        else {
            setContoursVisible(false);
            popupMenu.getMenuItems().get(SatHistConstants.SELECT_CONTOUR).setEnabled(false);
        }
    }

    public void deleteAnimationObjetcs() {
        setContoursVisible(false);
        setRadialCenterVisible(false);
        setDirectionVisible(false);
    }



    long currentTime = System.currentTimeMillis();
    public void updateAnimation() {
        currentTime = System.currentTimeMillis();
        Texture initTexture = null;
        Texture endTexture = null;
        boolean correct = true;
        if (beforeController.getMagicWandTexture() != null) {
            initTexture = beforeController.getMagicWandTexture();
        }
        if (afterController.getMagicWandTexture() != null) {
            endTexture = afterController.getMagicWandTexture();
        }
        StagePlanAnimation oldAnimation = timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation();
        TimedStagePlanAnimation oldTimed = timeLineController.getSelectedStagePlanController().getTimedAnimation();
        StagePlanAnimation newAnimation;
        mainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if (selectedTransition.equals(Constants.BLEND_TRANSITION)) {
            //StagePlanAnimation oldAnimation = timeLineController.getSelectedParalelizedGroup().getAnimation();
            //timeLineController.getSelectedParalelizedGroup().setAnimation(new StagePlanAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration()));
            newAnimation = new StagePlanAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration());
            oldTimed.setAnimation(newAnimation);
            if (endTexture != null) {
                newAnimation.setEndMask(endTexture.getData().clone());
            }
        }
        else if (selectedTransition.equals(SatHistConstants.RADIAL_IN_TRANSITION) || selectedTransition.equals(SatHistConstants.RADIAL_OUT_TRANSITION)) {
            //StagePlanAnimation oldAnimation = timeLineController.getSelectedParalelizedGroup().getAnimation();
            int [] radialCenter = previewController.getAnimationManager().getAnimationRaster().getCoordinateInPx(startPoint);
            //timeLineController.getSelectedParalelizedGroup().setAnimation(new StageRadialPlanAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration(), radialCenter, blurredBorderEnabled, (int)Math.round(blurredBorderWidth/previewController.getPixelCoef())));
//            newAnimation =new StageRadialPlanAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration(), radialCenter, blurredBorderEnabled, (int)Math.round(blurredBorderWidth/previewController.getPixelCoef()));
            newAnimation =new StageRadialPlanAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration(), radialCenter, blurredBorderEnabled, (blurredBorderEnabled? (int)Math.round(blurredBorderWidth):0));
            oldTimed.setAnimation(newAnimation);
            if (endTexture != null) {
                newAnimation.setEndMask(endTexture.getData().clone());
            }
        }
        else if (selectedTransition.equals(Constants.DIRECTION_TRANSITION)) {
            //StagePlanAnimation oldAnimation = timeLineController.getSelectedParalelizedGroup().getAnimation();
            int [] startPointPx = previewController.getAnimationManager().getAnimationRaster().getCoordinateInPx(startPoint);
            int[] endPointPx = previewController.getAnimationManager().getAnimationRaster().getCoordinateInPx(endPoint);
            //timeLineController.getSelectedParalelizedGroup().setAnimation(new StageDirectionPlanAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration(), startPointPx, endPointPx, blurredBorderEnabled, (int)Math.round(blurredBorderWidth/previewController.getPixelCoef())));
            newAnimation = new StageDirectionPlanAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration(), startPointPx, endPointPx, blurredBorderEnabled, (blurredBorderEnabled? (int)Math.round(blurredBorderWidth):0));
            oldTimed.setAnimation(newAnimation);
            if (endTexture != null) {
                newAnimation.setEndMask(endTexture.getData().clone());
            }
        }
        else if (selectedTransition.equals(SatHistConstants.IMPORTED_TRANSITION)) {
            //StagePlanAnimation oldAnimation = timeLineController.getSelectedParalelizedGroup().getAnimation();
            MNTRasterGeoElement mnt = ((MNTRasterGeoElement) previewController.getLayerManager().getRasterLayerByName(selectedMnt).getElements().get(0));
            Mat importedPlan = mnt.getMat();
            //timeLineController.getSelectedParalelizedGroup().setAnimation(new StageTexturedPlanAnimation(AbstractAnimation.Type.SEMANTIC, oldAnimation.getDuration(), blurredBorderEnabled, (int)Math.round(blurredBorderWidth/previewController.getPixelCoef()), importedPlan, (float) mnt.getNoValue(), invertedImported));
            newAnimation = new StageTexturedPlanAnimation(AbstractAnimation.Type.SEMANTIC, oldAnimation.getDuration(), blurredBorderEnabled, (blurredBorderEnabled? (int)Math.round(blurredBorderWidth):0), importedPlan, (float) mnt.getNoValue(), invertedImported, getRasterPath(mnt.getName()));
            oldTimed.setAnimation(newAnimation);

        }
        else if (selectedTransition.equals(SatHistConstants.ERODE_NAME) || selectedTransition.equals(SatHistConstants.DILATE_NAME) || selectedTransition.equals(Constants.DEFORM_TRANSITION)) {
            //StagePlanAnimation oldAnimation = timeLineController.getSelectedParalelizedGroup().getAnimation();
            if (selectedTransition.equals(SatHistConstants.ERODE_NAME) && initTexture== null) {
                showOneMaskErrorAlert();
                correct = false;
            }
            else if (selectedTransition.equals(SatHistConstants.DILATE_NAME) && (initTexture == null || endTexture == null)) {
                showTwoMasksErrorAlert();
                correct = false;
            }
            else if (selectedTransition.equals(Constants.DEFORM_TRANSITION) && (initTexture == null || endTexture == null)) {
                showTwoMasksErrorAlert();
                correct = false;
            }
            else {
                if (selectedTransition.equals(SatHistConstants.ERODE_NAME)) {
                    if (endTexture == null && initTexture != null) {
                        endTexture = new Texture(timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture(), Mat.zeros(initTexture.getData().size(), CvType.CV_8U));
                        //((AnimationPlanManager)animationManager).addMaskErodedBlendAnimationPlan((StagedAnimationByPlan)animationManager.getCurrentStagedAnimation(),maskTextureInit, maskTextureEndEmpty, (int)(blendPixels/pixelCoef), blurBorder);
                    }
                }
                StageMorphAnimation morphAnimation = new StageMorphAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration(), blurredBorderEnabled, (blurredBorderEnabled? (int)Math.round(blurredBorderWidth):0));
                oldTimed.setAnimation(morphAnimation);
                if (endTexture != null) {
                    morphAnimation.setEndMask(endTexture.getData().clone());
                }
            }
        }
        else if (selectedTransition.equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
            if (initTexture == null || endTexture == null) {
                showTwoMasksErrorAlert();
                correct = false;
            } else {
                if (beforeController.getSelectedContourMask() != null && afterController.getSelectedContourMask() != null) {
                    initTexture = new Texture(beforeController.getVisibleRaster(), beforeController.getSelectedContourMask());
                    endTexture = new Texture(afterController.getVisibleRaster(), afterController.getSelectedContourMask());
                    try {
                        long currentTime = System.currentTimeMillis();
                        Pair<List<Double[]>, List<Double[]>> matches = MorphingAlgorithm.getContours(initTexture.getData().clone(), endTexture.getData().clone());
                        StageMorphVectorAnimation stageMorphVectorAnimation = new StageMorphVectorAnimation(SatHistConstants.namesTransitions.get(selectedTransition), oldAnimation.getDuration(), blurredBorderEnabled, (int) Math.round(blurredBorderWidth), matches.getValue1(), matches.getValue2(), 100);
                        oldTimed.setAnimation(stageMorphVectorAnimation);
                        if (endTexture != null) {
                            stageMorphVectorAnimation.setEndMask(endTexture.getData().clone());
                        }
                    } catch (NotEnoughCornersExeption notEnoughCornersExeption) {
                        JOptionPane.showMessageDialog(mainFrame, notEnoughCornersExeption.getMessage());
                        correct = false;
                    }

                } else {
                    JOptionPane.showMessageDialog(mainFrame, "You need to select a contour in both canvas");
                    correct = false;
                }
            }
        }
        if (correct) {
            if (initTexture != null) {
                timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().setInitMask(initTexture.getData().clone());
            }

            timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().setType(SatHistConstants.namesTransitions.get(selectedTransition));
            previewController.replaceStage(timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan(), oldAnimation, timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation());
            if (timeLineController.getSelectedStagePlanController().checkCompleteness()) {

                //previewController.updateStage(timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan(), timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation(),
                //initTexture, endTexture, selectedTransition, blurredBorderWidth, blurredBorderEnabled);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if (loadingDialog == null) {
                            loadingDialog = new LoadingDialog(mainFrame);
                        }
                        loadingDialog.update(SatHistConstants.UPDATING_WAIT_MESSAGE);
                        loadingDialog.setLocationRelativeTo(mainFrame);
                        loadingDialog.setVisible(true);
                    }
                });
                UpdateAnimationWorker updateAnimationWorker = new UpdateAnimationWorker(this, initTexture, endTexture);
                updateAnimationWorker.execute();


            } else {
                mainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
            //timeLineController.getSelectedStagePlanController().updateView();

        } else {
            mainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void updateAnimationInPreview(Texture initTexture, Texture endTexture) {
        int blurredBorderWidthEnabled = 0;
        if (blurredBorderEnabled) {
            blurredBorderWidthEnabled = blurredBorderWidth;
        }
        previewController.updateStage(timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan(), timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation(),
                initTexture, endTexture, selectedTransition, blurredBorderWidthEnabled, blurredBorderEnabled);
    }

    public void finishUpdatingAnimation() {
        timeLineController.getSelectedStagePlanController().updateView();
        mainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        loadingDialog.setVisible(false);
        clearChanges();
        setUpdateButtonEnabled(false);
    }


    public void addDefaultStage() {
        previewController.addDefaultStage();
        timeLineController.addDefaultStage(previewController.getCurrentAnimation(),animationSelectionStateMachine);
        selectedTransition = SatHistConstants.transitionNames.get(Constants.BLEND_TRANSITION);
    }

    public void addEqualizedAnimation() {
        //mainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run(){
                if (loadingDialog== null) {
                    loadingDialog = new LoadingDialog(mainFrame);
                }
                loadingDialog.update(SatHistConstants.EQUALIZATION_WAIT_MESSAGE);
                loadingDialog.setLocationRelativeTo(mainFrame);
                loadingDialog.setVisible(true);
            }
        });
        EqualizationWorker equalizationWorker = new EqualizationWorker(this);
        equalizationWorker.execute();


        //previewController.addEqualizedStagedAnimation();

        //timeLineController.addStagePlan(previewController.getStagedAnimationByPlan(previewController.getNumStagedAnimations()-2).getLastStagePlanAnimation(), stateMachine);
//        timeLineController.getSelectedStagedAnimationByPlan().addStagePlan(previewController.getEqualizedStagePlanAnimation(), animationSelectionStateMachine, 0);
//        timeLineController.getSelectedStagedAnimationByPlan().setEqualizedController(previewController.getEqualizedStagePlanAnimation());

    }

    public void endAddingEqualizedAnimation() {
        timeLineController.addEqualizedAnimation(previewController.getEqualizedStagePlanAnimation(), animationSelectionStateMachine);

        //mainFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        loadingDialog.setVisible(false);
    }

    public void deleteAnimation(AbstractAnimation animation) {
        if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation()==animation) {
            beforeController.deleteMasks(false);
            afterController.deleteMasks(false);
        }
        previewController.deleteAnimation(animation);
        timeLineController.deleteAnimation(animation);
        if (previewController.getCurrentStagedAnimation()==null) {
            beforeController.updateRasterSelected(null);
            beforeController.deleteMasks();
            afterController.updateRasterSelected(null);
            afterController.deleteMasks();
        }
        //setAnimationTime(0.0f);
        //timeLineController.updateCurrentTime(0.0f);
    }

    public void deleteStagedAnimation(StagedAnimationByPlanView view) {
        StagedAnimationByPlanController controller = timeLineController.getStagedAnimationControllerByView(view);
        if (previewController.getCurrentStagedAnimation()==controller.getStagedAnimationByPlan()) {
            beforeController.updateRasterSelected(null);
            beforeController.deleteMasks();
            afterController.updateRasterSelected(null);
            afterController.deleteMasks();
        }
        previewController.deleteStagedAnimation(controller.getStagedAnimationByPlan());
        timeLineController.deleteStagedAnimation(controller);

    }


    public void finishMovingAnimation() {
        timeLineController.finishMoving();
        previewController.updateAnimationPlan(timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan());
    }

    public void animationDurationChanged() {
        ((AnimationPlanManager)previewController.getAnimationManager()).updateCompleteAnimationPlanTimed((StagedAnimationByPlan)previewController.getCurrentStagedAnimation());
    }

    public void updateDuration (float newDuration) {

        timeLineController.updateDuration(newDuration);
        ((AnimationPlanManager)previewController.getAnimationManager()).updateCompleteAnimationPlanTimed(timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan());

    }

    public void saveAnimation() {
        previewController.getAnimationPlanToSave();
        TiledImage tiledImage = CVUtilities.matToJAI(previewController.getAnimationPlanToSave());
        int returnVal = fileChooser.showSaveDialog(mainFrame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            //System.out.println("file absolute path "+file.getAbsolutePath());
            //System.out.println("file name "+file.getName());
            JAI.create("filestore",tiledImage,file.toPath().toString(),"TIFF");
            //CVUtilities.writeImage(previewController.getAnimationPlanToSave(), file.toPath().toString());
        }

    }



    public void backwardAnimation() {
        timeLineController.updateCurrentTime(0.0f);
        previewController.setAnimationTime(0.0f);
    }

    public void forwardAnimation() {
        timeLineController.updateCurrentTime(previewController.getCurrentStagedAnimation().getDuration());
        previewController.setAnimationTime(previewController.getCurrentStagedAnimation().getDuration());
    }

    public void stepBackwardAnimation() {
        //System.out.println("(float)timeLineController.getCurrentTime()) "+(float)timeLineController.getCurrentTime());
        //System.out.println("time! "+previewController.getStartTimeCurrentAnimation((float)timeLineController.getCurrentTime()));
        float newTime = previewController.getStartTimeCurrentAnimation((float)timeLineController.getCurrentTime()-0.01f);
        //System.out.println("new time "+newTime);
        previewController.setAnimationTime(newTime);
        timeLineController.updateCurrentTime(newTime);
    }

    public void stepForwardAnimation() {
        int index = previewController.getCurrentStagedAnimation().getIndexOfStage(previewController.getCurrentAnimation((float)timeLineController.getCurrentTime()+0.01f));
        //System.out.println("index! "+index);
        float newTime = previewController.getCurrentStagedAnimation().getAnimationStartTime(index+1);
        //System.out.println("new time! "+newTime);
        previewController.setAnimationTime(newTime);
        timeLineController.updateCurrentTime(newTime);
    }




    public void showRadialAlert() {
        JOptionPane.showMessageDialog(mainFrame,SatHistConstants.RADIAL_MESSAGE);
    }

    public void showDirectionAlert() {
        JOptionPane.showMessageDialog(mainFrame, SatHistConstants.DIRECTION_MESSAGE);
    }

    public void showOneMaskErrorAlert() { JOptionPane.showMessageDialog(mainFrame, SatHistConstants.MASK_ERROR_MESSAGE);}

    public void showTwoMasksErrorAlert() { JOptionPane.showMessageDialog(mainFrame, SatHistConstants.TWO_MASKS_ERROR_MESSAGE);}

    public void showDurationDialog() {
        //JOptionPane.showInputDialog(mainFrame, "Insert the new duration");
        durationDialog.update(timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().getDuration());
        durationDialog.setLocationRelativeTo(mainFrame);
        durationDialog.setVisible(true);
    }

    public PopUpMenu getTimeLinePopUpMenu() {
        return timeLinePopUpMenu;
    }

    public void setAddPanelEnabled(boolean enabled) {
        animationAddPanel.setAllEnabled(enabled);
    }

    public void updateAddPanel() {
        if (selectedTransition.equals(SatHistConstants.IMPORTED_TRANSITION)) {
            animationAddPanel.setMntComboBoxVisible(true);
        }
        else {
            animationAddPanel.setMntComboBoxVisible(false);
        }
    }

    public void setModeToolBarStageSelected() {
        modeToolBar.enableAllButtons(true);
        selectionModesToolBar.enableAllButtons(true);
    }

    public void setModeTooBarNoSelection() {
        modeToolBar.enableAllButtons(false);
        selectionModesToolBar.enableAllButtons(false);
    }

    public void setModeToolBarStagedAnimationSelected() {
        modeToolBar.enableButton(NAVIGATION_MODE, true);
        modeToolBar.enableButton(StateMachineBuilder.lasso, false);
        modeToolBar.enableButton(StateMachineBuilder.lassoDelete, false);
        modeToolBar.enableButton(MAGIC_WAND, false);
        modeToolBar.enableButton(MAGIC_WAND_ALL, false);
        selectionModesToolBar.enableAllButtons(false);
    }

    public AnimationAddPanel getAnimationAddPanel() {
        return animationAddPanel;
    }

    public void exportAnimation() {
        int returnVal = fileChooser.showSaveDialog(mainFrame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (loadingDialog == null) {
                        loadingDialog = new LoadingDialog(mainFrame);
                    }
                    loadingDialog.update(SatHistConstants.EXPORTING_WAIT_MESSAGE);
                    loadingDialog.setLocationRelativeTo(mainFrame);
                    loadingDialog.setVisible(true);
                }
            });
            File file = fileChooser.getSelectedFile();
            //System.out.println("file absolute path "+file.getAbsolutePath());
            //System.out.println("file name "+file.getName());
            previewController.exportAnimation(file.getPath());

        }

    }


    public void exportVideoAndScreenShot() {
        File movieFile = new File(SatHistConstants.LOG_FOLDER+"/"+participant+"-"+trial+".mov");
        int iMovie = 0;
        while (movieFile.exists()) {
            iMovie++;
            movieFile = new File(SatHistConstants.LOG_FOLDER+"/"+participant+"-"+trial+"-"+iMovie+".mov");
        }
        File screenShotFile = new File(SatHistConstants.LOG_FOLDER+"/"+participant+"-"+trial+".png");
        int iScreenShot = 0;
        while(screenShotFile.exists()) {
            iScreenShot++;
            screenShotFile = new File(SatHistConstants.LOG_FOLDER+"/"+participant+"-"+trial+"-"+iScreenShot+".png");
        }

        previewController.exportAnimation(movieFile.getPath());
        Rectangle screenRect = new Rectangle(mainFrame.getBounds());
        BufferedImage capture = null;
        try {
            capture = new Robot().createScreenCapture(screenRect);
        } catch (AWTException e) {
            e.printStackTrace();
        }
        try {
            ImageIO.write(capture, "png", screenShotFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveProject() {
        int returnVal = fileChooser.showSaveDialog(mainFrame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (loadingDialog == null) {
                        loadingDialog = new LoadingDialog(mainFrame);
                    }
                    loadingDialog.update(SatHistConstants.SAVING_PROJECT_MESSAGE);
                    loadingDialog.setLocationRelativeTo(mainFrame);
                    loadingDialog.setVisible(true);
                }
            });
            File file = fileChooser.getSelectedFile();
            SaveProjectWorker saveProjectWorker = new SaveProjectWorker(this, file.getPath());
            saveProjectWorker.execute();
//            previewController.saveProject(file.getPath(), getRasterPath(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture().getName()),
//                    getRasterPath(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getEndTexture().getName()));

        }
    }

    public void saveProject(String path) {
        previewController.saveProject(path, getRasterPath(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getInitTexture().getName()),
                getRasterPath(((StagedAnimationByPlan)previewController.getCurrentStagedAnimation()).getEndTexture().getName()));
    }

    public void finishSavingProject() {
        loadingDialog.setVisible(false);
    }

    public void loadProject() {
        int returnVal = fileChooser.showOpenDialog(mainFrame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            //loadProject(file.getPath());
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (loadingDialog == null) {
                        loadingDialog = new LoadingDialog(mainFrame);
                    }
                    loadingDialog.update(SatHistConstants.OPENING_PROJECT_MESSAGE);
                    loadingDialog.setLocationRelativeTo(mainFrame);
                    loadingDialog.setVisible(true);
                }
            });
            LoadProjectWorker loadProjectWorker = new LoadProjectWorker(this, file.getPath());
            loadProjectWorker.execute();

        }


    }

    public void loadProject(String path) {
        ProjectLoader projectLoader = new ProjectLoader(this);
        projectLoader.openProject(path);


    }

    public void finishLoadingProject() {
        loadingDialog.setVisible(false);
        if (rasterNames.size()>1) {
            addStagedAnimationButton.setEnabled(true);
        }
        updateMNTNames();
    }

    public void quitApplication() {
        mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING));
    }



    public void openFolder() {
        int returnVal = folderChooser.showOpenDialog(mainFrame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (loadingDialog == null) {
                        loadingDialog = new LoadingDialog(mainFrame);
                    }
                    loadingDialog.update(SatHistConstants.LOADING_WAIT_MESSAGE);
                    loadingDialog.setLocationRelativeTo(mainFrame);
                    loadingDialog.setVisible(true);
                }
            });
//

            File file = folderChooser.getSelectedFile();
            LoadImagesWorker loadImagesWorker = new LoadImagesWorker(this, file);
            loadImagesWorker.execute();

        }
    }

    public void loadFiles(File folder) {
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                //System.out.println("File " + listOfFiles[i].getName());
                String ext = FilenameUtils.getExtension(listOfFiles[i].getAbsolutePath());
                Constants.EXT extEnum = null;
                if (ext.equals("shp") || ext.equals("SHP")) {
                    initVectorLayers(false, listOfFiles[i].getAbsolutePath(), listOfFiles[i].getName());
                } else if (ext.equals("tif") || (ext.equals("TIF") || (ext.equals("tiff") || (ext.equals("TIFF"))))) {
                    extEnum = Constants.EXT.TIF;
                } else if (ext.equals("ecw") || (ext.equals("ECW"))) {
                    extEnum = Constants.EXT.ECW;
                } else if (ext.equals("jpg") || ext.equals("JPG")) {
                    extEnum = Constants.EXT.JPG;
                }
                if (extEnum == Constants.EXT.TIF || extEnum == Constants.EXT.JPG || extEnum == Constants.EXT.ECW) {
                    addNewRaster(listOfFiles[i].getAbsolutePath(), false, listOfFiles[i].getName(), extEnum, false);
                }
            } else if (listOfFiles[i].isDirectory()) {
                //System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
        updateRasterCombos();
        updateMNTNames();


        }

    public void finishLoadingImages() {
        loadingDialog.setVisible(false);
        if (rasterNames.size()>1) {
            addStagedAnimationButton.setEnabled(true);
        }
    }


    @Override
    public void animationStep(float v, MapGLCanvas canvas) {
        //System.out.println("animation step! "+v);
        timeLineController.updateCurrentTime(v);
    }

    @Override
    public void animationPaused(float v) {
        animationControlPanel.setPlayButtonPlay();
    }

    @Override
    public  void finishedLoadingRasters(MapGLCanvas canvas) {
    }

    @Override
    public void finishedExportingMovie() {
        loadingDialog.setVisible(false);
    }

    public TimeLineController getTimeLineController() {
        return timeLineController;
    }

    public AnimatedImageCanvasController getPreviewController() {
        return previewController;
    }


    public boolean getUndoable(String canvasName) {
        return ((StaticImageCanvasController)getControllerByName(canvasName)).isUndoable();
    }


    @Override
    public void staticImageCanvasControllerChangedMask(String canvasName) {
        //System.out.println("staticImageCanvasControllerChangedMask! "+timeLineController.getSelectedStagePlanController());
        if (timeLineController.getSelectedStagePlanController()!=null && timeLineController.getSelectedStagedAnimationByPlan()!=null &&
                timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture()!=null) {
            //System.out.println("staticImageCanvasControllerChangedMask! 0");
            //System.out.println ("canvas name "+canvasName);
            if (canvasName == BEFORE_CANVAS) {
                //System.out.println("staticImageCanvasControllerChangedMask! 1");
                Mat magicWandMat;
                Mat animationMat;
                if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().getInitMask() == null) {
                    animationMat = Mat.ones((int)timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture().getPixelHeight(),
                            (int)timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture().getPixelWidth(), CvType.CV_8U);
                }
                else {
                    animationMat = timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation().getInitMask();
                }

                if ( ((StaticImageCanvasController) getControllerByName(canvasName)).getMagicWandTexture() == null) {
                    magicWandMat = Mat.ones((int)timeLineController.getSelectedStagedAnimationByPlan().getInitTexture().getPixelHeight(),
                            (int)timeLineController.getSelectedStagedAnimationByPlan().getInitTexture().getPixelWidth(), CvType.CV_8U);
                }
                else {
                    magicWandMat = ((StaticImageCanvasController) getControllerByName(canvasName)).getMagicWandTexture().getData();
                }

                if (CVUtilities.isZero(CVUtilities.calculateDiff(animationMat,magicWandMat))) {
                    if (changes.contains(SatHistViewer.CHANGES.MASK)) {
                        changes.remove(CHANGES.MASK);
                    }
                    animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, false));
                }
                else {
                    if (!changes.contains(SatHistViewer.CHANGES.MASK)) {
                        changes.add(CHANGES.MASK);
                    }
                    animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, true));
                }

            } else {
                if (timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageMorphAnimation) {

                    Mat magicWandMat;
                    Mat animationMat;
                    StageMorphAnimation stageMorphAnimation = (StageMorphAnimation) timeLineController.getSelectedStagePlanController().getTimedAnimation().getAnimation();
                    if (stageMorphAnimation.getEndMask() == null) {
                        animationMat = Mat.ones((int)timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture().getPixelHeight(),
                                (int)timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture().getPixelWidth(), CvType.CV_8U);
                    }
                    else {
                        animationMat = stageMorphAnimation.getEndMask();
                    }

                    if ( ((StaticImageCanvasController) getControllerByName(canvasName)).getMagicWandTexture() == null) {
                        magicWandMat = Mat.ones((int)timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture().getPixelHeight(),
                                (int)timeLineController.getSelectedStagedAnimationByPlan().getStagedAnimationByPlan().getInitTexture().getPixelWidth(), CvType.CV_8U);
                    }
                    else {
                        magicWandMat = ((StaticImageCanvasController) getControllerByName(canvasName)).getMagicWandTexture().getData();
                    }

                    if (CVUtilities.isZero(CVUtilities.calculateDiff(animationMat,magicWandMat))) {
                        if (changes.contains(SatHistViewer.CHANGES.MASK)) {
                            changes.remove(CHANGES.MASK);
                        }
                        animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, false));

                    }
                    else {
                        if (!changes.contains(SatHistViewer.CHANGES.MASK)) {
                            changes.add(CHANGES.MASK);
                        }
                        animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.MASK_CHANGED, true));

                    }

                }
            }
        }

    }

    @Override
    public void staticImageCanvasControllerChangedAnimationObjects(String canvasName) {
        animationSelectionStateMachine.handleEvent(new AnimationUpdatedEvent(SatHistConstants.OBJECT_CHANGED, true));
    }

    public void setUpdateButtonEnabled(boolean enabled) {
        if (enabled) {
            previewFrame.getRootPane().setBorder(new LineBorder(new Color(238,174,77), 2));
            animationAddPanel.setUpdateButtonEnabled(enabled);
        }
        else {
            if (changes.size()==0) {
                previewFrame.getRootPane().setBorder(new LineBorder(new Color(238, 238, 238), 2));
                animationAddPanel.setUpdateButtonEnabled(enabled);
            }
        }
    }

    public void clearChanges() {
        changes.clear();
    }
}
