/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

/**
 * Created by mjlobo on 19/04/2017.
 */
public interface StaticImageCanvasControllerListener {
    public void staticImageCanvasControllerChangedMask(String canvasName);
    public void staticImageCanvasControllerChangedAnimationObjects(String canvasName);
}
