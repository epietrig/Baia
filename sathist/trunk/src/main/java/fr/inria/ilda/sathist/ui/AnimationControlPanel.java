/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.mmtools.stateMachine.*;
import fr.inria.ilda.mmtools.ui.StringComboPanel;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.sathist.SatHistConstants;
import fr.inria.ilda.sathist.SatHistViewer;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mjlobo on 09/06/16.
 */
public class AnimationControlPanel extends JPanel {
    JButton playButton;
    JButton stepButtonForward;
    JButton stepButtonBackward;
    JButton forwardButton;
    JButton backwardButton;


    JPanel controlsPanel;

    List<JButton> buttons;
    HashMap<String, ImageIcon> iconHashMap;



    static int BUTTON_WIDTH = 16;
    static int BUTTON_HEIHT = 16;



    public AnimationControlPanel() {

        iconHashMap = new HashMap<>();
        buttons = new ArrayList<>();
        backwardButton = initControlButtons("B", SatHistConstants.BACKWARD_ACTION);
        stepButtonBackward = initControlButtons("SB", SatHistConstants.STEP_BACKWARD_ACTION);
        playButton = initControlButtons("Play", SatHistConstants.PLAY_PAUSE_ACTION);
        stepButtonForward=initControlButtons("SF", SatHistConstants.STEP_FOWARD_ACTION);
        forwardButton=initControlButtons("F", SatHistConstants.FORWARD_ACTION);




        ImageIcon icon = new ImageIcon(SatHistConstants.ICONS_PATH+SatHistConstants.PAUSE_ACTION+".png");
        Image newimg = icon.getImage().getScaledInstance( BUTTON_WIDTH, BUTTON_HEIHT,  java.awt.Image.SCALE_SMOOTH ) ;
        icon = new ImageIcon( newimg );
        iconHashMap.put(SatHistConstants.PAUSE_ACTION, icon);


        controlsPanel = new JPanel();
        controlsPanel.setLayout(new BoxLayout(controlsPanel, BoxLayout.LINE_AXIS));
        for (JButton button : buttons) {
            controlsPanel.add(button);
        }

        //controlsPanel.add(Box.createHorizontalGlue());
        JButton nextButton = new JButton("Next");
        //controlsPanel.add(nextButton);




//        setLayout(new GridLayout(1,3));
//        add(new JPanel());
        add(controlsPanel);
//        add(nextButton);




    }

    public JButton initControlButtons(String text, String action) {
        JButton button = new JButton(text);
        button.setActionCommand(action);
        ImageIcon icon = new ImageIcon(SatHistConstants.ICONS_PATH+action+".png");
        Image newimg = icon.getImage().getScaledInstance( BUTTON_WIDTH, BUTTON_HEIHT,  java.awt.Image.SCALE_SMOOTH ) ;
        icon = new ImageIcon( newimg );
        iconHashMap.put(action, icon);
        if (iconHashMap.get(action)!=null) {
            button.setIcon(iconHashMap.get(action));
            button.setText("");
        }
        buttons.add(button);
        button.setBorderPainted(false);
        return button;
    }

    public void addListeners(final StateMachine stateMachine) {
        for (JButton button: buttons) {
            addListenersToButton(button, stateMachine);
        }


    }

    public void addListenersToButton(final JButton button, final StateMachine stateMachine ) {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new ButtonEvent(button, button.getActionCommand()));
            }
        });
    }


    public void setPlayButtonPaused () {
        //playButton.setText(SatHistConstants.PAUSE_BUTTON_TEXT);
        playButton.setIcon(iconHashMap.get(SatHistConstants.PAUSE_ACTION));
    }

    public void setPlayButtonPlay() {
        //playButton.setText(SatHistConstants.PLAY_BUTTON_TEXT);
        playButton.setIcon(iconHashMap.get(SatHistConstants.PLAY_PAUSE_ACTION));
    }


    public JPanel getControlsPanel() {
        return controlsPanel;
    }

    public void setEnabled(boolean enabled) {
        for (JButton button : buttons) {
            button.setEnabled(enabled);
        }
    }





}
