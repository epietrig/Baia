/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import fr.inria.ilda.mmtools.animation.*;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.gl.render.RenderFeatures;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.mmtools.utilties.Pair;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.awt.*;
import java.awt.image.Raster;
import java.sql.Time;
import java.util.*;
import java.util.List;

/**
 * Created by mjlobo on 24/05/16.
 */
public class AnimatedImageCanvasController extends ImageCanvasController {

    AbstractAnimationManager animationManager;
    GLWindow glWindow;
    //List<AnimationController> stageControllers;
    //TimeLineController timeLineController;
    double pixelCoef;

    public AnimatedImageCanvasController(String name, GLCapabilities glCapabilities, boolean isRetina, RasterGeoElement sampleRaster) {
        super(name);
        glWindow = GLWindow.create(glCapabilities);
        MapGLCanvas previewCanvas = new MapGLCanvas(layerManager, glWindow, isRetina);
        //timeLineController = new TimeLineController();
        canvasPanel = new AnimatedImageCanvasPanel(previewCanvas, name);
        animationManager = new AnimationPlanManager(layerManager, canvasPanel.getCanvas(), sampleRaster);
        canvasPanel.getCanvas().setAnimationManager(animationManager);
        glWindow.addGLEventListener(previewCanvas);


    }

    public AnimatedImageCanvasController (String name, GLCapabilities glCapabilities, boolean isRetina, RasterGeoElement defaultStartRaster,
                                          RasterGeoElement defaultEndRaster) {
        super(name);
        glWindow = GLWindow.create(glCapabilities);
        MapGLCanvas previewCanvas = new MapGLCanvas(layerManager, glWindow, isRetina);
        //timeLineController = new TimeLineController(100,100);
        canvasPanel = new AnimatedImageCanvasPanel(previewCanvas, name);
        //animationManager = new AnimationPlanManager(layerManager, canvasPanel.getCanvas(),defaultStartRaster, defaultEndRaster, SatHistViewer.BEFORE_CANVAS, SatHistViewer.AFTER_CANVAS);
        animationManager = new AnimationPlanManager(layerManager, canvasPanel.getCanvas(), defaultStartRaster);
        canvasPanel.getCanvas().setAnimationManager(animationManager);
        glWindow.addGLEventListener(previewCanvas);
        //stageControllers = new ArrayList<>();
        //pixelCoef = Math.min(((double)SatHistConstants.TARGET_WIDTH)/(double)animationManager.getAnimationRaster().getImageWidth(), ((double)SatHistConstants.TARGET_HEIGHT)/(double)animationManager.getAnimationRaster().getImageHeight());
        pixelCoef = Math.min(((double)SatHistConstants.TARGET_WIDTH)/(double)defaultStartRaster.getImageWidth(), ((double)SatHistConstants.TARGET_HEIGHT)/(double)defaultStartRaster.getImageHeight());
    }

    public AnimatedImageCanvasController(String name, GLCapabilities glCapabilities, boolean isRetina) {
        super(name);
        glWindow = GLWindow.create(glCapabilities);
        MapGLCanvas previewCanvas = new MapGLCanvas(layerManager, glWindow, isRetina);
        canvasPanel = new AnimatedImageCanvasPanel(previewCanvas, name);
        animationManager = new AnimationPlanManager(layerManager, canvasPanel.getCanvas());
        canvasPanel.getCanvas().setAnimationManager(animationManager);
        glWindow.addGLEventListener(previewCanvas);
        //stageControllers = new ArrayList<>();
    }

    public void playAnimation () {
        if (!canvasPanel.getCanvas().getIsAnimating() && !canvasPanel.getCanvas().getPauseAnimation()) {
            canvasPanel.getCanvas().startAnimating();
        }
        else if (canvasPanel.getCanvas().getIsAnimating() && !canvasPanel.getCanvas().getPauseAnimation()) {
            canvasPanel.getCanvas().pauseAnimation(true);
        }
        else if (canvasPanel.getCanvas().getIsAnimating() && canvasPanel.getCanvas().getPauseAnimation()) {
            canvasPanel.getCanvas().pauseAnimation(false);
        }

    }

    public void setAnimationTime(float time) {
        long newTime = ((long)(time*1000));
        canvasPanel.getCanvas().setAnimationTime(newTime);
    }


    public void addNewStagedAnimation(int level) {
        ((AnimationPlanManager)animationManager).createStagedAnimation();
        if (((AnimationPlanManager)animationManager).getStagedAnimations().size()>1) {
            ((StagedAnimationByPlan)animationManager.getCurrentStagedAnimation()).setInitTexture(((StagedAnimationByPlan)animationManager.getStagedAnimations().get(animationManager.getStagedAnimations().size()-2)).getEndTexture());
        }
        ((AnimationPlanManager)animationManager).createBlendAnimationPlan(level);
    }

    public void addNewStagedAnimation(StagedAnimationByPlan stagedAnimationByPlan) {
        ((AnimationPlanManager)animationManager).addStagedAnimation(stagedAnimationByPlan);
    }


    public void updateStage(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation stagePlanAnimation, Texture maskTextureInit, Texture maskTetxureEnd,
                            String transitionType, int blendPixels, boolean blurBorder) {
        ((AnimationPlanManager)animationManager).updateStagePlan(stagedAnimationByPlan, stagePlanAnimation, SatHistConstants.namesTransitions.get(transitionType),
                maskTextureInit, maskTetxureEnd, (int)Math.round(blendPixels), blurBorder );
    }

    public void replaceStage(StagedAnimationByPlan stagedAnimationByPlan, StagePlanAnimation oldAnimation, StagePlanAnimation newAnimation) {
        stagedAnimationByPlan.replaceStage(oldAnimation, newAnimation);
    }

    public void addDefaultStage() {
        ((AnimationPlanManager)animationManager).createBlendAnimationPlan((StagedAnimationByPlan)animationManager.getCurrentStagedAnimation(),0);
    }


    public void addEqualizedStagedAnimation() {
        ((AnimationPlanManager)animationManager).addEqualizedStagedAnimationByPlan(0,2);
    }

    public void updateAnimationPlan() {
        ((AnimationPlanManager)animationManager).updateCompleteAnimationPlan();
    }

    public void updateAnimationPlan(StagedAnimationByPlan stagedAnimationByPlan) {
        ((AnimationPlanManager)animationManager).updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }

    public void swapStages(int movingIndex, int destinationIndex) {
        AbstractAnimation movingAnimation = ((AnimationPlanManager)animationManager).getCurrentStagedAnimation().getStages().get(movingIndex);
        ((AnimationPlanManager)animationManager).getCurrentStagedAnimation().getStages().remove(movingIndex);
        ((AnimationPlanManager)animationManager).getCurrentStagedAnimation().getStages().add(destinationIndex, movingAnimation);
        updateAnimationPlan();
    }

    public void paralelizeStages(TreeMap<Float,TimedStagePlanAnimation> selectedStages, StagedAnimationByPlan stagedAnimationByPlan, float startTime) {

        ((AnimationPlanManager)animationManager).updateCompleteAnimationPlanTimed(stagedAnimationByPlan);
    }

    public GLWindow getGlWindow() {
        return glWindow;
    }

    public void updateMaskInAnimation(String textureName, AbstractAnimation animation, Mat newMaskMat) {
        ((StagePlanAnimation)animation).setMaskMat(newMaskMat);
        ((AnimationPlanManager)animationManager).updateCompleteAnimationPlan();
    }

    public void setInitRaster(RasterGeoElement initRaster) {
        ((AnimationPlanManager)animationManager).setInitTexture(initRaster);
    }

    public void setEndRaster(RasterGeoElement endRaster) {
        ((AnimationPlanManager)animationManager).setEndTexture(endRaster);
    }

    public void deleteAnimation(AbstractAnimation animation) {
        ((AnimationPlanManager)animationManager).deleteAnimation(animation);
    }

    public void deleteStagedAnimation(AbstractStagedAnimation stagedAnimation) {
        ((AnimationPlanManager)animationManager).deleteStagedAnimation(stagedAnimation);
    }

    public void exportAnimation(String filePath) {
        ((AnimationPlanManager)animationManager).exportAnimation(filePath);


    }

    public void saveProject(String filePath, String beforePath, String afterPath) {
        ((AnimationPlanManager)animationManager).exportProject(filePath, beforePath, afterPath);
    }


    public AbstractAnimationManager getAnimationManager() {
        return animationManager;
    }

    public TimedStagePlanAnimation getCurrentAnimation () {
        return ((StagedAnimationByPlan)animationManager.getCurrentStagedAnimation()).getCurrentAnimation();
    }

    public StagePlanAnimation getCurrentAnimation(float time) {
        Pair<Float, AbstractStagedAnimation> stagedAnimationInTime = animationManager.getCurrentAnimation(time);
        StagePlanAnimation stagePlanAnimation = (StagePlanAnimation)((StagedAnimationByPlan)stagedAnimationInTime.getValue2()).getStagePlanTimeAnimationAtTime(time-stagedAnimationInTime.getValue1());
        return  stagePlanAnimation;
    }

    public float getStartTimeCurrentAnimation(float time) {
        Pair<Float, AbstractStagedAnimation> stagedAnimationInTime = animationManager.getCurrentAnimation(time);
        float startTime= 0;;
        if (stagedAnimationInTime != null) {
             startTime = stagedAnimationInTime.getValue2().getStagePlanTimeAnimationAndTimeAtTime(time).getValue1();
        }
        return  startTime;
    }


    public AbstractStagedAnimation getCurrentStagedAnimation() {
        return ((StagedAnimationByPlan)animationManager.getCurrentStagedAnimation());
    }

    public StagedAnimationByPlan getStagedAnimationByPlan(int index) {
        return ((StagedAnimationByPlan)animationManager.getStagedAnimation(index));

    }

    public TimedStagePlanAnimation getEqualizedStagePlanAnimation() {
        return ((StagedAnimationByPlan)animationManager.getCurrentStagedAnimation()).getEqualizedAnimation().getCurrentAnimation();
    }

    public Mat getAnimationPlanToSave() {
        //System.out.println("get animation plan to save!");
        return ((AnimationPlanManager)animationManager).getAnimationPlanWithDuration();
    }

    public double getPixelCoef() {
        return pixelCoef;
    }

    public void setCurrentStagedAnimation(StagedAnimationByPlan stagedAnimation) {
        animationManager.setCurrentStagedAnimation(stagedAnimation);
    }

    public void createAnimationRaster(RasterGeoElement rasterGeoElement) {
        ((AnimationPlanManager)animationManager).createAnimationRaster(rasterGeoElement);
    }

    public void reset() {
        Iterator<Map.Entry<Layer, HashMap<GeoElement,RenderFeatures>>> iter = layerManager.getRenderingFeatures().entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Layer, HashMap<GeoElement,RenderFeatures>> entry = iter.next();
            if(entry.getKey() != animationManager.getAnimationLayer()){
                iter.remove();
            }
        }
        ((AnimationPlanManager)animationManager).reset();

    }





}
