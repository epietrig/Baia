/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import javax.swing.*;

public class SaveProjectWorker  extends SwingWorker {

    SatHistViewer satHistViewer;
    String path;

    public SaveProjectWorker (SatHistViewer satHistViewer, String path) {
        this.satHistViewer = satHistViewer;
        this.path = path;
    }
    @Override
    protected Object doInBackground() throws Exception {
        satHistViewer.saveProject(path);
        return null;
    }

    protected void done() {
        satHistViewer.finishSavingProject();
    }
}
