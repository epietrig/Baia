/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.canvasEvents.*;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import org.opencv.core.Mat;

import javax.vecmath.Vector3f;
import java.awt.*;

/**
 * Created by mjlobo on 24/05/16.
 */
public class ImageCanvasController {
    RasterGeoElement visibleRaster;
    ImageCanvasPanel canvasPanel;
    LayerManager layerManager;
    String name;
    int [] startPointPx = new int[2];

    public ImageCanvasController(String name) {
        layerManager = new LayerManager();
        this.name = name;
    }

    public LayerManager getLayerManager() {
        return layerManager;
    }

    public void setCanvasPanel(ImageCanvasPanel canvasPanel) {
        this.canvasPanel = canvasPanel;
    }

    public ImageCanvasPanel getCanvasPanel() {
        return canvasPanel;
    }

    public String getName() {
        return name;
    }

    //Camera operations
    //Camera operations
    public void startPanning(int x, int y) {
        startPointPx = new int[] {x,y};
    }

    public void pan(int x, int y) {
        canvasPanel.getCanvas().getCamera().moveCamera(startPointPx[0]-x,y-startPointPx[1]);
        startPointPx[0] = x;
        startPointPx[1] = y;
    }

    public void zoomIn(int x, int y) {
        //System.out.println("zoom in x and y "+x+" "+y);
        canvasPanel.getCanvas().getCamera().zoomIn(x,y);
    }

    public void zoomInToCenter (double[] zoomCenter) {
        canvasPanel.getCanvas().getCamera().zoomInToCenter(zoomCenter);
    }

    public void zoomOutToCenter(double[] zoomCenter) {
        canvasPanel.getCanvas().getCamera().zoumOutToCenter(zoomCenter);
    }

    public void zoomOut(int x, int y) {
        canvasPanel.getCanvas().getCamera().zoomOut(x,y);

    }

    public void setVisibleRaster(String rasterName) {
        //System.out.println("raster name "+rasterName);
        Layer newLayer;
        if (rasterName!=null) {
            newLayer = layerManager.getRasterLayerByName(rasterName);
        }
        else {
            newLayer = null;
        }

        for (Layer layer : layerManager.getRenderingFeatures().keySet()) {
            if (layer instanceof RasterLayer) {
                if (layer == newLayer) {
                    //System.out.println("set object visible!");
                    layerManager.getRenderingFeatures().get(layer).get(layer.getElements().get(0)).setVisible(true);
                } else {
                    //System.out.println("set object not visible!");
                    layerManager.getRenderingFeatures().get(layer).get(layer.getElements().get(0)).setVisible(false);
                }
            }
        }
        if (newLayer != null) {
            visibleRaster = (RasterGeoElement) newLayer.getElements().get(0);

        }
        else {
            //System.out.println("visible raster null!!!!!!!");
            visibleRaster = null;
        }
    }

    public void setCursor(Cursor cursor) {
        canvasPanel.getCanvas().setCursor(cursor);
    }


    public RasterGeoElement getVisibleRaster() {
        return visibleRaster;
    }

    public void reset() {
        layerManager.getRenderingFeatures().clear();
        visibleRaster = null;
    }

    public void newRaster (RasterGeoElement rasterGeoElement, Layer rasterLayer) {
        canvasPanel.getCanvas().pushEvent(new CanvasLayerEvent(CanvasEvent.NEW_LAYER, rasterLayer));
        canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(((RasterGeoElement)rasterLayer.getElements().get(0)).getTexture(), ((RasterGeoElement)rasterLayer.getElements().get(0)).getMat(), CanvasEvent.NEW_TEXTURE_FROM_MAT));
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, rasterLayer.getElements().get(0), rasterLayer));
        canvasPanel.getCanvas().pushEvent(new CanvasRasterEvent(CanvasEvent.NEW_RASTER, rasterGeoElement, rasterLayer));
    }




}
