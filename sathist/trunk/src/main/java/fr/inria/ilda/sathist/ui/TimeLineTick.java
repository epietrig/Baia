/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mjlobo on 01/08/16.
 */
public class TimeLineTick extends JPanel {

    int height;
    int posX;

    public TimeLineTick(int height, int initialPosX) {
        setOpaque(false);
        JLabel testLabel = new JLabel("test");
        setLayout(new FlowLayout(FlowLayout.LEFT));
        this.height = height;
        this.posX = initialPosX;
        repaint();
        //add(testLabel);

    }


    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g = (Graphics2D) graphics;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setStroke(new BasicStroke(3));
        g.setColor(new Color(240,59,32,150));
        g.drawLine(posX,0,posX,height);
    }

    public void setPosX(int posX) {
        this.posX = posX;
        repaint();
    }
}
