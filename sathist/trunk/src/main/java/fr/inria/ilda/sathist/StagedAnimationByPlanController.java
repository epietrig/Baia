/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.animation.StagePlanAnimation;
import fr.inria.ilda.mmtools.animation.StagedAnimationByPlan;
import fr.inria.ilda.mmtools.animation.TimedStagePlanAnimation;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.sathist.ui.StagePlanViewLayout;
import fr.inria.ilda.sathist.ui.StagedAnimationByPlanView;
import javafx.stage.Stage;
import org.geotools.xml.xsi.XSISimpleTypes;

import javax.swing.*;
import java.awt.*;
import java.awt.dnd.DnDConstants;
import java.awt.geom.Arc2D;
import java.sql.Time;
import java.util.*;
import java.util.List;

/**
 * Created by mjlobo on 06/09/16.
 */
public class StagedAnimationByPlanController {
    StagedAnimationByPlan stagedAnimationByPlan;
    StagedAnimationByPlanView stagedAnimationByPlanView;
    TimeLineController timeLineController;
    double pixelsPerSecond;
    List<StagePlanController> stagePlanControllers;
    StagePlanController equalizedController;
    HashMap<Float, List<StagePlanController>> startTimesAux = new HashMap<>();
    HashMap<Float, List<StagePlanController>> endTimesAux = new HashMap<>();

    int height;
    public static int minHeight = 24;

    public StagedAnimationByPlanController(StagedAnimationByPlan stagedAnimationByPlan, TimeLineController timeLineController, double pixelsPerSecond,
                                           StateMachine stateMachine, int height) {
        this.stagedAnimationByPlan = stagedAnimationByPlan;
        this.timeLineController = timeLineController;
        this.pixelsPerSecond = pixelsPerSecond;
        this.height = height;
        stagedAnimationByPlanView = new StagedAnimationByPlanView(4*height/5, this, pixelsPerSecond);
        stagePlanControllers = new ArrayList<>();
        stagedAnimationByPlanView.addListeners(stateMachine);

        for (int level : stagedAnimationByPlan.getTimedStages().keySet()) {
            for (TimedStagePlanAnimation timedStagePlanAnimation: stagedAnimationByPlan.getTimedStages().get(level)) {
                addStagePlan(timedStagePlanAnimation, stateMachine);
            }
        }
        if (stagedAnimationByPlan.getInitTexture()!=null) {
            updateTitle(SatHistViewer.BEFORE_CANVAS, stagedAnimationByPlan.getInitTexture().getName());
        }
        if (stagedAnimationByPlan.getEndTexture() != null) {
            updateTitle(SatHistViewer.AFTER_CANVAS, stagedAnimationByPlan.getEndTexture().getName());
        }
        checkCompleteness();

    }

    public void addStagePlan(TimedStagePlanAnimation stagePlanAnimation, StateMachine stateMachine) {
        StagePlanController controller;
        if (checkCompleteness()) {
            controller = new StagePlanController(stagePlanAnimation, minHeight, pixelsPerSecond, this, stateMachine, timeLineController.getView());
        }
        else {
            controller = new StagePlanController(stagePlanAnimation, minHeight, pixelsPerSecond, this, stateMachine, false, timeLineController.getView());
        }

        stagePlanControllers.add(controller);
        stagedAnimationByPlanView.addStagePlanViewTimed(controller.getStagePlanView(), controller.getTimedAnimation());


        updateDuration();
    }

    public void addStagePlan(TimedStagePlanAnimation stagePlanAnimation, StateMachine stateMachine, int index) {
        StagePlanController controller;
        if (checkCompleteness()) {
           controller = new StagePlanController(stagePlanAnimation, minHeight, pixelsPerSecond, this, stateMachine, timeLineController.getView());
        }
        else {
            controller = new StagePlanController(stagePlanAnimation, minHeight, pixelsPerSecond, this, stateMachine, false, timeLineController.getView());

        }

        stagePlanControllers.add(controller);
        stagedAnimationByPlanView.addStagePlanViewTimed(controller.getStagePlanView(), index, controller.getTimedAnimation());
        updateDuration();
    }

    public void modifyStagePlanDuration(StagePlanController selectedStagePlanController, float newDuration) {
        float diffDuration = selectedStagePlanController.getTimedAnimation().getEndTime()-selectedStagePlanController.getTimedAnimation().getStartTime()-newDuration;
        selectedStagePlanController.updateAnimationDuration(newDuration);
        selectedStagePlanController.getTimedAnimation().setEndTime(selectedStagePlanController.getTimedAnimation().getStartTime()+selectedStagePlanController.getTimedAnimation().getAnimation().getDuration());

        List<TimedStagePlanAnimation> animationsAtLevel = stagedAnimationByPlan.getTimedStages().get(selectedStagePlanController.getTimedAnimation().getLevel());
        animationsAtLevel.sort(new StagePlanStartTimeComparator());
        int index = animationsAtLevel.indexOf(selectedStagePlanController.getTimedAnimation());
        if (index >=0) {
            if (index < animationsAtLevel.size()) {
                for (int i = index + 1; i < animationsAtLevel.size(); i++) {
                    animationsAtLevel.get(i).setStartTime(animationsAtLevel.get(i).getStartTime() - diffDuration);
                    animationsAtLevel.get(i).setEndTime(animationsAtLevel.get(i).getEndTime() - diffDuration);
                }
            }
        }
        updateDuration();

    }

    public void updateDuration() {

        stagedAnimationByPlanView.updateTitle();
        stagedAnimationByPlanView.updateView();

    }

    public void offsetStagePlan(StagePlanController stagePlanController, int offsetX, int offsetY) {

        stagedAnimationByPlanView.offsetStagePlan(stagePlanController, offsetX, offsetY);
        }


    public void removeStagePlan(StagePlanAnimation stagePlanAnimation) {
//
        StagePlanController stagePlanController = getStagePlanControllerByAnimation(stagePlanAnimation);

        if (stagePlanController.getTimedAnimation().getLevel()<stagedAnimationByPlan.getMaxLevel()) {
            if (stagedAnimationByPlan.getTimedStages().get(stagePlanController.getTimedAnimation().getLevel())==null || stagedAnimationByPlan.getTimedStages().get(stagePlanController.getTimedAnimation().getLevel()).size()==0) {
                for (int level = stagePlanController.getTimedAnimation().getLevel()+1; level<=stagedAnimationByPlan.getMaxLevel(); level++) {
                    stagedAnimationByPlan.getTimedStages().put(level-1, stagedAnimationByPlan.getTimedStages().get(level));
                    for (TimedStagePlanAnimation timedStagePlanAnimation: stagedAnimationByPlan.getTimedStages().get(level-1)) {
                        //System.out.println("set level! "+(level-1));
                        timedStagePlanAnimation.setLevel(level-1);
                    }
                }
                stagedAnimationByPlan.getTimedStages().remove(stagedAnimationByPlan.getMaxLevel());
            }

        }


        stagePlanControllers.remove(stagePlanController);
        stagedAnimationByPlanView.removeStagePlanView(stagePlanController.getStagePlanView());
    }


    public void finishMoving(StagePlanController stagePlanControllerSelected) {
        int newLevel = (int)(stagePlanControllerSelected.getStagePlanView().getBounds().y/minHeight);
        float diffEq = equalizedController == null ? 0 : (equalizedController.getTimedAnimation().getEndTime() - equalizedController.getTimedAnimation().getStartTime());
        float newStartTime = (float)((stagePlanControllerSelected.getStagePlanView().getBounds().x-diffEq*pixelsPerSecond)/pixelsPerSecond);
        float newEndTime = newStartTime + (stagePlanControllerSelected.getTimedAnimation().getEndTime()-stagePlanControllerSelected.getTimedAnimation().getStartTime());
        boolean intersects = false;
        for (StagePlanController stagePlanController: stagePlanControllers) {
            if (!intersects) {
                if (stagePlanController == equalizedController) {
                    if (newLevel == stagePlanController.getTimedAnimation().getLevel() && ((newStartTime + diffEq >= stagePlanController.getTimedAnimation().getStartTime() && newStartTime + diffEq < stagePlanController.getTimedAnimation().getEndTime()) ||
                            (newEndTime + diffEq > stagePlanController.getTimedAnimation().getStartTime() && newEndTime + diffEq <= stagePlanController.getTimedAnimation().getEndTime()))) {
                        intersects = true;
                    }
                }
                else if (stagePlanController != stagePlanControllerSelected) {
                    if (newLevel == stagePlanController.getTimedAnimation().getLevel() && ((newStartTime >= stagePlanController.getTimedAnimation().getStartTime() && newStartTime < stagePlanController.getTimedAnimation().getEndTime()) ||
                            (newEndTime > stagePlanController.getTimedAnimation().getStartTime() && newEndTime <= stagePlanController.getTimedAnimation().getEndTime()))) {
                        intersects = true;
                    }
                }
            }
        }

        if (!intersects) {
            stagePlanControllerSelected.getTimedAnimation().setStartTime(newStartTime);
            stagePlanControllerSelected.getTimedAnimation().setEndTime(newEndTime);
            stagedAnimationByPlan.changeTimedStageLevel(stagePlanControllerSelected.getTimedAnimation(), newLevel);

        }

        updateDuration();
        stagedAnimationByPlanView.updateView();
    }



    public StagePlanController getStagePlanControllerByAnimation(StagePlanAnimation animation) {
        for (StagePlanController controller: stagePlanControllers) {
            if (controller.getTimedAnimation().getAnimation() == animation) {
                return controller;
            }
        }
        return null;
    }


    public void select() {
        stagedAnimationByPlanView.select();
    }

    public void selectStagePlanController(StagePlanController stagePlanController) {
        stagePlanController.getStagePlanView().select();
        stagedAnimationByPlanView.bringStagePlanToFront(stagePlanController);
    }

    public void unselect() {
        stagedAnimationByPlanView.unselect();
    }

    public StagedAnimationByPlanView getStagedAnimationByPlanView() {
        return stagedAnimationByPlanView;
    }

    public StagedAnimationByPlan getStagedAnimationByPlan() {
        return stagedAnimationByPlan;
    }

    public StagePlanController getStagePlanControllerAt(int x, int y) {
        for (StagePlanController stagePlanController: stagePlanControllers) {
            Point relativePoint = SwingUtilities.convertPoint(timeLineController.getLayeredStagesAndSlider(),x,y,stagePlanController.getStagePlanView());
            //if (stagePlanController.getStagePlanView().contains(x,y)) {
            if (stagePlanController.getStagePlanView().contains(relativePoint)) {
                //System.out.println("stage plan contains!");
                return stagePlanController;
            }
        }
        //System.out.println("does not contain!");
        return null;
    }

    public List<StagePlanController> getStagePlanControllers() {
        return  stagePlanControllers;
    }

   public boolean checkCompleteness () {
       if (stagedAnimationByPlan.getInitTexture() !=null && stagedAnimationByPlan.getEndTexture()!=null) {
           stagedAnimationByPlanView.setComplete(true);
           for (StagePlanController controller : stagePlanControllers) {
               controller.updateView();
           }
           return true;
       }
       else {
           stagedAnimationByPlanView.setComplete(false);
           for (StagePlanController controller : stagePlanControllers) {
               controller.updateView(false);
           }
           return false;
       }
   }

    public double getDuration () {
        if (stagedAnimationByPlan.getEqualizedAnimation() != null) {
            return stagedAnimationByPlan.getDuration() + stagedAnimationByPlan.getEqualizedAnimation().getDuration();
        }
        else {
            return  stagedAnimationByPlan.getDuration();
        }
    }

    public void updateTitle(String canvasName, String newTitle) {
        if (canvasName == SatHistViewer.BEFORE_CANVAS) {
            stagedAnimationByPlanView.setBeforeName(newTitle);
        }
        else if (canvasName == SatHistViewer.AFTER_CANVAS) {
            stagedAnimationByPlanView.setAfterName(newTitle);
        }

    }



    public void setEqualizedController(TimedStagePlanAnimation equalizedAnimation) {
        for (StagePlanController controller : stagePlanControllers) {
            if (controller.getTimedAnimation() == equalizedAnimation) {
                equalizedController = controller;
                stagedAnimationByPlanView.updateView();
            }
        }
    }

    public StagePlanController getEqualizedController() {
        return equalizedController;
    }

    public Texture getInitTexture() {
        if (stagedAnimationByPlan.getEqualizedAnimation()!=null) {
            return stagedAnimationByPlan.getEqualizedAnimation().getInitTexture();
        }else {
            return stagedAnimationByPlan.getInitTexture();
        }
    }

    public void resize(int newHeight) {
        stagedAnimationByPlanView.setPreferredSize(new Dimension((int) (stagedAnimationByPlan.getDurationTimed() * pixelsPerSecond), 4 * newHeight / 5));
        stagedAnimationByPlanView.setMinimumSize(new Dimension((int) (stagedAnimationByPlan.getDurationTimed() * pixelsPerSecond), 4 * newHeight / 5));
        stagedAnimationByPlanView.resize(4 * newHeight / 5);
        stagedAnimationByPlanView.updateTitle();
        stagedAnimationByPlanView.updateView();

    }

    public void addStagePlanToAuxStart(StagePlanController stagePlanController, float diffEq) {
        if (!startTimesAux.containsKey(stagePlanController.getTimedAnimation().getStartTime()+diffEq)) {
            startTimesAux.put(stagePlanController.getTimedAnimation().getStartTime()+diffEq, new ArrayList<>());
        }

        startTimesAux.get(stagePlanController.getTimedAnimation().getStartTime()+diffEq).add(stagePlanController);
    }

    public void addStagePlanToAuxEnd(StagePlanController stagePlanController, float diffEq) {
        if (!endTimesAux.containsKey(stagePlanController.getTimedAnimation().getEndTime()+diffEq)) {
            endTimesAux.put(stagePlanController.getTimedAnimation().getEndTime()+diffEq, new ArrayList<>());
        }
        endTimesAux.get(stagePlanController.getTimedAnimation().getEndTime()+diffEq).add(stagePlanController);
    }


    public void fillAuxTimes() {
        startTimesAux.clear();
        endTimesAux.clear();
        float diffEq = equalizedController == null ? 0 : (equalizedController.getTimedAnimation().getEndTime() - equalizedController.getTimedAnimation().getStartTime());
        if (diffEq >0) {
            addStagePlanToAuxStart(equalizedController, 0);
            addStagePlanToAuxEnd(equalizedController, 0);
        }
        for (StagePlanController stagePlanController: stagePlanControllers) {
            if (stagePlanController != timeLineController.getSelectedStagePlanController()) {
                addStagePlanToAuxStart(stagePlanController, diffEq);
                addStagePlanToAuxEnd(stagePlanController, diffEq);
            }
        }
    }

    public HashMap<Float, List<StagePlanController>> getEndTimesAux() {
        return endTimesAux;
    }

    public HashMap<Float, List<StagePlanController>> getStartTimesAux() {
        return startTimesAux;
    }
}
