/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.stateMachine.ButtonEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.mmtools.ui.StringComboPanel;
import fr.inria.ilda.sathist.ui.MaskOptions;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by mjlobo on 24/05/16.
 */
public class StaticImageCanvasPanel extends ImageCanvasPanel{
    StringComboPanel rasterOptions;
    MaskOptions maskOptions;
    JButton deleteMasksButton;
    static int topPanelHeight = 40;


    public StaticImageCanvasPanel(MapGLCanvas canvas, StringComboPanel rasterOptions, String name ) {
        super(canvas,name);
        this.rasterOptions = rasterOptions;
        rasterOptions.setName(name);
        rasterOptions.getComboBox().setSelectedIndex(-1);
        deleteMasksButton = new JButton("Delete masks");
        deleteMasksButton.setActionCommand(SatHistConstants.DELETE_MASKS_ACTION);
        deleteMasksButton.setName(name);

        setLayout(new BorderLayout());
        GridBagConstraints c = new GridBagConstraints();
        JPanel topPanel = new JPanel();
        topPanel.add(rasterOptions);
        topPanel.add(deleteMasksButton);
        topPanel.setPreferredSize(new Dimension(canvas.getWidth(), topPanelHeight));
        add(topPanel, BorderLayout.PAGE_START);
        add(canvas, BorderLayout.CENTER);
        maskOptions = new MaskOptions((float)SatHistConstants.defaultThreshold,name);
        add(maskOptions,BorderLayout.PAGE_END);
        maskOptions.setVisible(false);
        rasterOptions.getComboBox().setEnabled(false);
    }

    public void addListeners(final StateMachine stateMachine) {
        rasterOptions.addListeners(stateMachine);
        maskOptions.addListeners(stateMachine);
        deleteMasksButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new ButtonEvent(deleteMasksButton,SatHistConstants.DELETE_MASKS_ACTION));
            }
        });


    }

    public void addKeyListeners ( final SatHistViewer satHistViewer) {
        canvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyChar()== 'p') {
                    satHistViewer.displayPreviewWindow();
                }
                if (e.getKeyChar()=='e') {
                    satHistViewer.exportVideoAndScreenShot();
                }
            }
        });
    }

    public void update(String newSelectedText) {
        if (newSelectedText != null) {
            rasterOptions.update(newSelectedText);
        }
        else {
            rasterOptions.getComboBox().setSelectedIndex(-1);
        }
    }

    public void updateMaskOptions (String imageModel, float threshold) {
        maskOptions.updateColorModel(imageModel);
        maskOptions.updateSliderValue(threshold);
    }





    public MaskOptions getMaskOptions() {
        return maskOptions;
    }

    public void enableStagedAnimationOptions(boolean enable) {
        rasterOptions.getComboBox().setEnabled(enable);
    }

    public void enableDeleteMasksButton(boolean enable) {
        deleteMasksButton.setEnabled(enable);
    }
}
