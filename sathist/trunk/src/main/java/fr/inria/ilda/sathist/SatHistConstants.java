/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.animation.AbstractAnimation;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.geotools.temporal.object.DefaultIntervalLength;

import javax.vecmath.Vector3f;
import java.awt.*;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by mjlobo on 25/05/16.
 */
public class SatHistConstants {
    public static double defaultThreshold = 10;
    public static String TRANSITION_CB = "TransitionCB";
    public static String MNT_CB = "MntCB";

    public static String PLAY_PAUSE_ACTION = "play_action";
    public static String FORWARD_ACTION = "forward_action";
    public static String BACKWARD_ACTION = "backward_action";
    public static String STEP_FOWARD_ACTION = "step_forward_action";
    public static String STEP_BACKWARD_ACTION = "step_backward_action";
    public static String ADD_ANIMATION_ACTION = "add_animation_action";
    public static String PAUSE_ACTION = "pause_action";
    public static String NEGATE_MASK_ACTION = "Mask inverse";
    public static String ADD_EQ_ANIMATION_ACTION = "Add Equalization step";
    public static String SAVE_ANIMATION_PLAN = "save_animation_plan";
    public static String DELETE_STAGED_ANIMATION_ACTION = "delete_staged_animation_action";

    public static String TIMELINE_SLIDER = "TimelineSlider";

    public static String IMPORTED_TRANSITION = "Imported";
    public static String RADIAL_IN_TRANSITION = "Radial in";
    public static String RADIAL_OUT_TRANSITION = "Radial out";
    public static String EQUALIZE_TRANSITION = "Equalize";
    public static String DILATE_NAME = "Expand";
    public static String ERODE_NAME = "Contract";
    public static String MORPH_VECTOR_TRANSITION = "Morph";

    public static String DELETE_MASKS_ACTION = "deletemasks";
    public static String UNDO_LAST_ACTION = "Undo";
    public static String SAVE_MASK = "Save Mask";
    public static String LOAD_MASK = "Load Mask";
    public static String COPY_MASK = "Copy Mask";
    public static String PASTE_MASK = "Paste Mask";
    public static String SELECT_CONTOUR = "Select Contour";
    public static String EDIT_DIRECTION = "Edit Direction";
    public static String EDIT_RADIAL_CENTER = "Edit Radial Center";
    public static String REPLICATE_MASK = "Replicate mask";

    public static String ADD_STAGED_ANIMATION_ACTION = "Add Before/After transition";
    public static String ADD_STAGE_ACTION = "Add Stage";
    public static String PARALELIZE_STAGES = "Paralelize stages";
    public static String SPLIT_STAGES = "Split stages";

    public static String ICONS_PATH = "icons/";
    static int TARGET_WIDTH = 1675;
    static int TARGET_HEIGHT = 814;
    public static int BLEND_PIXELS = 25;

    static String RADIAL_MESSAGE = "Please select a center for the animation by clicking in one of the canvases";
    static String DIRECTION_MESSAGE = "Please draw a direction by pressing and dragging in one of the two canvases";
    static String MASK_ERROR_MESSAGE = "Please create a mask in the left canvas to create this transition";
    static String TWO_MASKS_ERROR_MESSAGE = "Please create masks in both canvas to create this transition";
    static String CONTOUR_ERROR = "Please select another mask";

    public static String BLURRED_CHECKBOX = "blurred_checkbox";
    public static String BLURRED_TEXT_AREA = "blurred_text_area";

    public static String INVERT_IMPORTED_CHECKBOX = "invert_imported_checkbox";

    public static String SAVE_MENU_TEXT = "Save";
    public static String OPEN_MENU_TEXT = "Open files";
    public static String EXPORT_MENU_TEXT = "Quicktime Export";
    public static String SAVE_PROJECT_MENU_TEXT = "Save project";
    public static String OPEN_PROJECT_MENU_TEXT = "Open project";
    public static String QUIT_APPLICATION_MENU_TEXT = "Quit";

    public static String DEFAULT_CURSOR = "default_cursor";

    public static String LOG_FOLDER = "logs";

    public static String EQUALIZATION_WAIT_MESSAGE = "Creating Equalization Step...";
    public static String WAIT_MESSAGE = "It may take a significant time (up to a few minutes), be patient.";
    public static String UPDATING_WAIT_MESSAGE = "Updating animation...";
    public static String EXPORTING_WAIT_MESSAGE = "Exporting movie...";
    public static String LOADING_WAIT_MESSAGE = "Loading files...";
    public static String OPENING_PROJECT_MESSAGE = "Opening Project...";
    public static String SAVING_PROJECT_MESSAGE = "Saving Project...";

    public static final HashMap<AbstractAnimation.Type, Color> transitionColors = new HashMap<>();
    static {
        transitionColors.put(AbstractAnimation.Type.BLEND, new Color(255,255,204));
        transitionColors.put(AbstractAnimation.Type.ERODE, new Color(204,235,197));
        transitionColors.put(AbstractAnimation.Type.DILATE, new Color(179, 205,227));
        transitionColors.put(AbstractAnimation.Type.DEFORM, new Color(222,203,228));
        transitionColors.put(AbstractAnimation.Type.DIRECTION, new Color(229,216,189));
        transitionColors.put(AbstractAnimation.Type.RADIAL_IN, new Color(254,217,166));
        transitionColors.put(AbstractAnimation.Type.RADIAL_OUT, new Color(253,218,236));
        transitionColors.put(AbstractAnimation.Type.SEMANTIC, new Color(251,180,174));
        transitionColors.put(AbstractAnimation.Type.EQ, new Color(242, 242, 242));
    }

    public static final HashMap<AbstractAnimation.Type, String> transitionNames = new HashMap<>();
    static {
        transitionNames.put(AbstractAnimation.Type.BLEND, Constants.BLEND_TRANSITION);
        transitionNames.put(AbstractAnimation.Type.ERODE, ERODE_NAME);
        transitionNames.put(AbstractAnimation.Type.DILATE, DILATE_NAME);
        transitionNames.put(AbstractAnimation.Type.DEFORM, Constants.DEFORM_TRANSITION);
        transitionNames.put(AbstractAnimation.Type.DIRECTION, Constants.DIRECTION_TRANSITION);
        transitionNames.put(AbstractAnimation.Type.RADIAL_IN, RADIAL_IN_TRANSITION);
        transitionNames.put(AbstractAnimation.Type.RADIAL_OUT, RADIAL_OUT_TRANSITION);
        transitionNames.put(AbstractAnimation.Type.SEMANTIC, IMPORTED_TRANSITION);
        transitionNames.put(AbstractAnimation.Type.EQ, EQUALIZE_TRANSITION);
        transitionNames.put(AbstractAnimation.Type.VECTOR_MORPH, MORPH_VECTOR_TRANSITION);
    }

    public static final HashMap<String, AbstractAnimation.Type> namesTransitions = new HashMap<>();
    static {
        namesTransitions.put(Constants.BLEND_TRANSITION, AbstractAnimation.Type.BLEND);
        namesTransitions.put(ERODE_NAME, AbstractAnimation.Type.ERODE);
        namesTransitions.put(DILATE_NAME, AbstractAnimation.Type.DILATE);
        namesTransitions.put(Constants.DEFORM_TRANSITION, AbstractAnimation.Type.DEFORM);
        namesTransitions.put(Constants.DIRECTION_TRANSITION, AbstractAnimation.Type.DIRECTION);
        namesTransitions.put(RADIAL_IN_TRANSITION, AbstractAnimation.Type.RADIAL_IN);
        namesTransitions.put(RADIAL_OUT_TRANSITION, AbstractAnimation.Type.RADIAL_OUT);
        namesTransitions.put(IMPORTED_TRANSITION, AbstractAnimation.Type.SEMANTIC);
        namesTransitions.put(EQUALIZE_TRANSITION, AbstractAnimation.Type.EQ);
        namesTransitions.put(MORPH_VECTOR_TRANSITION, AbstractAnimation.Type.VECTOR_MORPH);
    }

    public static Color selectedColor = new Color(211, 231, 250);
    public static Color changedColor = new Color(238,174,77);
    public static Color selectedSatgeColor = new Color(235,244,252);
    public static Vector3f maskColor = new Vector3f(0.84f,0.13f,0.27f);

    static String MASK_CHANGED = "mask_changed";
    static String OBJECT_CHANGED = "object_changed";









}
