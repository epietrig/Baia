/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.mmtools.animation.AnimationPlanManager;
import fr.inria.ilda.mmtools.animation.StagedAnimationByPlan;
import fr.inria.ilda.mmtools.animation.TimedStagePlanAnimation;
import fr.inria.ilda.mmtools.stateMachine.ButtonEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.sathist.SatHistConstants;
import fr.inria.ilda.sathist.StagePlanController;
import fr.inria.ilda.sathist.StagedAnimationByPlanController;
import fr.inria.ilda.sathist.StagedAnimationViewEvent;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.xml.ws.spi.http.HttpHandler;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by mjlobo on 06/09/16.
 */
public class StagedAnimationByPlanView extends JLayeredPane{

    StagedAnimationByPlanController controller;
    double pixelsPerSecond;
    //JPanel stagesPanel;
    JLayeredPane stagesPanel;
    JPanel titlePanel;
    int defaultLineWidth = 1;
    int selectedLineWidth = 2;

    static String DELETE_ICON = "delete";
    static int buttonWidth = 12;
    static int buttonHeight = 12;
    public static int labelHeight = 14;

    JButton deleteButton;

    Color completeBackgroundColor;
    Color incompleteBackgroundColor;
    Color unselectedColor;

    JLabel titleLabel;
    JLabel afterLabel;
    JLabel middleLabel;
    String beforeName = "Before";
    String afterName = "After";

    JPanel zeroPanel;
    InsideBorderPanel insideBorderPanel;

    //HashMap<Integer, JPanel> levelStages;

    int diffForSnap = 16;

    int currentHeight;
    int currentWidth;

    static int borderWidth = 2;


    public StagedAnimationByPlanView(int height, StagedAnimationByPlanController controller, double pixelsPerSecond) {
        this.controller = controller;
        this.pixelsPerSecond = pixelsPerSecond;
        //levelStages = new HashMap<>();
        zeroPanel = new JPanel();
        stagesPanel = new JLayeredPane();
        stagesPanel.setLayout(null);

        //stagesPanel.setBorder(new EmptyBorder(2,10,2,0));
        titlePanel = new JPanel();
        //titleLabel = new JLabel(beforeName+"/"+afterName);
        //titleLabel = new JLabel(beforeName+"/"+afterName);
        titleLabel = new JLabel(beforeName);
        middleLabel = new JLabel("/");
        afterLabel = new JLabel(afterName);
        if (controller.getStagedAnimationByPlan().getStagedAnimationByPlanType() == AnimationPlanManager.StagedAnimationByPlanType.EQ) {
            titleLabel.setText("Equalization");
        }
        ImageIcon deleteIcon = new ImageIcon(SatHistConstants.ICONS_PATH+DELETE_ICON+".png");
        Image newimg = deleteIcon.getImage().getScaledInstance(buttonWidth-2, buttonHeight-2,  java.awt.Image.SCALE_SMOOTH ) ;
        deleteIcon = new ImageIcon( newimg );
        deleteButton = new JButton(deleteIcon);
        deleteButton.setBorder(null);
        deleteButton.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
        deleteButton.setMaximumSize(new Dimension(buttonWidth, buttonHeight));
        deleteButton.setActionCommand(SatHistConstants.DELETE_STAGED_ANIMATION_ACTION);
        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.LINE_AXIS));

        Font boldFont = new Font(titleLabel.getFont().getName(), Font.BOLD, titleLabel.getFont().getSize());
        titleLabel.setFont(boldFont);
        middleLabel.setFont(boldFont);
        afterLabel.setFont(boldFont);
        titlePanel.add(titleLabel);
        titlePanel.add(middleLabel);
        titlePanel.add(afterLabel);
        titlePanel.add(Box.createHorizontalGlue());
        titlePanel.add(deleteButton);
        //titlePanel.setBorder(new LineBorder(Color.RED));
        //titlePanel.setPreferredSize(new Dimension(getWidth(), height/4));

        //setLayout(new BorderLayout());
        zeroPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 0;
        c.anchor = GridBagConstraints.PAGE_START;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 0.0;
        c.weightx = 1.0;
        c.insets = new Insets(2,4,2,3);
        zeroPanel.add(titlePanel, c);
        //add(titlePanel, BorderLayout.PAGE_START);

        c.gridy++;
        c.fill = GridBagConstraints.BOTH;
        c.anchor= GridBagConstraints.PAGE_END;
        c.weighty = 1.0;
        c.weightx = 1.0;
        c.insets = new Insets(0,0,0,0);
        zeroPanel.add(stagesPanel, c);


        completeBackgroundColor = Color.WHITE;
        float[] hsv = new float[3];
        Color.RGBtoHSB(completeBackgroundColor.getRed(), completeBackgroundColor.getGreen(), completeBackgroundColor.getBlue(), hsv);
        //System.out.println("HSV: "+hsv[0]+" "+hsv[1]+" "+hsv[2]);
        hsv[2] = hsv[2]-5f/100f;
        incompleteBackgroundColor = Color.getHSBColor(hsv[0], hsv[1], hsv[2]);
        incompleteBackgroundColor = new Color(217,217,217);
        zeroPanel.setBackground(incompleteBackgroundColor);
        titlePanel.setBackground(incompleteBackgroundColor);
        stagesPanel.setBackground(incompleteBackgroundColor);
        unselectedColor = incompleteBackgroundColor;
        //titlePanel.setBorder(new EmptyBorder(2,1,2,3));

        currentHeight = labelHeight + (int)((controller.getStagedAnimationByPlan().getMaxLevel()+2)*controller.minHeight)+borderWidth*4;
        insideBorderPanel = new InsideBorderPanel(2,Color.GRAY,100, (int)(controller.getDuration()*pixelsPerSecond),currentHeight);
        insideBorderPanel.setBounds(0,0, (int)(controller.getDuration()*pixelsPerSecond), currentHeight);
        insideBorderPanel.setOpaque(false);
        add(insideBorderPanel, new Integer(1));

        zeroPanel.setBounds(0,0, (int)(controller.getDuration()*pixelsPerSecond),currentHeight);
        add(zeroPanel,new Integer(0));



    }


    public void addStagePlanViewTimed(StagePlanViewLayout stagePlanViewLayout, TimedStagePlanAnimation timedStagePlanAnimation) {
        addStagePlanViewTimed(stagePlanViewLayout, 0,timedStagePlanAnimation);
    }

    public void addStagePlanViewTimed(StagePlanViewLayout stagePlanViewLayout, int index, TimedStagePlanAnimation stagePlanAnimation) {
        stagesPanel.add(stagePlanViewLayout);

    }

    public void bringStagePlanToFront(StagePlanController stagePlan) {
        stagesPanel.moveToFront(stagePlan.getStagePlanView());
    }

    public void updateView() {

        if (controller.getStagedAnimationByPlan().getEqualizedAnimation()== null) {
            for (StagePlanController stagePlanController : controller.getStagePlanControllers()) {
                    stagePlanController.getStagePlanView().setBounds((int) (stagePlanController.getTimedAnimation().getStartTime() * pixelsPerSecond), stagePlanController.getTimedAnimation().getLevel() * StagedAnimationByPlanController.minHeight, (int) (stagePlanController.getTimedAnimation().getAnimation().getDuration() * pixelsPerSecond), StagedAnimationByPlanController.minHeight);
                }
        }
        else {
            for (StagePlanController stagePlanController : controller.getStagePlanControllers()) {
                if (stagePlanController == controller.getEqualizedController()) {
                    stagePlanController.getStagePlanView().setBounds((int) (stagePlanController.getTimedAnimation().getStartTime() * pixelsPerSecond), stagePlanController.getTimedAnimation().getLevel() * StagedAnimationByPlanController.minHeight, (int) (stagePlanController.getTimedAnimation().getAnimation().getDuration() * pixelsPerSecond), StagedAnimationByPlanController.minHeight);
                }
                else {
                    stagePlanController.getStagePlanView().setBounds((int) ((stagePlanController.getTimedAnimation().getStartTime() + controller.getStagedAnimationByPlan().getEqualizedAnimation().getDuration()) * pixelsPerSecond), stagePlanController.getTimedAnimation().getLevel() * StagedAnimationByPlanController.minHeight, (int) (stagePlanController.getTimedAnimation().getAnimation().getDuration() * pixelsPerSecond), StagedAnimationByPlanController.minHeight);
                    //stagePlanController.getStagePlanView().setBounds((int) ((stagePlanController.getTimedAnimation().getStartTime()) * pixelsPerSecond), stagePlanController.getTimedAnimation().getLevel() * StagedAnimationByPlanController.minHeight, (int) (stagePlanController.getTimedAnimation().getAnimation().getDuration() * pixelsPerSecond), StagedAnimationByPlanController.minHeight);
                }

            }
        }
        currentHeight = labelHeight + (int)((controller.getStagedAnimationByPlan().getMaxLevel()+2)*controller.minHeight)+borderWidth*4;
        setPreferredSize(new Dimension((int) (controller.getStagedAnimationByPlan().getDurationTimed() * pixelsPerSecond), currentHeight));
        setMinimumSize(new Dimension((int) (controller.getStagedAnimationByPlan().getDurationTimed() * pixelsPerSecond), currentHeight));
        stagesPanel.revalidate();
        zeroPanel.setBounds(0,0, (int)(controller.getStagedAnimationByPlan().getDurationTimed()*pixelsPerSecond), currentHeight);
        insideBorderPanel.setBounds(0,0, (int)(controller.getStagedAnimationByPlan().getDurationTimed()*pixelsPerSecond), currentHeight);

    }

    public void removeStagePlanView(StagePlanViewLayout stagePlanViewLayout) {
        stagesPanel.remove(stagePlanViewLayout);
        updateView();
    }


    public void offsetStagePlan(StagePlanController stagePlanController, int offsetX, int offsetY) {
        double diffEq = controller.getEqualizedController() == null ? 0 : (controller.getEqualizedController().getTimedAnimation().getEndTime() - controller.getEqualizedController().getTimedAnimation().getStartTime());
        double finalX = (stagePlanController.getTimedAnimation().getStartTime()+diffEq) * pixelsPerSecond+offsetX>=0? (stagePlanController.getTimedAnimation().getStartTime() + diffEq) * pixelsPerSecond+offsetX : 0;
        double finalY = stagePlanController.getTimedAnimation().getLevel() * StagedAnimationByPlanController.minHeight + offsetY>=0 ?stagePlanController.getTimedAnimation().getLevel() *  StagedAnimationByPlanController.minHeight + offsetY : 0;
        int newWidth = getWidth();
        int newHeight = getHeight();
        finalX = getCloserStartorEndTime(finalX, (stagePlanController.getTimedAnimation().getEndTime()-stagePlanController.getTimedAnimation().getStartTime())*pixelsPerSecond);
        if (finalX+(stagePlanController.getTimedAnimation().getEndTime()-stagePlanController.getTimedAnimation().getStartTime())*pixelsPerSecond>=controller.getStagedAnimationByPlan().getDurationTimed()*pixelsPerSecond) {
            newWidth = (int)(finalX+(stagePlanController.getTimedAnimation().getEndTime()-stagePlanController.getTimedAnimation().getStartTime())*pixelsPerSecond);
        }
        else if (stagePlanController.getTimedAnimation().getEndTime() == controller.getStagedAnimationByPlan().getDurationTimed() && !controller.getEndTimesAux().containsKey(stagePlanController.getTimedAnimation().getEndTime())) {
            float maxEndTime=-1;
            for (float endTime : controller.getEndTimesAux().keySet()) {
                if (endTime>maxEndTime) {
                    maxEndTime = endTime;
                }
            }
            if (finalX+(stagePlanController.getTimedAnimation().getEndTime()-stagePlanController.getTimedAnimation().getStartTime())*pixelsPerSecond<controller.getStagedAnimationByPlan().getDurationTimed()*pixelsPerSecond
                    && finalX+(stagePlanController.getTimedAnimation().getEndTime()-stagePlanController.getTimedAnimation().getStartTime())*pixelsPerSecond > maxEndTime * pixelsPerSecond - diffForSnap) {
                newWidth = (int)(finalX+(stagePlanController.getTimedAnimation().getEndTime()-stagePlanController.getTimedAnimation().getStartTime())*pixelsPerSecond);
            }
        }
        finalY = getCloserLevel(finalY);

        setPreferredSize(new Dimension(newWidth, getHeight()));
        setMinimumSize(new Dimension(newWidth, getHeight()));
        stagesPanel.setBounds(0,0,newWidth,
                currentHeight);
        stagesPanel.revalidate();
        zeroPanel.setBounds(0,0, newWidth, currentHeight);
        insideBorderPanel.setBounds(0,0, newWidth, currentHeight);

        stagePlanController.getStagePlanView().setBounds((int) (finalX), (int)finalY, (int) (stagePlanController.getTimedAnimation().getAnimation().getDuration() * pixelsPerSecond), StagedAnimationByPlanController.minHeight);
        currentWidth = (int)finalX;
    }

    public double getCloserStartorEndTime(double x, double duration) {
        double minDiff = Integer.MAX_VALUE;
        double newX=x;
        for (double startTime : controller.getStartTimesAux().keySet()) {
            if (Math.abs(startTime*pixelsPerSecond-x)<minDiff) {
                minDiff = Math.abs(startTime*pixelsPerSecond-x);
                newX = (int)(startTime*pixelsPerSecond);
            }
            if (Math.abs(startTime*pixelsPerSecond-(x+duration))<minDiff) {
                minDiff = Math.abs(startTime*pixelsPerSecond-(x+duration));
                newX = (int)(startTime*pixelsPerSecond-duration);
            }
        }
        for (double endTime : controller.getEndTimesAux().keySet()) {
            if (Math.abs(endTime*pixelsPerSecond-(x+duration))<minDiff) {
                minDiff = Math.abs(endTime*pixelsPerSecond-(x+duration));
                newX = (int)(endTime*pixelsPerSecond-duration);
            }
            if (Math.abs(endTime*pixelsPerSecond-x)<minDiff) {
                minDiff = Math.abs(endTime*pixelsPerSecond-x);
                newX = (int)(endTime*pixelsPerSecond);
            }
        }

        if (minDiff<diffForSnap) {
            //System.out.println("new x "+newX);
            return newX;
        }
        else {
            return x;
        }
    }

    public int getCloserLevel(double y) {
        double minDiff = Integer.MAX_VALUE;
        double newY = y;
        ArrayList<Integer> levels = new ArrayList<>();
        levels.addAll(controller.getStagedAnimationByPlan().getTimedStages().keySet());
        Collections.sort(levels);
        levels.add(levels.get(levels.size()-1)+1);
        for (int level=0; level <= levels.get(levels.size()-1); level++) {
            if (Math.abs(level*StagedAnimationByPlanController.minHeight-y)<minDiff) {
                minDiff = Math.abs(level*StagedAnimationByPlanController.minHeight-y);
                newY = level*StagedAnimationByPlanController.minHeight;
            }
        }
        return (int)newY;
    }

    public void resize(int newHeight) {
        zeroPanel.setBounds(0,0, (int)(controller.getStagedAnimationByPlan().getDurationTimed()*pixelsPerSecond), newHeight);
        insideBorderPanel.setBounds(0,0, (int)(controller.getStagedAnimationByPlan().getDurationTimed()*pixelsPerSecond), newHeight);
    }

    public void addListeners(final StateMachine stateMachine) {
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("delete staged animation!");
                stateMachine.handleEvent(new StagedAnimationViewEvent(StagedAnimationByPlanView.this, deleteButton.getActionCommand()));
            }
        });
    }

    public void select() {

        insideBorderPanel.setLineWidth(4);
        zeroPanel.setBackground(SatHistConstants.selectedColor);
        titlePanel.setBackground(SatHistConstants.selectedColor);
        stagesPanel.setBackground(SatHistConstants.selectedColor);


    }

    public void unselect() {
        insideBorderPanel.setLineWidth(2);
        if(controller.checkCompleteness()) {
            unselectedColor = completeBackgroundColor;
        }
        else {
            unselectedColor = incompleteBackgroundColor;
        }
        zeroPanel.setBackground(unselectedColor);
        titlePanel.setBackground(unselectedColor);
        stagesPanel.setBackground(unselectedColor);
        middleLabel.setForeground(Color.BLACK);
        afterLabel.setForeground(Color.BLACK);
        titleLabel.setForeground(Color.BLACK);
    }

    public void setComplete(boolean complete) {
        if (complete) {
//            setBackground(completeBackgroundColor);
//            stagesPanel.setBackground(completeBackgroundColor);
//            titlePanel.setBackground(completeBackgroundColor);
//            zeroPanel.setBackground(completeBackgroundColor);
            unselectedColor = completeBackgroundColor;
        }
        else {
//            setBackground(incompleteBackgroundColor);
//            stagesPanel.setBackground(incompleteBackgroundColor);
//            titlePanel.setBackground(incompleteBackgroundColor);
//            zeroPanel.setBackground(incompleteBackgroundColor);
            unselectedColor = incompleteBackgroundColor;
        }
    }

    public void setBeforeName(String beforeName) {
        if (beforeName!=null) {
            this.beforeName = beforeName;
            updateTitle();
        }
    }

    public void setAfterName(String afterName) {
        if (afterName!=null) {
            this.afterName = afterName;
            updateTitle();
        }
    }

    public void updateTitle() {
        //titleLabel.setText(beforeName+"/"+afterName);
        //System.out.println("update title width "+(controller.getDuration()*pixelsPerSecond/3)+" height "+titlePanel.getHeight());
        titleLabel.setText(beforeName);
        titleLabel.setPreferredSize(new Dimension((int)(controller.getDuration()*pixelsPerSecond/3), labelHeight));
        titleLabel.setMaximumSize(new Dimension((int)(controller.getDuration()*pixelsPerSecond/3), labelHeight));
        afterLabel.setText(afterName);
        afterLabel.setPreferredSize(new Dimension((int)(controller.getDuration()*pixelsPerSecond/3), labelHeight));
        afterLabel.setMaximumSize(new Dimension((int)(controller.getDuration()*pixelsPerSecond/3), labelHeight));
        //titlePanel.setToolTipText(beforeName+"/"+afterName);
    }

    public void updateDuration() {
        zeroPanel.setBounds(0,0, (int)(controller.getDuration()*pixelsPerSecond), zeroPanel.getHeight());
        insideBorderPanel.setBounds(0,0, (int)(controller.getDuration()*pixelsPerSecond), insideBorderPanel.getHeight());
    }

    public int getCurrentHeight() {
        return currentHeight;
    }

    public int getCurrentWidth() {
        return currentWidth;
    }


}
