/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.animation.StageDirectionPlanAnimation;
import fr.inria.ilda.mmtools.animation.StageRadialPlanAnimation;
import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.stateMachine.*;
import fr.inria.ilda.mmtools.stateMachine.Action;
import fr.inria.ilda.mmtools.stateMachine.Event;
import fr.inria.ilda.mmtools.ui.ModeToolBar;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.mmtools.utilties.Pair;
import fr.inria.ilda.sathist.ui.StagePlanViewLayout;
import fr.inria.ilda.sathist.ui.StagedAnimationByPlanView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.Arc2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mjlobo on 28/04/16.
 */
public class StateMachineBuilder {
    static String navigationMode = "navigationMode";
    static String panning = "panning";
    static String magicWandAll = "magicWandAll";
    static String magicWand = "magicWand";
    static String radial = "radial";
    static String direction = "direction";
    static String drawingDirectionVector = "drawingdirection";
    static List<String> states = new ArrayList<>();
    static String lasso = "lasso";
    static String drawingLasso = "drawingLasso";
    static String lassoDelete = "lassoDelete";

    static String noSelection = "noSelection";
    static String stagedAnimationSelected = "stagedAnimationSelected";
    static String transitionSelected = "transitionSelected";
    static String transitionMoving = "transitionMoving";
    static String multipleSelectionCtrl = "multipleSelectionCtrl";
    static String morphing = "morphing";

    static String previousState;
    static String previousSateView;
    static boolean ctrlPressed = false;



    public static StateMachine createStateMachine(List<ImageCanvasController> controllers, final SatHistViewer satHistViewer) {
        StateMachine stateMachine = new StateMachine();
        for (ImageCanvasController imageCanvasController : controllers) {
            addNavigationState(imageCanvasController.getCanvasPanel().getCanvas(), imageCanvasController.getName(), stateMachine, satHistViewer);
            if (imageCanvasController instanceof StaticImageCanvasController) {
                addMagicWandAllState(imageCanvasController.getName(), stateMachine, satHistViewer, imageCanvasController.getCanvasPanel().getCanvas());
                addMagicWandState(imageCanvasController.getName(), stateMachine, satHistViewer, imageCanvasController.getCanvasPanel().getCanvas());
                addRadialTransitionState(imageCanvasController.getName(), stateMachine, satHistViewer);
                addDirectionTransitionState(imageCanvasController.getName(), stateMachine, satHistViewer);
                addDrawingMaskState(imageCanvasController.getName(), stateMachine, satHistViewer, imageCanvasController.getCanvasPanel().getCanvas());
                addDrawDeleteLassoMaskState(imageCanvasController.getName(), stateMachine, satHistViewer, imageCanvasController.getCanvasPanel().getCanvas());
                addMorphingTransitionState(imageCanvasController.getName(), stateMachine, satHistViewer);
            }
            stateMachine.setSource(imageCanvasController.getName(), imageCanvasController.getCanvasPanel().getCanvas());
        }

        addButtonAnimationAction(stateMachine.getState(navigationMode), satHistViewer);
        addButtonAnimationAction(stateMachine.getState(magicWandAll), satHistViewer);
        addButtonAnimationAction(stateMachine.getState(magicWand), satHistViewer);
        addButtonAnimationAction(stateMachine.getState(lasso), satHistViewer);
        addButtonAnimationAction(stateMachine.getState(lassoDelete), satHistViewer);
        addButtonAnimationAction(stateMachine.getState(morphing), satHistViewer);
        addCBChangeAction(stateMachine.getState(magicWandAll), satHistViewer);
        addCBChangeAction(stateMachine.getState(magicWand), satHistViewer);
        addCBChangeAction(stateMachine.getState(navigationMode), satHistViewer);
        addCBChangeAction(stateMachine.getState(lasso), satHistViewer);
        addCBChangeAction(stateMachine.getState(lassoDelete), satHistViewer);
        addCBChangeAction(stateMachine.getState(morphing), satHistViewer);
        addTimeSliderAction(stateMachine.getState(navigationMode), satHistViewer);
        addTimeSliderAction(stateMachine.getState(lasso), satHistViewer);
        addTimeSliderAction(stateMachine.getState(lassoDelete), satHistViewer);
        addCheckBoxEventAction(stateMachine.getState(navigationMode), satHistViewer);
        addCheckBoxEventAction(stateMachine.getState(magicWand), satHistViewer);
        addCheckBoxEventAction(stateMachine.getState(magicWandAll), satHistViewer);
        addCheckBoxEventAction(stateMachine.getState(lasso), satHistViewer);
        addCheckBoxEventAction(stateMachine.getState(lassoDelete), satHistViewer);
        addCheckBoxEventAction(stateMachine.getState(morphing), satHistViewer);
        addTextAreaAction(stateMachine.getState(navigationMode), satHistViewer);
        addTextAreaAction(stateMachine.getState(magicWand), satHistViewer);
        addTextAreaAction(stateMachine.getState(magicWandAll), satHistViewer);
        addTextAreaAction(stateMachine.getState(lasso), satHistViewer);
        addTextAreaAction(stateMachine.getState(lassoDelete), satHistViewer);
        addTextAreaAction(stateMachine.getState(morphing), satHistViewer);


        states.add(navigationMode);
        states.add(panning);
        states.add(magicWandAll);
        states.add(magicWand);
        states.add(lasso);
        states.add(lassoDelete);


        return stateMachine;
    }

    public static StateMachine createAnimationSelectionStateMachine(final SatHistViewer satHistViewer, StateMachine viewStateMachine) {
        StateMachine animationSelectionStateMachine = new StateMachine();
        addNoSelectionState(animationSelectionStateMachine, satHistViewer, viewStateMachine);
        addSelectedStagedAnimationState(animationSelectionStateMachine, satHistViewer,viewStateMachine);
        addSelectedTransitionState(animationSelectionStateMachine, satHistViewer, viewStateMachine);
        addMovingTransitionState(animationSelectionStateMachine, satHistViewer, viewStateMachine);
        addTimeLineActions(animationSelectionStateMachine.getState(stagedAnimationSelected), satHistViewer);
        addTimeLineActions(animationSelectionStateMachine.getState(noSelection), satHistViewer);
        addTimeLineActions(animationSelectionStateMachine.getState(transitionSelected), satHistViewer);
        addButtonCreateAnimationAction(animationSelectionStateMachine.getState(noSelection), satHistViewer);
        addButtonCreateAnimationAction(animationSelectionStateMachine.getState(stagedAnimationSelected), satHistViewer);
        addStageAnimationViewActions(animationSelectionStateMachine.getState(noSelection), satHistViewer);
        addStageAnimationViewActions(animationSelectionStateMachine.getState(stagedAnimationSelected), satHistViewer);
        addStageAnimationViewActions(animationSelectionStateMachine.getState(transitionSelected), satHistViewer);

        return animationSelectionStateMachine;
    }

    public static void addNavigationState(final MapGLCanvas canvas, final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer) {
        stateMachine.addToState(navigationMode, new State());
        stateMachine.getState(navigationMode)
                .addAction(StateChangeEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        satHistViewer.setMaskOptionsVisible(false);
                        satHistViewer.setCursor(navigationMode);
                        satHistViewer.setContoursVisible(false);
                        satHistViewer.getModeToolBar().setOnlyButtonSelected(SatHistViewer.NAVIGATION_MODE);
                        return null;
                    }
                })
                .addAction(LeftMouseDownEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        LeftMouseDownEvent mouseEvent = (LeftMouseDownEvent) event;
                        satHistViewer.startPanning(mouseEvent.getX(), mouseEvent.getY());
                        return panning;
                    }
                })
                .addAction(MouseWheelMovedEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        MouseWheelMovedEvent mouseEvent = (MouseWheelMovedEvent) event;
                        if (mouseEvent.getWheelRotation() > 0) {
                            satHistViewer.zoomIn(mouseEvent.getX(), mouseEvent.getY(), canvasName);
                        } else {
                            satHistViewer.zoomOut(mouseEvent.getX(), mouseEvent.getY(), canvasName);
                        }

                        return null;
                    }
                })
                .addAction(MouseEnteredEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        canvas.requestFocus();
                        return null;
                    }
                });

        addChangeModeAction(stateMachine.getState(navigationMode), satHistViewer);

        stateMachine.addToState(panning, new State());
        stateMachine.getState(panning).addAction(MouseMoveEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                MouseMoveEvent mouseEvent = (MouseMoveEvent) event;
                satHistViewer.pan(mouseEvent.getX(), mouseEvent.getY());
                return panning;
            }
        })
                .addAction(LeftMouseUpEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        return navigationMode;
                    }
                })
                .addAction(StateChangeEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        return null;
                    }
                });
        addPopUpAction(stateMachine.getState(navigationMode), satHistViewer, canvasName);
    }

    public static void addMagicWandAllState(final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer, final MapGLCanvas canvas) {
        stateMachine.addToState(magicWandAll, new State());
        stateMachine.getState(magicWandAll)
                .addAction(StateChangeEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        //System.out.println("Magic wand all state");
                        satHistViewer.setMaskOptionsVisible(true);
                        satHistViewer.setCursor(magicWandAll);
                        satHistViewer.setContoursVisible(false);
                        satHistViewer.getModeToolBar().setOnlyButtonSelected(SatHistViewer.MAGIC_WAND_ALL);
                        return null;
                    }
                })
                .addAction(FloatSliderChangeEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {

                        FloatSliderChangeEvent floatSliderChangeEvent = (FloatSliderChangeEvent) event;
                        if (floatSliderChangeEvent.getTarget().getName().equals(SatHistConstants.TIMELINE_SLIDER)) {
                            satHistViewer.setAnimationTime(floatSliderChangeEvent.getValue());
                        } else {
                            satHistViewer.setMagicWandThreshold(floatSliderChangeEvent.getTarget().getName(), floatSliderChangeEvent.getValue());
                        }
                        return null;
                    }
                })
                .addAction(FloatSliderFinishedChangingEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        FloatSliderFinishedChangingEvent floatSliderFinishedChangingEvent = (FloatSliderFinishedChangingEvent) event;
                        satHistViewer.updateMagicWandAll(floatSliderFinishedChangingEvent.getTarget().getName());
                        return null;
                    }
                })
                .addAction(LeftMouseDownEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent) event;
                        satHistViewer.doMagicWandAll(canvasName, leftMouseDownEvent.getX(), leftMouseDownEvent.getY());
                        return null;
                    }
                })
                .addAction(MouseEnteredEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        canvas.requestFocus();
                        return null;
                    }
                });
        addChangeModeAction(stateMachine.getState(magicWandAll), satHistViewer);
        addPopUpAction(stateMachine.getState(magicWandAll), satHistViewer, canvasName);
    }

    public static void addMagicWandState(final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer, final MapGLCanvas canvas) {
        stateMachine.addToState(magicWand, new State());
        stateMachine.getState(magicWand)
                .addAction(StateChangeEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        //System.out.println("Magic wand state");
                        satHistViewer.setMaskOptionsVisible(true);
                        satHistViewer.setCursor(magicWand);
                        satHistViewer.setContoursVisible(false);
                        satHistViewer.getModeToolBar().setOnlyButtonSelected(SatHistViewer.MAGIC_WAND);
                        return null;
                    }
                })
                .addAction(FloatSliderChangeEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {

                        FloatSliderChangeEvent floatSliderChangeEvent = (FloatSliderChangeEvent) event;
                        //System.out.println("Slider event! " + floatSliderChangeEvent.getTarget().getName());
                        if (floatSliderChangeEvent.getTarget().getName().equals(SatHistConstants.TIMELINE_SLIDER)) {
                            satHistViewer.setAnimationTime(floatSliderChangeEvent.getValue());
                        } else {
                            satHistViewer.setMagicWandThreshold(floatSliderChangeEvent.getTarget().getName(), floatSliderChangeEvent.getValue());
                            satHistViewer.updateMagicWand(floatSliderChangeEvent.getTarget().getName());
                        }
                        return null;
                    }
                })
                .addAction(FloatSliderFinishedChangingEvent.class, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        FloatSliderFinishedChangingEvent floatSliderFinishedChangingEvent = (FloatSliderFinishedChangingEvent) event;
                        return null;
                    }
                })
                .addAction(LeftMouseDownEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent) event;
                        satHistViewer.doMagicWand(canvasName, leftMouseDownEvent.getX(), leftMouseDownEvent.getY());
                        return null;
                    }
                })
                .addAction(MouseEnteredEvent.class, canvasName, new Action() {
                    @Override
                    public String perform(StateMachine stateMachine, Event event) {
                        canvas.requestFocus();
                        return null;
                    }
                });
        addChangeModeAction(stateMachine.getState(magicWand), satHistViewer);
        addPopUpAction(stateMachine.getState(magicWand), satHistViewer, canvasName);

    }

    public static void addRadialTransitionState(final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer) {
        stateMachine.addToState(radial, new State());
        stateMachine.getState(radial).addAction(LeftMouseDownEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent) event;
                satHistViewer.defineRadialTransitionCenter(leftMouseDownEvent.getX(), leftMouseDownEvent.getY(), canvasName);
                return previousState;
            }
        }).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.setMaskOptionsVisible(false);
                satHistViewer.showRadialAlert();
                satHistViewer.setCursor(SatHistConstants.DEFAULT_CURSOR);
                return null;
            }
        });

    }

    public static void addMorphingTransitionState(final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer) {
        stateMachine.addToState(morphing, new State());
        stateMachine.getState(morphing).addAction(LeftMouseDownEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent)event;
                satHistViewer.selectContour(leftMouseDownEvent.getX(), leftMouseDownEvent.getY(), canvasName);
                return null;
            }
        }).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.setCursor(SatHistConstants.DEFAULT_CURSOR);
                return null;
            }
        });
    }

    public static void addDirectionTransitionState(final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer) {
        stateMachine.addToState(direction, new State());
        stateMachine.getState(direction).addAction(LeftMouseDownEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent) event;
                satHistViewer.addNewVectorDirection(leftMouseDownEvent.getX(), leftMouseDownEvent.getY(), canvasName);
                return drawingDirectionVector;
            }
        }).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.setMaskOptionsVisible(false);
                satHistViewer.showDirectionAlert();
                satHistViewer.setCursor(SatHistConstants.DEFAULT_CURSOR);
                return null;
            }
        });

        stateMachine.addToState(drawingDirectionVector, new State());
        stateMachine.getState(drawingDirectionVector).addAction(MouseMoveEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                MouseMoveEvent mouseMoveEvent = (MouseMoveEvent) event;
                satHistViewer.modifiyVectorDirecton(mouseMoveEvent.getX(), mouseMoveEvent.getY(), canvasName);
                return null;
            }
        }).addAction(LeftMouseUpEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                return previousState;
            }
        });
    }

    public static void addDrawingMaskState(final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer, final MapGLCanvas canvas) {
        stateMachine.addToState(lasso, new State());
        stateMachine.getState(lasso).addAction(LeftMouseDownEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent) event;
                satHistViewer.startDrawingLassoMask(leftMouseDownEvent.getX(), leftMouseDownEvent.getY(), canvasName, true);
                previousState = lasso;
                return drawingLasso;
            }
        }).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.setMaskOptionsVisible(false);
                satHistViewer.setCursor(lasso);
                satHistViewer.getModeToolBar().setOnlyButtonSelected(lasso);
                return null;
            }
        }).addAction(MouseEnteredEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                canvas.requestFocus();
                return null;
            }
        });
        stateMachine.addToState(drawingLasso, new State());
        stateMachine.getState(drawingLasso).addAction(MouseMoveEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                MouseMoveEvent mouseMoveEvent = (MouseMoveEvent) event;
                satHistViewer.drawLassoMask(mouseMoveEvent.getX(), mouseMoveEvent.getY(), canvasName);
                return null;
            }
        }).addAction(LeftMouseUpEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.finishLassoMask(canvasName);
                return previousState;
            }
        });
        addChangeModeAction(stateMachine.getState(lasso), satHistViewer);
        addPopUpAction(stateMachine.getState(lasso), satHistViewer, canvasName);
        addStageAnimationViewActions(stateMachine.getState(lasso), satHistViewer);

    }

    public static void addDrawDeleteLassoMaskState(final String canvasName, StateMachine stateMachine, final SatHistViewer satHistViewer, final MapGLCanvas canvas) {
        stateMachine.addToState(lassoDelete, new State());
        stateMachine.getState(lassoDelete).addAction(LeftMouseDownEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent) event;
                satHistViewer.startDrawingLassoMask(leftMouseDownEvent.getX(), leftMouseDownEvent.getY(), canvasName, false);
                previousState = lassoDelete;
                return drawingLasso;
            }
        }).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.setCursor(lassoDelete);
                satHistViewer.setMaskOptionsVisible(false);
                satHistViewer.getModeToolBar().setOnlyButtonSelected(lassoDelete);
                return null;
            }
        }).addAction(MouseEnteredEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                canvas.requestFocus();
                return null;
            }
        });
        addChangeModeAction(stateMachine.getState(lassoDelete), satHistViewer);
        addPopUpAction(stateMachine.getState(lassoDelete), satHistViewer, canvasName);
        addStageAnimationViewActions(stateMachine.getState(lasso), satHistViewer);
    }

    public static void addSelectedStagedAnimationState(StateMachine stateMachine, final SatHistViewer satHistViewer, final StateMachine viewStateMachine) {
        stateMachine.addToState(stagedAnimationSelected, new State());
        stateMachine.getState(stagedAnimationSelected).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.clearChanges();
                satHistViewer.enableStagedAnimationOptions(true);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_STAGED_ANIMATION_ACTION, false);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_STAGE_ACTION, true);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_EQ_ANIMATION_ACTION, true);
                satHistViewer.setPopUpMenuItemsEnabled(false);
                satHistViewer.setAddPanelEnabled(false);
                satHistViewer.setModeToolBarStagedAnimationSelected();
                satHistViewer.updateRasters();
                satHistViewer.clearMasks();
                satHistViewer.deleteAnimationObjetcs();
                satHistViewer.getAnimationAddPanel().reset();
                satHistViewer.getTimeLineController().getView().requestFocus();
                satHistViewer.enableDeleteMasksButton(false);
                previousSateView = viewStateMachine.getStateName(viewStateMachine.getState());
                viewStateMachine.setState(viewStateMachine.getState(navigationMode),true);
                return null;
            }
        });
    }

    public static void addNoSelectionState(StateMachine stateMachine, final SatHistViewer satHistViewer, final StateMachine viewStateMachine) {
        stateMachine.addToState(noSelection, new State());
        stateMachine.getState(noSelection).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.clearChanges();
                satHistViewer.enableStagedAnimationOptions(false);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_STAGED_ANIMATION_ACTION, true);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_STAGE_ACTION, false);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_EQ_ANIMATION_ACTION, false);
                satHistViewer.setPopUpMenuItemsEnabled(false);
                satHistViewer.setAddPanelEnabled(false);
                satHistViewer.setModeTooBarNoSelection();
                satHistViewer.updateRasters();
                satHistViewer.clearMasks();
                satHistViewer.deleteAnimationObjetcs();
                satHistViewer.getAnimationAddPanel().reset();
                satHistViewer.enableDeleteMasksButton(false);
                satHistViewer.getTimeLineController().getView().requestFocus();
                previousSateView = viewStateMachine.getStateName(viewStateMachine.getState());
                viewStateMachine.setState(viewStateMachine.getState(navigationMode),true);
                return null;
            }
        });
    }

    public static void addMovingTransitionState(StateMachine stateMachine, final SatHistViewer satHistViewer, final StateMachine viewStateMachine) {
        stateMachine.addState(transitionMoving, new State());
        stateMachine.getState(transitionMoving).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                return null;
            }
        });
        stateMachine.getState(transitionMoving).addAction(LeftMouseUpEvent.class, satHistViewer.getTimeLineController().getName(), new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.finishMovingAnimation();
                return transitionSelected;
            }
        });
        stateMachine.getState(transitionMoving).addAction(MouseMoveEvent.class, satHistViewer.getTimeLineController().getName(), new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                MouseMoveEvent mouseMoveEvent = (MouseMoveEvent)event;
                satHistViewer.getTimeLineController().moveStagePlan(mouseMoveEvent.getX(), mouseMoveEvent.getY());
                return null;
            }
        });
    }

    public static void addSelectedTransitionState(StateMachine stateMachine, final SatHistViewer satHistViewer, final StateMachine viewStateMachine) {
        stateMachine.addToState(transitionSelected, new State());
        stateMachine.getState(transitionSelected).addAction(StateChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.clearChanges();
                satHistViewer.enableStagedAnimationOptions(false);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_STAGED_ANIMATION_ACTION, false);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_STAGE_ACTION, false);
                satHistViewer.getTimeLinePopUpMenu().enableMenuItem(SatHistConstants.ADD_EQ_ANIMATION_ACTION, false);
                satHistViewer.setPopUpMenuItemsEnabled(true);
                satHistViewer.setAddPanelEnabled(true);
                satHistViewer.updateAddPanel();
                satHistViewer.setModeToolBarStageSelected();
                satHistViewer.updateRasters();
                satHistViewer.updateMaks();
                satHistViewer.updateAnimationObjects();
                satHistViewer.getTimeLineController().getView().requestFocus();
                satHistViewer.enableDeleteMasksButton(true);
                return null;
            }
        });
        stateMachine.getState(transitionSelected).addAction(ButtonEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                ButtonEvent buttonEvent = (ButtonEvent)event;
                if (buttonEvent.getAction().equals(SatHistConstants.ADD_ANIMATION_ACTION)) {
                    satHistViewer.updateAnimation();
                    if (viewStateMachine.getStateName().equals(morphing)) {
                        System.out.println("morphing! previous state "+previousState);
                        viewStateMachine.setState(previousState);
                    }
                }
                if (buttonEvent.getAction().equals(SatHistConstants.ADD_STAGED_ANIMATION_ACTION)) {
                    satHistViewer.addNewStagedAnimation();
                    return stagedAnimationSelected;
                }
                return null;
            }
        });
        stateMachine.getState(transitionSelected).addAction(LeftMouseDoubleDownEvent.class, satHistViewer.getTimeLineController().getName(), new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                satHistViewer.showDurationDialog();
                return null;
            }
        });
        stateMachine.getState(transitionSelected).addAction(TextFieldEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                TextFieldEvent textFieldEvent = (TextFieldEvent)event;
                try {
                    float f = Float.valueOf(textFieldEvent.getText());
                    satHistViewer.updateDuration(f);
                } catch (NumberFormatException exception) {
                    //exception.printStackTrace();
                }
                return null;
            }
        });
        stateMachine.getState(transitionSelected).addAction(AnimationUpdatedEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                AnimationUpdatedEvent animationUpdatedEvent = (AnimationUpdatedEvent)event;
                satHistViewer.setUpdateButtonEnabled(animationUpdatedEvent.getChanged());
                return null;
            }
        });

    }



    public static String changeMode(String mode, SatHistViewer satHistViewer, ModeToolBar modeToolBar) {
        satHistViewer.changeSelectedModeToolBar(mode, modeToolBar);
        if (states.contains(mode)) {
            return mode;
        } else {
            return null;
        }
    }


    public static void addChangeModeAction(State state, final SatHistViewer satHistViewer) {
        state.addAction(ModeButtonPressedEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                ModeButtonPressedEvent modeButtonPressedEvent = (ModeButtonPressedEvent) event;
                return changeMode(modeButtonPressedEvent.getMode(), satHistViewer, modeButtonPressedEvent.getModeToolBar());
            }
        });
    }

    public static void addButtonAnimationAction(State state, final SatHistViewer satHistViewer) {
        state.addAction(ButtonEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                ButtonEvent buttonEvent = (ButtonEvent) event;
                if (buttonEvent.getAction().equals(SatHistConstants.PLAY_PAUSE_ACTION)) {
                    satHistViewer.playAnimation();
                } else if (buttonEvent.getAction().equals(SatHistConstants.BACKWARD_ACTION)) {
                    //System.out.println("backward action!");
                    satHistViewer.backwardAnimation();
                } else if (buttonEvent.getAction().equals(SatHistConstants.FORWARD_ACTION)) {
                    satHistViewer.forwardAnimation();
                } else if (buttonEvent.getAction().equals(SatHistConstants.STEP_BACKWARD_ACTION)) {
                    satHistViewer.stepBackwardAnimation();
                } else if (buttonEvent.getAction().equals(SatHistConstants.STEP_FOWARD_ACTION)) {
                    satHistViewer.stepForwardAnimation();
                }
                else if (buttonEvent.getAction().equals(SatHistConstants.ADD_EQ_ANIMATION_ACTION)) {
                    satHistViewer.addEqualizedAnimation();
                } else if (buttonEvent.getAction().equals(SatHistConstants.SAVE_ANIMATION_PLAN)) {
                    satHistViewer.saveAnimation();
                } else if (buttonEvent.getAction().equals(SatHistConstants.DELETE_MASKS_ACTION)) {
                    satHistViewer.deleteMasks(buttonEvent.getTarget().getName());
                }
                return null;
            }
        });
    }

    public static void addButtonCreateAnimationAction(State state, final SatHistViewer satHistViewer) {
        state.addAction(ButtonEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                ButtonEvent buttonEvent = (ButtonEvent) event;
                if (buttonEvent.getAction().equals(SatHistConstants.ADD_STAGED_ANIMATION_ACTION)) {
                    satHistViewer.addNewStagedAnimation();
                    return stagedAnimationSelected;
                }
                return null;
            }
        });
    }

    public static void addCBChangeAction(final State state, final SatHistViewer satHistViewer) {
        state.addAction(ComboBoxEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                ComboBoxEvent comboBoxEvent = (ComboBoxEvent) event;
                JComboBox target = (JComboBox) comboBoxEvent.getTarget();
                satHistViewer.getPopUpMenu().getMenuItems().get(SatHistConstants.SELECT_CONTOUR).setEnabled(false);
                satHistViewer.getPopUpMenu().getMenuItems().get(SatHistConstants.EDIT_DIRECTION).setEnabled(false);
                satHistViewer.getPopUpMenu().getMenuItems().get(SatHistConstants.EDIT_RADIAL_CENTER).setEnabled(false);
                if (comboBoxEvent.getSelectedText() != null) {
                    if (target.getName().equals(SatHistConstants.TRANSITION_CB)) {
                        satHistViewer.changeAnimationTransition(comboBoxEvent.getSelectedText());
                        satHistViewer.setMNTModelsCBVisible(false);
                        if (!comboBoxEvent.getSelectedText().equals(SatHistConstants.MORPH_VECTOR_TRANSITION)){
                            satHistViewer.setContoursVisible(false);
                        }
                        if (!comboBoxEvent.getSelectedText().equals(Constants.DIRECTION_TRANSITION)) {
                            satHistViewer.setDirectionVisible(false);
                        }
                        if (!(comboBoxEvent.getSelectedText().equals(SatHistConstants.RADIAL_IN_TRANSITION) || comboBoxEvent.getSelectedText().equals(SatHistConstants.RADIAL_OUT_TRANSITION))) {
                            satHistViewer.setRadialCenterVisible(false);
                        }
                        if (comboBoxEvent.getSelectedText().equals(SatHistConstants.RADIAL_OUT_TRANSITION) || comboBoxEvent.getSelectedText().equals(SatHistConstants.RADIAL_IN_TRANSITION)) {
                            previousState = stateMachine.getStateName(state);
                            satHistViewer.getPopUpMenu().getMenuItems().get(SatHistConstants.EDIT_RADIAL_CENTER).setEnabled(true);
                            satHistViewer.setRadialCenterVisible(true);
                            if(!(satHistViewer.getTimeLineController().getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageRadialPlanAnimation)) {
                                return radial;
                            }

                        } else if (comboBoxEvent.getSelectedText().equals(Constants.DIRECTION_TRANSITION)) {
                            previousState = stateMachine.getStateName(state);
                            satHistViewer.getPopUpMenu().getMenuItems().get(SatHistConstants.EDIT_DIRECTION).setEnabled(true);
                            satHistViewer.setDirectionVisible(true);
                            if (!(satHistViewer.getTimeLineController().getSelectedStagePlanController().getTimedAnimation().getAnimation() instanceof StageDirectionPlanAnimation)) {
                                return direction;
                            }
                        } else if (comboBoxEvent.getSelectedText().equals(SatHistConstants.IMPORTED_TRANSITION)) {
                            satHistViewer.setMNTModelsCBVisible(true);
                        }
                        else if (comboBoxEvent.getSelectedText().equals(SatHistConstants.MORPH_VECTOR_TRANSITION)) {
                            previousState = stateMachine.getStateName(state);
                            satHistViewer.setContoursVisible(true);
                            satHistViewer.getPopUpMenu().getMenuItems().get(SatHistConstants.SELECT_CONTOUR).setEnabled(true);
                        }
                    } else if (target.getName().equals(Constants.COLOR_MODEL)) {
                        satHistViewer.setImageModel(target, comboBoxEvent.getSelectedText());
                    } else if (target.getName().equals(SatHistConstants.MNT_CB)) {
                        satHistViewer.setMntSelected(comboBoxEvent.getSelectedText());
                    } else {
                        satHistViewer.changeRaster(target.getName(), comboBoxEvent.getSelectedText());
                    }
                }
                    return null;

            }
        });
    }

    static Component popUpMenuTarget;
    static int [] lastPopUpLocation;
    static String lastCanvas;

    public static void addPopUpAction(final State state, final SatHistViewer satHistViewer, final String canvasName) {
        state.addAction(RightMouseDownEvent.class, canvasName, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                RightMouseDownEvent mouseEvent = (RightMouseDownEvent) event;
                satHistViewer.getPopUpMenu().enableMenuItem(SatHistConstants.UNDO_LAST_ACTION, satHistViewer.getUndoable(canvasName));
                satHistViewer.getPopUpMenu().showPopUpMenu(mouseEvent.getX(), mouseEvent.getY(), mouseEvent.getTarget());
                popUpMenuTarget = mouseEvent.getTarget();
                lastPopUpLocation = new int[] {mouseEvent.getX(), mouseEvent.getY()};
                lastCanvas = canvasName;
                return null;
            }
        });

        state.addAction(MenuActionEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                MenuActionEvent menuActionEvent = (MenuActionEvent) event;
                if (menuActionEvent.getText().equals(SatHistConstants.NEGATE_MASK_ACTION)) {
                    satHistViewer.invertMask(popUpMenuTarget.getName());
                } else if (menuActionEvent.getText().equals(SatHistConstants.UNDO_LAST_ACTION)) {
                    satHistViewer.undoLastMagicWand(popUpMenuTarget.getName());

                } else if (menuActionEvent.getText().equals(SatHistConstants.SAVE_MASK)) {
                    satHistViewer.saveMask(popUpMenuTarget.getName());
                } else if (menuActionEvent.getText().equals(SatHistConstants.LOAD_MASK)) {
                    satHistViewer.loadMask(popUpMenuTarget.getName());
                } else if (menuActionEvent.getText().equals(SatHistConstants.COPY_MASK)) {
                    satHistViewer.copyMask(popUpMenuTarget.getName());
                } else if (menuActionEvent.getText().equals(SatHistConstants.PASTE_MASK)) {
                    satHistViewer.pasteMask(popUpMenuTarget.getName());
                } else if (menuActionEvent.getText().equals(SatHistConstants.SAVE_MENU_TEXT)) {
                    satHistViewer.saveAnimation();
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.REPLICATE_MASK)) {
                    satHistViewer.replicateMask(popUpMenuTarget.getName());
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.EXPORT_MENU_TEXT)) {
                    satHistViewer.exportAnimation();
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.OPEN_PROJECT_MENU_TEXT)) {
                    satHistViewer.loadProject();
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.SELECT_CONTOUR)) {
                    satHistViewer.selectContour(lastPopUpLocation[0], lastPopUpLocation[1], lastCanvas);
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.EDIT_DIRECTION)) {
                    previousState = stateMachine.getStateName(state);
                    return direction;
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.EDIT_RADIAL_CENTER)) {
                    previousState = stateMachine.getStateName(state);
                    return radial;
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.SAVE_PROJECT_MENU_TEXT)) {
                    satHistViewer.saveProject();
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.OPEN_MENU_TEXT)) {
                    satHistViewer.openFolder();
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.QUIT_APPLICATION_MENU_TEXT)) {
                    satHistViewer.quitApplication();
                }

                return null;

            }
        });
    }

    public static void addTimeSliderAction(State state, final SatHistViewer satHistViewer) {
        state.addAction(FloatSliderChangeEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                FloatSliderChangeEvent floatSliderChangeEvent = (FloatSliderChangeEvent) event;
                satHistViewer.setAnimationTime(floatSliderChangeEvent.getValue());
                return null;
            }
        });
    }

    public static void addStageAnimationViewActions(State state, final SatHistViewer satHistViewer) {
        state.addAction(AnimationViewEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                AnimationViewEvent animationViewEvent = (AnimationViewEvent) event;
                satHistViewer.deleteAnimation(animationViewEvent.getAnimation());
                return null;
            }
        });
    }


    public static void addCheckBoxEventAction(State state, final SatHistViewer satHistViewer) {
        state.addAction(CheckBoxEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                CheckBoxEvent checkBoxEvent = (CheckBoxEvent) event;
                if (checkBoxEvent.getText().equals(SatHistConstants.BLURRED_CHECKBOX)) {
                    satHistViewer.setBlurredBorderEnabled(checkBoxEvent.getIsChecked());
                }
                else if (checkBoxEvent.getText().equals(SatHistConstants.INVERT_IMPORTED_CHECKBOX)) {
                    satHistViewer.setInvertedImported(checkBoxEvent.getIsChecked());
                }
                return null;
            }
        });
    }

    public static void addTextAreaAction(State state, final SatHistViewer satHistViewer) {
        state.addAction(TextFieldEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                TextFieldEvent textFieldEvent = (TextFieldEvent) event;
                if (textFieldEvent.getTarget() instanceof StagePlanViewLayout) {
                    //System.out.println("Stage plan layeredStagesAndSlider layout!");
                }
                else if (textFieldEvent.getTarget().getName().equals(SatHistConstants.BLURRED_TEXT_AREA)) {
                    try {
                        int i = Integer.valueOf(textFieldEvent.getText());
                        satHistViewer.setBlurredBorderWidth(i);
                        //stateMachine.handleEvent(new LayerOptionChanged(d, layerName, textField.getName()));
                    } catch (NumberFormatException exception) {
                        //exception.printStackTrace();
                        //System.out.println("not a number!");
                    }
                }
                return null;
            }
        });
    }

    public static void addTimeLineActions(final State state, final SatHistViewer satHistViewer) {
        state.addAction(LeftMouseDownEvent.class, satHistViewer.getTimeLineController().getName(), new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                LeftMouseDownEvent leftMouseDownEvent = (LeftMouseDownEvent) event;
                satHistViewer.getTimeLineController().getSelectedStagePlanController();
                satHistViewer.selectStagedAnimation(leftMouseDownEvent.getX(), leftMouseDownEvent.getY());
                if (satHistViewer.getTimeLineController().getSelectedStagedAnimationByPlan() != null) {

                    if (satHistViewer.getTimeLineController().getSelectedStagePlanController()!=null) {
                        previousState = stateMachine.getStateName(state);
                        if (satHistViewer.getTimeLineController().getSelectedStagePlanControllerList().size() == 1) {
                                if (satHistViewer.getTimeLineController().selectedStagedAnimationByPlan.getEqualizedController() != satHistViewer.getTimeLineController().getSelectedStagePlanController()) {
                                    satHistViewer.getTimeLineController().startMoving(leftMouseDownEvent.getX(), leftMouseDownEvent.getY());
                                    return transitionMoving;
                                }
                                else {
                                    return transitionSelected;
                                }
                        }
                        else {
                            return stagedAnimationSelected;
                        }
                    } else {
                        previousState = stateMachine.getStateName(state);
                        return stagedAnimationSelected;
                    }
                } else {
                    return noSelection;
                }
            }
        });

        state.addAction(StagedAnimationViewEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                StagedAnimationViewEvent stagedAnimationViewEvent = (StagedAnimationViewEvent) event;
                StagedAnimationByPlanView view = (StagedAnimationByPlanView) stagedAnimationViewEvent.getTarget();
                satHistViewer.deleteStagedAnimation(view);
                return noSelection;
                //return null;
            }
        });
        state.addAction(RightMouseDownEvent.class, satHistViewer.getTimeLineController().getName(), new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                RightMouseDownEvent mouseEvent = (RightMouseDownEvent) event;
                satHistViewer.getTimeLinePopUpMenu().showPopUpMenu(mouseEvent.getX(), mouseEvent.getY(), mouseEvent.getTarget());
                return null;
            }
        });

        state.addAction(MenuActionEvent.class, new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {

                MenuActionEvent menuActionEvent = (MenuActionEvent) event;
                if (menuActionEvent.getText().equals(SatHistConstants.ADD_STAGED_ANIMATION_ACTION)) {
                    satHistViewer.addNewStagedAnimation();
                } else if (menuActionEvent.getText().equals(SatHistConstants.ADD_STAGE_ACTION)) {
                    satHistViewer.addDefaultStage();
                }
                else if (menuActionEvent.getText().equals(SatHistConstants.ADD_EQ_ANIMATION_ACTION)) {
                    satHistViewer.addEqualizedAnimation();
                }


                return null;

            }
        });
//

        state.addAction(KeyReleasedEvent.class, satHistViewer.getTimeLineController().getName(), new Action() {
            @Override
            public String perform(StateMachine stateMachine, Event event) {
                KeyReleasedEvent keyReleasedEvent = (KeyReleasedEvent)event;
                if (keyReleasedEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
                    previousState = stateMachine.getStateName(state);
                    ctrlPressed = false;
                }
                return null;
            }
        });


    }
}
