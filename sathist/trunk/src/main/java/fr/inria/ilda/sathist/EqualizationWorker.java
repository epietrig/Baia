/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import javax.swing.*;

/**
 * Created by mjlobo on 14/02/2017.
 */
public class EqualizationWorker extends SwingWorker {
    SatHistViewer satHistViewer;
    public EqualizationWorker(SatHistViewer satHistViewer) {
        this.satHistViewer = satHistViewer;
    }
    @Override
    protected Object doInBackground() throws Exception {
        satHistViewer.getPreviewController().addEqualizedStagedAnimation();
        return null;
    }

    protected void done() {
        satHistViewer.endAddingEqualizedAnimation();
    }
}
