/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.mmtools.stateMachine.TextFieldEvent;
import fr.inria.ilda.sathist.StagePlanViewDropTargetListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

/**
 * Created by mjlobo on 29/01/2017.
 */
public class DurationDialog extends JDialog {

    JButton cancelButton;
    JButton okButton;
    JTextField textField;

    public DurationDialog(JFrame frame) {
        super(frame);
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());
        JLabel durationLabel = new JLabel("Duration:");
        textField = new JTextField(2);
        topPanel.add(durationLabel);
        topPanel.add(textField);
        JPanel bottomPanel = new JPanel();
        okButton = new JButton("Ok");
        cancelButton = new JButton("Cancel");
        bottomPanel.setLayout(new FlowLayout());
        bottomPanel.add(okButton);
        bottomPanel.add(cancelButton);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        add(topPanel);
        add(bottomPanel);
        //setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - getHeight()/2);
        pack();
    }

    public void addListeners(final StateMachine stateMachine) {
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new TextFieldEvent(textField, textField.getText()));
                setVisible(false);
            }
        });
        textField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new TextFieldEvent(textField, textField.getText()));
                setVisible(false);
            }
        });
    }

    public void update(float duration) {
        textField.setText(""+duration);
    }



}
