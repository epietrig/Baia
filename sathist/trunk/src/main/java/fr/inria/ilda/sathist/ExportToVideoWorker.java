/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;
import javax.swing.*;
import java.util.*;
import java.awt.image.BufferedImage;

/**
 * Created by mjlobo on 12/12/2017.
 */
public class ExportToVideoWorker extends SwingWorker {
    String filename;
    int duration;
    List<BufferedImage> images;

    public ExportToVideoWorker(String filename, int duration, List<BufferedImage> images) {
        this.filename = filename;
        this.duration = duration;
        this.images = images;
    }

    @Override
    protected Object doInBackground() throws Exception {
        return null;
    }
}
