/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import fr.inria.ilda.mmtools.gl.MapGLCanvas;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import javax.swing.*;


/**
 * Created by mjlobo on 24/05/16.
 */
public abstract class ImageCanvasPanel extends JPanel{
    MapGLCanvas canvas;
    String name;


    public ImageCanvasPanel(MapGLCanvas canvas, String name ) {
        this.canvas = canvas;
        this.name = name;
        canvas.setName(name);
    }

    public MapGLCanvas getCanvas() {
        return canvas;
    }

    public abstract void addListeners(StateMachine stateMachine);

    public abstract void update(String newSelectedText);


}
