/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import fr.inria.ilda.mmtools.canvasEvents.*;
import fr.inria.ilda.mmtools.cv.CVUtilities;
import fr.inria.ilda.mmtools.geo.*;
import fr.inria.ilda.mmtools.gl.ShaderManager;
import fr.inria.ilda.mmtools.gl.Texture;
import fr.inria.ilda.mmtools.utilties.Constants;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;

import javax.vecmath.Vector3f;
import java.util.*;

/**
 * Created by mjlobo on 24/05/16.
 */
public class StaticImageCanvasController extends ImageCanvasController {
    ColorThresholdGeoElement colorSelectionDrawn;
    GeometryBuilder geometryBuilder;
    Layer colorSelectionLayer;
    Constants.ImageModelName selectedColorModel = Constants.ImageModelName.CIELAB;
    double magicWandAllThreshold = SatHistConstants.defaultThreshold;
    int[] lastMagicWandAllpx;
    int[] beforeLastMagicWandAllpx;
    double[] lastMagicWandWorld;
    Mat lastMagicWandMat;
    Texture alreadyInMask;
    List<int[]> magicWandPoints;
    Texture magicWandTexture;
    Vector3f compareColor;
    Layer animationObjectsLayer;
    DrawnGeoElement radialCenter;
    double [] startPoint;
    double[] endPoint;
    List<DrawnGeoElement> directionVectorDrawns;
    DrawnGeoElement drawingVectorDirection;
    HashMap<DrawnGeoElement, Boolean> maskDrawns;
    DrawnGeoElement drawingMask;
    List<org.opencv.core.Point> maskDrawnPoints;
    Mat drawnMasksMat;
    static Vector3f WHITE = new Vector3f(255,255,255);
    static Vector3f BLACK = new Vector3f(0.0f,0.0f,0.0f);
    DrawnGeoElement selectedContourDrawn;
    List<DrawnGeoElement> contourDrawns;
    boolean undoable = false;

    List<StaticImageCanvasControllerListener> listeners = new ArrayList<>();





    public StaticImageCanvasController(String name) {
        super(name);
        colorSelectionLayer = new DrawnLayer(false);
        animationObjectsLayer = new DrawnLayer(false);
        layerManager.addLayer(colorSelectionLayer);
        layerManager.addLayer(animationObjectsLayer);
        animationObjectsLayer.setVisibility(true);
        geometryBuilder = new GeometryBuilder();
        directionVectorDrawns = new ArrayList<>();
        maskDrawns = new HashMap<>();
        maskDrawnPoints = new ArrayList<>();
        magicWandPoints = new ArrayList<>();
        contourDrawns = new ArrayList<>();

    }


    public void doMagicWandAllInc(int x, int y) {
        lastMagicWandWorld = canvasPanel.getCanvas().getCamera().screenToWorld(x, y);
        if (lastMagicWandAllpx!=null) {
            beforeLastMagicWandAllpx = lastMagicWandAllpx;
        }
        lastMagicWandAllpx = visibleRaster.getCoordinateInPx(lastMagicWandWorld);
        Mat matModel = ((RGBRasterGeoElement) visibleRaster).getImageModelByType(selectedColorModel).getMat();
        double L = matModel.get(lastMagicWandAllpx[1],lastMagicWandAllpx[0])[0];
        double A = matModel.get(lastMagicWandAllpx[1],lastMagicWandAllpx[0])[1];
        double B = matModel.get(lastMagicWandAllpx[1],lastMagicWandAllpx[0])[2];
        compareColor = new Vector3f((float)L,(float)A,(float)B);
        Mat mask = CVUtilities.maskByDistance(matModel,compareColor,magicWandAllThreshold);

        if (magicWandTexture == null) {
            lastMagicWandMat = Mat.zeros(visibleRaster.getImageHeight(), visibleRaster.getImageWidth(), CvType.CV_8U);
            magicWandTexture = new Texture(visibleRaster, CVUtilities.or(lastMagicWandMat, mask));
        }
        else {
            lastMagicWandMat = magicWandTexture.getData();
            magicWandTexture.setData(CVUtilities.or(lastMagicWandMat, mask));
        }
        if (alreadyInMask == null) {
            alreadyInMask = new Texture(visibleRaster, lastMagicWandMat);
        }
        else {
            alreadyInMask.setData(lastMagicWandMat);
        }
        canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(magicWandTexture,mask, CanvasEvent.NEW_TEXTURE_FROM_MAT));
        canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(alreadyInMask,lastMagicWandMat, CanvasEvent.NEW_TEXTURE_FROM_MAT));
        displayColorSelectionDrawn(new Vector3f(compareColor.x, compareColor.y, compareColor.z));
        undoable = true;
        throwCanvasChanged();
    }


    public void doMagicWandInc(int x, int y) {
        lastMagicWandWorld = canvasPanel.getCanvas().getCamera().screenToWorld(x, y);
        if (lastMagicWandAllpx!=null) {
            beforeLastMagicWandAllpx = lastMagicWandAllpx;
        }
        lastMagicWandAllpx = visibleRaster.getCoordinateInPx(lastMagicWandWorld);
        Mat mask;
        if (magicWandTexture == null) {
            lastMagicWandMat = Mat.zeros(visibleRaster.getImageHeight(), visibleRaster.getImageWidth(), CvType.CV_8U);
            mask = CVUtilities.or(lastMagicWandMat, CVUtilities.floodFill(((RGBRasterGeoElement) visibleRaster).getImageModelByType(selectedColorModel).getMat(), lastMagicWandAllpx[0], lastMagicWandAllpx[1], (int)magicWandAllThreshold, (int)magicWandAllThreshold));
            magicWandTexture = new Texture(visibleRaster, mask);
        }else {
            lastMagicWandMat = magicWandTexture.getData();
            mask = CVUtilities.or(lastMagicWandMat, CVUtilities.floodFill(((RGBRasterGeoElement) visibleRaster).getImageModelByType(selectedColorModel).getMat(), lastMagicWandAllpx[0], lastMagicWandAllpx[1], (int)magicWandAllThreshold, (int)magicWandAllThreshold));
            magicWandTexture.setData(mask);
        }
        canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(magicWandTexture,mask, CanvasEvent.NEW_TEXTURE_FROM_MAT));
        displayColorSelectionDrawn();
        undoable = true;
        throwCanvasChanged();
    }


    public void updateMagicWandInc() {
        Mat mask = CVUtilities.or(lastMagicWandMat, CVUtilities.floodFill(((RGBRasterGeoElement) visibleRaster).getImageModelByType(selectedColorModel).getMat(), lastMagicWandAllpx[0], lastMagicWandAllpx[1], (int)magicWandAllThreshold, (int)magicWandAllThreshold));
        magicWandTexture.setData(mask);
        colorSelectionDrawn.setChangedMaskTexture(true);
        throwCanvasChanged();
    }



    public void updateMagicWandAllInc() {
        Mat mask = CVUtilities.or(lastMagicWandMat, CVUtilities.maskByDistance(((RGBRasterGeoElement) visibleRaster).getImageModelByType(selectedColorModel).getMat(),compareColor,magicWandAllThreshold));
        magicWandTexture.setData(mask);
        throwCanvasChanged();

    }


    public void displayColorSelectionDrawn (Vector3f color) {
        if (colorSelectionDrawn == null) {
            Polygon backgroundBox = geometryBuilder.box(visibleRaster.getX()-visibleRaster.getRealWidth()/2, visibleRaster.getY()-visibleRaster.getRealHeight()/2, visibleRaster.getX()+visibleRaster.getRealWidth()/2,
                    visibleRaster.getY()+visibleRaster.getRealHeight()/2);
            colorSelectionDrawn = new ColorThresholdGeoElement(backgroundBox, SatHistConstants.defaultThreshold, color, ((RGBRasterGeoElement)visibleRaster).getImageModelByType(Constants.ImageModelName.CIELAB).getTexture(), name,SatHistConstants.maskColor);
            //colorSelectionDrawn.setMaskedTexture(magicWandTexture);
            //System.out.println("color thresholded texture pixel width "+((RGBRasterGeoElement)visibleRaster).getImageModelByType(Constants.ImageModelName.CIELAB).getTexture().getPixelWidth());
            colorSelectionDrawn.drawCompletePolygon(0, visibleRaster.getTexture());
            colorSelectionLayer.addElement(colorSelectionDrawn);
            layerManager.addElementToLayer(colorSelectionDrawn, colorSelectionLayer, true);
            canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, colorSelectionDrawn, colorSelectionLayer));

        }
        else {
            //System.out.println("color selection drawn threshold "+colorSelectionDrawn.getTextureToThreshold());
            if (colorSelectionDrawn.getTextureToThreshold()==null) {
                colorSelectionDrawn.setTextureToThreshold( ((RGBRasterGeoElement)visibleRaster).getImageModelByType(Constants.ImageModelName.CIELAB).getTexture());
            }
            colorSelectionDrawn.setCompareColor(color);
            //colorSelectionDrawn.setTextureToThreshold(((RGBRasterGeoElement)visibleRaster).getImageModelByType(Constants.ImageModelName.CIELAB).getTexture());
            colorSelectionDrawn.setThreshold(magicWandAllThreshold);
        }
        //colorSelectionDrawn.setMaskedTexture(magicWandTexture);
        colorSelectionDrawn.setMaskedTexture(alreadyInMask);
        colorSelectionDrawn.setRenderingType(ShaderManager.RenderedElementTypes.COLOR_DIFF);
        //colorSelectionDrawn.setRenderingType(ShaderManager.RenderedElementTypes.MASK);
        colorSelectionDrawn.setVisible(true);
        //pushMaskChangedEvent();
    }

    public void displayColorSelectionDrawn () {
        Vector3f color = new Vector3f(1.0f,0.0f,0.0f);
        if (colorSelectionDrawn == null) {
            Polygon backgroundBox = geometryBuilder.box(visibleRaster.getX()-visibleRaster.getRealWidth()/2, visibleRaster.getY()-visibleRaster.getRealHeight()/2, visibleRaster.getX()+visibleRaster.getRealWidth()/2,
                    visibleRaster.getY()+visibleRaster.getRealHeight()/2);
            colorSelectionDrawn = new ColorThresholdGeoElement(backgroundBox, 0, color, name, SatHistConstants.maskColor, magicWandTexture);
            colorSelectionDrawn.drawCompletePolygon(0, magicWandTexture);
            layerManager.addElementToLayer(colorSelectionDrawn, colorSelectionLayer, true);
            canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, colorSelectionDrawn, colorSelectionLayer));

        }
        colorSelectionDrawn.setMaskedTexture(magicWandTexture);
        colorSelectionDrawn.setRenderingType(ShaderManager.RenderedElementTypes.MASK);
        colorSelectionDrawn.setVisible(true);
    }

    public void setMaskOptionsVisible(boolean visible) {
        ((StaticImageCanvasPanel)canvasPanel).getMaskOptions().setVisible(visible);
    }

    public void setMagicWandThreshold(float newValue, boolean updatePanel) {
        if (updatePanel) {
            ((StaticImageCanvasPanel) canvasPanel).updateMaskOptions(selectedColorModel.toString(), newValue);
        }
        this.magicWandAllThreshold = newValue;
        if (colorSelectionDrawn!=null) {
            colorSelectionDrawn.setThreshold(newValue);
        }
    }

    public void changeColorModel (String colorModelName, boolean updatePanel) {
        setImageModelName(colorModelName);
        if (updatePanel) {
            ((StaticImageCanvasPanel) canvasPanel).updateMaskOptions(colorModelName, (float) magicWandAllThreshold);
        }
        if (colorSelectionDrawn!=null && lastMagicWandAllpx!=null) {
            Mat matModel = ((RGBRasterGeoElement) visibleRaster).getImageModelByType(selectedColorModel).getMat();
            double C1 = matModel.get(lastMagicWandAllpx[1],lastMagicWandAllpx[0])[0];
            double C2 = matModel.get(lastMagicWandAllpx[1],lastMagicWandAllpx[0])[1];
            double C3 = matModel.get(lastMagicWandAllpx[1],lastMagicWandAllpx[0])[2];
            if (compareColor == null) {
                compareColor = new Vector3f((float)C1, (float)C2, (float)C3);
            }
            else {
                compareColor.set((float) C1, (float) C2, (float) C3);
            }
            colorSelectionDrawn.setCompareColor(new Vector3f((float)C1,(float)C2,(float)C3));
            colorSelectionDrawn.setTextureToThreshold(((RGBRasterGeoElement)visibleRaster).getImageModelByType(selectedColorModel).getTexture());
                updateMagicWandInc();
            }


        
        //throwCanvasChanged();

    }

    public void setImageModelName(String colorModelName) {
        //System.out.println("set image model name in static canvas controller");
        for (Constants.ImageModelName model : Constants.ImageModelName.values()) {
            if (model.toString().equals(colorModelName)) {
                this.selectedColorModel = model;
                //System.out.println("found enum!");
            }
        }
    }

    public void pushMaskChangedEvent() {
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.MASK_UPDATED,colorSelectionDrawn, colorSelectionLayer), true);
        //throwCanvasChanged();
    }

    public void invertMask() {
        //System.out.println("invert mask!");
        //CVUtilities.writeImage(magicWandTexture.getData(), "colorinvertedmask.png ");
        //colorSelectionDrawn.getMaskedTexture().setData(CVUtilities.bitwiseNot(colorSelectionDrawn.getMaskedTexture().getData()));
        //colorSelectionDrawn.getMaskedTexture().setData(CVUtilities.bitwiseNot(magicWandTexture.getData()));
        lastMagicWandMat = magicWandTexture.getData();
        magicWandTexture.setData(CVUtilities.bitwiseNot(magicWandTexture.getData()));

        if (colorSelectionDrawn.getRenderingType() == ShaderManager.RenderedElementTypes.COLOR_DIFF) {
            colorSelectionDrawn.setRenderingType(ShaderManager.RenderedElementTypes.MASK);
            colorSelectionDrawn.setMaskedTexture(magicWandTexture);
            //canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(colorSelectionDrawn.getMaskedTexture(),colorSelectionDrawn.getMaskedTexture().getData(), CanvasEvent.NEW_TEXTURE_FROM_MAT));
        }
        //CVUtilities.writeImage(colorSelectionDrawn.getMaskedTexture().getData(), "invertedMask.png");
        //System.out.println("max in inverted mask "+CVUtilities.getMinMaxMat(colorSelectionDrawn.getMaskedTexture().getData())[1]);
        colorSelectionDrawn.setChangedMaskTexture(true);
        undoable = true;
        throwCanvasChanged();

    }

    public void defineRadialCenter(int x, int y) {
        startPoint  = canvasPanel.getCanvas().getCamera().screenToWorld(x, y);
        Polygon point = geometryBuilder.circle(startPoint[0], startPoint[1],visibleRaster.getRealWidth()/150, 20);
        if (radialCenter!=null) {
            animationObjectsLayer.removeElement(radialCenter);
        }
        radialCenter = new DrawnGeoElement(point);
        radialCenter.drawCompletePolygon(0, visibleRaster.getTexture());
        radialCenter.setIsPoint(true);
        animationObjectsLayer.addElement(radialCenter);
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, radialCenter, animationObjectsLayer));
        radialCenter.setRenderingType(ShaderManager.RenderedElementTypes.FILLED);
        radialCenter.setVisible(true);
        throwObjectsChanged();
    }

    public void displayVectorDirectionDrawns(int x, int y) {
        startPoint = canvasPanel.getCanvas().getCamera().screenToWorld(x, y);
        DrawnGeoElement segment = new DrawnGeoElement();
        segment.addPoint(startPoint);
        if (drawingVectorDirection!=null) {
            animationObjectsLayer.removeElement(drawingVectorDirection);
        }
        animationObjectsLayer.addElement(segment);
        directionVectorDrawns.add(segment);
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED,segment, animationObjectsLayer));
            //canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.POINT_ADDED,segment, animationObjectsLayer));
        segment.addPoint(startPoint);
        drawingVectorDirection = segment;


        drawingVectorDirection.setVisible(true);
        drawingVectorDirection.setRenderingType(ShaderManager.RenderedElementTypes.FILLED);

    }

    public void drawSegmentDrawnGeoElement(int x, int y) {
        double[] worldCoord = canvasPanel.getCanvas().getCamera().screenToWorld(x,y);

        drawingVectorDirection.addLastPoint(worldCoord);
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.POINT_ADDED,drawingVectorDirection, animationObjectsLayer));
        endPoint = worldCoord;
        throwObjectsChanged();
        //drawingVectorDirection.printCoordinates();
    }

    public void startDrawingLassoMask (int x, int y, boolean add) {
        if (drawnMasksMat == null) {
            drawnMasksMat = Mat.zeros(visibleRaster.getImageHeight(), visibleRaster.getImageWidth(), CvType.CV_8U);
        }
        maskDrawnPoints.clear();
        double[] startPointMask = canvasPanel.getCanvas().getCamera().screenToWorld(x, y);
        if (add) {
            lastMagicWandWorld = startPointMask;
        }
        int[] coordInPicture = visibleRaster.getCoordinateInPx(startPointMask);
        maskDrawnPoints.add(new org.opencv.core.Point(coordInPicture[0],coordInPicture[1]));
        drawingMask= new DrawnGeoElement();
        drawingMask.addLineFirst(new Vector3f(1.0f,1.0f,1.0f),4);
        drawingMask.addPoint(startPointMask);
        //System.out.println("add element!");
        colorSelectionLayer.addElement(drawingMask);
        maskDrawns.put(drawingMask, add);
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED,drawingMask, colorSelectionLayer));
        drawingMask.setVisible(true);

    }

    public void drawLassoMask(int x, int y) {
        double[] startPointMask = canvasPanel.getCanvas().getCamera().screenToWorld(x, y);
        int[] coordInPicture = visibleRaster.getCoordinateInPx(startPointMask);
        maskDrawnPoints.add(new org.opencv.core.Point(coordInPicture[0],coordInPicture[1]));
        drawingMask.addPoint(startPointMask);
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.POINT_ADDED,drawingMask, colorSelectionLayer));

    }

    public void finishLassoMask() {
        if (drawingMask.isDrawable()) {
            try {
                if (magicWandTexture == null) {
                    lastMagicWandMat = Mat.zeros(visibleRaster.getImageHeight(), visibleRaster.getImageWidth(), CvType.CV_8U);
                    Mat mask = CVUtilities.or(lastMagicWandMat, drawnMasksMat);
                    magicWandTexture = new Texture(visibleRaster, mask);
                    canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(magicWandTexture,mask, CanvasEvent.NEW_TEXTURE_FROM_MAT));
                }
                else {
                    lastMagicWandMat = magicWandTexture.getData();
                }


                if (maskDrawns.get(drawingMask)) {
                    Mat drawnPolygonMat = CVUtilities.drawPolygon(Mat.zeros(lastMagicWandMat.size(), lastMagicWandMat.type()), maskDrawnPoints, WHITE);

                    List<MatOfPoint> contours = CVUtilities.findContoursExternal(drawnPolygonMat.clone());
                    Mat contoursImage = CVUtilities.drawContours(drawnPolygonMat.clone(), contours, 2);
                    contours = CVUtilities.findContoursExternal(contoursImage);

                    if (contours.get(0).rows() > 0) {
                        GeometryFactory geometryFactory = new GeometryFactory();
                        double[] coord;
                        List<Coordinate> coordinates = new ArrayList<>();
                        for (int contourCount = 0; contourCount < contours.size(); contourCount++) {
                            coordinates.clear();
                            int length = contours.get(contourCount).rows();
                            for (int i = 0; i < length; i++) {
                                coord = visibleRaster.pxToCoordinate(new int[]{(int) contours.get(contourCount).get(i, 0)[0], (int) contours.get(contourCount).get(i, 0)[1]});
                                if (i == 0) {
                                    coordinates.add(new Coordinate(coord[0], coord[1]));
                                }
                                if (i > 0 && !(coordinates.get(i - 1).x == coord[0] && coordinates.get(i - 1).y == coord[1])) {
                                    coordinates.add(new Coordinate(coord[0], coord[1]));
                                }

                            }
                            if (coordinates.size() > 2) {
                                Coordinate[] coordinatesArray = new Coordinate[coordinates.size() + 1];
                                for (int i = 0; i < coordinatesArray.length - 1; i++) {
                                    coordinatesArray[i] = coordinates.get(i);
                                }
                                coordinatesArray[coordinatesArray.length - 1] = coordinates.get(0);

                                CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinatesArray);
                                LinearRing linearRing = new LinearRing(coordinateArraySequence, geometryFactory);
                                Geometry geometry = new Polygon(linearRing, null, geometryFactory);

                                lastMagicWandWorld = new double[] {geometry.getInteriorPoint().getX(), geometry.getInteriorPoint().getY()};

                            }
                        }
                    }

                    magicWandTexture.setData(CVUtilities.or(lastMagicWandMat,drawnPolygonMat));
                }
                else {
                    magicWandTexture.setData(CVUtilities.substract(lastMagicWandMat,CVUtilities.drawPolygon(Mat.zeros(lastMagicWandMat.size(), lastMagicWandMat.type()), maskDrawnPoints, WHITE)));
                }
                displayColorSelectionDrawn();
                colorSelectionDrawn.setChangedMaskTexture(true);
                for (DrawnGeoElement maskDrawn : maskDrawns.keySet()) {
                    colorSelectionLayer.removeElement(maskDrawn);
                }
                maskDrawns.clear();
                maskDrawnPoints.clear();

            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
        else {
            colorSelectionLayer.removeElement(drawingMask);
        }
        undoable = true;
        throwCanvasChanged();

    }

    public void deleteMasks() {
        //System.out.println("delete masks!");
        deleteMasks(false);

    }

    public void deleteMasks(boolean throwEvent) {
        magicWandTexture = null;
//        colorSelectionLayer.removeElement(colorSelectionDrawn);
//        colorSelectionDrawn  = null;
        if (colorSelectionDrawn != null) {
            colorSelectionDrawn.setVisible(false);
            //colorSelectionDrawn.setMaskedTexture(null);
        }
        for (DrawnGeoElement maskDrawn: maskDrawns.keySet()) {

            colorSelectionLayer.removeElement(maskDrawn);
        }
        maskDrawns.clear();
        magicWandPoints.clear();
        undoable = false;
        if (throwEvent) {
            throwCanvasChanged();
        }
    }

    public void removeLastMagicWand() {
        magicWandPoints.remove(magicWandPoints.size()-1);
    }

    public void undoLast() {
        magicWandTexture.setData(lastMagicWandMat);
        colorSelectionDrawn.setChangedMaskTexture(true);
        colorSelectionDrawn.setRenderingType(ShaderManager.RenderedElementTypes.MASK);
        lastMagicWandAllpx = beforeLastMagicWandAllpx;
        undoable = false;
    }

    public void saveMask(String path) {
        CVUtilities.writeImage(magicWandTexture.getData(), path);
    }

    public void loadMask(Mat mat) {
        //System.out.println("Mat type loaded "+(mat.type()==CvType.CV_8U));
        //System.out.println("load mask ! "+CVUtilities.getMinMaxMat(mat)[0]+" "+CVUtilities.getMinMaxMat(mat)[1]);
        loadMask(mat,false);

    }

    public void loadMask(Mat mat, boolean throwEvent) {
        if (magicWandTexture == null) {
            magicWandTexture = new Texture(visibleRaster, mat);
        }
        else {

            magicWandTexture.setData(mat);
        }
        canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(magicWandTexture,mat, CanvasEvent.NEW_TEXTURE_FROM_MAT));
        displayColorSelectionDrawn();
        if (throwEvent) {
            throwCanvasChanged();
        }
    }

    public void pasteMask(Mat mat) {
        if (magicWandTexture == null) {
            lastMagicWandMat = Mat.zeros(visibleRaster.getImageHeight(), visibleRaster.getImageWidth(), CvType.CV_8U);
            magicWandTexture = new Texture(visibleRaster, mat);
        }
        else {
            lastMagicWandMat = magicWandTexture.getData();
            magicWandTexture.setData(CVUtilities.or(magicWandTexture.getData(), mat));
        }
        canvasPanel.getCanvas().pushEvent(new CanvasNewTextureMatEvent(magicWandTexture,magicWandTexture.getData(), CanvasEvent.NEW_TEXTURE_FROM_MAT));
        displayColorSelectionDrawn();
        undoable = true;
        throwCanvasChanged();
    }


    public void enableStagedAnimationsOptions(boolean enable) {
        ((StaticImageCanvasPanel)canvasPanel).enableStagedAnimationOptions(enable);
    }

    public void enableDeleteMasksButton (boolean enable) {
        ((StaticImageCanvasPanel)canvasPanel).enableDeleteMasksButton(enable);
    }


    public void updateRasterSelected(String rasterSelected) {
        if (rasterSelected == null) {
            setVisibleRaster(null);
        }
        else {
            setVisibleRaster(rasterSelected);
        }
        ((StaticImageCanvasPanel)canvasPanel).update(rasterSelected);
    }

    public void displayContour() {
        if (magicWandTexture != null) {
            Mat contour = magicWandTexture.getData().clone();
            List<MatOfPoint> contours = CVUtilities.findContoursExternal(contour);
            contours = CVUtilities.findContoursExternal(contour);
            animationObjectsLayer.getElements().clear();
            contourDrawns.clear();
            if (contours.size() > 0) {
                if (contours.get(0).rows() > 0) {
                    GeometryFactory geometryFactory = new GeometryFactory();
                    double[] coord;
                    List<Coordinate> coordinates = new ArrayList<>();
                    for (int contourCount = 0; contourCount < contours.size(); contourCount++) {
                        coordinates.clear();
                        int length = contours.get(contourCount).rows();
                        for (int i = 0; i < length; i++) {

                            coord = visibleRaster.pxToCoordinate(new int[]{(int) contours.get(contourCount).get(i, 0)[0], (int) contours.get(contourCount).get(i, 0)[1]});
                            if (i == 0) {
                                coordinates.add(new Coordinate(coord[0], coord[1]));
                            }
                            if (i > 0 && !(coordinates.get(i - 1).x == coord[0] && coordinates.get(i - 1).y == coord[1])) {
                                coordinates.add(new Coordinate(coord[0], coord[1]));
                            }

                        }
                        if (coordinates.size() > 2) {
                            Coordinate[] coordinatesArray = new Coordinate[coordinates.size() + 1];
                            for (int i = 0; i < coordinatesArray.length - 1; i++) {
                                coordinatesArray[i] = coordinates.get(i);
                            }
                            coordinatesArray[coordinatesArray.length - 1] = coordinates.get(0);

                            CoordinateArraySequence coordinateArraySequence = new CoordinateArraySequence(coordinatesArray);
                            LinearRing linearRing = new LinearRing(coordinateArraySequence, geometryFactory);
                            Geometry geometry = new Polygon(linearRing, null, geometryFactory);


                            DrawnGeoElement contourDrawn = new DrawnGeoElement(geometry);

                            contourDrawn.setOutlineColor(new Vector3f(0.0f, 0.0f, 0.0f));
                            contourDrawn.setOutlineWidth(2.0f);
                            if (lastMagicWandWorld != null) {
                                if (contourDrawn.containsPoint(lastMagicWandWorld[0], lastMagicWandWorld[1])) {
                                    if (selectedContourDrawn != null) {
                                        selectedContourDrawn.deselect();
                                    }
                                    selectedContourDrawn = contourDrawn;
                                    selectedContourDrawn.select();
                                }
                            } else {
                                if (selectedContourDrawn == null) {
                                    selectedContourDrawn = contourDrawn;
                                    selectedContourDrawn.select();
                                }
                            }

                            contourDrawns.add(contourDrawn);
                            contourDrawn.setVisible(true);
                            contourDrawn.setRenderingType(ShaderManager.RenderedElementTypes.FILLED);
                            canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, contourDrawn, animationObjectsLayer));
                            animationObjectsLayer.addElement(contourDrawn);
                        }

                    }

                }
            }
        }
    }

    public void selectContour(int x, int y) {
        double[] worldcoord = canvasPanel.getCanvas().getCamera().screenToWorld(x, y);
        for (DrawnGeoElement contourElement : contourDrawns) {
            if (contourElement.containsPoint(worldcoord[0], worldcoord[1])) {
                if (contourElement != selectedContourDrawn && selectedContourDrawn != null) {
                    selectedContourDrawn.deselect();
                    selectedContourDrawn = contourElement;
                    selectedContourDrawn.select();
                    throwObjectsChanged();
                }

            }
        }

    }

    public Mat getSelectedContourMask(){
        List<int[]> imagePoints = new ArrayList<>();
        if (selectedContourDrawn!=null) {
            for (int i = 0; i < selectedContourDrawn.getCoordinates().size(); i++) {
                imagePoints.add(visibleRaster.getCoordinateInPx(selectedContourDrawn.getCoordinates().get(i)));
            }
            Mat maskContour = CVUtilities.drawPolygonMask(imagePoints, visibleRaster.getImageHeight(), visibleRaster.getImageWidth());
            return maskContour;
        }
        else {
            return null;
        }
    }

    public void deleteContourDrawns() {
        selectedContourDrawn = null;
        lastMagicWandWorld = null;
        for (DrawnGeoElement contourDrawn : contourDrawns) {
            animationObjectsLayer.removeElement(contourDrawn);
        }
        contourDrawns.clear();

    }

    public void deleteDirectionObject() {
        if (drawingVectorDirection != null) {
            animationObjectsLayer.removeElement(drawingVectorDirection);
            drawingVectorDirection = null;
        }
    }

    public void deleteRadialCenter() {
        if (radialCenter!=null) {
            animationObjectsLayer.removeElement(radialCenter);
            radialCenter = null;
        }
    }

    public void displayDirectionObject(int[] startPoint, int[] endPoint) {
        DrawnGeoElement segment = new DrawnGeoElement();

        double[] startPointWorld = visibleRaster.pxToCoordinate(startPoint);
        double[] endPointWorld = visibleRaster.pxToCoordinate(endPoint);
        segment.addPoint(startPointWorld);
        segment.addPoint(startPointWorld);
        segment.addPoint(endPointWorld);
        if (drawingVectorDirection!=null) {
            animationObjectsLayer.removeElement(drawingVectorDirection);
        }
        animationObjectsLayer.addElement(segment);
        directionVectorDrawns.add(segment);
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED,segment, animationObjectsLayer));
        drawingVectorDirection = segment;
        drawingVectorDirection.setVisible(true);
        drawingVectorDirection.setRenderingType(ShaderManager.RenderedElementTypes.FILLED);
    }

    public void displayRadialCenter(int [] coords) {
        double[] wordCoord = visibleRaster.pxToCoordinate(coords);
        Polygon point = geometryBuilder.circle(wordCoord[0], wordCoord[1],((double)visibleRaster.getRealWidth())/150, 20);
        if (radialCenter!=null) {
            animationObjectsLayer.removeElement(radialCenter);
        }
        radialCenter = new DrawnGeoElement(point);
        radialCenter.drawCompletePolygon(0, visibleRaster.getTexture());
        radialCenter.setIsPoint(true);
        animationObjectsLayer.addElement(radialCenter);
        canvasPanel.getCanvas().pushEvent(new CanvasDrawnEvent(CanvasEvent.DRAWN_CREATED, radialCenter, animationObjectsLayer));
        radialCenter.setRenderingType(ShaderManager.RenderedElementTypes.FILLED);
        radialCenter.setVisible(true);
    }





    public ColorThresholdGeoElement getColorSelectionDrawn() {
        return colorSelectionDrawn;
    }

    public double[] getStartPoint() {
        return startPoint;
    }

    public double[] getEndPoint() {
        return endPoint;
    }

    public Texture getMagicWandTexture() {
            return magicWandTexture;
    }

    public void reset() {
        for (int i=0; i<layerManager.getLayers().size(); i++) {
            if (layerManager.getLayers().get(i)!=animationObjectsLayer && layerManager.getLayers().get(i)!=colorSelectionLayer) {
                layerManager.removeLayer(layerManager.getLayers().get(i));
                i--;
            }
        }
        layerManager.getRenderingFeatures().get(animationObjectsLayer).clear();
        layerManager.getRenderingFeatures().get(colorSelectionLayer).clear();
        layerManager.getTextureIds().clear();
        animationObjectsLayer.getElements().clear();
        colorSelectionLayer.getElements().clear();
        //directionVectorDrawns = new ArrayList<>();
        maskDrawns = new HashMap<>();
        maskDrawnPoints = new ArrayList<>();
        magicWandPoints = new ArrayList<>();
        contourDrawns = new ArrayList<>();
        colorSelectionDrawn = null;
        magicWandTexture = null;
        drawnMasksMat = null;
        lastMagicWandMat = null;


    }

    public void addListener(StaticImageCanvasControllerListener listener) {
        listeners.add(listener);
    }

    public void throwCanvasChanged() {
        for (StaticImageCanvasControllerListener listener : listeners) {
            listener.staticImageCanvasControllerChangedMask(getName());
        }
    }

    public void throwObjectsChanged() {
        for (StaticImageCanvasControllerListener listener : listeners) {
            listener.staticImageCanvasControllerChangedAnimationObjects(getName());
        }
    }

    public boolean isUndoable() {
        return undoable;
    }





}
