/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by mjlobo on 24/04/2017.
 */
public class CustomColorButton extends JButton
{


    private Color normalColor = null;


    public CustomColorButton(Color backgroundColor, String text)
    {
        super(text);
        this.normalColor = backgroundColor;
        setContentAreaFilled(false);
    }

    /**
     * Overpainting component, so it can have different colors
     */
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;



        // Draws the rounded opaque panel with borders
        g2d.setBackground(normalColor);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // For High quality
        g2d.fillRoundRect(0, 0, getWidth(), getHeight(), 7, 7);

        g2d.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 7, 7);

        super.paintComponent(g);
    }
}