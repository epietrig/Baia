/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import javax.swing.*;
import java.io.File;

public class LoadImagesWorker extends SwingWorker {
    SatHistViewer satHistViewer;
    File folder;

    public LoadImagesWorker (SatHistViewer satHistViewer, File folder) {
        this.satHistViewer = satHistViewer;
        this.folder = folder;
    }

    @Override
    protected Object doInBackground() throws Exception {
        satHistViewer.loadFiles(folder);
        return null;
    }

    protected void done() {
        satHistViewer.finishLoadingImages();
    }
}
