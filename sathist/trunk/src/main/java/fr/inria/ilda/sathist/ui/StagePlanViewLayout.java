/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist.ui;

import fr.inria.ilda.mmtools.stateMachine.AnimationViewEvent;
import fr.inria.ilda.mmtools.stateMachine.ButtonEvent;
import fr.inria.ilda.mmtools.stateMachine.StateMachine;
import fr.inria.ilda.mmtools.stateMachine.TextFieldEvent;
import fr.inria.ilda.mmtools.ui.StringComboPanel;
import fr.inria.ilda.mmtools.utilties.Constants;
import fr.inria.ilda.sathist.SatHistConstants;
import fr.inria.ilda.sathist.StagePlanController;
import fr.inria.ilda.sathist.StagePlanViewDropTargetListener;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by mjlobo on 16/06/16.
 */
public class StagePlanViewLayout extends JLayeredPane {
    JLabel stageType;
    JTextField duration;
    JLabel durationLabel;
    JButton playButton;
    JButton deleteButton;
    StagePlanController controller;
    int width;
    int height;
    JPanel topPanel;
    JPanel bottomPanel;
    int buttonHeight = 12;
    int buttonWidth = 12;
    int durationWidth = 50;
    int durationHeight = 30;
    static Border selectedBorder = BorderFactory.createLineBorder(Color.GRAY, 2);
    static Border unselectedBorder = BorderFactory.createLineBorder(Color.GRAY);

    static String DELETE_ICON = "delete";

    Color completeColor;
    Color incompleteColor;

    JPanel zeroPanel;
    InsideBorderPanel borderPanel;

    Color borderColor = new Color(240,240,240);
    boolean isSelected = false;

    Color unselectedColor;

    public StagePlanViewLayout(StagePlanController controller, int height, final int pixelsPerSecond, boolean complete) {
        this.controller = controller;
        this.width = (int)controller.getTimedAnimation().getAnimation().getDuration()*pixelsPerSecond;
        this.height = height;
        zeroPanel = new JPanel();
        zeroPanel.setOpaque(true);
        borderPanel = new InsideBorderPanel(2, Color.GRAY, 100,width, height);
        stageType = new JLabel(SatHistConstants.transitionNames.get(controller.getTimedAnimation().getAnimation()));
//        completeColor = (SatHistConstants.transitionColors.get(controller.getAnimation().getType()));
//        float[] hsv = new float[3];
//        Color.RGBtoHSB(completeColor.getRed(), completeColor.getGreen(), completeColor.getBlue(), hsv);
//        hsv[1] = hsv[1]-20f/255f;
//        incompleteColor = Color.getHSBColor(hsv[0], hsv[1], hsv[2]);
//        setBackground(incompleteColor);

        incompleteColor = new Color(217, 217, 217);
        unselectedColor = incompleteColor;


        setPreferredSize(new Dimension(width, height));
        //setBorder(BorderFactory.createLineBorder(Color.GRAY));
        setOpaque(true);
        topPanel = new JPanel();
        topPanel.setOpaque(false);
        bottomPanel = new JPanel();
        bottomPanel.setOpaque(false);

        stageType.setOpaque(false);
        playButton = new JButton("P");
        playButton.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
        playButton.setMaximumSize(new Dimension(buttonWidth, buttonHeight));
        ImageIcon deleteIcon = new ImageIcon(SatHistConstants.ICONS_PATH+DELETE_ICON+".png");
        Image newimg = deleteIcon.getImage().getScaledInstance(buttonWidth-2, buttonHeight-2,  java.awt.Image.SCALE_SMOOTH ) ;
        deleteIcon = new ImageIcon( newimg );
        //deleteButton = new JButton("X");
        deleteButton = new JButton(deleteIcon);
        deleteButton.setBorder(null);
        deleteButton.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
        deleteButton.setMaximumSize(new Dimension(buttonWidth, buttonHeight));
        deleteButton.setMinimumSize(new Dimension(buttonWidth, buttonHeight));
        duration = new JTextField(""+controller.getTimedAnimation().getAnimation().getDuration(), 2);
        duration.setPreferredSize(new Dimension(durationWidth, durationHeight));
        duration.setMaximumSize(new Dimension(durationWidth, durationHeight));
        JLabel secondsLabel = new JLabel("s");

        //topPanel.setLayout(new BoxLayout(topPanel,BoxLayout.LINE_AXIS));
        topPanel.setLayout(null);

        stageType.setBorder(new EmptyBorder(2,2,2,2));
        topPanel.setBorder(new EmptyBorder(2,2,2,3));
        stageType.setBounds(2,1,(int)stageType.getPreferredSize().getWidth(), (int)stageType.getPreferredSize().getHeight());
        //topPanel.add(stageType);
        durationLabel = new JLabel(stageType.getText()+" ("+controller.getTimedAnimation().getAnimation().getDuration()+")");
        durationLabel.setBounds(3,3,(int)durationLabel.getPreferredSize().getWidth(), (int)durationLabel.getPreferredSize().getHeight());
        topPanel.add(durationLabel);
        //topPanel.add(playButton);
        topPanel.add(Box.createHorizontalGlue());
        //deleteButton.setBorder(new EmptyBorder(2,2,2,2));
        deleteButton.setBounds(width-buttonWidth-3, 3, buttonWidth, buttonHeight);
        topPanel.add(deleteButton);
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
        bottomPanel.add(duration);
        bottomPanel.add(secondsLabel);
        zeroPanel.setLayout(new BoxLayout(zeroPanel, BoxLayout.PAGE_AXIS));
        zeroPanel.add(topPanel);
        //zeroPanel.add(bottomPanel);
        zeroPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        //System.out.println("height in initialization "+this.height);

//        playButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.print("new width "+width*2);
//                setPreferredSize(new Dimension(300,getHeight()));
//                revalidate();
//                repaint();
//            }
//        });

        //setTransferHandler(new TransferHandler("stagePlanViewLayout"));
        zeroPanel.setBounds(0,0,width,height);
        borderPanel.setBounds(0,0,width,height);
        borderPanel.setOpaque(false);
        add(zeroPanel, new Integer(0));
        add(borderPanel, new Integer(1));
        //this.getDropTarget().setActive(false);
        //setDropTarget(null);
        //revalidate();
        //repaint();
        update(complete, pixelsPerSecond);
        //update(pixelsPerSecond);
        new StagePlanViewDropTargetListener(this.controller);


    }

    public void addListeners(final StateMachine stateMachine, final JComponent toPropagate) {
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stateMachine.handleEvent(new ButtonEvent(playButton,playButton.getActionCommand()));
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("delete button!");
                stateMachine.handleEvent(new AnimationViewEvent(controller.getTimedAnimation().getAnimation(), Constants.DELETE_ANIMATION));
            }
        });
        duration.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //stateMachine.handleEvent(new TextFieldEvent(duration, duration.getText(), StagePlanView.this));
                try {
                    float d = Float.valueOf(duration.getText());
                    controller.updateAnimationDuration(d);
                    //stateMachine.handleEvent(new LayerOptionChanged(d, layerName, textField.getName()));
                    //stateMachine.handleEvent(new TextFieldEvent(StagePlanViewLayout.this, duration.getText()));
                }
                catch(NumberFormatException exception) {
                    exception.printStackTrace();
                }
            }
        });

//        addMouseListener(new MouseAdapter() {
//            @Override
//            public void mousePressed(MouseEvent e) {
//                System.out.println("getParent() "+getParent());
//                System.out.println("click!");
//                toPropagate.dispatchEvent(e);
//            }
//        });
    }

    public void update(double pixelsPerSecond) {
        duration.setText(""+controller.getTimedAnimation().getAnimation().getDuration());
        durationLabel.setText(stageType.getText()+" ("+controller.getTimedAnimation().getAnimation().getDuration()+")");
        this.width = (int)(controller.getTimedAnimation().getAnimation().getDuration()*pixelsPerSecond);
        int newWidth = (int)durationLabel.getPreferredSize().getWidth();
        if (width<buttonWidth+durationLabel.getPreferredSize().getWidth()) {
            newWidth =  width-buttonWidth;
        }
        zeroPanel.setBounds(0,0,(int)(controller.getTimedAnimation().getAnimation().getDuration()*pixelsPerSecond), height);
        deleteButton.setBounds(width-buttonWidth-3, 3, buttonWidth, buttonHeight);
        durationLabel.setBounds(3,3,newWidth, (int)durationLabel.getPreferredSize().getHeight());
        zeroPanel.revalidate();
        borderPanel.setBounds(0,0,(int)(controller.getTimedAnimation().getAnimation().getDuration()*pixelsPerSecond), height);
        setPreferredSize(new Dimension((int)(controller.getTimedAnimation().getAnimation().getDuration()*pixelsPerSecond), height));
        //setMinimumSize(new Dimension((int)(controller.getAnimation().getDuration()*pixelsPerSecond-2), height));
        revalidate();
        repaint();
    }

    public void update(boolean complete, double pixelsPerSecond) {
        stageType.setText(SatHistConstants.transitionNames.get(controller.getTimedAnimation().getAnimation().getType()));
        durationLabel.setText(SatHistConstants.transitionNames.get(controller.getTimedAnimation().getAnimation().getType())+" ("+controller.getTimedAnimation().getAnimation().getDuration()+")");
        //completeColor = SatHistConstants.transitionColors.get(controller.getTimedAnimation().getAnimation().getType());
        completeColor = Color.WHITE;
        if (complete) {
            if (!isSelected) {
                unselectedColor = completeColor;
                zeroPanel.setBackground(completeColor);
            }
        }
        else {
//            float[] hsv = new float[3];
//            Color.RGBtoHSB(completeColor.getRed(), completeColor.getGreen(), completeColor.getBlue(), hsv);
//            hsv[1] = hsv[1]-20f/255f;
//            incompleteColor = Color.getHSBColor(hsv[0], hsv[1], hsv[2]);
            if (!isSelected) {
                unselectedColor = incompleteColor;
                incompleteColor = new Color(217, 217, 217);
                zeroPanel.setBackground(incompleteColor);
            }
        }
        update(pixelsPerSecond);

    }

    public void select() {
        //setBorder(selectedBorder);
        borderPanel.setLineWidth(4);
        isSelected = true;
        zeroPanel.setBackground(SatHistConstants.selectedSatgeColor);
        //durationLabel.setForeground(Color.WHITE);
    }

    public void unSelect() {

        setBorder(unselectedBorder);
        borderPanel.setLineWidth(2);
        isSelected = false;
        //durationLabel.setForeground(Color.BLACK);
        zeroPanel.setBackground(unselectedColor);
    }

    public int getWidth() {
        return width;
    }

    public boolean isSelected() {
        return isSelected;
    }



}
