/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;

/**
 * Created by mjlobo on 27/01/2017.
 */
public class StagePlanViewDropTargetListener extends DropTargetAdapter{
    private StagePlanController controller;
    DropTarget dropTarget;

    public StagePlanViewDropTargetListener(StagePlanController controller) {
        this.controller = controller;
        dropTarget = new DropTarget(controller.getStagePlanView(), DnDConstants.ACTION_COPY, this, true, null);
    }

    @Override
    public void drop(DropTargetDropEvent dtde) {
        //System.out.println("Drop in stage plan layeredStagesAndSlider!");
    }
}
