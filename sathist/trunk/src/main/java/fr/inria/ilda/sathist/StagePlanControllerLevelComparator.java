/****************************************************************************
* Copyright (c) 2015-2018, Maria-Jesus Lobo (INRIA)                        *
*                                                                          *
* Distributed under the terms of the BSD 3-Clause License.                 *
*                                                                          *
* Full license available in file LICENSE, distributed with this software.  *
****************************************************************************/
package fr.inria.ilda.sathist;

import java.util.Comparator;

/**
 * Created by mjlobo on 30/01/2017.
 */
public class StagePlanControllerLevelComparator implements Comparator<StagePlanController> {

    @Override
    public int compare(StagePlanController o1, StagePlanController o2) {
        float level1 = o1.getTimedAnimation().getLevel();
        float level2 = o2.getTimedAnimation().getLevel();
        if (level1 < level2) return -1;
        if (level1 > level2) return 1;
        return 0;
    }
}
